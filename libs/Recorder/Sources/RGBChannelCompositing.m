//
//  RGBChannelCompositing.m
//  SCRecorder
//
//  Created by Qaiser Butt on 5/13/16.
//  Copyright © 2016 rFlex. All rights reserved.
//

#import "RGBChannelCompositing.h"
#import "SCRecorder.h"

@implementation RGBChannelCompositing

- (instancetype)init {
    self = [super init];
    
    if (self)
    {
        rgbChannelCompositingKernel = [CIColorKernel kernelWithString:
                                       [NSString stringWithFormat:@"%@%@%@%@",
                                        @"kernel vec4 rgbChannelCompositing(__sample red, __sample green, __sample blue)",
                                        @"{",
                                        @"   return vec4(red.r, green.g, blue.b, 1.0);",
                                        @"}"]];
    }
    
    return self;
}

- (NSDictionary*) attributes {
    return @{
            kCIAttributeFilterDisplayName: @"RGB Compositing"//,
            
            /*@"inputRedImage": @{kCIAttributeIdentity: @0,
                                 kCIAttributeClass: @"CIImage",
                           kCIAttributeDisplayName: @"Red Image",
                                kCIAttributeType: kCIAttributeTypeImage},
            
            @"inputGreenImage": @{kCIAttributeIdentity: @0,
                                   kCIAttributeClass: @"CIImage",
                             kCIAttributeDisplayName: @"Green Image",
                                 kCIAttributeType: kCIAttributeTypeImage},
            
            @"inputBlueImage": @{kCIAttributeIdentity: @0,
                                  kCIAttributeClass: @"CIImage",
                            kCIAttributeDisplayName: @"Blue Image",
                                   kCIAttributeType: kCIAttributeTypeImage}*/
            };
}

- (CIImage*) outputImage {
    const CIImage* inputRedImage1 = self.inputRedImage, *inputGreenImage1 = self.inputGreenImage, *inputBlueImage1 = self.inputBlueImage;
    const CIColorKernel *rgbChannelCompositingKernel1 = rgbChannelCompositingKernel;
    
    const CGRect extent = CGRectUnion([inputRedImage1 extent], CGRectUnion([inputGreenImage1 extent], [inputBlueImage1 extent]));
    NSArray *arguments = @[inputRedImage1, inputGreenImage1, inputBlueImage1];
    
    return [rgbChannelCompositingKernel1 applyWithExtent:extent arguments:arguments];
}

@end



@implementation RGBChannelToneCurve

- (instancetype)init {
    self = [super init];
    
    if (self)
    {
        rgbChannelCompositing = [RGBChannelCompositing new];
        
        bumpDistortion = [CIFilter filterWithName:@"CIBumpDistortionLinear"];
        [bumpDistortion setDefaults];
    }
    
    return self;
}


-(void) setDefaults
{
    CGFloat redValues[] = { 0.0, 0.25, 0.5, 0.75, 1.0 };
    self.inputRedValuesY = [CIVector vectorWithValues:redValues count:5];
    
    CGFloat greenValues[] = { 0.0, 0.25, 0.5, 0.75, 1.0 };
    self.inputGreenValuesY = [CIVector vectorWithValues:greenValues count:5];
    
    CGFloat blueValues[] = { 0.0, 0.25, 0.5, 0.75, 1.0 };
    self.inputBlueValuesY = [CIVector vectorWithValues:blueValues count:5];
    
    rgbChannelCompositing = [RGBChannelCompositing new];
}

- (NSDictionary*) attributes {
    /*CGFloat redValues[] = { 0.0, 0.25, 0.5, 0.75, 1.0 };
    CGFloat greenValues[] = { 0.0, 0.25, 0.5, 0.75, 1.0 };
    CGFloat blueValues[] = { 0.0, 0.25, 0.5, 0.75, 1.0 };*/
    
    return @{
             kCIAttributeFilterDisplayName: @"RGB Tone Curve"//,
             
             /*@"inputImage": @{kCIAttributeIdentity: @0,
                               kCIAttributeClass: @"CIImage",
                         kCIAttributeDisplayName: @"Image",
                              kCIAttributeType: kCIAttributeTypeImage},

             @"inputRedValues": @{kCIAttributeIdentity: @0,
                                   kCIAttributeClass: @"CIVector",
                                 kCIAttributeDefault: [CIVector vectorWithValues:redValues count:5],
                             kCIAttributeDisplayName: @"Red 'y' Values",
                             kCIAttributeDescription: @"Red tone curve 'y' values at 'x' positions [0.0, 0.25, 0.5, 0.75, 1.0].",
                                    kCIAttributeType: kCIAttributeTypeOffset},
             
             @"inputGreenValues": @{kCIAttributeIdentity: @0,
                                     kCIAttributeClass: @"CIVector",
                                   kCIAttributeDefault: [CIVector vectorWithValues:greenValues count:5],
                               kCIAttributeDisplayName: @"Green 'y' Values",
                               kCIAttributeDescription: @"Green tone curve 'y' values at 'x' positions [0.0, 0.25, 0.5, 0.75, 1.0].",
                                   kCIAttributeType: kCIAttributeTypeOffset},
             
             @"inputBlueValues": @{kCIAttributeIdentity: @0,
                                    kCIAttributeClass: @"CIVector",
                                  kCIAttributeDefault: [CIVector vectorWithValues:blueValues count:5],
                              kCIAttributeDisplayName: @"Blue 'y' Values",
                              kCIAttributeDescription: @"Blue tone curve 'y' values at 'x' positions [0.0, 0.25, 0.5, 0.75, 1.0].",
                                  kCIAttributeType: kCIAttributeTypeOffset}*/
             };
}

- (CIImage*) outputImage {

    //const CIImage* inputImage1 = self.inputImage;
    
    /*CIImage *red = [inputImage1 imageByApplyingFilter:@"CIToneCurve"
                                  withInputParameters:@{
                                                        @"inputPoint0": [CIVector vectorWithX:0.0 Y:[self.inputRedValuesY valueAtIndex:0]],
                                                        @"inputPoint1": [CIVector vectorWithX:0.25 Y:[self.inputRedValuesY valueAtIndex:1]],
                                                        @"inputPoint2": [CIVector vectorWithX:0.5 Y:[self.inputRedValuesY valueAtIndex:2]],
                                                        @"inputPoint3": [CIVector vectorWithX:0.75 Y:[self.inputRedValuesY valueAtIndex:3]],
                                                        @"inputPoint4": [CIVector vectorWithX:1.0 Y:[self.inputRedValuesY valueAtIndex:4]]
                                                        }]; */

    CGSize imageSize = self.inputImage.extent.size;
    [bumpDistortion setValue: [CIVector vectorWithX:imageSize.width/2 Y:imageSize.height/2] forKey: kCIInputCenterKey];
    [bumpDistortion setValue: self.inputImage forKey: kCIInputImageKey];
    
    CGFloat value;
    
    if ([SCRecorder sharedRecorder].isVideoImported ||
        [SCRecorder sharedRecorder].isPhotoImported)
    {
        value = [SCRecorder sharedRecorder].vRGBControlRatio;
        [bumpDistortion setValue: @(value) forKey: kCIInputRadiusKey];
        [bumpDistortion setValue: @(2.0) forKey: kCIInputAngleKey];
        
    } else {
        /*if ([_currentFXName isEqualToString:@"CIPhotoEffectProcess"])
            value = (200*(MIN(imageSize.width, imageSize.height)/288));
        else value = 18.0f*(MIN(imageSize.width, imageSize.height)/288);*/
        
        CGFloat rgbValue = [SCRecorder sharedRecorder].vRGBControlRatio;
        value = (rgbValue*(MIN(imageSize.width, imageSize.height)/288));
        [bumpDistortion setValue: @(value) forKey: kCIInputRadiusKey];
    }
    
    CIImage *red = [bumpDistortion valueForKey: kCIOutputImageKey];
    
    /*CIImage *green = [inputImage1 imageByApplyingFilter:@"CIToneCurve"
                                    withInputParameters:@{
                                                          @"inputPoint0": [CIVector vectorWithX:0.0 Y:[self.inputGreenValuesY valueAtIndex:0]],
                                                          @"inputPoint1": [CIVector vectorWithX:0.25 Y:[self.inputGreenValuesY valueAtIndex:1]],
                                                          @"inputPoint2": [CIVector vectorWithX:0.5 Y:[self.inputGreenValuesY valueAtIndex:2]],
                                                          @"inputPoint3": [CIVector vectorWithX:0.75 Y:[self.inputGreenValuesY valueAtIndex:3]],
                                                          @"inputPoint4": [CIVector vectorWithX:1.0 Y:[self.inputGreenValuesY valueAtIndex:4]]
                                                          }];*/
    
    /*CIImage *blue = [inputImage1 imageByApplyingFilter:@"CIToneCurve"
                                   withInputParameters:@{
                                                         @"inputPoint0": [CIVector vectorWithX:0.0 Y:[self.inputBlueValuesY valueAtIndex:0]],
                                                         @"inputPoint1": [CIVector vectorWithX:0.25 Y:[self.inputBlueValuesY valueAtIndex:1]],
                                                         @"inputPoint2": [CIVector vectorWithX:0.5 Y:[self.inputBlueValuesY valueAtIndex:2]],
                                                         @"inputPoint3": [CIVector vectorWithX:0.75 Y:[self.inputBlueValuesY valueAtIndex:3]],
                                                         @"inputPoint4": [CIVector vectorWithX:1.0 Y:[self.inputBlueValuesY valueAtIndex:4]]
                                                         }]; */
    
    rgbChannelCompositing.inputRedImage     = red;
    rgbChannelCompositing.inputGreenImage   = self.inputImage;
    rgbChannelCompositing.inputBlueImage    = self.inputImage;
    
    return rgbChannelCompositing.outputImage;

}

@end


@implementation VHSTrackingLines

- (instancetype)init {
    self = [super init];
    
    if (self)
    {
        inputTime   = 0;
        moveCounter = 1;
    }
    
    return self;
}


-(void) setDefaults
{
    inputSpacing = 50;
    inputStripeHeight = 0.5;
    inputBackgroundNoise = 0.05;
}

- (CIImage*) outputImage {
    const CIImage* inputImage1 = _inputImage;
    
    //let tx = NSValue(CGAffineTransform: CGAffineTransformMakeTranslation(CGFloat(drand48() * 100), CGFloat(drand48() * 100)))
    //[myFilter setValue:[NSValue valueWithBytes:&xform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    CGFloat trx = (drand48() * 100);
    CGFloat try = (drand48() * 100);
    CGAffineTransform trans = CGAffineTransformMakeTranslation(trx, try);
    
    NSDictionary<NSString *,NSValue*> *tx = @{@"inputTransform":  [NSValue valueWithBytes:&trans objCType:@encode(CGAffineTransform)]};
    NSDictionary<NSString *,id> *params = @{kCIInputAspectRatioKey:  @5};
    
    const CIImage *noise = [[[[CIFilter filterWithName:@"CIRandomGenerator"].outputImage
                              imageByApplyingFilter:@"CIAffineTransform"
                              withInputParameters:tx]
                             imageByApplyingFilter:@"CILanczosScaleTransform"
                             withInputParameters:params]
                            imageByCroppingToRect:CGRectMake(inputImage1.extent.origin.x,
                                                             inputImage1.extent.origin.y + moveCounter,
                                                             inputImage1.extent.size.width, inputImage1.extent.size.height/6)];
    moveCounter = moveCounter + 35;
    if (moveCounter > inputImage1.extent.size.height) {
        moveCounter = 1;
    }
    
    CIColorKernel *kernel = [CIColorKernel kernelWithString:
                             [NSString stringWithFormat:@"%@%@%@%@%@%@",
                              @"kernel vec4 thresholdFilter(__sample image, __sample noise, float time, float spacing, float stripeHeight, float backgroundNoise)",
                              @"{",
                              @"   vec2 uv = samplerCoord(image); ",
                              @"   float stripe = smoothstep(1.0 - stripeHeight, 1.0, sin((time + uv.y) / spacing)); ",
                              //@"   return image + (noise * noise * stripe) + (noise * backgroundNoise); ",
                              @"   return image + (noise * noise) + (noise * backgroundNoise); ",
                              @"}"]];
    
    CGRect extent    = [inputImage1 extent];
    CGSize imageSize = extent.size;
    
    inputSpacing        = (18.7*(MIN(imageSize.width, imageSize.height)/288));
    inputStripeHeight   = (0.187*(MIN(imageSize.width, imageSize.height)/288));
    
    NSArray *arguments = @[inputImage1, noise,
                           [NSNumber numberWithFloat:inputTime],
                           [NSNumber numberWithFloat:inputSpacing],
                           [NSNumber numberWithFloat:inputStripeHeight], [NSNumber numberWithFloat:inputBackgroundNoise]];
    
    return [kernel applyWithExtent:extent arguments:arguments];
}

@end
