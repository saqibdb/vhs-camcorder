//
//  SCFilter.m
//  CoreImageShop
//
//  Created by Simon CORSIN on 16/05/14.
//
//

#import "SCFilter.h"
#import "SCRecorder.h"

#define TOTAL_NOISE_LINES 5
#define TOTAL_TILT_FRAMES 5

@interface SCFilter() {
    NSMutableDictionary *_unwrappedValues;
    NSMutableArray      *_subFilters;
    NSMutableArray      *_animations;
    //NSMutableArray    *_noiseObjects;
    //VHSTrackingLines    *CIVHSTrackingLinesFilter;
    CGFloat              vMoveCounter;
    int                  hGlitchCounter;
}

@property (strong, nonatomic) CIImage *overlayImage;

@end

static int noisecounter = 1;

@implementation SCFilter

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [self init];
    
    if (self) {
        _CIFilter = [aDecoder decodeObjectForKey:@"CoreImageFilter"];
        self.enabled = [aDecoder decodeBoolForKey:@"Enabled"];
        
        if ([aDecoder containsValueForKey:@"VectorsData"]) {
            NSArray *vectors = [aDecoder decodeObjectForKey:@"VectorsData"];
            for (NSArray *vectorData in vectors) {
                CGFloat *vectorValue = malloc(sizeof(CGFloat) * (vectorData.count - 1));
                
                if (vectorData != nil) {
                    for (int i = 1; i < vectorData.count; i++) {
                        NSNumber *value = [vectorData objectAtIndex:i];
                        vectorValue[i - 1] = (CGFloat)value.doubleValue;
                    }
                    NSString *key = vectorData.firstObject;
                    
                    [_CIFilter setValue:[CIVector vectorWithValues:vectorValue count:vectorData.count - 1] forKey:key];
                    free(vectorValue);
                }
            }
        }
        
        if ([aDecoder containsValueForKey:@"UnwrappedValues"]) {
            NSDictionary *unwrappedValues = [aDecoder decodeObjectForKey:@"UnwrappedValues"];
            
            for (NSString *key in unwrappedValues) {
                [self setParameterValue:[unwrappedValues objectForKey:key] forKey:key];
            }
        }
        
        if ([aDecoder containsValueForKey:@"SubFilters"]) {
            _subFilters = [[aDecoder decodeObjectForKey:@"SubFilters"] mutableCopy];
        }
        
        if ([aDecoder containsValueForKey:@"Name"]) {
            _name = [aDecoder decodeObjectForKey:@"Name"];
        } else {
            _name = [_CIFilter.attributes objectForKey:kCIAttributeFilterName];
        }
        
        if ([aDecoder containsValueForKey:@"Animations"]) {
            _animations = [[aDecoder decodeObjectForKey:@"Animations"] mutableCopy];
        }
    }
    
    return self;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _unwrappedValues = [NSMutableDictionary new];
        _subFilters = [NSMutableArray new];
        _animations = [NSMutableArray new];
        
        self.enabled = YES;
    }
    
    return self;
}

- (instancetype)initWithCIFilter:(CIFilter *)filter {
    self = [self init];
    
    if (self) {
        _name = [filter.attributes objectForKey:kCIAttributeFilterDisplayName];
        _CIFilter = filter;
        
        _CIToneCurveFilter = [RGBChannelToneCurve new];
        [_CIToneCurveFilter setDefaults];
        
        /*CIVHSTrackingLinesFilter = [VHSTrackingLines new];
        [CIVHSTrackingLinesFilter setDefaults];*/
        
        vMoveCounter = -5.0;
        hGlitchCounter = 1;
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    SCFilter *filter = [SCFilter emptyFilter];
    
    if (filter != nil) {
        filter->_name = [_name copy];
        filter->_CIFilter = [_CIFilter copy];
        filter->_unwrappedValues = [_unwrappedValues mutableCopy];
        filter->_subFilters = [_subFilters mutableCopy];
        filter->_animations = [_animations mutableCopy];
    }
    
    return filter;
}

- (id)_unwrappedValue:(id)value forKey:(NSString *)key {
    id unwrappedValue = [_unwrappedValues objectForKey:key];
    
    return unwrappedValue == nil ? value : unwrappedValue;
}

- (id)_wrappedValue:(id)value forKey:(NSString *)key {
    if (value == nil) {
        [_unwrappedValues removeObjectForKey:key];
    } else {
        if ([key isEqualToString:@"inputCubeData"]) {
            if ([value isKindOfClass:[NSData class]]) {
                NSData *data = value;
                
                CGDataProviderRef source = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
                if (source == nil) {
                    NSLog(@"Unable to get source provider for key %@", key);
                    return nil;
                }
                
                CGImageRef image = nil;
                
                if ([SCFilter data:data hasMagic:MagicPNG]) {
                    image = CGImageCreateWithPNGDataProvider(source, nil, NO, kCGRenderingIntentDefault);
                } else if ([SCFilter data:data hasMagic:MagicJPG]) {
                    image = CGImageCreateWithJPEGDataProvider(source, nil, NO, kCGRenderingIntentDefault);
                } else {
                    NSLog(@"Input data for key %@ must be either representing a PNG or a JPG file", key);
                    CGDataProviderRelease(source);
                    return nil;
                }
                
                if (image == nil) {
                    NSLog(@"Unable to create image for key %@ from input data", key);
                    CGDataProviderRelease(source);
                    return nil;
                }
                
                CGDataProviderRelease(source);
                
                NSInteger dimension = [[_CIFilter valueForKey:@"inputCubeDimension"] integerValue];
                
                [_unwrappedValues setObject:data forKey:key];
                
                value = [SCFilter colorCubeDataWithCGImage:image dimension:dimension];
                CGImageRelease(image);
            } else {
                NSLog(@"Value for key %@ must be of type NSData (got type: %@)", key, [value class]);
                return nil;
            }
        }
    }
    
    return value;
}

- (void)_didChangeParameter:(NSString *)key {
    if ([key isEqualToString:@"inputCubeDimension"]) {
        NSData *inputCubeData = [_unwrappedValues objectForKey:@"inputCubeData"];
        if (inputCubeData != nil) {
            [self setParameterValue:inputCubeData forKey:@"inputCubeData"];
        }
    }
}

- (id)parameterValueForKey:(NSString *)key {
    return [self _unwrappedValue:[_CIFilter valueForKey:key] forKey:key];
}

- (void)setParameterValue:(id)value forKey:(NSString *)key {
    value = [self _wrappedValue:value forKey:key];
    
    [_CIFilter setValue:value forKey:key];
    
    [self _didChangeParameter:key];
    
    id<SCFilterDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(filter:didChangeParameter:)]) {
        [delegate filter:self didChangeParameter:key];
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    CIFilter *copiedFilter = _CIFilter.copy;
    
    for (NSString *key in _unwrappedValues) {
        [copiedFilter setValue:nil forKey:key];
    }
    
    if (copiedFilter != nil) {
        [aCoder encodeObject:copiedFilter forKey:@"CoreImageFilter"];
    }
    
    [aCoder encodeBool:self.enabled forKey:@"Enabled"];
    
    [aCoder encodeObject:_unwrappedValues forKey:@"UnwrappedValues"];
    
    NSMutableArray *vectors = [NSMutableArray new];
    
    for (NSString *key in _CIFilter.inputKeys) {
        id value = [_CIFilter valueForKey:key];
        
        if ([value isKindOfClass:[CIVector class]]) {
            CIVector *vector = value;
            NSMutableArray *vectorData = [NSMutableArray new];
            [vectorData addObject:key];
            
            for (int i = 0; i < vector.count; i++) {
                CGFloat value = [vector valueAtIndex:i];
                [vectorData addObject:[NSNumber numberWithDouble:(double)value]];
            }
            [vectors addObject:vectorData];
        }
    }
    
    [aCoder encodeObject:vectors forKey:@"VectorsData"];
    
    [aCoder encodeObject:_subFilters forKey:@"SubFilters"];
    [aCoder encodeObject:_name forKey:@"Name"];
    [aCoder encodeObject:_animations forKey:@"Animations"];
}

- (void)resetToDefaults {
    [_CIFilter setDefaults];
    [_unwrappedValues removeAllObjects];
    
    for (SCFilter *subFilter in _subFilters) {
        [subFilter resetToDefaults];
    }
    
    id<SCFilterDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(filterDidResetToDefaults:)]) {
        [delegate filterDidResetToDefaults:self];
    }
}

- (void)addSubFilter:(SCFilter *)subFilter {
    [_subFilters addObject:subFilter];
}

- (void)removeSubFilter:(SCFilter *)subFilter {
    [_subFilters removeObject:subFilter];
}

- (void)insertSubFilter:(SCFilter *)subFilter atIndex:(NSInteger)index {
    [_subFilters insertObject:subFilter atIndex:index];
}

- (void)removeSubFilterAtIndex:(NSInteger)index {
    [_subFilters removeObjectAtIndex:index];
}

- (SCFilterAnimation *)addAnimationForParameterKey:(NSString *)key startValue:(id)startValue endValue:(id)endValue startTime:(CFTimeInterval)startTime duration:(CFTimeInterval)duration {
    SCFilterAnimation *animation = [SCFilterAnimation filterAnimationForParameterKey:key startValue:startValue endValue:endValue startTime:startTime duration:duration];
    
    [self addAnimation:animation];
    
    return animation;
}

- (void)addAnimation:(SCFilterAnimation *)animation {
    [_animations addObject:animation];
}

- (void)removeAnimation:(SCFilterAnimation *)animation {
    [_animations removeObject:animation];
}

- (void)removeAllAnimations {
    [_animations removeAllObjects];
}

- (NSArray *)subFilters {
    return _subFilters;
}

- (CIImage *)imageByProcessingImage:(CIImage *)image {
    return [self imageByProcessingImage:image atTime:0];
}

- (CIImage *)imageByProcessingImage:(CIImage *)image atTime:(CFTimeInterval)time {
    if (!self.enabled) {
        return image;
    }
    
    SCRecorder *recorder = [SCRecorder sharedRecorder];
    
    if ([recorder isSaving] && ![recorder isVideoImported]) {
        /*if ([[SCRecorder sharedRecorder] showRedBand]) {
            image = [self moveRedBandOnImage:image];
        }*/
        
        /* Vapor Effect */
        image = [self applyVaporEffect:image];
        image = [self applyGlitch:image];
        
        if (noisecounter > TOTAL_NOISE_LINES) noisecounter = 1;
        image = [self applySideGlitch:image]; noisecounter++;
        
        return image;
    }
    
    id<SCFilterDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(filter:willProcessImage:atTime:)]) {
        [delegate filter:self willProcessImage:image atTime:time];
    }
    
    for (SCFilter *filter in _subFilters) {
        image = [filter imageByProcessingImage:image atTime:time];
    }
    
    for (SCFilterAnimation *animation in _animations) {
        if ([animation hasValueAtTime:time]) {
            id value = [animation valueAtTime:time];
            [self setParameterValue:value forKey:animation.key];
        }
    }
    
    CIImage *overlayImage = _overlayImage;
    if (overlayImage != nil) {
        image = [overlayImage imageByCompositingOverImage:image];
    }
    
    CIFilter *ciFilter = _CIFilter;
    
    /* If Not the Vapor Effect */
    if (![ciFilter.name isEqualToString:@"CIPhotoEffectFade"] &&
        ![ciFilter.name isEqualToString:@"CIColorCube"] )
    {
        if (ciFilter == nil) {
            return image;
        }
        
        [ciFilter setValue:image forKey:kCIInputImageKey];
        image = [ciFilter valueForKey:kCIOutputImageKey];
    }
    
    // GREEN EFFECT
    if ([ciFilter.name isEqualToString:@"CIColorCube"] ) {
        image = [self chromaKeyImage:image];
    }
    
    CGFloat contrast    = recorder.vContrastControlRatio;
    //CGFloat brightness  = recorder.vBirghtnessControlRatio;
    
    CIFilter *contrastFilter = [CIFilter filterWithName:@"CIColorControls"];
    [contrastFilter setDefaults];
    [contrastFilter setValue:image forKey:kCIInputImageKey];
    [contrastFilter setValue:[NSNumber numberWithFloat:contrast] forKey:kCIInputContrastKey];
    //[contrastFilter setValue:[NSNumber numberWithFloat:brightness] forKey:kCIInputBrightnessKey];
    image = [contrastFilter valueForKey:kCIOutputImageKey];
    
    
    if (![[ciFilter name] isEqualToString:@"CIPhotoEffectNoir"] &&
        ![[ciFilter name] isEqualToString:@"CIPhotoEffectTonal"])
    {
        [_CIToneCurveFilter setInputImage:image];
        [_CIToneCurveFilter setCurrentFXName:[ciFilter name]];
        image = [_CIToneCurveFilter outputImage];
    }        
    
    /*[CIVHSTrackingLinesFilter setInputImage:image];
    image = [CIVHSTrackingLinesFilter outputImage];*/    
    
    image = [self cropAndApplyDistortion:image];
        
    return image;
}

- (CIImage *)cropAndApplyDistortion:(CIImage *)image {
    SCRecorder *recorder = [SCRecorder sharedRecorder];
    BOOL isVidImported   = [recorder isVideoImported];
    BOOL isVidSaving     = [recorder isSaving];
    BOOL isPortraitMode  = [SCRecorder sharedRecorder].videoConfiguration.sizeAsPortrait;
    BOOL isSquareMode    = [SCRecorder sharedRecorder].videoConfiguration.sizeAsSquare;

    /*if (isVidImported) {
        if (!isVidSaving) {
            return image;
        }
    }*/
    
    if (isVidSaving) {
        /* Single Liner Glitch */
        image = [self applyGlitch:image];
    }
    
    /* Crop it */
    if (!isVidImported /*&& (isSquareMode || isPortraitMode)*/) {
        CGFloat width       = image.extent.size.height;
        CGFloat offset      = image.extent.size.width - width;
        CGRect  visibleRect = CGRectMake(offset/2.0, 0, width, image.extent.size.height);
        
        if (isPortraitMode) {
            width = (9.0f/16.0f)*image.extent.size.height;
            offset = image.extent.size.width - width;
            visibleRect = CGRectMake(offset/2.0, 0, width, image.extent.size.height);
            
        } else if (!isSquareMode) { //Landscape
            CGFloat height = (9.0f/16.0f)*image.extent.size.width;
            offset = image.extent.size.height - height;
            visibleRect = CGRectMake(0, offset/2.0, image.extent.size.width, height);
        }
        image = [image imageByCroppingToRect:visibleRect];
    }
    
    if (isVidSaving && isVidImported) {
        /*if ([[SCRecorder sharedRecorder] showRedBand]) {
            image = [self moveRedBandOnImage:image];
        }*/
        
        /* Vapor Effect */
        image = [self applyVaporEffect:image];
        
        if (noisecounter > TOTAL_NOISE_LINES) noisecounter = 1;
        image = [self applySideGlitch:image]; noisecounter++;
    }
    
    return image;
}

- (CIImage *)applyGlitch:(CIImage *)image {
    SCRecorder *recorder = [SCRecorder sharedRecorder];
    
    if (recorder.showHDistortion) {
        recorder.showHDistortion = NO;
        
        NSInteger noofFrames = recorder.vOneLinerFrames.count;
        NSInteger index      = arc4random_uniform((uint32_t) noofFrames);
        CIImage  *receiver   = [recorder.vOneLinerFrames objectAtIndex:index];
        
        CGFloat scale = image.extent.size.width/receiver.extent.size.width;
        NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
        receiver = [receiver imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
        
        CGSize vSizeReceiver = receiver.extent.size;
        CGSize vSizeImage    = image.extent.size;
        if (vSizeReceiver.height > vSizeImage.height ) {
            receiver = [receiver imageByCroppingToRect:CGRectMake(0, 0, vSizeReceiver.width, vSizeImage.height)];
        }
        
        CGFloat           yPos   = arc4random_uniform((uint32_t) (image.extent.size.height*0.25f));
        
        hGlitchCounter++;
        if (hGlitchCounter > 3) {
            hGlitchCounter = 1;
        
            yPos  = arc4random_uniform((uint32_t) (image.extent.size.height*0.75f));
            yPos += image.extent.size.height*0.25f;
        }
        
        CGAffineTransform trans  = CGAffineTransformMakeTranslation(0, yPos);
        NSValue          *value  = [NSValue valueWithBytes:&trans objCType:@encode(CGAffineTransform)];
        NSDictionary<NSString *,NSValue*> *tx = @{@"inputTransform": value};
        
        receiver = [receiver imageByApplyingFilter:@"CIAffineTransform" withInputParameters:tx];
        image    = [receiver imageByCompositingOverImage:image];
    }
    return image;
}

// Vapor Effect
- (CIImage *)applyVaporEffect:(CIImage *)image {
    if ( [_CIFilter.name isEqualToString:@"CIPhotoEffectFade"]) {
        CIImage *receiver        = [SCRecorder sharedRecorder].vVaporCIImage;
        CGFloat  horizontalScale = image.extent.size.width / receiver.extent.size.width;
        CGFloat  verticalScale   = image.extent.size.height / receiver.extent.size.height;
        
        receiver = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
        image    = [receiver imageByCompositingOverImage:image];
    }
    return image;
}

- (CIImage *)applySideGlitch:(CIImage *)image {
    CIImage *receiver = [[SCRecorder sharedRecorder].sidelines objectAtIndex:noisecounter-1];
    CGFloat horizontalScale = image.extent.size.width / receiver.extent.size.width;
    CGFloat verticalScale   = image.extent.size.height / receiver.extent.size.height;
    
    receiver = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
    image    = [receiver imageByCompositingOverImage:image];
    
    return image;
}

- (CIImage *)chromaKeyImage:(CIImage *)myInputImage {
    // Allocate memory
    const unsigned int size = 64;
    float *cubeData = (float *)malloc (size * size * size * sizeof (float) * 4);
    float rgb[3], hsv[3], *c = cubeData;
    float minHueAngle = (120 - 30.0) / 360; //120 degree range = +20 -20
    float maxHueAngle = (120 + 30.0) / 360;
    
    // Populate cube with a simple gradient going from 0 to 1
    for (int z = 0; z < size; z++){
        rgb[2] = ((double)z)/(size-1); // Blue value
        for (int y = 0; y < size; y++){
            rgb[1] = ((double)y)/(size-1); // Green value
            for (int x = 0; x < size; x ++){
                rgb[0] = ((double)x)/(size-1); // Red value
                // Convert RGB to HSV
                // You can find publicly available rgbToHSV functions on the Internet
                rgbToHSV(rgb, hsv);
                // Use the hue value to determine which to make transparent
                // The minimum and maximum hue angle depends on
                // the color you want to remove
                float alpha = (hsv[0] > minHueAngle && hsv[0] < maxHueAngle) ? 0.0f: 1.0f;
                // Calculate premultiplied alpha values for the cube
                c[0] = rgb[0] * alpha;
                c[1] = rgb[1] * alpha;
                c[2] = rgb[2] * alpha;
                c[3] = alpha;
                c += 4; // advance our pointer into memory for the next color value
            }
        }
    }
    // Create memory with the cube data
    size_t cubeDataSize = size * size * size * sizeof ( float ) * 4;
    NSData *data = [NSData dataWithBytesNoCopy:cubeData
                                        length:cubeDataSize
                                  freeWhenDone:YES];
    CIFilter *colorCube = [CIFilter filterWithName:@"CIColorCube"];
    [colorCube setValue:@(size) forKey:@"inputCubeDimension"];
    // Set data for cube
    [colorCube setValue:data forKey:@"inputCubeData"];
    
    [colorCube setValue:myInputImage forKey:kCIInputImageKey];
    CIImage *result = [colorCube valueForKey:kCIOutputImageKey];
    
    CIImage  *receiver   = [SCRecorder sharedRecorder].vGreenEffectBGCIImage;
    CGFloat   scale      = result.extent.size.width/receiver.extent.size.width;
    NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
    receiver = [receiver imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
    
    CGSize vSizeReceiver = receiver.extent.size;
    CGSize vSizeImage    = result.extent.size;
    if (vSizeReceiver.height > vSizeImage.height ) {
        receiver = [receiver imageByCroppingToRect:CGRectMake(0, 0, vSizeReceiver.width, vSizeImage.height)];
    }
    
    CIFilter *ciComposeFilter = [CIFilter filterWithName:@"CISourceOverCompositing"];
    [ciComposeFilter setValue:result forKey:kCIInputImageKey];
    [ciComposeFilter setValue:receiver forKey:kCIInputBackgroundImageKey];
    result = [ciComposeFilter valueForKey:kCIOutputImageKey];
    
    return result;
}

void rgbToHSV(const float *rgb,float *hsv) {
    float minV = MIN(rgb[0], MIN(rgb[1], rgb[2]));
    float maxV = MAX(rgb[0], MAX(rgb[1], rgb[2]));
    
    float chroma = maxV - minV;
    
    hsv[0] = hsv[1] = 0.0;
    hsv[2] = maxV;
    
    if ( maxV != 0.0 )
        hsv[1] = chroma / maxV;
    
    if ( hsv[1] != 0.0 ) {
        if ( rgb[0] == maxV )
            hsv[0] = (rgb[1] - rgb[2])/chroma;
        else if ( rgb[1] == maxV )
            hsv[0] = 2.0 + (rgb[2] - rgb[0])/chroma;
        else
            hsv[0] = 4.0 + (rgb[0] - rgb[1])/chroma;
        
        hsv[0] /= 6.0;
        if ( hsv[0] < 0.0 )
            hsv[0] += 1.0;
    }
}

- (AVMutableVideoComposition *)videoCompositionWithAsset:(AVAsset *)asset {
    if ([[AVVideoComposition class] respondsToSelector:@selector(videoCompositionWithAsset:applyingCIFiltersWithHandler:)]) {
        CIContext *context = [CIContext contextWithOptions:@{kCIContextWorkingColorSpace : [NSNull null], kCIContextOutputColorSpace : [NSNull null]}];
        return [AVMutableVideoComposition videoCompositionWithAsset:asset applyingCIFiltersWithHandler:^(AVAsynchronousCIImageFilteringRequest * _Nonnull request) {
            CIImage *image = [self imageByProcessingImage:request.sourceImage atTime:CMTimeGetSeconds(request.compositionTime)];
            
            [request finishWithImage:image context:context];
        }];
        
    }
    return nil;
}

- (void)writeToFile:(NSURL *)fileUrl error:(NSError *__autoreleasing *)error {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [data writeToURL:fileUrl options:NSDataWritingAtomic error:error];
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    
    if (_CIFilter != nil) {
        return NO;
    }
    
    for (SCFilter *filter in _subFilters) {
        isEmpty &= filter.isEmpty;
    }
    
    return isEmpty;
}

- (NSArray *)animations {
    return _animations;
}

+ (SCFilter *)emptyFilter {
    return [SCFilter filterWithCIFilter:nil];
}

+ (SCFilter *)filterWithAffineTransform:(CGAffineTransform)affineTransform {
    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform"];
    [filter setValue:[NSValue valueWithBytes:&affineTransform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];
    
    return [SCFilter filterWithCIFilter:filter];
}

+ (SCFilter *)filterWithCIFilter:(CIFilter *)filterDescription {
    return [[SCFilter alloc] initWithCIFilter:filterDescription];
}

+ (SCFilter *)filterWithCIFilterName:(NSString *)name {
    CIFilter *coreImageFilter = [CIFilter filterWithName:name];
    [coreImageFilter setDefaults];
    
    return coreImageFilter != nil ? [SCFilter filterWithCIFilter:coreImageFilter] : nil;
}

+ (SCFilter *)filterWithData:(NSData *)data error:(NSError *__autoreleasing *)error {
    id obj = nil;
    @try {
        obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    } @catch (NSException *exception) {
        if (error != nil) {
            *error = [NSError errorWithDomain:@"SCFilterGroup" code:200 userInfo:@{
                                                                                   NSLocalizedDescriptionKey : exception.reason
                                                                                   }];
            return nil;
        }
    }
    
    if (![obj isKindOfClass:[SCFilter class]]) {
        obj = nil;
        if (error != nil) {
            *error = [NSError errorWithDomain:@"FilterDomain" code:200 userInfo:@{
                                                                                  NSLocalizedDescriptionKey : @"Invalid serialized class type"
                                                                                  }];
        }
    }
    
    return obj;
}

+ (SCFilter *)filterWithData:(NSData *)data {
    return [SCFilter filterWithData:data error:nil];
}

+ (SCFilter *)filterWithContentsOfURL:(NSURL *)url {
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    if (data != nil) {
        return [SCFilter filterWithData:data];
    }
    
    return nil;
}

+ (SCFilter *)filterWithFilters:(NSArray *)filters {
    SCFilter *filter = [SCFilter emptyFilter];
    
    for (SCFilter *subFilter in filters) {
        [filter addSubFilter:subFilter];
    }
    
    return filter;
}

+ (SCFilter *)filterWithCIImage:(CIImage *)image {
    SCFilter *filter = [SCFilter emptyFilter];
    filter.overlayImage = image;
    
    return filter;
}

static UInt32 MagicPNG = 0x474e5089;
static UInt32 MagicJPG = 0xe0ffd8ff;

+ (BOOL)data:(NSData *)data hasMagic:(UInt32)magic {
    if (data.length > sizeof(magic)) {
        const UInt32 *bytes = data.bytes;
        UInt32 actualMagic = *bytes;
        
        return actualMagic == magic;
    }
    
    return NO;
}

/////
// These two functions were taken from https://github.com/NghiaTranUIT/FeSlideFilter
//
+ (NSData *)colorCubeDataWithCGImage:(CGImageRef )image dimension:(NSInteger)n {
    if (n == 0) {
        return nil;
    }
    
    NSInteger width = CGImageGetWidth(image);
    NSInteger height = CGImageGetHeight(image);
    NSInteger rowNum = height / n;
    NSInteger columnNum = width / n;
    
    if ((width % n != 0) || (height % n != 0) || (rowNum * columnNum != n)) {
        return nil;
    }
    
    UInt8 *bitmap = [self createRGBABitmapFromImage:image];
    
    if (bitmap == nil) {
        return nil;
    }
    
    const int colorChannels = 4;
    
    NSInteger size = n * n * n * sizeof(float) * colorChannels;
    float *data = malloc(size);
    
    if (data == nil) {
        free(bitmap);
        return nil;
    }
    
    UInt8 *bitmapPtr = bitmap;
    
    int z = 0;
    for (int row = 0; row <  rowNum; row++) {
        for (int y = 0; y < n; y++) {
            int tmp = z;
            for (int col = 0; col < columnNum; col++) {
                for (int x = 0; x < n; x++) {
                    UInt8 r = *bitmapPtr++;
                    UInt8 g = *bitmapPtr++;
                    UInt8 b = *bitmapPtr++;
                    UInt8 a = *bitmapPtr++;
                    
                    NSInteger dataOffset = (z * n * n + y * n + x) * 4;
                    
                    data[dataOffset] = r / 255.0;
                    data[dataOffset + 1] = g / 255.0;
                    data[dataOffset + 2] = b / 255.0;
                    data[dataOffset + 3] = a / 255.0;
                }
                z++;
            }
            z = tmp;
        }
        z += columnNum;
    }
    
    free(bitmap);
    
    return [NSData dataWithBytesNoCopy:data length:size freeWhenDone:YES];
}

+ (unsigned char *)createRGBABitmapFromImage:(CGImageRef)image {
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    
    NSInteger bytesPerRow = (width * 4);
    NSInteger bitmapSize = (bytesPerRow * height);
    
    unsigned char *bitmap = malloc(bitmapSize);
    if (bitmap == nil) {
        return nil;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    if (colorSpace == nil) {
        free(bitmap);
        return nil;
    }
    
    CGContextRef context = CGBitmapContextCreate(bitmap,
                                                 width,
                                                 height,
                                                 8,
                                                 bytesPerRow,
                                                 colorSpace,
                                                 (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGColorSpaceRelease(colorSpace);
    
    if (context == nil) {
        free(bitmap);
        return nil;
    }
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
    
    CGContextRelease(context);
    
    return bitmap;
}

/*- (CIImage *)moveRedBandOnImage:(CIImage *)image {
 SCRecorder *recorder = [SCRecorder sharedRecorder];
 CIImage *redBand = recorder.vRedBandCIImage;
 CGFloat  scale   = image.extent.size.width/redBand.extent.size.width;
 NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
 redBand = [redBand imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
 
 BOOL resetMoveCounter = NO;
 vMoveCounter    += 5.0;
 
 if (vMoveCounter > (image.extent.size.height - redBand.extent.size.height)) {
 resetMoveCounter = YES;
 vMoveCounter = image.extent.size.height - redBand.extent.size.height;
 }
 
 CGAffineTransform trans  = CGAffineTransformMakeTranslation(0, vMoveCounter);
 NSValue          *value  = [NSValue valueWithBytes:&trans objCType:@encode(CGAffineTransform)];
 NSDictionary<NSString *,NSValue*> *tx = @{@"inputTransform": value};
 
 CIImage *redBandTransFormed = [redBand imageByApplyingFilter:@"CIAffineTransform" withInputParameters:tx];
 image = [redBandTransFormed imageByCompositingOverImage:image];
 
 if (resetMoveCounter) {
 recorder.showRedBand = NO;
 [recorder scheduleRedBand];
 vMoveCounter = -5.0;
 }
 
 return image;
 }*/

@end
