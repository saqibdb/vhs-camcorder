//
//  SCFilterImageView.m
//  SCRecorder
//
//  Created by Simon Corsin on 10/8/15.
//  Copyright © 2015 rFlex. All rights reserved.
//

#import "SCFilterImageView.h"
#import "SCRecorder.h"

#define TOTAL_NOISE_LINES 5

@implementation SCFilterImageView

- (CIImage *)renderedCIImageInRect:(CGRect)rect {
    CIImage *image = [super renderedCIImageInRect:rect];

    if (image != nil) {
        if (_filter != nil) {
            if (![SCRecorder sharedRecorder].isSaving) {
                image = [_filter imageByProcessingImage:image atTime:self.CIImageTime];
                
                /*[_trackingFilter setInputImage:image];
                image = [_trackingFilter outputImage];*/
            }
        }
    }

    return image;
}

- (void)setFilter:(SCFilter *)filter {
    _filter = filter;

    [self setNeedsDisplay];
}

/*- (void)setTrackingFilter:(VHSTrackingLines *)filter {
    _trackingFilter = filter;
    
    [self setNeedsDisplay];
}*/

@end
