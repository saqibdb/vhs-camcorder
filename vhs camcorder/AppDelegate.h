//
//  AppDelegate.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Google/Analytics.h>
#import <Photos/PHPhotoLibrary.h>

//@import GoogleMobileAds;

@interface AppDelegate : UIResponder <UIApplicationDelegate>//, GADInterstitialDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) GADInterstitial *interstitial;

@end

