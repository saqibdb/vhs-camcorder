//
//  AppDelegate.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "AppDelegate.h"
#import "ALSdk.h"
#import "IAPHelper.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Util.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIStoryboard *storyboard = [Util grabStoryboard];
    // show the storyboard
    self.window.rootViewController = [storyboard instantiateInitialViewController];
    
    // Configure tracker from GoogleService-Info.plist.
    /*NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release*/
    
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //_interstitial = [self createAndLoadInterstitial];
    
    BOOL firstRun = ![[NSUserDefaults standardUserDefaults] boolForKey:@"is_first_run"];
    if (firstRun) [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"noof_app_run"];
    
    [self initialize];
    //[self getBasicFBPermission];
    [self getSavePermissions];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)getSavePermissions {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined)
        {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status)
            {
                if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized)
                {
                    NSLog(@"PHAuthorizationStatusAuthorized");
                }
            }];
        }
    });
}

-(void)initialize {
    [Util createMyVideosFolderInDocuments];
    [ALSdk initializeSdk];
    [IAPHelper shared];
    
    NSUserDefaults* userDefaults  = [NSUserDefaults standardUserDefaults];
    /*NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
    [dateformater setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateformater setDateFormat:@"MMM.dd yyyy"];
    
    NSString *date = [dateformater stringFromDate: [NSDate date]];
    [userDefaults setObject:[NSString stringWithFormat:@"%@", date] forKey:@"fake_date_created"];*/

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"date_clicked_to_rate"])
        [Util handleRatePopup];
        
    [userDefaults setObject:@"ADD RETRO TITLES!" forKey:@"text_1"];
    [userDefaults setObject:@"(PRESS SETTINGS TO EDIT)" forKey:@"text_2"];
    [userDefaults setObject:@"white" forKey:@"text_color"];
    [userDefaults synchronize];
    
    [Util downloadAndCheckIFRateWindowEnable];
}

//- (GADInterstitial *)createAndLoadInterstitial {
//    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8900215750179558/9054019027"];
//    interstitial.delegate = self;
//    [interstitial loadRequest:[GADRequest request]];
//    return interstitial;
//}

- (void)getBasicFBPermission {
    ACAccountStore* accountStore = [[ACAccountStore alloc] init];
    NSDictionary *options = @{
                              ACFacebookAppIdKey: @"258044444584065",
                              ACFacebookPermissionsKey: @[@"email", ],
                              ACFacebookAudienceKey: ACFacebookAudienceOnlyMe
                              };
    ACAccountType *facebookAccountType = [accountStore
                                          accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    [accountStore requestAccessToAccountsWithType:facebookAccountType options:options completion:^(BOOL granted, NSError *error) {
        if (granted) {
            NSLog(@"access granted");
        } else {
            NSLog(@"access to facebook is not granted");
        }
    }];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"applicationDidEnterBackground");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Util handleRatePopup];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSInteger runTimes = [[NSUserDefaults standardUserDefaults] integerForKey:@"noof_app_run"];
    runTimes++;
    [[NSUserDefaults standardUserDefaults] setInteger:runTimes forKey:@"noof_app_run"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"applicationWillTerminate");
}

//#pragma mark - GADInterstitialDelegate
//
//- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
//    self.interstitial = [self createAndLoadInterstitial];
//}

@end
