//
//  AudioRecorder.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 4/27/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioRecorder : NSObject <AVAudioRecorderDelegate>
{
    AVAudioRecorder *recorder;
    NSURL           *audioOutputFileURL;
}

- (void)beginRecording;
- (void)endRecording;
- (void)resetAudioRecordSession;
- (BOOL)isAudioRecording;
- (NSURL*)getAudioOutputFileURL;

@end
