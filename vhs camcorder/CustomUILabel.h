//
//  CustomUILabel.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/11/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUILabel : UILabel

//@property (nonatomic) IBInspectable CGFloat iphoneFontSize;
//@property (nonatomic) IBInspectable CGFloat ipadFontSize;

@end
