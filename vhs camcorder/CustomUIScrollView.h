//
//  CustomUIScrollView.h
//  VidEditor
//
//  Created by ibuildx on 11/2/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>

typedef NS_ENUM(NSInteger, ContentMode) {
    CameraModeVideo,
    CameraModePhoto
};

@interface CustomUIScrollView : UIScrollView

@property(nonatomic, assign) BOOL                    isContentLoaded;
@property(nonatomic, assign) ContentMode             type;
@property(nonatomic, strong) NSURL                  *assetURL;
@property(nonatomic, assign) CGRect                  defaultFrameRect;
@property(nonatomic, assign) CGSize                  defaultContentSize;
@property(nonatomic, assign) AVPlayerViewController *playerController;
@property(nonatomic, assign) NSDictionary           *resizerInfoDic;

@end
