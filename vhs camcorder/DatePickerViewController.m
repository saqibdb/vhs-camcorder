//
//  DatePickerViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/10/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    [self.timePicker addTarget:self action:@selector(timePickerChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)timePickerChanged:(UIDatePicker *)timePicker {
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) [outputFormatter setDateFormat:@"aa h:mm"];
    else [outputFormatter setDateFormat:@"h:mm"];
    
    NSString *newTimeString = [outputFormatter stringFromDate:timePicker.date];
    
    [[NSUserDefaults standardUserDefaults] setObject:newTimeString forKey:@"fake_time_created"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)datePickerChanged:(UIDatePicker *)datePicker {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"MMM.dd yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    
    [[NSUserDefaults standardUserDefaults] setObject:strDate forKey:@"fake_date_created"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)doneAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
