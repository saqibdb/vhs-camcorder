//
//  EditViewController.h
//  VidEditor
//
//  Created by ibuildx on 10/28/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALiAsset.h"
#import "SAVideoRangeSlider.h"
#import <AVKit/AVKit.h>



@interface EditViewController : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate,SAVideoRangeSliderDelegate , AVPlayerViewControllerDelegate>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *MainView;
@property (weak, nonatomic) IBOutlet UIView *shutterView;

- (IBAction)playAction:(UIButton *)sender;
- (IBAction)saveAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomOfSutter;
- (IBAction)shutterDownAction:(UIButton *)sender;
- (IBAction)cornerRadiusSlider:(UISlider *)sender;
- (IBAction)frameSizeSlider:(UISlider *)sender;
@property (nonatomic, strong) NSURL *selectedURL;
@property (weak, nonatomic) IBOutlet UISlider *cornerSlider;
@property (weak, nonatomic) IBOutlet UISlider *frameSlider;
@property (weak, nonatomic) IBOutlet UIImageView *thumOfVideo;
    @property (strong, nonatomic) NSString *tmpVideoPath;
@property (nonatomic) NSMutableArray *theScrollers;
@property BOOL isWithAudio ;
@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchObject;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property BOOL isSaveClicked ;

@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *pinchGesture;

@property (nonatomic) AVPlayerViewController *playerViewController;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *cropBtn;






@end
