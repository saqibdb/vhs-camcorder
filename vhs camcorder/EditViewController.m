//
//  EditViewController.m
//  VidEditor
//
//  Created by ibuildx on 10/28/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "EditViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ALiAsset.h"
#import "GUIPlayerView.h"
#import "CustomUIScrollView.h"
#import "ALiAsset.h"
#import <AVKit/AVKit.h>
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "UIView+ALi.h"
#import "UIButton+ALi.h"
#import <Photos/Photos.h>

#define WEAKSELF(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define SCREEN_W [UIScreen mainScreen].bounds.size.width
#define SCREEN_H [UIScreen mainScreen].bounds.size.height

@interface EditViewController (){
    
    
    CGPoint center;
    // AVAssetExportSession *exporter;
    CGRect mainVieRect ;
    UIButton           *playBTN;
    NSTimer            *previewTimer;
    CGFloat             timeSpent;
    CGFloat videoDiementions ;
    UIScrollView *zomingView ;
    CGFloat croppingPercent ;
    BOOL disableVerticalScroll ;
    BOOL isFirstTime ;
    UIEdgeInsets originalScrollViewInsets;
    CGSize videosize;
    UIView *orangeView;
}

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view layoutIfNeeded] ;
    self.cropBtn.layer.cornerRadius = 3.0 ;
    [self.cropBtn setClipsToBounds:YES] ;
    isFirstTime = NO;
    self.bottomOfSutter.constant=-self.view.size.height;
    //zoom
    self.MainView.minimumZoomScale=1.0;
    self.MainView.maximumZoomScale=3;
    self.MainView.clipsToBounds=YES;
    self.MainView.contentSize = self.MainView.frame.size;
    
    
    self.MainView.delegate=self;
    
    
    if (!_playerViewController) {
        _playerViewController = [[AVPlayerViewController alloc] init];
        _playerViewController.delegate = self;
        _playerViewController.view.frame = self.playerView.bounds;
        _playerViewController.showsPlaybackControls = NO;
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:self.selectedURL];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
        _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
        
        [ _playerViewController.view setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                                          UIViewAutoresizingFlexibleHeight )];
        [self.playerView setAutoresizesSubviews:YES ];
        
        [self.playerView.layer addSublayer:_playerViewController.view.layer];
    }
    center = self.MainView.center;
    NSString *tempDir = NSTemporaryDirectory();
    self.tmpVideoPath = [tempDir stringByAppendingPathComponent:@"tmpMov.mov"];
}
- (void)layoutSubviews {
    // resize your layers based on the view's new bounds
    self.playerView.bounds = self.MainView.bounds;
}
-(void) viewDidAppear:(BOOL)animated {
    [self.MainView setZoomScale:0.5 animated:YES];
    originalScrollViewInsets = self.MainView.contentInset;
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.selectedURL]] options:nil];
    videosize = [[[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize] ;
    videoDiementions = videosize.width * videosize.height ;
    CGRect croppingRect ;
    croppingRect.origin.x = 0 ;
    croppingRect.origin.y = 0 ;
    
    
    UIImageOrientation videoOrientation = [self getVideoOrientationFromAsset:asset];
    if (videoOrientation == 0 || videoOrientation == 1) { // Potrait
        croppingRect.size.width = videosize.height ;
        croppingRect.size.height = videosize.height ;
        croppingPercent = videosize.width / videosize.height ;
        disableVerticalScroll = NO ;
        
        mainVieRect = CGRectMake((self.MainView.frame.size.width / 2) - ((self.MainView.frame.size.width / (croppingPercent * 1)) / 2) , (self.MainView.frame.size.height / 2) - ((self.MainView.frame.size.height / (croppingPercent * 1)) / 2), self.MainView.frame.size.width / (croppingPercent * 1), self.MainView.frame.size.height / (croppingPercent * 1)) ;
        
        [self.MainView zoomToRect:mainVieRect animated:YES];
    }
    else if (videoOrientation == 2 || videoOrientation == 3){// Landscape        croppingRect.size.width = videosize.height ;
        croppingRect.size.height = videosize.height ;
        croppingPercent = videosize.width / videosize.height ;
        disableVerticalScroll = YES ;
        
       mainVieRect = CGRectMake((self.MainView.frame.size.width / 2) - ((self.MainView.frame.size.width / croppingPercent) / 2), (self.MainView.frame.size.height / 2) - ((self.MainView.frame.size.height / croppingPercent) / 2), self.MainView.frame.size.width / croppingPercent, self.MainView.frame.size.height / croppingPercent) ;
       
        
        
        [self.MainView zoomToRect:mainVieRect animated:YES];
        
        [self.MainView setContentOffset:CGPointMake(0, (self.MainView.contentSize.height / 2)- ((self.MainView.contentSize.height / croppingPercent) / 2))];
        
    }
    else{
        NSLog(@"no supported orientation has been found in this video");
    }
    
    [self.MainView setUserInteractionEnabled:YES] ;
    // CGSize scrollableSize = CGSizeMake(self.MainView.frame.size.width * croppingPercent, mainVieRect.size.height);
    
    self.MainView.delegate = self ;
    self.MainView.bounces = NO ;
    //[self.MainView setContentSize:scrollableSize];
    
    //self.MainView.hidden = YES ;
    self.MainView.pinchGestureRecognizer.enabled = YES;
    self.MainView.panGestureRecognizer.enabled = YES;
    isFirstTime = YES;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    //NSLog(@"%f  *  %f",videosize.width , videosize.height);
    //NSLog(@"zoom scale is %f ", scrollView.zoomScale);
    if (disableVerticalScroll) {
        if (scrollView.zoomScale == croppingPercent){
            if (scrollView.contentOffset.y > (self.MainView.contentSize.height / 2)- ((self.MainView.contentSize.height / croppingPercent) / 2) || scrollView.contentOffset.y < (self.MainView.contentSize.height / 2)- ((self.MainView.contentSize.height / croppingPercent) / 2))
            {
                [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, (self.MainView.contentSize.height / 2)- ((self.MainView.contentSize.height / croppingPercent) / 2))];
                
            }
            
        }
        else {
            /*NSLog(@"-----------------------------------");
             
             NSLog(@"scrollView.zoomScale             %f",scrollView.zoomScale);
             NSLog(@"mainVieRect.origin.y             %f",mainVieRect.origin.y);
             NSLog(@"scrollView.contentOffset.y       %f",scrollView.contentOffset.y);
             NSLog(@"mainVieRect.size.height          %f",mainVieRect.size.height);
             NSLog(@"scrollView.contentSize.height    %f",scrollView.contentSize.height);
             NSLog(@"scrollView.contentSize.width     %f",scrollView.contentSize.width);
             NSLog(@"Added values = %f" , scrollView.contentOffset.y + (mainVieRect.size.height * scrollView.zoomScale));
             
             
             
             
             if (!orangeView) {
             orangeView = [[UIView alloc] initWithFrame:CGRectZero];
             [self.MainView addSubview:orangeView];
             }
             */
            float yAxis = ((scrollView.contentSize.height / croppingPercent) + (mainVieRect.origin.y * scrollView.zoomScale));
            /*NSLog(@"yAxis = %f" , yAxis);
             
             
             orangeView.frame = CGRectMake(0, yAxis - 40, 40, 40);
             
             
             
             orangeView.backgroundColor = [UIColor orangeColor];
             [self.MainView bringSubviewToFront:orangeView];
             */
            if (scrollView.contentOffset.y > (mainVieRect.origin.y * scrollView.zoomScale) && (scrollView.contentSize.height - (mainVieRect.origin.y * scrollView.zoomScale)) <= yAxis) {// scroll any where -- for check of scroll up and down
                [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y)];
            }
            else if(scrollView.contentOffset.y <= (mainVieRect.origin.y * scrollView.zoomScale)){// not make scroll up
                [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, mainVieRect.origin.y * scrollView.zoomScale)];
            }
            else if((scrollView.contentSize.height - (mainVieRect.origin.y * scrollView.zoomScale)) > yAxis){// not make scroll down
                //[scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, yAxis -((mainVieRect.size.height / croppingPercent) * scrollView.zoomScale))];
            }
            else {
                NSLog(@"Something Unusual here.");
            }
            
            
        }
        
    }
    else{
        if (scrollView.zoomScale == croppingPercent){
            if (scrollView.contentOffset.x > (self.MainView.contentSize.width / 2) - ((self.MainView.contentSize.width / croppingPercent) / 2) || scrollView.contentOffset.x < (self.MainView.contentSize.width / 2) - ((self.MainView.contentSize.width / croppingPercent) / 2))
            {
                [scrollView setContentOffset:CGPointMake((self.MainView.contentSize.width / 2) - ((self.MainView.contentSize.width / croppingPercent) / 2), scrollView.contentOffset.y)];
                
            }
            
        }
        else {
            
            
            float xAxis = ((scrollView.contentSize.width / croppingPercent) + (mainVieRect.origin.x * scrollView.zoomScale));
            
            
            if (scrollView.contentOffset.x > (mainVieRect.origin.x * scrollView.zoomScale) && (scrollView.contentSize.width - (mainVieRect.origin.x * scrollView.zoomScale)) <= xAxis) {// scroll any where -- for check of scroll up and down
                [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y)];
            }
            else if(scrollView.contentOffset.x <= (mainVieRect.origin.x * scrollView.zoomScale)){// not make scroll up
                [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x * scrollView.zoomScale, mainVieRect.origin.y )];
            }
            else if((scrollView.contentSize.width - (mainVieRect.origin.x * scrollView.zoomScale)) > xAxis){// not make scroll down
                [scrollView setContentOffset:CGPointMake(xAxis -((mainVieRect.size.width / croppingPercent) * scrollView.zoomScale) , scrollView.contentOffset.y )];
            }
            else {
                NSLog(@"Something Unusual here.");
            }
            
            
        }
        
    }
    /*NSLog(@"scrollViewDidScroll");
     
     NSLog(@"%f xxx",scrollView.contentOffset.x);
     NSLog(@"%f yyy",scrollView.contentOffset.y);
     NSLog(@"%f croppingPercent",croppingPercent);
     
     
     if (scrollView.contentOffset.y <= 80 || scrollView.contentOffset.y >= 150)
     {
     [self.MainView setContentOffset:CGPointMake(0, (self.MainView.contentSize.height / 2)- ((self.MainView.contentSize.height / croppingPercent) / 2))];
     
     // NSLog(@"yyyyyyyyy");
     
     }
     
     
     else if (scrollView.contentOffset.x <= 80 || scrollView.contentOffset.x >= 150)
     {
     // NSLog(@"xxxxxxx");
     }
     */
    
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale{
    NSLog(@"sfd%f",scrollView.zoomScale);
    
    
    
    if ([scrollView zoomScale] <= croppingPercent){
        
        [scrollView zoomToRect:mainVieRect animated:YES];
        
        //[self.MainView setContentOffset:CGPointMake(0, (self.MainView.contentSize.height / 2)- ((self.MainView.contentSize.height / croppingPercent) / 2))];
        
    }
    else{
        CGFloat excessiveWidth = MAX(0.0, self.MainView.bounds.size.width - self.MainView.contentSize.width),
        excessiveHeight = MAX(0.0, self.MainView.bounds.size.height - self.MainView.contentSize.height),
        insetX = excessiveWidth / 2.0,
        insetY = excessiveHeight / 2.0;
        
        self.MainView.contentInset = UIEdgeInsetsMake(
                                                      MAX(insetY, originalScrollViewInsets.top),
                                                      MAX(insetX, originalScrollViewInsets.left),
                                                      MAX(insetY, originalScrollViewInsets.bottom),
                                                      MAX(insetX, originalScrollViewInsets.right)
                                                      );
    }
    if (disableVerticalScroll) {
        if(scrollView.contentOffset.y > ((mainVieRect.origin.y * scrollView.zoomScale) + (mainVieRect.size.height ))){
            [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, (mainVieRect.origin.y * scrollView.zoomScale) + (mainVieRect.size.height ))];
        }
    }
    
    
}

#pragma mark - Process

- (void)playVideo:(id)sender
{
    
    [[_playerViewController player] play];
    /*
     timeSpent = 0.0;
     previewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
     target:self
     selector:@selector(checkPlayerTimer:)
     userInfo:nil
     repeats:YES];
     */
}
#pragma mark - Video Player Delegate

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    playBTN.hidden = NO;
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    zomingView = scrollView ;
    return self.playerView;
    
}

-(NSString*) applicationDocumentsDirectory
{
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    return basePath;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backAction:(UIButton *)sender {
    self.isSaveClicked = NO ;
    [self performSegueWithIdentifier:@"editToGallery" sender:self];
}


- (IBAction)playAction:(UIButton *)sender {
    
    
    [self playVideo:self];
    //[self.playerView setHidden:NO];
    //[self.MainView setHidden:YES];
    
}
- (CGRect)zoomRectForScrollView:(UIScrollView *)scrollView withCenter:(CGPoint)centera {
    
    
    CGRect zoomRect;
    CGFloat newZoomScale = self.MainView.zoomScale ;
    newZoomScale = MIN(newZoomScale, self.MainView.maximumZoomScale);
    
    NSString *tempString = [NSString stringWithFormat:@"%f" , videoDiementions];
    tempString=[tempString substringToIndex:1];
    
    int devidend = [tempString intValue];
    CGFloat toDevide = devidend/2;
    if (toDevide == 1 || toDevide == 0){
        
        toDevide = toDevide + 0.8 ;
    }
    if (videoDiementions <= 600000){
        
    }
    else {
        newZoomScale = newZoomScale /toDevide;
    }
    
    zoomRect.size.height = scrollView.frame.size.height/newZoomScale;
    zoomRect.size.width  = scrollView.frame.size.width/newZoomScale;
    zoomRect.origin.x = centera.x;
    zoomRect.origin.y = centera.y;
    
    return zoomRect ;
}

- (IBAction)saveAction:(UIButton *)sender {
    
    [SVProgressHUD showWithStatus:@"Cropping ..."];
    NSLog(@"zoom scale is %f ", self.MainView.zoomScale);
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.selectedURL]] options:nil];
    
    
    videosize = [[[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize] ;
    NSLog(@"%f  *  %f",videosize.width , videosize.height);
    CGRect croppingRect ;
    UIImageOrientation videoOrientation = [self getVideoOrientationFromAsset:asset];
    if (videoOrientation == 0 || videoOrientation == 1) { // Potrait
        croppingRect.size.width = videosize.height ;
        croppingRect.size.height = videosize.height ;
        croppingRect.origin.x = 0 ;
        croppingRect.origin.y =  (videosize.width / 2) - (videosize.height / 2)  ;
    }
    else if (videoOrientation == 2 || videoOrientation == 3){// Landscape
        if (self.MainView.zoomScale == croppingPercent) {
            croppingRect.size.width = videosize.height / (self.MainView.zoomScale);
            croppingRect.size.height = videosize.height / (self.MainView.zoomScale );
            
            
            
            croppingRect.origin.x = (videosize.width / 2) - (videosize.height / 2) ;
            croppingRect.origin.y = 0 ;
        }
        else{
            croppingRect.size.width = videosize.width / (self.MainView.zoomScale);
            croppingRect.size.height = videosize.width / (self.MainView.zoomScale );
            
            NSLog(@"self.MainView.contentOffset.y    is %f", self.MainView.contentOffset.y);
            NSLog(@"mainVieRect.origin.y             is %f", mainVieRect.origin.y);
            NSLog(@"self.MainView.contentSize.height is %f", self.MainView.contentSize.height);
            
            croppingRect.origin.x = (videosize.width / self.MainView.contentSize.width) * self.MainView.contentOffset.x ;
            
            float f1 = self.MainView.contentSize.height - (mainVieRect.origin.y * self.MainView.zoomScale);
            float f2 = (videosize.height / f1 ) * self.MainView.contentOffset.y;
            float f3 = f2 - (mainVieRect.origin.y / croppingPercent);
            
            float nf1 = (self.MainView.contentOffset.y / self.MainView.zoomScale) - mainVieRect.origin.y;
            //float nf2 = (videosize.height / f1 ) * self.MainView.contentOffset.y;
            //float nf3 = f2 - (mainVieRect.origin.y / croppingPercent);
            
            
            croppingRect.origin.y = (f1) * (f3) ;
            float newFormula = (videosize.height / (self.MainView.contentSize.height -(mainVieRect.origin.y * self.MainView.zoomScale))) - (mainVieRect.origin.y / croppingRect.origin.y) ;
            NSLog(@"newFormula is %f", newFormula);
            croppingRect.origin.y = nf1;
            // 0.37 * 428 - 426
            
            //croppingRect.origin.y = (self.MainView.contentOffset.y  / (self.MainView.zoomScale ) - mainVieRect.origin.y) ;
            
        }
        
        
        
        
    }
    else{
        NSLog(@"no supported orientation has been found in this video");
    }
    
    //CGFloat offsetRatio = self.MainView.contentSize.width / self.MainView.contentOffset.x ;
   // CGFloat offsetRatioy = self.MainView.contentSize.height / self.MainView.contentOffset.y ;
    
    //croppingRect.origin.x = videosize.width / offsetRatio ;
    //scroppingRect.origin.y = videosize.height / offsetRatioy ;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    NSDate *date = [NSDate date];
    NSString *tempDir   = [NSTemporaryDirectory() stringByAppendingString:[formatter stringFromDate:date]];
    NSString *d         = [NSString stringWithFormat:@"%@%lld.mov", tempDir, arc4random() % 5000000000];
    NSURL    *outputURL = [NSURL fileURLWithPath:d];
    
    CGFloat nearestWidth = (int)croppingRect.size.width - ((int)croppingRect.size.width  % 16 );
    CGFloat nearestHeight = (int)croppingRect.size.height - ((int)croppingRect.size.height  % 16 );

    //CGRect tempRect = [self zoomRectForScrollView:self.MainView  withCenter:croppingRect.origin];
    croppingRect = CGRectMake(croppingRect.origin.x, croppingRect.origin.y, nearestWidth, nearestHeight);
    
    
    
    AVAssetTrack *videoAsset3Track = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CMTime duration3 = videoAsset3Track.timeRange.duration;
    
    
    
    
    
    
    
    
    
    [self applyCropToVideoWithAsset:asset AtRect:croppingRect OnTimeRange:CMTimeRangeMake(kCMTimeZero, duration3) ExportToUrl:outputURL ExistingExportSession:nil WithCompletion:^(BOOL success, NSError *error, NSURL *videoUrl) {
        if (success) {
            //[self cutFinalFrameVideoForURL:videoUrl];
            
            [SVProgressHUD dismiss] ;

            self.selectedURL = videoUrl;
            
            self.isSaveClicked = YES ;
            
            [self performSegueWithIdentifier:@"editToGallery" sender:nil];
            
            
            
            
            
        }
        else{
            [SVProgressHUD dismiss] ;
            NSLog(@"There is an error %@" , error.localizedDescription) ;
            
        }
    }];
    
}

- (CGRect)estimateScrollContentSize:(UIScrollView *)scroller withAsset:(NSURL *)mediaURL
{
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:mediaURL options:nil];
    AVAssetTrack *track = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    
    CGSize mSize = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
    CGSize mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
    CGSize scrollSize = scroller.frame.size;
    
    CGRect newRect;
    if (scrollSize.width >= scrollSize.height)
    {
        // Landscape
        if (mediaSize.width >= mediaSize.height)
        {
            // Landscape
            float scrollAspect = scrollSize.width/scrollSize.height;
            float mediaAspect = mediaSize.width/mediaSize.height;
            
            if (mediaAspect <= scrollAspect) {
                float wRatio = scrollSize.width/mediaSize.width;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            } else {
                float wRatio = scrollSize.height/mediaSize.height;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            }
        } else
        {
            // Portrait
            float wRatio = scrollSize.width/mediaSize.width;
            newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
        }
        
    } else
    {
        // Portrait
        if (mediaSize.width < mediaSize.height)
        {
            // Portrait
            float scrollAspect = scrollSize.width/scrollSize.height;
            float mediaAspect = mediaSize.width/mediaSize.height;
            
            if (mediaAspect <= scrollAspect) {
                float wRatio = scrollSize.width/mediaSize.width;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            } else {
                float wRatio = scrollSize.height/mediaSize.height;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            }
        } else
        {
            // Landscape
            float wRatio = scrollSize.height/mediaSize.height;
            newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
        }
    }
    
    return newRect;
}
- (void)resizeAssetAtURL:(NSURL *)mediaURL withContentRect:(CGRect)contentRect
{
    AVAsset    *videoAsset  = [AVAsset assetWithURL:mediaURL];
    AVURLAsset *asset       = [AVURLAsset URLAssetWithURL:mediaURL options:nil];
    
    AVAssetTrack *track     = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    CGSize        mSize     = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
    CGSize        mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
    
    CGFloat      scalValue  =  contentRect.size.width / mediaSize.width;
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.renderSize = contentRect.size;
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    
    AVMutableVideoCompositionLayerInstruction *transformer = [AVMutableVideoCompositionLayerInstruction
                                                              videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    //CGAffineTransform t1 = CGAffineTransformTranslate(clipVideoTrack.preferredTransform, 0, 0);
    //CGAffineTransform t = CGAffineTransformRotate(t1, M_PI_2);
    CGAffineTransform scaleTranform = CGAffineTransformMakeScale(scalValue, scalValue);
    CGAffineTransform transform     = CGAffineTransformConcat(clipVideoTrack.preferredTransform, scaleTranform);
    
    [transformer setTransform:transform atTime:kCMTimeZero];
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject:instruction];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    NSDate *date = [NSDate date];
    
    NSString *tempDir   = [NSTemporaryDirectory() stringByAppendingString:[formatter stringFromDate:date]];
    NSString *d         = [NSString stringWithFormat:@"%@%d.mov", tempDir, arc4random() % 5000];
    NSURL    *outputURL = [NSURL fileURLWithPath:d];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:videoAsset presetName:AVAssetExportPresetHighestQuality];
    exporter.videoComposition = videoComposition;
    exporter.outputURL = outputURL;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exporter.status == AVAssetExportSessionStatusCompleted)
            {
                self.selectedURL = outputURL;
                self.isSaveClicked = YES ;
                [self performSegueWithIdentifier:@"editToGallery" sender:nil];
            }
            else{
                NSLog(@"Export failed: %@", [[exporter error] localizedDescription]);
            }
        });
    }];
}



- (IBAction)shutterDownAction:(UIButton *)sender {
    [UIView animateWithDuration:0.5 animations:^{
        
        self.bottomOfSutter.constant=-self.view.size.height;
        
        [self.shutterView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        
    }];
    
}

- (IBAction)cornerRadiusSlider:(UISlider *)sender {
    
    self.MainView.layer.cornerRadius = self.cornerSlider.value;
    
}

- (IBAction)frameSizeSlider:(UISlider *)sender {
    
    
}


- (CGRect) zoomedFrame{
    CGRect zoomedFrame;
    
    zoomedFrame.origin = self.MainView.contentOffset;
    zoomedFrame.origin.x -= self.playerViewController.view.frame.origin.x;
    zoomedFrame.origin.y -= self.playerViewController.view.frame.origin.y;
    zoomedFrame.size = self.MainView.contentSize;
    return zoomedFrame;
}

- (void) CropVideoSquare{
    NSLog(@"%@",self.selectedURL);
    CGRect contentRect =   [self estimateScrollContentSize:self.MainView withAsset:self.selectedURL];
    
    AVAsset    *videoAsset  = [AVAsset assetWithURL:self.selectedURL];
    AVURLAsset *asset       = [AVURLAsset URLAssetWithURL:self.selectedURL options:nil];
    
    AVAssetTrack *track     = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    CGSize        mSize     = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
    CGSize        mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
    
    CGFloat      scalValue  =  contentRect.size.width / mediaSize.width;
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.renderSize = contentRect.size;
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    
    AVAssetTrack *videoAsset3Track = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CMTime duration3 = videoAsset3Track.timeRange.duration;
    
    
    CMTime _10 = CMTimeMakeWithSeconds(1, 30);
    CMTime tMinus10 = CMTimeSubtract(duration3, _10);

    
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, tMinus10);
    
    AVMutableVideoCompositionLayerInstruction *transformer = [AVMutableVideoCompositionLayerInstruction
                                                              videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    //CGAffineTransform t1 = CGAffineTransformTranslate(clipVideoTrack.preferredTransform, 0, 0);
    //CGAffineTransform t = CGAffineTransformRotate(t1, M_PI_2);
    CGAffineTransform scaleTranform = CGAffineTransformMakeScale(scalValue, scalValue);
    CGAffineTransform transform     = CGAffineTransformConcat(clipVideoTrack.preferredTransform, scaleTranform);
    
    [transformer setTransform:transform atTime:kCMTimeZero];
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject:instruction];
    
    
    
    NSString* documentsDirectory= [self applicationDocumentsDirectory];
    
    NSTimeInterval timeInSeiconds = [[NSDate date] timeIntervalSince1970];
    NSString *timeInSeicondsStr = [NSString stringWithFormat:@"%f.mp4",timeInSeiconds];
    NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:timeInSeicondsStr];
    
    NSURL    *outputURL = [NSURL fileURLWithPath:myDocumentPath];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:videoAsset presetName:AVAssetExportPresetHighestQuality];
    exporter.videoComposition = videoComposition;
    exporter.outputURL = outputURL;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exporter.status == AVAssetExportSessionStatusCompleted)
            {
                [self exportDidFinish:exporter];            }
            
            /*
             CustomUIScrollView *scroller = [_collageFrame viewWithTag:selectedScrollerIndex];
             scroller.assetURL   = exporter.outputURL;
             scroller.type       = CameraModeVideo;
             scroller.isContentLoaded = YES;
             [_theScrollers addObject:scroller];
             
             [self tuneScroll:scroller withContentSize:scroller.frame.size  withContent:YES];
             [[_collageFrame viewWithTag:selectedScrollerIndex+1] setHidden:YES];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             */
        });
    }];
    
    
    
    
    //
    //
    //  CGRect visibleRect = self.playerView.frame;
    //
    //    CGAffineTransform t1 = CGAffineTransformIdentity;
    //    CGAffineTransform t2 = CGAffineTransformIdentity;
    //
    //
    //    //load our movie Asset
    //    AVAsset *asset = [AVAsset assetWithURL:self.selectedURL];
    //    UIImageOrientation videoOrientation = [self getVideoOrientationFromAsset:asset];
    //    //create an avassetrack with our asset
    //    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    //
    //    //create a video composition and preset some settings
    //    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    //    videoComposition.frameDuration = CMTimeMake(1, 30);
    //    //here we are setting its render size to its height x height (Square)
    //    videoComposition.renderSize = CGSizeMake(visibleRect.size.width, visibleRect.size.height);
    //    //create a video instruction
    //    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    //    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
    //
    //    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    //
    //    //Here we shift the viewing square up to the TOP of the video so we only see the top
    //    //CGAffineTransform t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, 0 );
    //
    //    //Use this code if you want the viewing square to be in the middle of the video
    // //t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, -(clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height) /2 );
    //    t1=CGAffineTransformScale(self.MainView.transform, 3, 3);
    //
    //    //Make sure the square is portrait
    //    // t2 = CGAffineTransformRotate(t1, 0);
    //
    ////    switch (videoOrientation) {
    ////        case UIImageOrientationUp:
    ////            t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height - visibleRect.origin.x, 0 - visibleRect.origin.y );
    ////            t2 = CGAffineTransformRotate(t1, M_PI_2 );
    ////            break;
    ////        case UIImageOrientationDown:
    ////            t1 = CGAffineTransformMakeTranslation(0 - visibleRect.origin.x, clipVideoTrack.naturalSize.width - visibleRect.origin.y ); // not fixed width is the real height in upside down
    ////            t2 = CGAffineTransformRotate(t1, - M_PI_2 );
    ////            break;
    ////        case UIImageOrientationRight:
    ////            t1 =CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height, -(clipVideoTrack.naturalSize.width - clipVideoTrack.naturalSize.height) /2  );
    ////            t2 = CGAffineTransformRotate(t1, 0 );
    ////            break;
    ////        case UIImageOrientationLeft:
    ////            t1 =CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.width - visibleRect.origin.x, clipVideoTrack.naturalSize.height - visibleRect.origin.y );
    ////            t2 = CGAffineTransformRotate(t1, M_PI  );
    ////            break;
    ////        default:
    ////            NSLog(@"no supported orientation has been found in this video");
    ////            break;
    ////    }
    //
    //    CGAffineTransform finalTransform = t2;
    //    [transformer setTransform:finalTransform atTime:kCMTimeZero];
    //    [transformer setCropRectangleRampFromStartCropRectangle:visibleRect toEndCropRectangle:visibleRect timeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)];
    //
    //    //add the transformer layer instructions, then add to video composition
    //    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    //    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    //
    //    NSString* documentsDirectory= [self applicationDocumentsDirectory];
    //
    //    NSTimeInterval timeInSeiconds = [[NSDate date] timeIntervalSince1970];
    //    NSString *timeInSeicondsStr = [NSString stringWithFormat:@"%f.mp4",timeInSeiconds];
    //    NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:timeInSeicondsStr];
    //
    //    NSURL *exportUrl = [NSURL fileURLWithPath:myDocumentPath];
    //
    //    //Remove any prevouis videos at that path
    //    [[NSFileManager defaultManager]  removeItemAtURL:exportUrl error:nil];
    //
    //    //Export
    //    exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality] ;
    //    exporter.videoComposition = videoComposition;
    //    exporter.outputURL = exportUrl;
    //    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    //
    //    [exporter exportAsynchronouslyWithCompletionHandler:^
    //     {
    //         dispatch_async(dispatch_get_main_queue(), ^{
    //             //Call when finished
    //             [self exportDidFinish:exporter];
    //        });
    //     }];
}


- (void)exportDidFinish:(AVAssetExportSession*)session
{
    //Play the New Cropped video
    NSURL *outputURL = session.outputURL;
    self.selectedURL=outputURL;
    NSLog(@"%@",self.selectedURL);
    
    
    
}


//**************************** NEW TEST ****************************
- (UIImageOrientation)getVideoOrientationFromAsset:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    
    //    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    //    CMTime time = CMTimeMake(1, 1);
    //    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    //    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    //    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    //
    //    if (thumbnail.size.height > thumbnail.size.width) {
    //        // potrait video
    //        NSLog(@"portrait");
    //         return UIImageOrientationUp;
    //    } else {
    //        // landscape
    //        NSLog(@"landscape");
    //        return UIImageOrientationRight;
    //
    //    }
    if (size.width == txf.tx && size.height == txf.ty)
        return UIImageOrientationLeft; //return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIImageOrientationRight; //return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIImageOrientationDown; //return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIImageOrientationUp;  //return UIInterfaceOrientationPortrait;
}
// apply the crop to passed video asset (set outputUrl to avoid the saving on disk ). Return the exporter session object
- (AVAssetExportSession*)applyCropToVideoWithAsset:(AVAsset*)asset AtRect:(CGRect)cropRect OnTimeRange:(CMTimeRange)cropTimeRange ExportToUrl:(NSURL*)outputUrl ExistingExportSession:(AVAssetExportSession*)exporter WithCompletion:(void(^)(BOOL success, NSError* error, NSURL* videoUrl))completion
{
    
    //create an avassetrack with our asset
    
    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    //create a video composition and preset some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    
    //AVMutableCompositionTrack * videoComposition = [videoComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];

    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    CGFloat cropOffX = cropRect.origin.x;
    CGFloat cropOffY = cropRect.origin.y;
    CGFloat cropWidth = cropRect.size.width;
    CGFloat cropHeight = cropRect.size.height;
    
    videoComposition.renderSize = CGSizeMake(cropWidth, cropHeight);
    
    //create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = cropTimeRange;
    
    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    
    UIImageOrientation videoOrientation = [self getVideoOrientationFromAsset:asset];
    
    CGAffineTransform t1 = CGAffineTransformIdentity;
    CGAffineTransform t2 = CGAffineTransformIdentity;
    
    switch (videoOrientation) {
        case UIImageOrientationUp:
            t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height - cropOffX, 0 - cropOffY );
            t2 = CGAffineTransformRotate(t1, M_PI_2 );
            break;
        case UIImageOrientationDown:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, clipVideoTrack.naturalSize.width - cropOffY ); // not fixed width is the real height in upside down
            t2 = CGAffineTransformRotate(t1, - M_PI_2 );
            break;
        case UIImageOrientationRight:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, 0 - cropOffY );
            t2 = CGAffineTransformRotate(t1, 0 );
            break;
        case UIImageOrientationLeft:
            t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.width - cropOffX, clipVideoTrack.naturalSize.height - cropOffY );
            t2 = CGAffineTransformRotate(t1, M_PI  );
            break;
        default:
            NSLog(@"no supported orientation has been found in this video");
            break;
    }
    
    
    
    CGAffineTransform finalTransform = t2;
    [transformer setTransform:finalTransform atTime:kCMTimeZero];
    
    //add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL:outputUrl error:nil];
    
    if (!exporter){
        exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality] ;
    }
    
    // assign all instruction for the video processing (in this case the transformation for cropping the video
    exporter.videoComposition = videoComposition;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    if (outputUrl){
        
        exporter.outputURL = outputUrl;
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([exporter status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"crop Export failed: %@", [[exporter error] description]);
                    if (completion){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(NO,[exporter error],nil);
                        });
                        return;
                    }
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"crop Export canceled");
                    if (completion){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(NO,nil,nil);
                        });
                        return;
                    }
                    break;
                default:
                    break;
            }
            
            if (completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,outputUrl);
                });
            }
            
        }];
    }
    
    return exporter;
}



-(void)cutFinalFrameVideoForURL :(NSURL *)assetURL
{

    AVAsset *anAsset;
    anAsset = [[AVURLAsset alloc] initWithURL:assetURL options:nil];

    
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetHighestQuality];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs ;
        
        if(self.selectedURL != nil){
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalVideoEdtorFramed-%d.mov", arc4random() % 100000]];
            exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        }
        else{
            
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalAudioFramed-%d.m4a", arc4random() % 100000]];
            exportSession.outputFileType = AVFileTypeMPEG4;
            
        }
        NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
        
        exportSession.outputURL = outputurl;
        
        
        CMTime start = CMTimeMakeWithSeconds(0 , anAsset.duration.timescale);
        //CMTime duration = CMTimeMakeWithSeconds((anAsset.duration.value ) - (3 * anAsset.duration.timescale), anAsset.duration.timescale);
        
        CMTime duration = anAsset.duration;
        
        
        CMTime newTimeDuration = CMTimeMakeWithSeconds((anAsset.duration.value/anAsset.duration.timescale ) - (1), anAsset.duration.timescale);
        
        CMTimeRange range = CMTimeRangeMake(start, newTimeDuration);
        exportSession.timeRange = range;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];

                if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                    self.selectedURL = exportSession.outputURL;
                    
                    self.isSaveClicked = YES ;
                    
                    [self performSegueWithIdentifier:@"editToGallery" sender:nil];
                    
                    
                    
                    
                } else if (exportSession.status == AVAssetExportSessionStatusFailed)
                {
                    NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                   
                    
                    
                } else if (exportSession.status == AVAssetExportSessionStatusCancelled)
                {
                    NSLog(@"Export canceled");
                    
                }
            });
        }];
    }
    
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}


@end
