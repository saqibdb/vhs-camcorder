//
//  IAPHelper.m
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//

// 1
#import "IAPHelper.h"
#import <StoreKit/StoreKit.h>
//#import "ThirdPartyConfig.h"

#define PREMIUM_IAP @"com.shadiosta.vhseditor.premium"

NSString *const IAPHelperProductPurchasedNotification  = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductPurchasingNotification = @"IAPHelperProductPurchasingNotification";

// 2
@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end

static IAPHelper *sharedInstance = NULL;

// 3
@implementation IAPHelper {
    SKProductsRequest * _productsRequest;
    RequestProductsCompletionHandler _completionHandler;
        
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;
    int purchaseComplete; //0=idle , 1=inprogress , 2=complete , 3=fail , 4 = failed to load
    
    int restoreComplete; //0 = idle , 1 = inprogress , 2 = complete , 3 = fail
    NSString *currentPurchaseInProgress;
}

+ (IAPHelper*) shared {
    if (!sharedInstance) {
        sharedInstance = [[IAPHelper alloc] initWithProductIdentifiers:[NSSet setWithObject:PREMIUM_IAP]];
        [sharedInstance requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                //            for (SKProduct *obj in products)
                //            {
                //                NSLog(@"Product id %@" , obj.productIdentifier);
                //            }
                
                sharedInstance.productsList = products;
            }
        }];
    }
    
    return sharedInstance;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if ((self = [super init])) {
        
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
//        // Check for previously purchased products
//        _purchasedProductIdentifiers = [NSMutableSet set];
//        for (NSString * productIdentifier in _productIdentifiers) {
//            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
//            if (productPurchased) {
//                [_purchasedProductIdentifiers addObject:productIdentifier];
//                NSLog(@"Previously purchased: %@", productIdentifier);
//            } else {
//                NSLog(@"Not purchased: %@", productIdentifier);
//            }
//        }
        
        // Add self as transaction observer
        purchaseComplete = 0;
        restoreComplete = 0;
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
    }
    return self;
    
}


- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    if (!_completionHandler) {
        // 1
        _completionHandler = [completionHandler copy];
        
        // 2
        _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
        _productsRequest.delegate = self;
        [_productsRequest start];
    }
    else
    {
        NSLog(@"Duplicate call!");
    }
}

- (BOOL)productPurchased:(NSString *)productIdentifier {
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    if (!product) {
        return;
    }
    
    NSLog(@"Buying %@...", product.productIdentifier);
    
    currentPurchaseInProgress = product.productIdentifier;
    purchaseComplete = 1;
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];    
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    purchaseComplete = 4;
    
}

#pragma mark SKPaymentTransactionOBserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasingNotification
                                                                    object:nil userInfo:nil];
                break;
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

/*- (NSData *)receiptForPaymentTransaction:(SKPaymentTransaction *)transaction {
    if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
        return transaction.transactionReceipt;
    } else if (transaction.transactionState == SKPaymentTransactionStateRestored) {
        return transaction.originalTransaction.transactionReceipt;
    }
    return nil;
}*/

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    purchaseComplete = 2;
    
    //NSData *rec = [self receiptForPaymentTransaction:transaction];
    currentPurchaseInProgress = @"";
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    
    //NSData *rec = [self receiptForPaymentTransaction:transaction];
    currentPurchaseInProgress = PREMIUM_IAP;
    
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"failedTransaction...");
    
    if (transaction.error.code != SKErrorPaymentCancelled) {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    purchaseComplete = 3;
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification
                                                        object:nil
                                                      userInfo:@{@"status": @NO,
                                                                 @"message": transaction.error.localizedDescription}];
}

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    [_purchasedProductIdentifiers addObject:productIdentifier];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"off_watermark"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_premium_purchased"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification
                                                        object:nil
                                                      userInfo:@{@"status": @YES,
                                                                 @"message": @"Completed Transaction!"}];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    for (SKPaymentTransaction *transaction in queue.transactions) {
        NSLog(@"Product id : %@" , transaction.payment.productIdentifier );
        
        if ([transaction.payment.productIdentifier isEqualToString:PREMIUM_IAP]) { //id here
            NSLog(@"Restore Complete");
            restoreComplete = 2;
            return;
        }
    }
    
    restoreComplete = 3;
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"Restore Failed");
    restoreComplete = 3; //restore failed
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification
                                                        object:nil
                                                      userInfo:@{@"status": @NO,
                                                                 @"message": error.localizedDescription}];
}

- (void)restoreCompletedTransactions {
    restoreComplete = 1; //in progress
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(int)getPurchaseCompleted
{
    return purchaseComplete;
}

- (void)clearPurchaseComplete
{
    purchaseComplete = 0;
}

-(int) getRestoreComplete
{
    return restoreComplete;
}

-(void) clearRestoreComplete
{
    restoreComplete = 0;
}

@end
