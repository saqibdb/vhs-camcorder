//
//  SettingsViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/9/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "SAVideoRangeSlider.h"
#import "SCRecorder.h"
#import "CustomUIButton.h"
#import "Util.h"

@interface ImportViewController : UIViewController <SAVideoRangeSliderDelegate, UIScrollViewDelegate, SCPlayerDelegate> {
    
    __weak IBOutlet UIImageView *vGlitchView;
    __weak IBOutlet UIImageView *vVaporView;
    //__weak IBOutlet NSLayoutConstraint *vRedBandBottomConstraint;
    //__weak IBOutlet UIImageView *vRedBand;

    CMTime               vOriginalClipFrameTime;
    CMTime               frameTime;
    //NSTimer             *vRedBandMoveTimer;
    NSTimer             *vGlitchViewControlTimer;
    NSTimer             *vOneLinerFrameChanger;
    NSArray<UIImage*>   *vOneLinerHNoiseFrames;
    
    AVAssetReader       *reader;
    AVAssetWriter       *writer;
    AVAssetWriterInputPixelBufferAdaptor *assetWriterPixelBufferAdaptor;

    UIImageView            *vImageContentView;
    UIImage                *filterPhoto;
    UIView                 *vZoomingView;
    AVPlayerViewController *vPlayerController;
    SAVideoRangeSlider     *videoFrameRangeSlider;
    NSArray                *vEffects;
    NSArray                *vGlitchFrames;
    SCPlayer               *vPlayer;
    NSURL                  *origVideoUrl;
    
    int                     hGlitchCounter;
    int                     currentEffectIndex;
    BOOL                    vFirstLaunch;
    BOOL                    isOpeningShareScreen;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoContainerWidthContraint;
@property (weak, nonatomic) IBOutlet UIImageView  *sidesAnimView;
@property (weak, nonatomic) IBOutlet UIImageView  *vhsTrackingLines;
@property (weak, nonatomic) IBOutlet UIView       *vCheckMarkContainer;
@property (weak, nonatomic) IBOutlet UIView       *vVideoContentView;
@property (weak, nonatomic) IBOutlet UIScrollView *vVideoScroller;
@property (weak, nonatomic) IBOutlet UIView       *vVideoFrames;
@property (weak, nonatomic) IBOutlet UIButton     *dateBTN;
@property (weak, nonatomic) IBOutlet UIButton     *dYesBox;
@property (weak, nonatomic) IBOutlet UIButton     *dNoBox;
@property (weak, nonatomic) IBOutlet UILabel      *vPlayText;
@property (weak, nonatomic) IBOutlet UILabel      *vTimeText;
@property (weak, nonatomic) IBOutlet UILabel      *vDateText;
@property (weak, nonatomic) IBOutlet UIButton     *vTriangleIcon;
@property (weak, nonatomic) IBOutlet CustomUIButton *vPlayButton;

@property (strong, nonatomic)        UIImage      *capturedPhoto;
@property (strong, nonatomic)        NSURL        *videoUrl;
@property (strong, nonatomic)        NSURL        *customAudioUrl;
@property (weak,   nonatomic)        NSString     *useTime;
@property (assign, nonatomic)        CameraMode    cammode;
@property (assign, nonatomic)        BOOL          useCustomAudio;


-(IBAction) checkBoxAction: (id)sender;
-(IBAction) dateAction: (id)sender;

@end
