//
//  ImportViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/9/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "ImportViewController.h"
#import "MBProgressHUD.h"
#import "ShareViewController.h"
#import "SCWatermarkOverlayView.h"
//#import "FLEXManager.h"

#define MAX_DELAY_PER_GLITCH   2.0f

@implementation ImportViewController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Util resetFakeDateTime];
    
    [SCRecorder sharedRecorder].vRGBControlRatio          = 50.0f;
    [SCRecorder sharedRecorder].vContrastControlRatio     = 0.75f;
    //[SCRecorder sharedRecorder].vBirghtnessControlRatio   = 0.0f;
    [SCRecorder sharedRecorder].vNoiseControlRatio        = MAX_DELAY_PER_GLITCH - 1.0f;
    [SCRecorder sharedRecorder].startTime                 = 0.0f;
    [SCRecorder sharedRecorder].stopTime                  = 0.0f;
    
    vFirstLaunch         = YES;
    currentEffectIndex   = 0;
    hGlitchCounter       = 1;
    origVideoUrl         = self.videoUrl;
    filterPhoto          = nil;
    
    vEffects = [NSArray arrayWithObjects:
                @"CIPhotoEffectInstant", @"CIPhotoEffectProcess", @"CIPhotoEffectNoir",
                @"CIPhotoEffectTonal",@"CIPhotoEffectChrome", @"CIPhotoEffectFade", nil]; //CIPhotoEffectFade = Vaporwave
    
    self.vVideoScroller.delegate = self;
    [self updateDateView];
    
    if (self.cammode == CameraModePhoto) {
        self.vPlayButton.hidden  = YES;
        self.vVideoFrames.hidden = YES;
        //_sidesAnimView.hidden    = YES;
        _vhsTrackingLines.hidden    = YES;
        
        [_sidesAnimView setImage:[UIImage imageWithContentsOfFile:[Util getSideLine:1]]];
        //[_vhsTrackingLines setImage:[UIImage imageNamed:[Util getNoiseImageWithIndex:1]]];
        
    } else {
        /*vOneLinerHNoiseFrames = @[[UIImage imageNamed:[Util getOneLinerDistortion:1]],
                                  [UIImage imageNamed:[Util getOneLinerDistortion:2]],
                                  [UIImage imageNamed:[Util getOneLinerDistortion:3]],
                                  [UIImage imageNamed:[Util getOneLinerDistortion:4]],
                                  [UIImage imageNamed:[Util getOneLinerDistortion:5]]];*/
        
        /*NSString* baseFramesPath = [Util getSideAnimBasePath];
        NSArray<UIImage *> * _Nullable animFrames = @[[UIImage imageNamed:[baseFramesPath stringByAppendingString:@"1.png"]],
                                                      [UIImage imageNamed:[baseFramesPath stringByAppendingString:@"2.png"]],
                                                      [UIImage imageNamed:[baseFramesPath stringByAppendingString:@"3.png"]],
                                                      [UIImage imageNamed:[baseFramesPath stringByAppendingString:@"4.png"]],
                                                      [UIImage imageNamed:[baseFramesPath stringByAppendingString:@"5.png"]]];
        [_sidesAnimView setAnimationImages:animFrames];
        [_sidesAnimView startAnimating];*/
        
        NSArray<UIImage *> * _Nullable animFrames = @[[UIImage imageWithContentsOfFile:[Util getSideLine:1]],
                                                      [UIImage imageWithContentsOfFile:[Util getSideLine:2]],
                                                      [UIImage imageWithContentsOfFile:[Util getSideLine:3]],
                                                      [UIImage imageWithContentsOfFile:[Util getSideLine:4]],
                                                      [UIImage imageWithContentsOfFile:[Util getSideLine:5]]];
        [_sidesAnimView setAnimationImages:animFrames];
        //[_sidesAnimView startAnimating];
        
        _vhsTrackingLines.hidden    = YES;
        
        /*NSArray<UIImage *> * _Nullable animFrames1 = @[[UIImage imageNamed:[Util getNoiseImageWithIndex:1]],
                                                       [UIImage imageNamed:[Util getNoiseImageWithIndex:2]],
                                                       [UIImage imageNamed:[Util getNoiseImageWithIndex:3]],
                                                       [UIImage imageNamed:[Util getNoiseImageWithIndex:4]],
                                                       [UIImage imageNamed:[Util getNoiseImageWithIndex:5]]];
        [_vhsTrackingLines setAnimationImages:animFrames1];
        [_vhsTrackingLines startAnimating];*/
        
        //vRedBand.image = [UIImage imageWithContentsOfFile:[Util getRedBandDistortion]];
        
        vGlitchFrames = [NSArray arrayWithObjects:
                         [UIImage imageWithContentsOfFile:[Util getOneLinerDistortion:1]],
                         [UIImage imageWithContentsOfFile:[Util getOneLinerDistortion:2]],
                         [UIImage imageWithContentsOfFile:[Util getOneLinerDistortion:3]],
                         [UIImage imageWithContentsOfFile:[Util getOneLinerDistortion:4]],
                         [UIImage imageWithContentsOfFile:[Util getOneLinerDistortion:5]],
                         nil];
        
        [[SCRecorder sharedRecorder] startRunning];
    }
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    // Setup Player view
    [MBProgressHUD showHUDAddedTo:self.vVideoContentView animated:YES];
    [self performSelector:@selector(setupContentPlayer) withObject:nil afterDelay:1.0f];
    
    [MBProgressHUD showHUDAddedTo:self.vVideoFrames animated:YES];
    [self performSelector:@selector(createRangeSliderForTextPanel) withObject:nil afterDelay:0.25f];
}


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (vFirstLaunch) {
        vFirstLaunch = NO;
        
        CGRect  vRect           = self.vVideoContentView.frame;
        CGFloat vExpectedWidth  = (self.view.frame.size.width*0.35);
        CGFloat dividend        = (NSInteger)(vExpectedWidth/16);
        CGFloat width           = dividend*16;
        vRect.size              = CGSizeMake(width, width);
        
        [self.vVideoContentView setFrame:vRect];
        self.videoContainerWidthContraint.constant = width;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (isOpeningShareScreen) {
        isOpeningShareScreen = NO;
        self.useCustomAudio  = NO;
        self.videoUrl        = origVideoUrl;
    }
    
    NSString* fakeTime = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_time_created"];
    if ([fakeTime isEqualToString:@""] || !fakeTime) fakeTime = [Util getCurrentTime]; // Default text
    NSString* fakeDate = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_date_created"];
    NSString *vCurrentDate = [NSString stringWithFormat:@"DATE: %@ %@", fakeDate, fakeTime];
    
    [self.dateBTN setTitle:vCurrentDate forState:UIControlStateNormal];
    [self.vDateText setText:fakeDate];
    [self.vTimeText setText:fakeTime];
    
    self.useTime = fakeTime;
    
    NSString *vEffectName   = [vEffects objectAtIndex:currentEffectIndex];
    BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
    
    if (isVaporEffect && self.cammode == CameraModeVideo) { //Vapor Effect
        vVaporView.image  = [UIImage imageWithContentsOfFile:[Util getVaporDistortion:@"f6"]];
        vVaporView.hidden = !isVaporEffect;
    }
    
    if (self.cammode == CameraModeVideo) {
        [_sidesAnimView startAnimating];
        //vRedBand.hidden     = YES;
        
        /*vOneLinerFrameChanger = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                 target:self
                                                               selector:@selector(updateHNoiseFrame)
                                                               userInfo:nil
                                                                repeats:YES];*/
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSString *vEffectName   = [vEffects objectAtIndex:currentEffectIndex];
    BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
    if (isVaporEffect && self.cammode == CameraModeVideo) { //Vapor Effect
        vVaporView.image  = nil;
        vVaporView.hidden = YES;
    }
    
    if (self.cammode == CameraModeVideo) {
        if (!isOpeningShareScreen) {
            [[SCRecorder sharedRecorder] stopRunning];
            [[SCRecorder sharedRecorder].session deinitialize];
        }
        
        [_sidesAnimView stopAnimating];
        [[SCRecorder sharedRecorder] removeHDistortionDisplayTimer];
        [self removeGlitchTimer];
        
        /*if (vRedBandMoveTimer) {
            [vRedBandMoveTimer invalidate];
            vRedBandMoveTimer = nil;
        }*/
        
        /*[vOneLinerFrameChanger invalidate];
        vOneLinerFrameChanger = nil;*/
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {}

- (IBAction)RGBControlSliderAction:(id)sender {
    UISlider *slider = (UISlider *) sender;
    [[SCRecorder sharedRecorder] setVRGBControlRatio:slider.value];
    [self applyControlEffectOnPreview:VariableEffectRGB];
}

- (IBAction)ContrastControlSliderAction:(id)sender {
    UISlider *slider = (UISlider *) sender;
    [[SCRecorder sharedRecorder] setVContrastControlRatio:slider.value];
    [self applyControlEffectOnPreview:VariableEffectContrast];
}

- (IBAction)birghtnessControlAction:(id)sender {
    //UISlider *slider = (UISlider *) sender;
    //[[SCRecorder sharedRecorder] setVBirghtnessControlRatio:slider.value];
    //[self applyControlEffectOnPreview:VariableEffectBirghtness];
}

- (IBAction)noiseControlAction:(id)sender {
    UISlider *slider = (UISlider *) sender;
    
    [[SCRecorder sharedRecorder] setVNoiseControlRatio:(MAX_DELAY_PER_GLITCH - slider.value)];
    if ([[SCRecorder sharedRecorder] checkHDistortionTimerRunning]) {
        [[SCRecorder sharedRecorder] removeHDistortionDisplayTimer];
        [self removeGlitchTimer];
        
        [[SCRecorder sharedRecorder] startHDistortionTimer];
        vGlitchViewControlTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0/30.0)
                                                                   target:self
                                                                 selector:@selector(updateGlitchView)
                                                                 userInfo:nil
                                                                  repeats:YES];
    }
}

- (IBAction)playVideoAction:(id)sender {
    if (vPlayer.isPlaying) {
        [vPlayer pause];
        [self.vPlayButton setTitle:@"PLAY" forState:UIControlStateNormal];        
        [[SCRecorder sharedRecorder] removeHDistortionDisplayTimer];
        [self removeGlitchTimer];
        
        /*[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRedBand) object:nil];
        vRedBand.hidden = YES;
        if (vRedBandMoveTimer) {
            [vRedBandMoveTimer invalidate];
            vRedBandMoveTimer = nil;
        }*/
        
    } else {
        //[vPlayer seekToTime:kCMTimeZero];
        [vPlayer play];
        [self.vPlayButton setTitle:@"PAUSE" forState:UIControlStateNormal];
        [[SCRecorder sharedRecorder] startHDistortionTimer];
        vGlitchViewControlTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0/30.0)
                                                                   target:self
                                                                 selector:@selector(updateGlitchView)
                                                                 userInfo:nil
                                                                  repeats:YES];
        /*[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRedBand) object:nil];
        [self performSelector:@selector(showRedBand) withObject:nil afterDelay:5.0];*/
    }
}

- (IBAction)dateAction:(id)sender {
    [self performSegueWithIdentifier:@"show_date_cont" sender:self];
}

- (IBAction)checkBoxAction:(id)sender {
    NSUserDefaults* userDefaults        = [NSUserDefaults standardUserDefaults];
    UIButton*       otherCheckMarkBTN   = nil;
    UIButton*       btnClicked          = (UIButton*) sender;
    CHECKBOXTYPES   tag                 = (int) btnClicked.tag;
    
    [btnClicked setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];

    switch (tag) {
        case CHECKBOX_E1: {
            currentEffectIndex = 0;
            [self unselectOtherCheckMarks:CHECKBOX_E1];
            [self applyCIEffect];
        }
            break;
        case CHECKBOX_E2: {
            //[[FLEXManager sharedManager] showExplorer];
            //return;
            currentEffectIndex = 1;
            [self unselectOtherCheckMarks:CHECKBOX_E2];
            [self applyCIEffect];
        }
            break;
        case CHECKBOX_E3: {
            currentEffectIndex = 2;
            [self unselectOtherCheckMarks:CHECKBOX_E3];
            [self applyCIEffect];
        }
            break;
        case CHECKBOX_E4: {
            currentEffectIndex = 3;
            [self unselectOtherCheckMarks:CHECKBOX_E4];
            [self applyCIEffect];
        }
            break;
        case CHECKBOX_E5: {
            currentEffectIndex = 4;
            [self unselectOtherCheckMarks:CHECKBOX_E5];
            [self applyCIEffect];
        }
            break;
        case CHECKBOX_E6: {
            // Vapor Effect
            currentEffectIndex = 5;
            [self unselectOtherCheckMarks:CHECKBOX_E6];
            [self applyCIEffect];
        }
            break;
        case CHECKBOX_DISPLAY_DATE_YES: {
            otherCheckMarkBTN = (UIButton*) [self.view viewWithTag:CHECKBOX_DISPLAY_DATE_NO];
            [otherCheckMarkBTN setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            [userDefaults setBool:NO forKey:@"off_date"];
            
            [self updateTextviews:NO];
        }
            break;
        case CHECKBOX_DISPLAY_DATE_NO: {
            otherCheckMarkBTN = (UIButton*) [self.view viewWithTag:CHECKBOX_DISPLAY_DATE_YES];
            [otherCheckMarkBTN setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            [userDefaults setBool:YES forKey:@"off_date"];
            
            [self updateTextviews:YES];
        }
            break;

        default:
            break;
    }
    
    [userDefaults synchronize];
}

- (IBAction)cancelAction:(id)sender {
    /* Delete Temp Directory */
    [Util clearTmpDirectory];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)applyEffectsAction:(id)sender {
    if (![Util isPremiumUser]) {
        [Util presentPremiumPage:self];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.vVideoContentView animated:YES].label.text = @"Cropping";
    
    if (self.cammode == CameraModeVideo) {
        AVAsset *srcAsset   = [AVAsset assetWithURL:self.videoUrl];
        NSArray *trackArray = [srcAsset tracksWithMediaType:AVMediaTypeAudio]; // Checking if audio exist
        
        if (self.useCustomAudio || !trackArray.count) {
            //[self cropAndTrimVideo];
            /* To let indicator keep running */
            [self performSelector:@selector(cropAndTrimVideo) withObject:nil afterDelay:0.25f];
            
        } else {
            [Util extractAudioFromVideoViaComposition/*extractAudioFromVideoWithInputURL*/:self.videoUrl
                                         andHandler:^(AVAssetExportSession *exportSession)
            {
                if (exportSession) {
                    if (exportSession && exportSession.status == AVAssetExportSessionStatusCompleted) {
                        self.useCustomAudio = YES;
                        self.customAudioUrl = exportSession.outputURL;
                        [self cropAndTrimVideo];
                        
                    } else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [MBProgressHUD hideHUDForView:self.vVideoContentView animated:YES];
                            [self showAlertWithMessage:@"Failed To Extract Audio. Try Again!" andTitle:@""];
                        });
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.vVideoContentView animated:YES];
                        [self showAlertWithMessage:@"Failed To Extract Audio. Try Again!" andTitle:@""];
                    });
                }
                
             }];
        }
    } else {
        [self drawFrame:vImageContentView.image];
        //[self prepareDistortionLines];
        [self applyFiltersOnPhoto];
        [self presentShareScene];
    }
}

#pragma mark - Process Methods

- (void)showAlertWithMessage:(NSString*)msg andTitle:(NSString*)title {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.offset = CGPointMake(hud.offset.x, 150.f);
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:2];
}

- (void)applyControlEffectOnPreview:(EffectType)type {
    if (self.cammode == CameraModeVideo) {
        SCFilterImageView *scimageview = (SCFilterImageView *)vPlayer.SCImageView;
        [scimageview setNeedsDisplay];
    } else {
        [self applyCIEffect];
    }
}

/*- (void)showRedBand {
    vRedBand.hidden   = NO;
    vRedBandMoveTimer = [NSTimer scheduledTimerWithTimeInterval:0.10
                                                         target:self
                                                       selector:@selector(moveRedBand)
                                                       userInfo:nil
                                                        repeats:YES];
}

- (void)moveRedBand {
    vRedBandBottomConstraint.constant = vRedBandBottomConstraint.constant - 15;
    if (vRedBandBottomConstraint.constant < -_vVideoContentView.frame.size.height) {
        vRedBandBottomConstraint.constant = 0;
        vRedBand.hidden = YES;
        
        if (vRedBandMoveTimer) {
            [vRedBandMoveTimer invalidate];
            vRedBandMoveTimer = nil;
        }
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRedBand) object:nil];
        [self performSelector:@selector(showRedBand) withObject:nil afterDelay:5.0];
    }
}*/

- (void)updateGlitchView {
    SCRecorder *recorder = [SCRecorder sharedRecorder];
    
    if (recorder.showHDistortion) {
        recorder.showHDistortion = NO;
        
        NSInteger noofFrames = vGlitchFrames.count;
        NSInteger index      = arc4random_uniform((uint32_t) noofFrames);
        UIImage  *vGlitch    = [vGlitchFrames objectAtIndex:index];
        CGFloat   yPos       = arc4random_uniform((uint32_t) (self.vVideoContentView.frame.size.height/4.0f /*- vGlitch.size.height*/));
        
        yPos += self.vVideoContentView.frame.size.height*0.75f;
        
        hGlitchCounter++;
        if (hGlitchCounter > 3) {
            hGlitchCounter = 1;
            
            yPos = arc4random_uniform((uint32_t) (self.vVideoContentView.frame.size.height*0.75f));
        }
        
        vGlitchView.image    = vGlitch;
        
        CGRect gFrame     = vGlitchView.frame;
        gFrame.origin.y   = yPos;
        vGlitchView.frame = gFrame;
        vGlitchView.hidden = NO;
        
    } else {
        vGlitchView.hidden = YES;
        vGlitchView.image  = nil;
    }
}

- (void)removeGlitchTimer {
    if (vGlitchViewControlTimer) {
        [vGlitchViewControlTimer invalidate];
        vGlitchViewControlTimer = nil;
    }
}

- (void)updateHNoiseFrame {
    _vhsTrackingLines.image = [vOneLinerHNoiseFrames objectAtIndex:arc4random_uniform((uint32_t) [vOneLinerHNoiseFrames count])];
}

- (void)applyFiltersOnPhoto {
    CIImage *ciPhoto  = [CIImage imageWithCGImage:filterPhoto.CGImage];
    /*CIImage *receiver = [[SCRecorder sharedRecorder].importVidlines objectAtIndex:0];
    CGFloat  scale    = ciPhoto.extent.size.width/receiver.extent.size.width;
    
    NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
    receiver = [receiver imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
    
    CGSize vSizeReceiver = receiver.extent.size;
    CGSize vSizeImage    = ciPhoto.extent.size;
    if (vSizeReceiver.height > vSizeImage.height ) {
        receiver = [receiver imageByCroppingToRect:CGRectMake(0, 0, vSizeReceiver.width, vSizeImage.height)];
    }
    ciPhoto = [receiver imageByCompositingOverImage:ciPhoto];*/
    
    NSInteger noofFrames = [SCRecorder sharedRecorder].vOneLinerFrames.count;
    NSInteger index      = arc4random_uniform((uint32_t) noofFrames);
    CIImage  *receiver   = [[SCRecorder sharedRecorder].vOneLinerFrames objectAtIndex:index];
    
    CGFloat scale = ciPhoto.extent.size.width/receiver.extent.size.width;
    NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
    receiver = [receiver imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
    
    CGSize vSizeReceiver = receiver.extent.size;
    CGSize vSizeImage    = ciPhoto.extent.size;
    if (vSizeReceiver.height > vSizeImage.height ) {
        receiver = [receiver imageByCroppingToRect:CGRectMake(0, 0, vSizeReceiver.width, vSizeImage.height)];
    }
    
    CGFloat           yPos   = arc4random_uniform((uint32_t) (ciPhoto.extent.size.height/4.0f /*- receiver.extent.size.height*/));
    CGAffineTransform trans  = CGAffineTransformMakeTranslation(0, yPos);
    NSValue          *value  = [NSValue valueWithBytes:&trans objCType:@encode(CGAffineTransform)];
    NSDictionary<NSString *,NSValue*> *tx = @{@"inputTransform": value};
    
    receiver   = [receiver imageByApplyingFilter:@"CIAffineTransform" withInputParameters:tx];
    ciPhoto    = [receiver imageByCompositingOverImage:ciPhoto];
    
    /*CGFloat horizontalScale = ciPhoto.extent.size.width / receiver.extent.size.width;
    CGFloat verticalScale   = ciPhoto.extent.size.height / receiver.extent.size.height;
    
    receiver   = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
    ciPhoto    = [receiver imageByCompositingOverImage:ciPhoto];*/
    
    /*CIImage */receiver = [[SCRecorder sharedRecorder].sidelines objectAtIndex:0];
    CGFloat horizontalScale = ciPhoto.extent.size.width / receiver.extent.size.width;
    CGFloat verticalScale   = ciPhoto.extent.size.height / receiver.extent.size.height;
    
    receiver = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
    ciPhoto  = [receiver imageByCompositingOverImage:ciPhoto];
    
    [SCWatermarkOverlayView setCameraMode:CameraModePhoto];
    [SCWatermarkOverlayView setImportedPhoto:YES];
    [SCWatermarkOverlayView setImportedPhotoFontSize:20.0*(ciPhoto.extent.size.width/288/*352*/)];
    
    SCWatermarkOverlayView *overlay = [SCWatermarkOverlayView new];
    overlay.frame = CGRectMake(0,0,ciPhoto.extent.size.width,ciPhoto.extent.size.height);
    [[overlay timeLabel] setText:self.useTime];
    
    ciPhoto = [[Util changeViewToImage : (UIView *) overlay] imageByCompositingOverImage:ciPhoto];
    
    [SCWatermarkOverlayView setCameraMode:CameraModeVideo];
    [SCWatermarkOverlayView setImportedPhoto:NO];
    [SCWatermarkOverlayView setImportedPhotoFontSize:16.0];
    
    CGImageRef image   = [[CIContext contextWithOptions:nil] createCGImage:ciPhoto fromRect:ciPhoto.extent];
    filterPhoto = [UIImage imageWithCGImage:image];
    CGImageRelease(image);
}

- (void)applyCIEffect {
    if (self.cammode == CameraModeVideo) {
        NSString *vEffectName   = [vEffects objectAtIndex:currentEffectIndex];
        UIImage  *vVaporImage   = nil;
        BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
        
        if (isVaporEffect) { //Vapor Effect
            vVaporImage  = [UIImage imageWithContentsOfFile:[Util getVaporDistortion:@"f6_500"]];
        }
        
        vVaporView.image  = vVaporImage;
        vVaporView.hidden = !isVaporEffect;
        
        SCFilter *vSCFilter = [SCFilter filterWithCIFilterName:vEffectName];
        ((SCFilterImageView *)vPlayer.SCImageView).filter = vSCFilter;
        
    } else {
        UIImage  *photo         = self.capturedPhoto;
        CIFilter *ciFilter      = nil;
        CIFilter *vColorControl = nil;
        CIImage  *ciPhoto       = [CIImage imageWithCGImage:photo.CGImage];
        
        ciFilter = [CIFilter filterWithName:[vEffects objectAtIndex:currentEffectIndex]];
        if (ciFilter == nil) return;
        
        [ciFilter setValue:ciPhoto forKey:kCIInputImageKey];
        ciPhoto = [ciFilter valueForKey:kCIOutputImageKey];
        
        vColorControl = [CIFilter filterWithName:@"CIColorControls"];
        [vColorControl setDefaults];
        [vColorControl setValue:ciPhoto forKey:kCIInputImageKey];
        
        CGFloat contrast    = [SCRecorder sharedRecorder].vContrastControlRatio;
        //CGFloat brightness  = [SCRecorder sharedRecorder].vBirghtnessControlRatio;
        [vColorControl setValue:[NSNumber numberWithFloat:contrast] forKey:kCIInputContrastKey];
        //[vColorControl setValue:[NSNumber numberWithFloat:brightness] forKey:kCIInputBrightnessKey];
        ciPhoto = [vColorControl valueForKey:kCIOutputImageKey];
        
        if (![[ciFilter name] isEqualToString:@"CIPhotoEffectNoir"] &&
            ![[ciFilter name] isEqualToString:@"CIPhotoEffectTonal"])
        {
            RGBChannelToneCurve *_CIToneCurveFilter = [RGBChannelToneCurve new];
            [_CIToneCurveFilter setDefaults];
            [_CIToneCurveFilter setInputImage:ciPhoto];
            [_CIToneCurveFilter setCurrentFXName:[ciFilter name]];
            ciPhoto = [_CIToneCurveFilter outputImage];
        }
        
        NSString *vEffectName   = [vEffects objectAtIndex:currentEffectIndex];
        BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
        
        if (isVaporEffect) { //Vapor Effect
            CIImage *receiver        = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getVaporDistortion:@"f6_500"]]];
            CGFloat  horizontalScale = ciPhoto.extent.size.width / receiver.extent.size.width;
            CGFloat  verticalScale   = ciPhoto.extent.size.height / receiver.extent.size.height;
            
            receiver = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
            ciPhoto  = [receiver imageByCompositingOverImage:ciPhoto];
        }
        
        CGImageRef        image = [[CIContext contextWithOptions:nil] createCGImage:ciPhoto fromRect:ciPhoto.extent];
        vImageContentView.image = [UIImage imageWithCGImage:image scale:1.0 orientation:photo.imageOrientation];
        CGImageRelease(image);
    }
}

- (void)cropAndTrimVideo {    
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];*/
    NSString *tempDirectory = NSTemporaryDirectory();
    NSString *fileLastName  = [NSString stringWithFormat:@"FinalVideo-%d.m4v", arc4random() % 1000000];
    NSString *myPathDocs    =  [tempDirectory stringByAppendingPathComponent:fileLastName];
    
    NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
    if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    
    AVAsset *asset      = [AVAsset assetWithURL:self.videoUrl];
    int32_t timescale   = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject].nominalFrameRate;
    
    frameTime              = CMTimeMake(0, timescale);
    vOriginalClipFrameTime = CMTimeMake(0, timescale);
    
    reader = [Util setupReader:asset];
    [reader startReading];
    
    writer = [self setupAssetWriterWithOutput:outputurl];
    [writer startWriting];
    [writer startSessionAtSourceTime:kCMTimeZero];
    
    [self drawVideoFrame];
    
    [[[writer inputs] objectAtIndex:0] markAsFinished];
    [writer finishWritingWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (writer.status != AVAssetWriterStatusFailed && writer.status == AVAssetWriterStatusCompleted) {
                self.videoUrl = outputurl;
                
                /* Replace the current segment url */
                NSArray *allSeqments = [[SCRecorder sharedRecorder].session segments];
                SCRecordSessionSegment *segment = (SCRecordSessionSegment *)[allSeqments objectAtIndex:0];
                [segment setUrl:self.videoUrl];
                
                [self presentShareScene];
            }
            
            CVPixelBufferPoolRelease(assetWriterPixelBufferAdaptor.pixelBufferPool);
            writer = nil;
        });
    }];
}

- (AVAssetWriter *)setupAssetWriterWithOutput:(NSURL *)outputVideoFileURL {
    if ([[NSFileManager defaultManager] fileExistsAtPath:[outputVideoFileURL path]]) {
        [[NSFileManager defaultManager] removeItemAtURL:outputVideoFileURL error:nil];
    }
    
    CGSize frameSize = [Util finalVideoRect:self.vVideoScroller.frame.size].size;
    
    NSError *error = nil;
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:outputVideoFileURL
                                                           fileType:AVFileTypeAppleM4V
                                                              error:&error];
    if(error) {
        NSLog(@"error creating AssetWriter: %@",[error description]);
    }
    
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:frameSize.width*[UIScreen mainScreen].scale], AVVideoWidthKey,
                                   [NSNumber numberWithInt:frameSize.height*[UIScreen mainScreen].scale], AVVideoHeightKey,
                                   AVVideoScalingModeResizeAspectFill, AVVideoScalingModeKey,
                                   nil];
    
    AVAssetWriterInput* writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                                         outputSettings:videoSettings];
    writerInput.expectsMediaDataInRealTime = YES;
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:[NSNumber numberWithUnsignedInt:kCVPixelFormatType_32ARGB] forKey:(NSString*)kCVPixelBufferPixelFormatTypeKey];
    [attributes setObject:[NSNumber numberWithUnsignedInt:frameSize.width*[UIScreen mainScreen].scale] forKey:(NSString*)kCVPixelBufferWidthKey];
    [attributes setObject:[NSNumber numberWithUnsignedInt:frameSize.height*[UIScreen mainScreen].scale] forKey:(NSString*)kCVPixelBufferHeightKey];
    [attributes setObject:[NSNumber numberWithBool:YES] forKey:(NSString*)kCVPixelBufferCGImageCompatibilityKey];
    [attributes setObject:[NSNumber numberWithBool:YES] forKey:(NSString*)kCVPixelBufferCGBitmapContextCompatibilityKey];
    
    assetWriterPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor
                                     assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput
                                     sourcePixelBufferAttributes:attributes];
    [videoWriter addInput:writerInput];
    
    //return [videoWriter autorelease];
    return videoWriter;
}

- (void)drawVideoFrame {
    @autoreleasepool {
        while ([reader status] == AVAssetReaderStatusReading) {
            @autoreleasepool {
                AVAsset       *asset  = [reader asset];
                AVAssetTrack  *track  = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
                CGSize         mSize     = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
                CGSize         mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
                
                UIImageOrientation videoAssetOrientation  = UIImageOrientationUp;
                CGAffineTransform videoTransform = track.preferredTransform;
                if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
                    videoAssetOrientation = UIImageOrientationRight;
                }
                if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
                    videoAssetOrientation =  UIImageOrientationLeft;
                }
                if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
                    videoAssetOrientation =  UIImageOrientationUp;
                }
                if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
                    videoAssetOrientation = UIImageOrientationDown;
                }
                
                AVAssetReaderOutput* output = [[reader outputs] objectAtIndex:0];
                CMSampleBufferRef sampleBuffer = [output copyNextSampleBuffer];
                
                if (sampleBuffer != NULL) {
                    UIImage *frame = [Util imageFromCMSampleBuffer:sampleBuffer withMediaSize:mediaSize andOrient:videoAssetOrientation];
                    CFRelease(sampleBuffer);
                    sampleBuffer = NULL;
                    
                    CGFloat vCurrentTime = CMTimeGetSeconds(vOriginalClipFrameTime);
                    CGFloat pStartTime   = [SCRecorder sharedRecorder].startTime;
                    CGFloat pStopTime    = [SCRecorder sharedRecorder].stopTime;
                    
                    if (vCurrentTime >= pStartTime && vCurrentTime <= pStopTime) {
                        [self drawFrame:frame];
                    } else {
                        CMTime cTimeCopy       = vOriginalClipFrameTime;
                        cTimeCopy.value       += 1;
                        vOriginalClipFrameTime = cTimeCopy;
                    }
                }
            }
        }
    }
}

- (void)drawFrame:(UIImage *)videoFrame {
    @autoreleasepool {
        CGRect videoRect = [Util finalVideoRect:self.vVideoScroller.frame.size];
        
        UIGraphicsBeginImageContextWithOptions(videoRect.size, NO, 0.0);
        
        UIScrollView *scroller   = self.vVideoScroller;
        CGRect        frameRect  = CGRectMake((int)[scroller frame].origin.x,
                                              (int)[scroller frame].origin.y,
                                              (int)[scroller frame].size.width,
                                              (int)[scroller frame].size.height);
        
        videoFrame = [Util cropImage:videoFrame FromScrollView:scroller];
        [videoFrame drawInRect:frameRect blendMode:kCGBlendModeNormal alpha:1];
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (self.cammode == CameraModeVideo) {
            CVPixelBufferRef buffer = [Util pixelBufferFromCGImage:[newImage CGImage] withAdapter:assetWriterPixelBufferAdaptor];
            [self newFrameReady:buffer];
            CVBufferRelease(buffer);
            
        } else {
            filterPhoto = newImage;
        }
    }
}

- (void)newFrameReady:(CVPixelBufferRef)pixelBuffer {
    CMTime frameTimeCopy = frameTime;
    
    @autoreleasepool {
        if (![[[writer inputs] objectAtIndex:0] isReadyForMoreMediaData]) {
            return;
        }
    }
    
    BOOL result = [assetWriterPixelBufferAdaptor appendPixelBuffer:pixelBuffer withPresentationTime:frameTimeCopy];
    
    if (result == NO) {
        NSLog(@"failed to append buffer");
        NSLog(@"The error is %@", [writer error]);
    } else {
        //float seconds = CMTimeGetSeconds(frameTime);
        //NSLog(@"frameTime = %f", seconds);
        
        frameTimeCopy.value            += 1;
        vOriginalClipFrameTime.value   += 1;
        frameTime                       = frameTimeCopy;
    }
}

- (void)updateTextviews:(BOOL)show {
    [self.vPlayText setHidden:show];
    [self.vTriangleIcon setHidden:show];
    [self.vDateText setHidden:show];
    [self.vTimeText setHidden:show];
}

- (void)unselectOtherCheckMarks:(int)pUnchangedMark {
    for (int tag=CHECKBOX_E1; tag<=CHECKBOX_E6; tag = tag+111) {
        if (tag == pUnchangedMark) continue;
        
        UIButton *checkMark = (UIButton*) [self.vCheckMarkContainer viewWithTag:tag];
        [checkMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
    }
}

- (void)updateDateView {
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_date"]) {
        [self.dNoBox setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [self.dYesBox setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        [self updateTextviews:YES];
    } else {
        [self.dYesBox setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [self.dNoBox setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        [self updateTextviews:NO];
    }
}

- (void)setupContentPlayer {
    if (self.cammode == CameraModeVideo) {
        vPlayer = [SCPlayer player];
        [vPlayer setItemByAsset:[SCRecorder sharedRecorder].session.assetRepresentingSegments];
        [vPlayer beginSendingPlayMessages];
        vPlayer.delegate = self;
        vPlayer.loopEnabled = NO;
        vPlayer.shouldSuppressPlayerRendering = NO;
        
        SCVideoPlayerView *vPlayerView = [[SCVideoPlayerView alloc] initWithPlayer:vPlayer];
        vPlayerView.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        vPlayerView.userInteractionEnabled = NO;
        [self.vVideoScroller addSubview:vPlayerView];
        
        vZoomingView = vPlayerView;
        
        AVURLAsset   *asset = [AVURLAsset URLAssetWithURL:self.videoUrl options:nil];
        AVAssetTrack *track = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
        CGSize mSize = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
        CGSize mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
        
        CGRect newRect;
        CGSize scrollSize = self.vVideoScroller.frame.size;
        if (scrollSize.width >= scrollSize.height) {            // Landscape
            if (mediaSize.width >= mediaSize.height) {              // Landscape
                float scrollAspect = scrollSize.width/scrollSize.height;
                float mediaAspect = mediaSize.width/mediaSize.height;
                
                if (mediaAspect <= scrollAspect) {
                    float wRatio = scrollSize.width/mediaSize.width;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                } else {
                    float wRatio = scrollSize.height/mediaSize.height;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                }
            } else {                // Portrait
                float wRatio = scrollSize.width/mediaSize.width;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            }
            
        } else {    // Portrait
            if (mediaSize.width < mediaSize.height) {               // Portrait
                float scrollAspect = scrollSize.width/scrollSize.height;
                float mediaAspect = mediaSize.width/mediaSize.height;
                
                if (mediaAspect <= scrollAspect) {
                    float wRatio = scrollSize.width/mediaSize.width;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                } else {
                    float wRatio = scrollSize.height/mediaSize.height;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                }
            } else {                // Landscape
                float wRatio = scrollSize.height/mediaSize.height;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            }
        }
        
        vPlayerView.frame = newRect;
        self.vVideoScroller.contentSize = newRect.size;
        
        SCFilterImageView *scimageview = [[SCFilterImageView alloc] initWithFrame:newRect];
        scimageview.filter = [SCFilter filterWithCIFilterName:[vEffects objectAtIndex:0]];
        [vPlayerView addSubview:scimageview];
        vPlayer.SCImageView = scimageview;
        
    } else {
        vImageContentView = [[UIImageView alloc] init];
        [self.vVideoScroller addSubview:vImageContentView];
        
        vZoomingView = vImageContentView;
        
        CGRect   newRect;
        CGSize   mediaSize  = CGSizeMake((self.capturedPhoto.size.width), (self.capturedPhoto.size.height));
        CGSize   scrollSize = self.vVideoScroller.frame.size;
        if (scrollSize.width >= scrollSize.height) {
            // Landscape
            if (mediaSize.width >= mediaSize.height) {
                // Landscape
                float scrollAspect = scrollSize.width/scrollSize.height;
                float mediaAspect = mediaSize.width/mediaSize.height;
                
                if (mediaAspect <= scrollAspect) {
                    float wRatio = scrollSize.width/mediaSize.width;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                } else {
                    float wRatio = scrollSize.height/mediaSize.height;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                }
                
            } else {
                // Portrait
                float wRatio = scrollSize.width/mediaSize.width;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            }
            
        } else {
            // Portrait
            if (mediaSize.width < mediaSize.height) {
                // Portrait
                float scrollAspect = scrollSize.width/scrollSize.height;
                float mediaAspect = mediaSize.width/mediaSize.height;
                
                if (mediaAspect <= scrollAspect) {
                    float wRatio = scrollSize.width/mediaSize.width;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                } else {
                    float wRatio = scrollSize.height/mediaSize.height;
                    newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
                }
                
            } else {
                // Landscape
                float wRatio = scrollSize.height/mediaSize.height;
                newRect = CGRectMake(0, 0, mediaSize.width*wRatio, mediaSize.height*wRatio);
            }
        }
        
        [self applyCIEffect];
        vImageContentView.frame = newRect;
        self.vVideoScroller.contentSize = newRect.size;
    }
    
    CGFloat scaleWidth = self.vVideoScroller.frame.size.width / self.vVideoScroller.contentSize.width;
    CGFloat scaleHeight = self.vVideoScroller.frame.size.height / self.vVideoScroller.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    self.vVideoScroller.minimumZoomScale = 1.0f;
    self.vVideoScroller.zoomScale = minScale;
    self.vVideoScroller.maximumZoomScale = 2.0f;
    
    [MBProgressHUD hideHUDForView:self.vVideoContentView animated:YES];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

- (void)createRangeSliderForTextPanel {
    if (self.videoUrl) {
        AVURLAsset *videoAsset = [[AVURLAsset alloc]initWithURL:self.videoUrl options:nil];
        CMTime      duration   = videoAsset.duration;
        CGFloat     maxGap     = CMTimeGetSeconds(duration);
        CGRect      sliderRect = CGRectMake(0,
                                            0,
                                            self.vVideoFrames.frame.size.width,
                                            self.vVideoFrames.frame.size.height*0.5);
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            sliderRect = CGRectMake(0,
                                    self.vVideoFrames.frame.size.height*0.25,
                                    self.vVideoFrames.frame.size.width,
                                    self.vVideoFrames.frame.size.height*0.5);
        }
        
        videoFrameRangeSlider = [[SAVideoRangeSlider alloc] initWithFrame:sliderRect videoUrl:self.videoUrl];
        
        CGSize bubbleSize = CGSizeMake(200, 50);
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )  bubbleSize = CGSizeMake(200, 100);
        
        [videoFrameRangeSlider setPopoverBubbleSize:bubbleSize.width height:bubbleSize.height];
        videoFrameRangeSlider.delegate = self;
        videoFrameRangeSlider.minGap = 0.0;
        videoFrameRangeSlider.maxGap = maxGap;
        [self.vVideoFrames addSubview:videoFrameRangeSlider];
        
        [SCRecorder sharedRecorder].startTime = videoFrameRangeSlider.leftPosition;
        [SCRecorder sharedRecorder].stopTime  = videoFrameRangeSlider.rightPosition;
    }
    
    [MBProgressHUD hideHUDForView:self.vVideoFrames animated:YES];
}

- (void)presentShareScene {
    [MBProgressHUD hideHUDForView:self.vVideoContentView animated:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString     *vStoryboardName = [Util is4InchIPhoneDevice] ? @"Main_Under4inch" : @"Main";
        UIStoryboard *mainStoryboard  = [UIStoryboard storyboardWithName:vStoryboardName bundle:nil];
        ShareViewController *shareController = [mainStoryboard instantiateViewControllerWithIdentifier:@"share_scene"];
        
        if (_cammode == CameraModeVideo) {
            isOpeningShareScreen            = YES;
            shareController.videUrl         = self.videoUrl;
            shareController.customAudioUrl  = self.customAudioUrl;
            shareController.useTime         = self.useTime;
            shareController.filterName      = [vEffects objectAtIndex:currentEffectIndex];
            shareController.cammode         = _cammode;
            shareController.capturedPhoto   = nil;
            shareController.useCustomAudio  = self.useCustomAudio;
            shareController.vAudioStartTime = CMTimeMake(vOriginalClipFrameTime.timescale*[SCRecorder sharedRecorder].startTime,
                                                         vOriginalClipFrameTime.timescale);
        } else {
            shareController.videUrl         = nil;
            shareController.customAudioUrl  = nil;
            shareController.useTime         = self.useTime;
            shareController.filterName      = nil;
            shareController.cammode         = _cammode;
            shareController.capturedPhoto   = filterPhoto;
            shareController.useCustomAudio  = NO;
            shareController.vAudioStartTime = kCMTimeZero;
        }
        
        [self presentViewController:shareController animated:YES completion:^{}];
    });
}

#pragma mark - SAVideoRangeSliderDelegate

- (void)videoRange:(SAVideoRangeSlider *)videoRange didChangeLeftPosition:(CGFloat)leftPosition
     rightPosition:(CGFloat)rightPosition
{
    [SCRecorder sharedRecorder].startTime = leftPosition;
    [SCRecorder sharedRecorder].stopTime  = rightPosition;
    
    CMTime vStartingTime = CMTimeMakeWithSeconds([SCRecorder sharedRecorder].startTime, NSEC_PER_SEC);
    [vPlayer seekToTime:vStartingTime];
}

#pragma mark - ScrollView delegates

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return  vZoomingView;
}

#pragma mark - SCPlayerDelegate
/**
 Called when the player has played some frames. The loopsCount will contains the number of
 loop if the curent item was set using setSmoothItem.
 */
- (void)player:(SCPlayer *__nonnull)player didPlay:(CMTime)currentTime loopsCount:(NSInteger)loopsCount {
    //NSLog(@"loopsCount = %lu, seconds = %f", loopsCount, CMTimeGetSeconds(currentTime));
    double vSeconds = CMTimeGetSeconds(currentTime);
    if (vSeconds >= [SCRecorder sharedRecorder].stopTime) {
        [vPlayer pause];
        
        CMTime vStartingTime = CMTimeMakeWithSeconds([SCRecorder sharedRecorder].startTime, NSEC_PER_SEC);
        [vPlayer seekToTime:vStartingTime];
        
        [self.vPlayButton setTitle:@"PLAY" forState:UIControlStateNormal];
        
        [[SCRecorder sharedRecorder] removeHDistortionDisplayTimer];
        [self removeGlitchTimer];
        
        /*[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRedBand) object:nil];
        vRedBand.hidden = YES;
        if (vRedBandMoveTimer) {
            [vRedBandMoveTimer invalidate];
            vRedBandMoveTimer = nil;
        }*/
    }
}

/**
 Called when the item has reached end
 */
- (void)player:(SCPlayer *__nonnull)player didReachEndForItem:(AVPlayerItem *__nonnull)item {
    CMTime vStartingTime = CMTimeMakeWithSeconds([SCRecorder sharedRecorder].startTime, NSEC_PER_SEC);
    [vPlayer seekToTime:vStartingTime];
    
    [self.vPlayButton setTitle:@"PLAY" forState:UIControlStateNormal];
    
    [[SCRecorder sharedRecorder] removeHDistortionDisplayTimer];
    [self removeGlitchTimer];
    
    /*[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRedBand) object:nil];
    vRedBand.hidden = YES;
    if (vRedBandMoveTimer) {
        [vRedBandMoveTimer invalidate];
        vRedBandMoveTimer = nil;
    }*/
}

- (void)dealloc {
    [vPlayer endSendingPlayMessages];
    [vPlayer pause];
    vPlayer = nil;
}

@end
