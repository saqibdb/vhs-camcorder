//
//  JoinedVideoViewController.m
//  VidEditor
//
//  Created by ibuildx on 11/1/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import "JoinedVideoViewController.h"
#import <AVKit/AVKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AMSmoothAlertView.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Util.h"

@interface JoinedVideoViewController (){
    UIButton           *playBTN;
    NSURL *newAssetURL ;
    BOOL isComingFromFacebookShare ;
    BOOL isComingFromFInstaShare ;
    BOOL isVideoSaved ;
}

@end

@implementation JoinedVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.view layoutIfNeeded] ;
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.delegate = self;
    _playerViewController.view.frame = CGRectMake(0, 0, self.playerView.frame.size.width, self.playerView.frame.size.height);
    
    _playerViewController.showsPlaybackControls = YES;
    
    
    // First create an AVPlayerItem
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:_selectedURL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    
    
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    
    [self.playerView addSubview:_playerViewController.view];
    UIImage *normImage = [UIImage imageNamed:@"preview"];
    playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [playBTN setImage:normImage forState:UIControlStateNormal];
    playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
    playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
    [_playerViewController.view addSubview:playBTN];
    //[_playerViewController.player play];
    
    
    playBTN.hidden = NO ;
    
    
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Process

- (void)playVideo:(id)sender
{
    
   playBTN.hidden = YES;
    [[_playerViewController player] play];
}
#pragma mark - Video Player Delegate

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    playBTN.hidden = NO;
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backAction:(UIButton *)sender {
    self.isSaveClicked=NO;
    [self performSegueWithIdentifier:@"joinedToGallery" sender:self];
}
- (IBAction)saveAction:(UIButton *)sender {
    
    if (isVideoSaved) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"Video Already Saved in Gallery" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else{
        ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
        [assetLibrary writeVideoAtPathToSavedPhotosAlbum:self.selectedURL completionBlock:^(NSURL *assetURL, NSError *error){
            NSLog(@"new url = %@" , assetURL) ;
            newAssetURL = assetURL ;
            isVideoSaved = YES ;
            if (isComingFromFacebookShare) {
                [self facebookAction:sender] ;
            }
            else if (isComingFromFInstaShare){
                [self instagramAction:sender] ;
            }
            else{
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Video Saved" andText:@"Saved To Photo Album" andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
            }
            
        }];
    }
    
}

-(IBAction)instagramAction:(UIButton *)sender {
    if (newAssetURL) {
        NSURL *videoFilePath = newAssetURL ; // Your local path to the video
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeVideoAtPathToSavedPhotosAlbum: videoFilePath completionBlock:^(NSURL *assetURL, NSError *error) {
            
            NSString *escapedString = [videoFilePath.absoluteString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
            NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@",escapedString ]];
    

            
            
            
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                [[UIApplication sharedApplication] openURL:instagramURL];
            }
            else{
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Instagram" andText:@"Please install Instagram application to share." andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
        }];
    }
    else{
        isComingFromFInstaShare = YES ;
        [self saveAction:sender] ;
    }
    
}

- (IBAction)facebookAction:(UIButton *)sender {
    
    if (newAssetURL) {
        FBSDKShareVideo *sdkVideo = [[FBSDKShareVideo alloc] init] ;
        sdkVideo.videoURL = newAssetURL ;
        FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init] ;
        content.video = sdkVideo ;
        [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil] ;
    }
    else{
        isComingFromFacebookShare = YES ;
        [self saveAction:sender] ;
    }
}

- (IBAction)rateAction:(UIButton *)sender {
    [Util rateApp];
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

@end
