//
//  MenuViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/28/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

typedef enum : NSUInteger {
    CamCorder,
    MyVideos,
    Import,
    Montage
} ButtonClickType;

@interface MenuViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,SKStoreProductViewControllerDelegate>

@end
