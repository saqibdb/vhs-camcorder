//
//  MenuViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/28/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import "MenuViewController.h"
#import "SCRecorder.h"
#import "MBProgressHUD.h"
//#import <Google/Analytics.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Util.h"
#import "ImportViewController.h"
#import "SCRecordSessionManager.h"
#import "VRecorderViewController.h"
#import "ShareViewController.h"
#import "ALInterstitialAd.h"
#import "AppDelegate.h"
#import "ImageAnimatorViewController.h"

@interface MenuNonRotatingUIImagePickerController : UIImagePickerController

@end

@implementation MenuNonRotatingUIImagePickerController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeRight;
}

@end

@interface MenuViewController () {
    __weak IBOutlet UIButton        *vCamCorderBTN;
    __weak IBOutlet UIButton        *vMyVideosBTN;
    __weak IBOutlet UIButton        *vVaporwaveBTN;
    __weak IBOutlet UIButton        *vMontageBTN;
    __weak IBOutlet UIButton        *vImportBTN;

    NSURL                           *importedVideoUrl;
    UIImage                         *_capturedPhoto;
    AVAudioPlayer                   *avAudioPlayer;
    ImageAnimatorViewController     *animatorViewController;
    CameraMode                       _cammode;
    ButtonClickType                  vTypeClicked;
    BOOL                             isFirstLaunch;
}

@end

@implementation MenuViewController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startIntroAnimator];
    
    _cammode            = CameraModeVideo;
    _capturedPhoto      = nil;
    importedVideoUrl    = nil;
    isFirstLaunch       = YES;
    
    NSString* resPath = [[NSBundle mainBundle] pathForResource:@"click_button.wav" ofType:nil];
    NSAssert(resPath, @"resPath is nil");
    avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:resPath] error:nil];
    NSAssert(avAudioPlayer, @"AVAudioPlayer could not be allocated");
    [avAudioPlayer prepareToPlay];
    
    [self performSelector:@selector(unHideMenuButtons) withObject:nil afterDelay:5.0f];
    
    //[self initializeRecorder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!isFirstLaunch) {
        [self resetVideoConfiguration];
    } else isFirstLaunch = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[ImportViewController class]]) {
        ImportViewController *pController = segue.destinationViewController;
        
        if (_cammode == CameraModeVideo) {
            pController.videoUrl       = importedVideoUrl;
            pController.customAudioUrl = nil;
            pController.useTime        = [Util getCurrentTime];
            pController.cammode        = _cammode;
            pController.capturedPhoto  = nil;
            pController.useCustomAudio = NO;
            
        } else {
            pController.videoUrl       = nil;
            pController.customAudioUrl = nil;
            pController.useTime        = [Util getCurrentTime];
            pController.cammode        = _cammode;
            pController.capturedPhoto  = _capturedPhoto;
            pController.useCustomAudio = NO;
        }
    }
}

- (IBAction)unwindToMenuViewController:(UIStoryboardSegue *)segue {
    [self startBackgroundLoopAnimator];
    [[SCRecorder sharedRecorder] setIsVideoImported:NO];
    [[SCRecorder sharedRecorder] setIsPhotoImported:NO];
    [SCRecorder sharedRecorder].delegate = nil;
    [SCRecorder sharedRecorder].previewView = nil;
    
    if (![Util isPremiumUser] && [ALInterstitialAd isReadyForDisplay]) {
        [ALInterstitialAd show];
    }
}

static NSInteger const kAppITunesItemIdentifier = 1231973737;

- (IBAction)openVaporAppPage:(id)sender {
    if (avAudioPlayer != nil) {
        [avAudioPlayer play];
    }
    [self openStoreProductViewControllerWithITunesItemIdentifier:kAppITunesItemIdentifier];        
}

- (IBAction)openCamCorder:(id)sender {
    if (avAudioPlayer != nil) {
        [avAudioPlayer play];
    }
    
    vTypeClicked = CamCorder;
    [self stopAnimatorType:2];
    [self startTransitionAnimator];
    //[self performSegueWithIdentifier:@"recorder_scene" sender:self];
}

- (IBAction)showMontageEditor:(id)sender {
    if (avAudioPlayer != nil) {
        [avAudioPlayer play];
    }
    vTypeClicked = Montage;
    [self stopAnimatorType:2];
    [self startTransitionAnimator];
    //[self performSegueWithIdentifier:@"to_montage" sender:self];
}

- (IBAction)showMyVideos:(id)sender {
    if (avAudioPlayer != nil) {
        [avAudioPlayer play];
    }
    vTypeClicked = MyVideos;
    [self stopAnimatorType:2];
    [self startTransitionAnimator];
    //[self performSegueWithIdentifier:@"my_videos_collection" sender:self];
}

- (IBAction)openDeviceGallery:(id)sender {
    if (avAudioPlayer != nil) {
        [avAudioPlayer play];
    }
    vTypeClicked = Import;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Loading Photos";
    
    MenuNonRotatingUIImagePickerController *picker = [[MenuNonRotatingUIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.videoQuality = UIImagePickerControllerQualityType640x480;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString*)kUTTypeImage, (NSString*)kUTTypeMovie, nil];
    //picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Intro Animation

- (void) startIntroAnimator {
    // Init animator data
    NSArray *names = [ImageAnimatorViewController arrayWithNumberedNames:@"splash"
                                                              rangeStart:14
                                                                rangeEnd:251
                                                            suffixFormat:@"%04i.jpg"];
    NSArray *URLs = [ImageAnimatorViewController arrayWithResourcePrefixedURLs:names];
    
    animatorViewController = [ImageAnimatorViewController imageAnimatorViewController];
    animatorViewController.animationOrientation = UIImageOrientationUp;
    animatorViewController.animationFrameDuration = ImageAnimator25FPS;
    animatorViewController.animationURLs = URLs;
    animatorViewController.animationRepeatCount = 0;
    animatorViewController.vAnimatorType = 1;
    animatorViewController.view.clipsToBounds = YES;
    
    NSString* resPath = [[NSBundle mainBundle] pathForResource:@"intro.wav" ofType:nil];
    NSAssert(resPath, @"resPath is nil");
    animatorViewController.animationAudioURL = [NSURL fileURLWithPath:resPath];
    
    [self.view addSubview:animatorViewController.view];
    [self.view insertSubview:animatorViewController.view belowSubview:vCamCorderBTN];
    
    // Register callbacks that will be invoked when the animation
    // starts, note that this callback is invoked at the start of
    // each animation loop.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(animationDidStopNotification:)
                                                 name:ImageAnimatorDidStopNotification
                                               object:animatorViewController];
    
    // Kick off animation loop
    [animatorViewController startAnimating];
}

- (void)startBackgroundLoopAnimator {
    // Init animator data
    NSArray *names = [ImageAnimatorViewController arrayWithNumberedNames:@"splash"
                                                              rangeStart:130
                                                                rangeEnd:234
                                                            suffixFormat:@"%04i.jpg"];
    NSArray *URLs = [ImageAnimatorViewController arrayWithResourcePrefixedURLs:names];
    
    animatorViewController = [ImageAnimatorViewController imageAnimatorViewController];
    animatorViewController.animationOrientation = UIImageOrientationUp;
    animatorViewController.animationFrameDuration = ImageAnimator25FPS;
    animatorViewController.animationURLs = URLs;
    animatorViewController.animationRepeatCount = 1000; //infinite
    animatorViewController.vAnimatorType = 2;
    animatorViewController.view.clipsToBounds = YES;
    [self.view addSubview:animatorViewController.view];
    [self.view insertSubview:animatorViewController.view belowSubview:vCamCorderBTN];
    
    // Kick off animation loop
    [animatorViewController startAnimating];
}

- (void)startTransitionAnimator {
    // Init animator data
    NSArray *names = [ImageAnimatorViewController arrayWithNumberedNames:@"trans"
                                                              rangeStart:1
                                                                rangeEnd:42
                                                            suffixFormat:@"%04i.jpg"];
    NSArray *URLs = [ImageAnimatorViewController arrayWithResourcePrefixedURLs:names];
    
    animatorViewController = [ImageAnimatorViewController imageAnimatorViewController];
    animatorViewController.animationOrientation = UIImageOrientationUp;
    animatorViewController.animationFrameDuration = ImageAnimator25FPS;
    animatorViewController.animationURLs = URLs;
    animatorViewController.animationRepeatCount = 0; //infinite
    animatorViewController.view.clipsToBounds = YES;
    animatorViewController.vAnimatorType = 3;
    [self.view addSubview:animatorViewController.view];
    
    // Register callbacks that will be invoked when the animation
    // starts, note that this callback is invoked at the start of
    // each animation loop.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(animationDidStopNotification:)
                                                 name:ImageAnimatorDidStopNotification
                                               object:animatorViewController];
    
    // Kick off animation loop
    [animatorViewController startAnimating];
}

- (void)stopAnimatorType:(NSInteger)pType {
    if (!animatorViewController) {
        return;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:ImageAnimatorDidStopNotification
                                                  object:animatorViewController];
    switch (pType) {
        case 1: // Intro Animator
        {
            [self removeAnimatorController];
            [self startBackgroundLoopAnimator];
        }
            break;
        case 2: //Loop Animator
        {
            [self removeAnimatorController];
        }
            break;
        case 3: // Transition Animator
        {
            switch (vTypeClicked) {
                case CamCorder:
                    [self performSegueWithIdentifier:@"recorder_scene" sender:self];
                    break;
                case MyVideos:
                    [self performSegueWithIdentifier:@"my_videos_collection" sender:self];
                    break;
                case Montage:
                    [self performSegueWithIdentifier:@"to_montage" sender:self];
                    break;
                case Import:
                    [self performSegueWithIdentifier:@"menu_to_import_scene" sender:self];
                    break;
                default:
                    break;
            }
            
            [self performSelector:@selector(removeAnimatorController) withObject:nil afterDelay:0.125f];
        }
            break;
        default:
            break;
    }
}

- (void)animationDidStopNotification:(NSNotification*)notification {
    if (animatorViewController) {
        [self stopAnimatorType:animatorViewController.vAnimatorType];
    }
}

- (void)removeAnimatorController {
    [animatorViewController stopAnimating];
    [animatorViewController.view removeFromSuperview];
    animatorViewController = nil;
}

- (void)unHideMenuButtons {
    [UIView animateWithDuration:1.0f animations:^{
        vCamCorderBTN.alpha = 1.0f;
        vMyVideosBTN.alpha = 1.0f;
        vImportBTN.alpha = 1.0f;
        vMontageBTN.alpha = 1.0f;
        vVaporwaveBTN.alpha = 1.0f;
        
    } completion:^(BOOL finished) {
        [self initializeRecorder];
        [self resetVideoConfiguration];
        
        vCamCorderBTN.enabled = YES;
        vMyVideosBTN.enabled = YES;
        vImportBTN.enabled = YES;
        vMontageBTN.enabled = YES;
        vVaporwaveBTN.enabled = YES;
    }];
}

#pragma mark - Process

- (void)initializeRecorder {
    SCRecorder *_recorder = [SCRecorder sharedRecorder];
    
    _recorder.captureSessionPreset       = AVCaptureSessionPreset352x288;
    //_recorder.delegate                  = self;
    _recorder.flashMode                  = SCFlashModeOff;
    _recorder.videoOrientation           = AVCaptureVideoOrientationLandscapeRight;
    //_recorder.previewView               = self.previewView;
    _recorder.initializeSessionLazily    = NO;
    _recorder.audioConfiguration.enabled = NO;
    _recorder.bestCapturePreset          = [SCRecorderTools bestCaptureSessionPresetCompatibleWithAllDevices];
    
    [self resetVideoConfiguration];    
    [Util loadDistortedFrames];
    
    NSError *error;
    if (![_recorder prepare:&error]) {
        NSLog(@"Prepare error: %@", error.localizedDescription);
    }
}

- (void)resetVideoConfiguration {
    // Get the video configuration object
    SCVideoConfiguration *video = [SCRecorder sharedRecorder].videoConfiguration;
    video.filter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectInstant"];
    video.sizeAsSquare = YES;
    video.size = CGSizeMake(288, 288);
    
    [SCRecorder sharedRecorder].videoConfiguration.sizeAsPortrait = NO;
}

- (void)openStoreProductViewControllerWithITunesItemIdentifier:(NSInteger)iTunesItemIdentifier {
    SKStoreProductViewController *storeViewController = [[SKStoreProductViewController alloc] init];
    storeViewController.delegate = self;
    
    NSNumber *identifier = [NSNumber numberWithInteger:iTunesItemIdentifier];
    NSDictionary *parameters = @{ SKStoreProductParameterITunesItemIdentifier:identifier };
    UIViewController *viewController = self;
    [storeViewController loadProductWithParameters:parameters
                                   completionBlock:^(BOOL result, NSError *error) {
                                       if (result)
                                           [viewController presentViewController:storeViewController
                                                                        animated:YES
                                                                      completion:nil];
                                       else NSLog(@"SKStoreProductViewController: %@", error);
                                   }];
}

#pragma mark - ImagePicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        _capturedPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        _cammode = CameraModePhoto;
        [[SCRecorder sharedRecorder] setIsPhotoImported:YES];
        
        [self stopAnimatorType:2];
        [self startTransitionAnimator];
        //[self performSegueWithIdentifier:@"menu_to_import_scene" sender:self];
        
    } else if ([mediaType isEqualToString:(NSString *)kUTTypeVideo] ||
               [mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL *url = info[UIImagePickerControllerMediaURL];
        [picker dismissViewControllerAnimated:YES completion:nil];                
        
        importedVideoUrl = url;
        _cammode         = CameraModeVideo;

        [Util resetImportSession];        
        [[SCRecorder sharedRecorder] setIsVideoImported:YES];
        
        SCRecordSessionSegment *segment = [SCRecordSessionSegment segmentWithURL:url info:nil];
        [[SCRecorder sharedRecorder].session addSegment:segment];
        
        [self stopAnimatorType:2];
        [self startTransitionAnimator];
        //[self performSegueWithIdentifier:@"menu_to_import_scene" sender:self];
        
    } else [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - SKStoreProductViewControllerDelegate

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

@end
