//
//  ViewController.h
//  ALiImagePicker
//
//
//  Created by iBuildx_Mac_Mini on 8/25/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "DNDDragAndDrop.h"
#import <MediaPlayer/MediaPlayer.h>
typedef void (^EditingResponse) (id object, BOOL status, NSError *error);//block as a typedef

@interface ViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource, AVPlayerViewControllerDelegate,UIScrollViewDelegate,AVAudioPlayerDelegate,DNDDragSourceDelegate,DNDDropTargetDelegate,MPMediaPickerControllerDelegate,CAAnimationDelegate , UITableViewDelegate , UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *ImagesToDisplay;


@property (weak, nonatomic) IBOutlet UIView *tempView;

@property (weak, nonatomic) IBOutlet UIButton *joinBtn;

@property (nonatomic, strong) NSMutableArray *allAssets;
@property (nonatomic, strong) NSMutableArray *allAssetsCopy;
@property (nonatomic, strong) NSMutableArray *allAssetsWithTransitions;
@property (nonatomic, strong) NSString *isFrom;
@property (nonatomic, strong) NSURL *selectedURL;
@property (nonatomic, strong) NSURL *joinedSelectedURL;
@property (nonatomic, strong) NSURL *selectedAudioFileUrl;

@property (weak, nonatomic) IBOutlet UIView *horizontalTableView;

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
- (IBAction)openGalleryAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *openGalleryBtn;
@property (weak, nonatomic) IBOutlet UIView *triangleSprite;

- (IBAction)joinAction:(UIButton *)sender;
@property (nonatomic) AVAudioPlayer *players;
@property (nonatomic) AVPlayerViewController *playerViewController;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dragViews;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *thumbImages;
@property (weak, nonatomic) IBOutlet UIView *dropView;
@property (weak, nonatomic) IBOutlet UIImageView *binImage;
@property (weak, nonatomic) IBOutlet UIScrollView *dragContainer;
- (IBAction)openTransitionWindowAction:(UIButton *)sender;
- (IBAction)hideTransitinWindowAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *tarnsitionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *transitionViewBottomSpace;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *videoTimeView;
- (IBAction)openAudioPickerAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *audioVisualizationView;
@property (weak, nonatomic) IBOutlet UIView *PlayerIconView;
- (IBAction)importFirstVideoAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *ThumbScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dragViewWidth;
@property (strong, nonatomic) IBOutlet DNDDragAndDropController *dragAndDropController;

@property (weak, nonatomic) IBOutlet UIView *temporaryView;

// lblSmallVideoTime
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblVideoTimeSmall;
// smallView
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *smallTimeView;


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *transitionBtns;
@property (weak, nonatomic) IBOutlet UIView *framesSlideViewMain;
- (IBAction)backClearAction:(UIButton *)sender;

@end

