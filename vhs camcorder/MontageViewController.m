//
//  ViewController.m
//  ALiImagePicker
//
//
//  Created by iBuildx_Mac_Mini on 8/25/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//



#import "vhs_camcorder-swift.h"
#import "SoundManager.h"
#import "FVSoundWaveView.h"
#import <AVFoundation/AVFoundation.h>
#import "MontageViewController.h"
#import "GalleryCollectionViewCell.h"
#import "PlayerViewController.h"
#import "GPUImage.h"
#import <MediaPlayer/MediaPlayer.h>
#import "PlayerViewController.h"
#import "EditViewController.h"
#import "SVProgressHUD.h"
#import "AMSmoothAlertView.h"
#import "JoinedVideoViewController.h"
#import <AVKit/AVKit.h>
#import <QuartzCore/QuartzCore.h>
#import "customShakingCell.h"
#import "DNDDragAndDrop.h"
#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "UIView+ALi.h"
#import "UIButton+ALi.h"
#import <Photos/Photos.h>
#import "ShareViewController.h"
#import "Util.h"

#define WEAKSELF(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define SCREEN_W [UIScreen mainScreen].bounds.size.width
#define SCREEN_H [UIScreen mainScreen].bounds.size.height




#define RADIANS(degrees) ((degrees * M_PI) / 180.0)


@interface ViewController (){
    bool isFirstVideo ;
    UIImage *secondthumbNailImageForOverlay;
    UIImage *thumbNailImageForOverlay;
    UIView *animateView;
    
    NSInteger indexToReplace;
    NSInteger selectedTransition;
    NSInteger transitionBtnTag;
    CGFloat videosTimeInSeconds;
    FVSoundWaveView* _soundWaveView;
    CGRect frameRect;
    CGSize screenSize;
    UIButton           *playBTN;
    NSTimer            *previewTimer;
    CGFloat             timeSpent;
    UIButton *deleteButton;
    int currentPlaying ;
    UITapGestureRecognizer *singleFingerTap ;
    UIView *animatingView ;
    UITapGestureRecognizer *vibratingTap ;
    NSArray *transitionImagesArray ;
    NSArray *  transitionImagesSelectedArray;
    NSArray *transitionNamesArray;
    UICollectionViewCell *cell;
    VideoThumbnailView *thumbView ;
    CMTime videosTotalTime;
    NSMutableArray *transitionsArray ;
    
    NSMutableArray *dummyImagesArray ;
    
}

@end

@implementation ViewController{
}

- (void)viewDidLoad {
    [self.temporaryView setHidden:YES];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //[self maskTriananlgesAndCircularBorderedImage];
    transitionImagesArray = [[NSArray alloc]initWithObjects:@"transA" ,@"glitch",@"fade_light",@"fade_dark",@"dissolve",@"corner_scale",@"rotate",@"scale", @"squared", nil];
    transitionImagesSelectedArray = [[NSArray alloc]initWithObjects:@"transAselected" ,@"glitch_selected", @"fade_light_selected",@"fade_dark_selected",@"dissolve_selected", @"corner_scale_selected", @"rotate_selected",@"scale_selected", @"squared_selected",  nil];
    transitionNamesArray = [[NSArray alloc]initWithObjects:@"Cut", @"Glitch", @"Fade Light",@"Fade Dark",@"Dissolve",@"Scale Corner",@"Rotate",@"Scale", @"Squared", nil];
    self.dragContainer.delegate=self;

#warning 
    /*
     * Umer Naseer, Added this
     * Qaiser Butt, Removing this as defaults are being cleared visiting montage scene
     */
    /*NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];*/
    
    self.players.delegate=self;
    [self.view layoutIfNeeded];
    self.transitionViewBottomSpace.constant= -self.tarnsitionView.frame.size.
    height;
    self.collectionView.dataSource=self;
    self.collectionView.delegate=self;
    self.dropView.tag  = 112 ;
    [self.framesSlideViewMain setHidden:YES];
    [self.playerView setHidden:YES];
    if (_dragViews.count) {
        UIView *firstDragView = _dragViews[0] ;
        firstDragView.alpha = 1.0 ;
        NSLog(@"%ld",(long)firstDragView.tag);
        singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(openVideoSelector)];
        [firstDragView addGestureRecognizer:singleFingerTap];
        
        
    }
    self.dragAndDropController = [[DNDDragAndDropController alloc] init];
    for(UIView *smallView in self.smallTimeView)
    {
        
        [smallView setHidden:YES];
        
    }
    [self.dragAndDropController registerDropTarget:self.dropView withDelegate:self];
    //[self.view layoutIfNeeded];
    self.dropView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    self.ImagesToDisplay = [[NSMutableArray alloc]init];
    [self.dropView setHidden:YES];
    _soundWaveView = [[FVSoundWaveView alloc] initWithFrame:self.audioVisualizationView.bounds];
    [self.audioVisualizationView addSubview:_soundWaveView];
    for(UIButton *btn in self.transitionBtns)
    {
        btn.userInteractionEnabled = NO;
    }
    transitionsArray = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"",@"", nil];
    
}
-(void) viewDidAppear:(BOOL)animated{
    
}
- (void)startWobble :(UIView *)view {
    view.transform = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(-5));
    [UIView animateWithDuration:0.20
                          delay:0.0
                        options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse)
                     animations:^ {
                         view.transform = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(5));
                     }
                     completion:NULL
     ];
}

- (void)stopWobble :(UIView *)view {
    [UIView animateWithDuration:0.20
                          delay:0.0
                        options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear)
                     animations:^ {
                         view.transform = CGAffineTransformIdentity;
                     }
                     completion:NULL
     ];
}

-(void)shakeView :(UIView *)view {
    
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
    [shake setDuration:0.1];
    [shake setRepeatCount:100];
    [shake setAutoreverses:YES];
    [shake setFromValue:[NSValue valueWithCGPoint:
                         CGPointMake(view.center.x - 5,view.center.y)]];
    [shake setToValue:[NSValue valueWithCGPoint:
                       CGPointMake(view.center.x + 5, view.center.y)]];
    [view.layer addAnimation:shake forKey:@"position"];
}
#pragma mark - Helper methods
-(void) openVideoSelector {
    [self openGalleryAction:nil];
}
/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}
- (void)viewDidLayoutSubviews
{
    [self.ThumbScrollView setContentSize:CGSizeMake(1200, self.dragContainer.frame.size.height)];
    [self.dragContainer setContentSize:CGSizeMake(1200, self.dragContainer.frame.size.height)];
    self.dragViewWidth.constant=1500;
}
-(void)playFromStart {
    currentPlaying = 0 ;
    NSURL *currentURL = self.allAssets[0] ;
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:currentURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    
}
-(NSString*) applicationDocumentsDirectory{
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSLog(@"%@",basePath);
    return basePath;
    
}
#pragma mark - Drag Source Delegate

- (UIView *)draggingViewForDragOperation:(DNDDragOperation *)operation {
    
    NSLog(@"Started Dragging") ;
    [self.dropView setHidden:NO];
    [self.binImage setHidden:NO];
    self.dropView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    
    
    [self stopWobble:operation.dragSourceView];
    
    NSUInteger i = operation.dragSourceView.tag;
    if (i >= self.dragViews.count) {
        return nil ;
    }
    UIView *view = self.dragViews[i];
    
    if (!view) {
        return nil ;
    }
    [view setHidden:YES];
    UIImage *img = [self.ImagesToDisplay objectAtIndex:i];
    UIView *dragView = [[UIView alloc] initWithFrame:CGRectMake(10,-10,view.frame.size.width+15,view.frame.size.height+15)];
    dragView.backgroundColor=[UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:dragView.frame];
    imageView.image = img;
    imageView.center=dragView.center;
    [dragView addSubview:imageView];
    
    [UIView animateWithDuration:0.5 animations:^{
        dragView.alpha = 1.0f;
        dragView.transform = CGAffineTransformRotate(dragView.transform, -M_PI/10);
    }];
    return dragView;
}

- (void)dragOperationWillCancel:(DNDDragOperation *)operation {
    NSLog(@"cancelled");
    
    [self.binImage setHidden:YES];
    [self.dropView setBackgroundColor:[UIColor clearColor]];
    [self.dropView setHidden:YES];
    
    NSUInteger i =operation.dragSourceView.tag;
    UIView *view = self.dragViews[i];
    [view setHidden:NO];
    UIView *smallView = self.smallTimeView[i];
    [smallView setHidden:NO];
    //        [operation removeDraggingViewAnimatedWithDuration:0.2 animations:^(UIView *draggingView) {
    //            draggingView.alpha = 0.0f;
    //            draggingView.center = [operation convertPoint:self.sIview1.center fromView:self.view];
    //        }];
}


#pragma mark - Drop Target Delegate
- (void)dragOperation:(DNDDragOperation *)operation didDropInDropTarget:(UIView *)target {
    
    [self.binImage setHidden:YES];
    [self.dropView setBackgroundColor:[UIColor clearColor]];
    [self.dropView setHidden:YES];
    
    if (target.tag == 112) {
        NSLog(@"dropped on BIN");
        
        NSUInteger i =operation.dragSourceView.tag;
        UIImage *Img=[self.allAssets objectAtIndex:i];
        [self.allAssets removeObject:Img];
        [self.allAssetsCopy removeObject:Img];
        
        
        [self.ImagesToDisplay removeObjectAtIndex:i];
        UIImageView *img = [self.thumbImages objectAtIndex:i];
        img.image= nil;
        UIView *view = self.dragViews[i];
        [view setHidden:YES];
        for (UIView *view in self.dragViews) {
            [self.dragAndDropController unregisterDragSource:view];
            [self.dragAndDropController unregisterDropTarget:view];
            
        }
        
        
        self.allAssets = [[NSMutableArray alloc] initWithArray:self.allAssetsCopy];
        
        for (int i = 0; i < transitionsArray.count; i++) {
            NSString *str = transitionsArray[i];
            if (!str.length && i != 0) {
                [transitionsArray replaceObjectAtIndex:i-1 withObject:@""];
                UIButton *btnToChange = self.transitionBtns[i-1];
                [btnToChange setImage:[UIImage imageNamed:@"transitionBtnBG"] forState:UIControlStateNormal];
                break;
            }
        }
        if (transitionsArray.count) {
            
            NSString *transitionStr = transitionsArray[0];
            if (transitionStr.length) {
                [SVProgressHUD showWithStatus:@"Applying Transition..." ];
                if ([transitionStr isEqualToString:@"Blur"] || [transitionStr isEqualToString:@"Pixels"]) {
                    [self applyTransitionsToVideosWithIndex:0 forFirstVideo:YES];
                }
                else{
                    [self applyTransitionsToVideosWithIndex:0 forFirstVideo:NO];
                }
            }
            
        }

        
       

        
        
        
        [self setImages];
        
        if (self.allAssets.count) {
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:self.allAssets[0]];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
            
            
            _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
            _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
        }
        else{
            self.playerViewController.player = nil ;
        }
        self.binImage.image = [UIImage imageNamed:@"binClosed"] ;
        
    }
    else {
        NSInteger value ;
        
        if (operation.dropTargetView.tag < operation.dragSourceView.tag){
            
            value = operation.dropTargetView.tag;
        }
        else{
            value = operation.dragSourceView.tag;
        }
        UIButton *btn = self.transitionBtns[value];
        [btn setImage:[UIImage imageNamed:@"transitionBtnBG"] forState:UIControlStateNormal];
        
        
        
        self.allAssetsCopy = [self moveObjectAtIndex:operation.dragSourceView.tag toIndex:operation.dropTargetView.tag inArray:self.allAssetsCopy] ;
        
        [self.allAssets replaceObjectAtIndex:operation.dragSourceView.tag withObject:self.allAssetsCopy[operation.dragSourceView.tag]];
        [self.allAssets replaceObjectAtIndex:operation.dropTargetView.tag withObject:self.allAssetsCopy[operation.dropTargetView.tag]];
        
        NSArray <NSURL *> *allUrls = [[NSArray alloc] initWithArray:self.allAssets];
        if (thumbView) {
            [thumbView removeFromSuperview];
        }
        thumbView = [[VideoThumbnailView alloc] initWithFrame:self.videoTimeView.bounds videoURL:allUrls thumbImageWidth:70];
        
        [ thumbView setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                         UIViewAutoresizingFlexibleHeight )];
        [self.videoTimeView setAutoresizesSubviews:YES ];
        [self.videoTimeView addSubview:thumbView];
        NSLog(@"Dragged Image index = %li", (long)operation.dragSourceView.tag);
        NSLog(@"Dropped Image index = %li", (long)operation.dropTargetView.tag);
        [animatingView removeFromSuperview] ;
        
        for (UIView *i in target.subviews){
            if([i isKindOfClass:[UIImageView class]]){
                UIImageView *newImageView = (UIImageView *)i;
                [newImageView setHidden:NO] ;
                [target setBackgroundColor:[UIColor colorWithRed:0.867 green:0.867 blue:0.867 alpha:1.0]] ;
            }
        }
        
        self.allAssets = [[NSMutableArray alloc] initWithArray:self.allAssetsCopy];
        /*
        for (int i = 0; i < transitionsArray.count; i++) {
            NSString *str = transitionsArray[i];
            if (!str.length && i != 0) {
                [transitionsArray replaceObjectAtIndex:i-1 withObject:@""];
                UIButton *btnToChange = self.transitionBtns[i-1];
                [btnToChange setImage:[UIImage imageNamed:@"transitionBtnBG"] forState:UIControlStateNormal];
                break;
            }
        }
         
         */
        if (transitionsArray.count) {
            
            NSString *transitionStr = transitionsArray[0];
            if (transitionStr.length) {
                [SVProgressHUD showWithStatus:@"Applying Transition..." ];
                if ([transitionStr isEqualToString:@"Blur"] || [transitionStr isEqualToString:@"Pixels"]) {
                    [self applyTransitionsToVideosWithIndex:0 forFirstVideo:YES];
                }
                else{
                    [self applyTransitionsToVideosWithIndex:0 forFirstVideo:NO];
                }
            }
            
        }
        
        [self setImages];
        [self.dropView setBackgroundColor:[UIColor clearColor]];
       //[self playFromStart] ;
    }
    
}

- (void)dragOperation:(DNDDragOperation *)operation didEnterDropTarget:(UIView *)target {
    
    
    
    if (target.tag == 112) {
        [self.binImage setHidden:NO];
        self.dropView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.7f];
        
        self.binImage.image = [UIImage imageNamed:@"binOpened"] ;
        
        NSLog(@"enterd");
        target.layer.borderColor = [operation.draggingView.backgroundColor CGColor];
    }
    else{
        NSLog(@"enterd on draggers...");
        [target setAlpha:1.0] ;
        //CGRect frame = [self.view convertRect:target.frame fromView:self.dragContainer];
        CGRect frame = target.frame;
        [animatingView removeFromSuperview] ;
        animatingView = [[UIView alloc] initWithFrame:frame];
        [self.dragContainer insertSubview:animatingView belowSubview:target];
        [animatingView setBackgroundColor:[UIColor redColor]] ;
        [self.dragContainer bringSubviewToFront:operation.draggingView] ;
        [self.dragContainer bringSubviewToFront:target] ;
        
        UIImageView *animatingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [animatingView addSubview:animatingImageView] ;
        [animatingImageView setContentMode:UIViewContentModeScaleAspectFill] ;
        animatingImageView.clipsToBounds = YES ;
        
        for (UIView *i in target.subviews){
            if([i isKindOfClass:[UIImageView class]]){
                UIImageView *newImageView = (UIImageView *)i;
                animatingImageView.image = newImageView.image ;
                [newImageView setHidden:YES] ;
                [target setBackgroundColor:[UIColor clearColor]] ;
            }
        }
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGRect newFrame = frame ;
                             
                             if (operation.dragSourceView.tag > target.tag) {
                                 newFrame.origin.x = operation.dragSourceView.frame.origin.x ;
                             }
                             else{
                                 newFrame.origin.x = operation.dragSourceView.frame.origin.x ;
                             }
                             animatingView.frame = newFrame ;
                         } completion:NULL] ;
        
    }
    
}
- (void)dragOperation:(DNDDragOperation *)operation didLeaveDropTarget:(UIView *)target {
    target.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.binImage.image = [UIImage imageNamed:@"binClosed"] ;
    
    //[self.binImage setHidden:NO];
    self.dropView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    [target setAlpha:1.0] ;
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         animatingView.frame = target.frame ;
                     } completion:^(BOOL finished) {
                         [animatingView removeFromSuperview] ;
                         
                         
                         for (UIView *i in target.subviews){
                             if([i isKindOfClass:[UIImageView class]]){
                                 UIImageView *newImageView = (UIImageView *)i;
                                 [newImageView setHidden:NO] ;
                                 [target setBackgroundColor:[UIColor colorWithRed:0.867 green:0.867 blue:0.867 alpha:1.0]] ;
                             }
                         }
                         
                         
                     }] ;
    
    
    
    
    NSLog(@"leave");
    
}

-(void)setImages{
    NSArray <NSURL *> *allUrls;
    if(self.allAssetsCopy.count > 0){
    allUrls = [[NSArray alloc] initWithArray:self.allAssetsCopy];
    }
    if (thumbView) {
        [thumbView removeFromSuperview];
    }
    if (self.allAssetsCopy.count >= 1) {
        [self.framesSlideViewMain setHidden:NO];
        [self.playerView setHidden:NO];
        [self.PlayerIconView setHidden:YES];
        thumbView = [[VideoThumbnailView alloc] initWithFrame:self.videoTimeView.bounds videoURL:allUrls thumbImageWidth:50];
        [thumbView setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                        UIViewAutoresizingFlexibleHeight )];
        [self.videoTimeView setAutoresizesSubviews:YES ];
        [self.videoTimeView addSubview:thumbView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer:)];
        [thumbView setUserInteractionEnabled:YES];
        [thumbView addGestureRecognizer:tapGesture];
        
    }
    else{
        [self.PlayerIconView setHidden:NO];
        [self.playerView setHidden:YES];
        [self.framesSlideViewMain setHidden:YES];
    }
    
    [self.ImagesToDisplay removeAllObjects] ;
    for (UIImageView *imageView in self.thumbImages) {
        imageView.image = [UIImage imageNamed:@"plusIcon"];
        imageView.contentMode = UIViewContentModeCenter ;
    }
    
    for (UIView *view in self.dragViews) {
        [view setHidden:NO ] ;
        view.alpha = 0.5 ;
        [view removeGestureRecognizer:vibratingTap];
         [view removeGestureRecognizer:singleFingerTap];
        
        
    }
    if (self.allAssets.count == 0) {
        UIView *view = self.dragViews[0];
        view.alpha = 1.0 ;
        singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(openVideoSelector)];
        [view addGestureRecognizer:singleFingerTap];
    }
    for (UIView *smlViews in self.smallTimeView){
        
        [smlViews setHidden:YES];
    }
    
    
    
    
    for(int i = 0;i<self.allAssetsCopy.count;i++){
       
        
        NSURL *currentURL = self.allAssetsCopy[i];
        //generte images
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:currentURL options:nil];
        AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        NSError *error = NULL;
        //CMTime time = CMTimeMake(1, 30);
        
        NSArray *movieTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
        AVAssetTrack *movieTrack = [movieTracks objectAtIndex:0];
        
        Float64 totalFrames = (asset.duration.value / asset.duration.timescale) * movieTrack.nominalFrameRate;

        Float64 secondsIn = (asset.duration.value / asset.duration.timescale);
        CMTime time = CMTimeMakeWithSeconds(secondsIn+600, 600);
        
        
        //CMTime time = CMTimeMake((asset.duration.value / asset.duration.timescale) - 1, 30);
        CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
        //NSLog(@"error==%@, Refimage==%@", error, refImg);
        
        
        
        
        
        
        UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
        CGImageRelease(refImg);
        //NSLog(@"error==%@, Refimage==%@", error, FrameImage);
        [self.ImagesToDisplay addObject:FrameImage];
        
        UIImageView *img =self.thumbImages[i];
        img.image=self.ImagesToDisplay[i];
        img.contentMode = UIViewContentModeScaleAspectFill ;
        img.clipsToBounds = YES ;
        UIView *currentView = self.dragViews[i];
        
        UIView *smallView = self.smallTimeView[i];
        [smallView setHidden:NO];
        UILabel *smallTimeLabel = self.lblVideoTimeSmall[i];
        
        
        CMTime videoDuration = asset.duration;
        float videoDurationSeconds = CMTimeGetSeconds(videoDuration);
        
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:videoDurationSeconds];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"mm:ss"];  //you can vary the date string. Ex: "mm:ss"
        NSString* result = [dateFormatter stringFromDate:date];
        
        smallTimeLabel.text = [NSString stringWithFormat:@"%@",result];
        
        
        //
        [self.dragAndDropController registerDragSource:currentView withDelegate:self];
        [self.dragAndDropController registerDropTarget:currentView withDelegate:self];
        
        [currentView removeGestureRecognizer:singleFingerTap];
        currentView.alpha = 1.0;
        
        
        vibratingTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(vibrateView:)];
        [currentView addGestureRecognizer:vibratingTap];
        
        
        if (i+1 == self.allAssets.count && i != 9) {
            UIView *view = self.dragViews[i+1];
            view.alpha = 1.0 ;
            NSLog(@"%ld",(long)view.tag);
            singleFingerTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(openVideoSelector)];
            [view addGestureRecognizer:singleFingerTap];
        }
    }
    
    for(UIButton *btn in self.transitionBtns)
    {
        btn.userInteractionEnabled = NO;
    }
    
    //......
    if (self.allAssets.count >= 2) {
        UIButton *btn = self.transitionBtns[0];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 3) {
        UIButton *btn = self.transitionBtns[1];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 4) {
        UIButton *btn = self.transitionBtns[2];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 5) {
        UIButton *btn = self.transitionBtns[3];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 6) {
        UIButton *btn = self.transitionBtns[4];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 7) {
        UIButton *btn = self.transitionBtns[5];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 8) {
        UIButton *btn = self.transitionBtns[6];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 9) {
        UIButton *btn = self.transitionBtns[7];
        [btn setUserInteractionEnabled:YES];
    }
    if (self.allAssets.count >= 10) {
        UIButton *btn = self.transitionBtns[8];
        [btn setUserInteractionEnabled:YES];
    }
    
    
    
}

- (void) vibrateView: (UITapGestureRecognizer *)recognizer{
    [playBTN setImage:[UIImage imageNamed:@"stopButton"] forState:UIControlStateNormal];
    currentPlaying = (int) recognizer.view.tag;
    if (self.allAssets.count > 0) {
    NSURL *currentURL = self.allAssets[currentPlaying] ;
    AVPlayerItem* playerItem=[[AVPlayerItem alloc]initWithURL:currentURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    _playerViewController.player=[AVPlayer playerWithPlayerItem:playerItem];
    playBTN.hidden = NO;
    playBTN.tag = 00;
    [[_playerViewController player] play];
    CGFloat newTime = 0.0;
    CMTime totalTime = kCMTimeZero;
    for (int i = currentPlaying ; i <  self.allAssets.count ; i++){
        id object = self.allAssets[i];
        AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",object]]];
        totalTime = CMTimeAdd(totalTime , asset.duration);
    }
    newTime = CMTimeGetSeconds(totalTime);
    [thumbView changeValueWithTotalTime:newTime tappedPoint: CGPointMake((videosTimeInSeconds - newTime)  * 50, 0.0)];
    if(self.selectedAudioFileUrl != nil){
        [_playerViewController.player setMuted:YES];
        self.players.currentTime = (videosTimeInSeconds - newTime);
        [self.players setVolume:1];
        [self.players setNumberOfLoops:-1];
        [self.players play];
    }
}
    
}
#pragma mark - Process

- (void)playVideo:(UIButton *)sender
{
    
    NSURL *currentURL = self.allAssets[0];
    AVPlayerItem* playerItem=[[AVPlayerItem alloc]initWithURL:currentURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    
    if(sender.tag == 11){
        sender.tag = 00;
        [playBTN setImage:[UIImage imageNamed:@"stopButton"] forState:UIControlStateNormal];
        [thumbView changeValueWithTotalTime:videosTimeInSeconds tappedPoint: CGPointMake(0.0, 0.0)];
        playBTN.hidden = NO;
        
        if(self.selectedAudioFileUrl != nil){
            [_playerViewController.player setMuted:YES];
        }
        [self.players setVolume:1];
        [self.players play];
        [[_playerViewController player] play];
        
    }
    else{
        sender.tag = 11 ;
        
        [playBTN setImage:[UIImage imageNamed:@"preview"] forState:UIControlStateNormal];
        [self.players stop];
        self.players.currentTime = 0.0;
        [[_playerViewController player] pause];
        [thumbView.videoScroll.layer removeAllAnimations];
        
        [thumbView.videoScroll setContentOffset:CGPointMake(0.0, 0.0)];
        
    }
}
#pragma mark - Optional EasyTableView delegate methods for section headers and footers
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self.PlayerIconView setHidden:YES];
    
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    NSLog(@"VideoURL = %@", videoURL);
    self.selectedURL = videoURL ;
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // Your main thread code goes in here
            
            [self performSegueWithIdentifier:@"Gallery-Player" sender:nil];
            
        });
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Navigation Methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindToGallery:(UIStoryboardSegue*)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        [self.view layoutIfNeeded] ;
        EditViewController *sendingVC = sender.sourceViewController ;
        PlayerViewController *playerVC = sender.sourceViewController;
        NSLog(@"allassets count = %lu",(unsigned long)self.allAssets.count);
        if (sendingVC.isSaveClicked) {
            if (!self.allAssets) {
                self.allAssets = [[NSMutableArray alloc] init] ;
                self.allAssetsCopy = [[NSMutableArray alloc] init] ;
                self.allAssetsWithTransitions = [[NSMutableArray alloc] init] ;
                
            }
            [self.allAssets addObject: sendingVC.selectedURL] ;
            [self.allAssetsCopy addObject: sendingVC.selectedURL] ;
            [self.allAssetsWithTransitions addObject: sendingVC.selectedURL] ;
            
            //code here
            if (self.allAssets.count % 2) {
                NSLog(@"%lu",(unsigned long)self.allAssets.count);
            }
            
            if (self.allAssets.count > 1) {
                NSString *transitionStr = @"Glitch";
                self.allAssets = [[NSMutableArray alloc] initWithArray:self.allAssetsCopy];
                [transitionsArray replaceObjectAtIndex:self.allAssets.count - 2 withObject:transitionStr];
                [self applyTransitionsToAll];
                
                
                [SVProgressHUD showWithStatus:@"Applying Transition..." ];
                
                [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
                selectedTransition = 1;
                isFirstVideo = YES;
                thumbNailImageForOverlay = [self testGenerateThumbNailDataWithVideo:self.allAssets[self.allAssets.count -1]];
                isFirstVideo = NO;
                secondthumbNailImageForOverlay = [self testGenerateThumbNailDataWithVideo:self.allAssets[self.allAssets.count - 2]];
                [self hideTransitinWindowAction:nil];
                if (self.allAssets.count > 1) {
                }
            }
            
            NSArray <NSURL *> *allUrls = [[NSArray alloc] initWithArray:self.allAssets];
            if (thumbView) {
                [thumbView removeFromSuperview];
            }
            thumbView = [[VideoThumbnailView alloc] initWithFrame:self.videoTimeView.bounds videoURL:allUrls thumbImageWidth:50];
            
            [ thumbView setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                             UIViewAutoresizingFlexibleHeight )];
            [self.videoTimeView setAutoresizesSubviews:YES ];
            [self.videoTimeView addSubview:thumbView];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer:)];
            [thumbView setUserInteractionEnabled:YES];
            [thumbView addGestureRecognizer:tapGesture];
            NSURL *currentURL = self.allAssets[0] ;
            currentPlaying = 0;
            
            //if (!_playerViewController) {
            _playerViewController = [[AVPlayerViewController alloc] init];
            _playerViewController.delegate = self;
            _playerViewController.view.frame = self.playerView.frame;
            _playerViewController.showsPlaybackControls = NO;
            
            // First create an AVPlayerItem
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:currentURL];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
            _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
            _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
            [self.playerView addSubview:_playerViewController.view];
            UIImage *normImage = [UIImage imageNamed:@"preview"];
            playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
            [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
            [playBTN setImage:normImage forState:UIControlStateNormal];
            playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
            playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
            playBTN.tag = 11;
            [_playerViewController.view addSubview:playBTN];
            playBTN.hidden = NO ;
            [self setImages];
            videosTotalTime = kCMTimeZero;
            for (int i = 0 ; i <  self.allAssets.count ; i++){
                id object = self.allAssets[i];
                AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",object]]];
                videosTotalTime = CMTimeAdd(videosTotalTime , asset.duration);
                videosTimeInSeconds  =  CMTimeGetSeconds(videosTotalTime);
            }
        }
        else if(playerVC.isWithAudio){
            
            [self.audioVisualizationView setHidden:NO];
            self.selectedAudioFileUrl = playerVC.selectedURL;
            _soundWaveView.soundURL = playerVC.selectedURL;
            
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            [audioSession setCategory:AVAudioSessionCategoryPlayback error:NULL];
            self.players = [[AVAudioPlayer alloc] initWithContentsOfURL:self.selectedAudioFileUrl error:nil];
            [self.players setNumberOfLoops:-1];
        }
    });
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    
    if ([segue.identifier isEqualToString:@"Gallery-Player"]) {
        PlayerViewController *destinationViewControler = segue.destinationViewController ;
        destinationViewControler.selectedURL = self.selectedURL ;
        destinationViewControler.selectedAudioFileUrl=self.selectedAudioFileUrl;
    }
    if ([segue.identifier isEqualToString:@"dashboadToJoinedViewController"]) {
        ShareViewController *shareController = segue.destinationViewController;
        shareController.videUrl         = self.joinedSelectedURL;
        shareController.customAudioUrl  = nil;
        shareController.useTime         = @"";
        shareController.filterName      = nil;
        shareController.useCustomAudio  = NO;
        shareController.vAudioStartTime = kCMTimeZero;
        //shareController.vAddRetroTitle  = ![vRetroText isHidden];
        shareController.vRetroTitleText = @"";
        shareController.vRetroTitleVisibleRanges = nil;
        shareController.isComingFromMontage = YES;
        
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)openGalleryAction:(UIButton *)sender {
    
    
    //    if(self.allAssets.count >=3) // AND use premium = yes or no
    //
    //    {
    //
    //        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Use    Premium to add more than 3 Videos" andCancelButton:NO forAlertType:AlertInfo];
    //        [alert show];
    //
    //    }
    //
    
    if (self.allAssets.count >= 10) {
        
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"You Cannot add more than 10 Videos." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else{
        [self.players stop];
        [self.playerViewController.player pause] ;
        self.ImagesToDisplay = [[NSMutableArray alloc]init];
        UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
        videoPicker.delegate = self;
        videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4] ;
        UIView *v = [self.view viewWithTag:88];
        v.hidden = YES;
        videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        
        [self presentViewController:videoPicker animated:YES completion:nil];
    }
}


#pragma mark - Gesture Methods

- (void)tappOnImage:(UIGestureRecognizer *)recognizer {
    NSLog(@"%ld", recognizer.view.tag);
}
- (void)tapGestureRecognizer:(UIGestureRecognizer *)recognizer {
    CGPoint tappedPoint = [recognizer locationInView:thumbView];
    [thumbView changeValueWithTotalTime:videosTimeInSeconds tappedPoint: tappedPoint];
    
}


#pragma mark - Video Edidtor Methods

- (void)MergeVideo {
    if (self.allAssets.count > 1) {
        [self.playerViewController.player pause] ;
        [SVProgressHUD showWithStatus:@"Joining Videos..."];
        int countCheck = (int)self.allAssets.count;
        if(self.selectedAudioFileUrl != nil) {
            [self.allAssets addObject:_selectedAudioFileUrl];
        }
        CGFloat totalDuration;
        totalDuration = 0;
        
        AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
        
        AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                            preferredTrackID:kCMPersistentTrackID_Invalid];
        AVMutableCompositionTrack *soundtrackTrack ;
        CMTime AllvideosTime = kCMTimeZero;
        
        AVAsset *soundAsset = [AVAsset assetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.allAssets[self.allAssets.count-1]]]];
        for (int i = 0 ; i <  countCheck ; i++)
        {
            id object = self.allAssets[i];
            
            AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",object]]];
            CMTime subtrahend = CMTimeMake(0, 1000);
            if([[asset tracksWithMediaType:AVMediaTypeVideo] count]){
        
                [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, CMTimeSubtract(asset.duration, subtrahend)) ofTrack:[[asset tracksWithMediaType:AVMediaTypeVideo] firstObject] atTime:AllvideosTime error:nil];
                
            }
            if (self.selectedAudioFileUrl == nil) {
                if([[asset tracksWithMediaType:AVMediaTypeAudio] count]){
                    
                    
                    soundtrackTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
                    
                    [soundtrackTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration) ofTrack:[[asset tracksWithMediaType:AVMediaTypeAudio] firstObject] atTime:AllvideosTime error:nil];
                    
                }
                else{
                    
                }
            }
            AllvideosTime = CMTimeAdd(AllvideosTime, asset.duration);
        }
        if (self.selectedAudioFileUrl != nil) {
            if([[soundAsset tracksWithMediaType:AVMediaTypeAudio] count]){
                
                
                soundtrackTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
                
                [soundtrackTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, AllvideosTime) ofTrack:[[soundAsset tracksWithMediaType:AVMediaTypeAudio] firstObject] atTime:kCMTimeZero error:nil];
            }
        }
        
        /*NSString* documentsDirectory = [self applicationDocumentsDirectory];
        NSString* myDocumentPath = [documentsDirectory stringByAppendingPathComponent:
                                    [NSString stringWithFormat:@"combined_video-%d.mov", arc4random() % 1000000]];
        NSURL* urlVideoMain = [[NSURL alloc] initFileURLWithPath: myDocumentPath];
        if([[NSFileManager defaultManager] fileExistsAtPath:myDocumentPath]){
            [[NSFileManager defaultManager] removeItemAtPath:myDocumentPath error:nil];
        }*/
        
        NSURL *urlVideoMain = [Util getMontageVideoOutputURL];
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc]
                                          initWithAsset:mixComposition
                                          presetName:AVAssetExportPresetHighestQuality];
        exporter.outputURL = urlVideoMain;
        exporter.outputFileType = @"com.apple.quicktime-movie";
        exporter.shouldOptimizeForNetworkUse = YES;
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            dispatch_async (dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                switch ([exporter status]){
                    case AVAssetExportSessionStatusFailed:{
                        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Error in joining videos" andCancelButton:NO forAlertType:AlertFailure];
                        [alert show];
                        NSLog(@"Error ++++++ %@" , [[exporter error] description]) ;
                        NSLog(@"Error ++++++ %@" , [[exporter error] localizedDescription]) ;
                        NSLog(@"Error ++++++ %@" , [[exporter error] debugDescription]) ;

                    }
                        break;
                        
                    case AVAssetExportSessionStatusCancelled:
                        break;
                        
                    case AVAssetExportSessionStatusCompleted:{
                        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Videos Join completed." andCancelButton:NO forAlertType:AlertSuccess];
                        [alert show];
                        
                        alert.completionBlock = ^void (AMSmoothAlertView *alertObj, UIButton *button) {
                            if(button == alertObj.defaultButton) {
                                self.joinedSelectedURL = urlVideoMain;
                                if(self.selectedAudioFileUrl != nil){
                                    [self.allAssets removeLastObject];
                                    
                                }
                                [self performSegueWithIdentifier:@"dashboadToJoinedViewController" sender:self];
                            }
                        };
                    }
                        break;
                        
                    default:
                        break;
                }
            });
        }];
        
    }
    else if (self.allAssets.count == 1){
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry" andText:@"Please select more than 1 Videos" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry" andText:@"Please Select Videos" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        
    }
}


-(IBAction)joinAction:(UIButton *)sender {
    if (![Util isPremiumUser]) {
        [Util presentPremiumPage:self];
        return;
    }
    [self MergeVideo];
}
-(void)applyTransitionsToVideosWithIndex :(int)ind forFirstVideo:(BOOL)isFirst {
    __block int index = ind;
    __block NSString *transitionStr = [transitionsArray objectAtIndex:index];
    
    if (transitionStr.length) {
        if([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]){
            
            [self videoOutputWithTransitionsIndex:index andAnimation:transitionStr andForFirst:isFirst sAndResponseBlock:^(id object, BOOL status, NSError *error) {
                if (status) {
                    UIButton *btnToChange = self.transitionBtns[index];
                    [btnToChange setImage:[UIImage imageNamed:@"transitionBtnSelectdBG"] forState:UIControlStateNormal];
                    NSURL *outputURL = object;
                    
                    if (isFirst) {
                        [self.allAssets replaceObjectAtIndex:index   withObject:outputURL];
                        [self.allAssetsWithTransitions replaceObjectAtIndex:index   withObject:outputURL];
                    }
                    else{
                        [self.allAssets replaceObjectAtIndex:index + 1   withObject:outputURL];
                        [self.allAssetsWithTransitions replaceObjectAtIndex:index + 1   withObject:outputURL];
                    }
                    
                    if (isFirst) {
                        [self applyTransitionsToVideosWithIndex:index  forFirstVideo:NO];
                    }
                    else{
                        if ((index + 1) < transitionsArray.count) {
                            transitionStr = [transitionsArray objectAtIndex:index + 1];
                            if ([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]) {
                                [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:YES];
                            }
                            else{
                                [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:NO];
                            }
                        }
                        else{
                            [SVProgressHUD dismiss];
                            [self setImages];
                            [self reloadVideoPlayer];
                        }
                    }
                }
                else{
                    [SVProgressHUD dismiss];
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertInfo];
                    [alert show];
                }
            }];
        }
        else if ([transitionStr isEqualToString:@"Dissolve"] || [transitionStr isEqualToString:@"Scale Corner"] || [transitionStr isEqualToString:@"Rotate"] || [transitionStr isEqualToString:@"Scale"] || [transitionStr isEqualToString:@"Rotate"] || [transitionStr isEqualToString:@"Squared"]){
            AVAsset *asset = [AVAsset assetWithURL:self.allAssets[index]];
            [self cutVideoOfAsset:asset andIndex:index andTransition:transitionStr sAndResponseBlock:^(id object, BOOL status, NSError *error) {
                if (status) {
                    NSURL *outputURL = object;
                    UIButton *btnToChange = self.transitionBtns[index];
                    [btnToChange setImage:[UIImage imageNamed:@"transitionBtnSelectdBG"] forState:UIControlStateNormal];
                    [self.allAssets replaceObjectAtIndex:index+1   withObject:outputURL];
                    [self.allAssetsWithTransitions replaceObjectAtIndex:index+1   withObject:outputURL];
                    
                    
                    if ((index + 1) < transitionsArray.count) {
                        transitionStr = [transitionsArray objectAtIndex:index + 1];
                        if ([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]) {
                            [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:YES];
                        }
                        else{
                            [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:NO];
                        }
                    }
                    else{
                        [SVProgressHUD dismiss];
                        [self setImages];
                        [self reloadVideoPlayer];
                    }
                }
                else{
                    [SVProgressHUD dismiss];
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertInfo];
                    [alert show];
                }
            }];
            [self cutOffLastPartVideoOfAsset:asset andIndex:index];
        }
        else if ([transitionStr isEqualToString:@"Cut"]){
            UIButton *btnToChange = self.transitionBtns[index];
            [btnToChange setImage:[UIImage imageNamed:@"transitionBtnSelectdBG"] forState:UIControlStateNormal];
            
            if ((index + 1) < transitionsArray.count) {
                transitionStr = [transitionsArray objectAtIndex:index + 1];
                if ([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]) {
                    [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:YES];
                }
                else{
                    [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:NO];
                }
            }
            else{
                [SVProgressHUD dismiss];
                [self setImages];
                [self reloadVideoPlayer];
            }
        }
        else if ([transitionStr isEqualToString:@"Glitch"]){
            int i = (arc4random() % 4)+ 1 ;
            NSString *fileName ;
            
            AVURLAsset * secondAsset = [AVURLAsset URLAssetWithURL:self.allAssets[index+1] options:nil];
            AVAssetTrack *videoAssetTrack = [[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
            UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
            BOOL isVideoAssetPortrait_  = NO;
            CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
            if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
                videoAssetOrientation_ = UIImageOrientationRight;
                isVideoAssetPortrait_ = YES;
            }
            if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
                videoAssetOrientation_ =  UIImageOrientationLeft;
                isVideoAssetPortrait_ = YES;
            }
            if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
                videoAssetOrientation_ =  UIImageOrientationUp;
            }
            if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
                videoAssetOrientation_ = UIImageOrientationDown;
            }
            CGSize naturalSize;
            if(isVideoAssetPortrait_){
                naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
            } else {
                naturalSize = videoAssetTrack.naturalSize;
            }
            
            if (naturalSize.width < 736) {
                fileName = [NSString stringWithFormat:@"tilt/192X144/%i" , i];
            }
            else if (naturalSize.width > 736 && naturalSize.width < 1600) {
                fileName = [NSString stringWithFormat:@"tilt/1280X720/%i" , i];
            }
            else {
                fileName = [NSString stringWithFormat:@"tilt/1920X1080/%i" , i];
            }

            //fileName = [NSString stringWithFormat:@"%i" , 1];


            
            NSURL *fileUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:fileName ofType:@"mp4"]] ;
                              
            
            
            
            [self overlapVideosWithIndex:index andCutUrl:fileUrl andTransition: transitionStr sAndResponseBlock:^(id object, BOOL status, NSError *error) {
                 if (status) {
                     UIButton *btnToChange = self.transitionBtns[index];
                     [btnToChange setImage:[UIImage imageNamed:@"transitionBtnSelectdBG"] forState:UIControlStateNormal];
                     __block NSURL *outputURL = object;
                     NSLog(@"Replacing at index %i" , index+1);
                     
                     [self.allAssets replaceObjectAtIndex:index+1   withObject:outputURL];
                     [self.allAssetsWithTransitions replaceObjectAtIndex:index+1   withObject:outputURL];
                     if ((index + 1) < transitionsArray.count) {
                         transitionStr = [transitionsArray objectAtIndex:index + 1];
                         if ([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]) {
                             [self applyTransitionsToVideosWithIndex:index+1  forFirstVideo:YES];
                         }
                         else{
                             [self applyTransitionsToVideosWithIndex:index+1  forFirstVideo:NO];
                         }
                     }
                     else{
                         [SVProgressHUD dismiss];
                         //[self setImages];
                         [self reloadVideoPlayer];
                     }
                 }
                 else{
                     [SVProgressHUD dismiss];
                     AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertInfo];
                     [alert show];
                 }

             }];
            
            /*
            
            
            [self joinVideoWithGlitchWithIndex:index sAndResponseBlock:^(id object, BOOL status, NSError *error) {
                if (status) {
                    UIButton *btnToChange = self.transitionBtns[index];
                    [btnToChange setImage:[UIImage imageNamed:@"transitionBtnSelectdBG"] forState:UIControlStateNormal];
                    NSURL *outputURL = object;
                    
                    [self.allAssets replaceObjectAtIndex:index + 1   withObject:outputURL];
                    [self.allAssetsWithTransitions replaceObjectAtIndex:index + 1   withObject:outputURL];
                    if ((index + 1) < transitionsArray.count) {
                        transitionStr = [transitionsArray objectAtIndex:index + 1];
                        if ([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]) {
                            [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:YES];
                        }
                        else{
                            [self applyTransitionsToVideosWithIndex:index + 1  forFirstVideo:NO];
                        }
                    }
                    else{
                        [SVProgressHUD dismiss];
                        [self setImages];
                        [self reloadVideoPlayer];
                    }
                }
                else{
                    [SVProgressHUD dismiss];
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.description andCancelButton:NO forAlertType:AlertInfo];
                    [alert show];
                }
            }];
            
            */
            
        }

    }
    else{
        [SVProgressHUD dismiss];
        [self setImages];
        [self reloadVideoPlayer];
    }
}

#pragma mark - CollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return transitionImagesArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [UIImage imageNamed:[transitionImagesArray objectAtIndex:indexPath.row]];
    UILabel *transitionName = (UILabel *)[cell viewWithTag:101];
    transitionName.text = [transitionNamesArray objectAtIndex: indexPath.row];
    
    NSString *transition = [transitionsArray objectAtIndex:transitionBtnTag];
    
    if (!transition.length && indexPath.row == 0 ) {
        recipeImageView.image = [UIImage imageNamed:@"transAselected"] ;
    }
    else if ([transition isEqualToString:[transitionNamesArray objectAtIndex: indexPath.row]]) {
        if ([transition isEqualToString:@"Cut"]) {
            recipeImageView.image = [UIImage imageNamed:@"transAselected"] ;
        }
        if ([transition isEqualToString:@"Fade Light"]) {
            recipeImageView.image = [UIImage imageNamed:@"fade_light_selected"] ;
        }
        if ([transition isEqualToString:@"Fade Dark"]) {
            recipeImageView.image = [UIImage imageNamed:@"fade_dark_selected"] ;
        }
        if ([transition isEqualToString:@"Dissolve"]) {
            recipeImageView.image = [UIImage imageNamed:@"dissolve_selected"] ;
        }
        if ([transition isEqualToString:@"Scale"]) {
            recipeImageView.image = [UIImage imageNamed:@"corner_scale_selected"] ;
        }
        if ([transition isEqualToString:@"Rotate"]) {
            recipeImageView.image = [UIImage imageNamed:@"rotate_selected"] ;
        }
        if ([transition isEqualToString:@"Scale Corner"]) {
            recipeImageView.image = [UIImage imageNamed:@"scale_selected"] ;
        }
        if ([transition isEqualToString:@"Glitch"]) {
            recipeImageView.image = [UIImage imageNamed:@"glitch_selected"] ;
        }
    }
    
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *transitionStr = @"";
    if (indexPath.row == 0) {
        transitionStr = @"Cut";
    }
    else if(indexPath.row == 1){
        transitionStr = @"Glitch";
    }
    else if(indexPath.row == 2){
        transitionStr = @"Fade Light";
    }
    else if(indexPath.row == 3){
        transitionStr = @"Fade Dark";
    }
    else if(indexPath.row == 4){
        transitionStr = @"Dissolve";
    }
    else if(indexPath.row == 5){
        transitionStr = @"Scale Corner";
    }
    else if(indexPath.row == 6){
        transitionStr = @"Rotate";
    }
    else if(indexPath.row == 7){
        transitionStr = @"Scale";
    }
    else if(indexPath.row == 8){
        transitionStr = @"Squared";
    }
    
    
    
    self.allAssets = [[NSMutableArray alloc] initWithArray:self.allAssetsCopy];
    
    [transitionsArray replaceObjectAtIndex:transitionBtnTag withObject:transitionStr];
    
    [self applyTransitionsToAll];
    
    
    [SVProgressHUD showWithStatus:@"Applying Transition..." ];
    
    
    if ([transitionStr isEqualToString:@"Fade Light"] || [transitionStr isEqualToString:@"Fade Dark"]) {
        [self applyTransitionsToVideosWithIndex:0 forFirstVideo:YES];
    }
    else if ([transitionStr isEqualToString:@"Dissolve"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Scale Corner"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Rotate"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Slide"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Cut"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Scale"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Glitch"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else if ([transitionStr isEqualToString:@"Squared"]){
        [self applyTransitionsToVideosWithIndex:0  forFirstVideo:NO];
    }
    else{
        [self applyTransitionsToVideosWithIndex:0 forFirstVideo:NO];
    }
    
    selectedTransition = indexPath.row;
    isFirstVideo = YES;
    thumbNailImageForOverlay = [self testGenerateThumbNailDataWithVideo:self.allAssets[transitionBtnTag]];
    isFirstVideo = NO;
    secondthumbNailImageForOverlay = [self testGenerateThumbNailDataWithVideo:self.allAssets[transitionBtnTag + 1]];
    [self hideTransitinWindowAction:nil];
    if (self.allAssets.count > 1) {
    }
    
}

#pragma mark - Video Player Delegate

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    if (currentPlaying+1 < self.allAssets.count) {
        currentPlaying = currentPlaying+1;
        NSURL *currentURL = self.allAssets[currentPlaying] ;
        if (!_playerViewController) {
            _playerViewController = [[AVPlayerViewController alloc] init];
            _playerViewController.delegate = self;
            _playerViewController.view.frame = self.playerView.frame;
            _playerViewController.showsPlaybackControls = YES;
            
            // First create an AVPlayerItem
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:currentURL];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
            
            
            _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
            _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
            
            
            [self.playerView addSubview:_playerViewController.view];
            UIImage *normImage = [UIImage imageNamed:@"preview"];
            playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
            [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
            [playBTN setImage:normImage forState:UIControlStateNormal];
            playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
            playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
            [_playerViewController.view addSubview:playBTN];
        }
        else{
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:currentURL];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
            _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
        }
        
        if (self.selectedAudioFileUrl !=nil) {
            [[self.playerViewController player] setMuted:YES];
        }
        
        [_playerViewController.player play];
        playBTN.hidden = NO ;
    }
    else{
        NSLog(@"_________________________currentPlaying = %d", currentPlaying) ;
        [playBTN setImage:[UIImage imageNamed:@"preview"] forState:UIControlStateNormal];
        playBTN.tag = 11 ;
        [self.players stop];
        self.players.currentTime = 0.0 ;
        currentPlaying = 0 ;
        NSURL *firstURL = self.allAssets[0] ;
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:firstURL];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
        playBTN.hidden = NO;
        AVPlayerItem *p = [notification object];
        [p seekToTime:kCMTimeZero];
        
        
    }
    
    /*
     playBTN.hidden = NO;
     AVPlayerItem *p = [notification object];
     [p seekToTime:kCMTimeZero];
     */
}

- (IBAction)PlayVideoAction:(UIButton *)sender {
    NSLog(@"%ld",(long)sender.tag);
    
    NSURL *currentURL = self.allAssets[sender.tag] ;
    AVPlayerItem* item=[[AVPlayerItem alloc]initWithURL:currentURL];
    NSLog(@"\n\nAVPlayerItem\n%@",item);
    _playerViewController.player=[AVPlayer playerWithPlayerItem:item];
    [_playerViewController.player play];
}


#pragma mark - Transition WindowMethods

- (NSMutableArray *)moveObjectAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex inArray :(NSMutableArray *)array{
    id object = [array objectAtIndex:fromIndex];
    [array removeObjectAtIndex:fromIndex];
    [array insertObject:object atIndex:toIndex];
    return array ;
}
- (IBAction)openTransitionWindowAction:(UIButton *)sender {
    transitionBtnTag = sender.tag;
    /*
    if ([sender.imageView.image isEqual:[UIImage imageNamed:@"transitionBtnSelectdBG"]]){
        int i = (int)transitionBtnTag;
        [self.allAssets replaceObjectAtIndex:i withObject:self.allAssetsCopy[i]];
        [self.allAssets replaceObjectAtIndex:i+1 withObject:self.allAssetsCopy[i+1]];
    }
    else{
        int i = (int)transitionBtnTag;
        [self.allAssets replaceObjectAtIndex:i withObject:self.allAssets[i]];
        [self.allAssets replaceObjectAtIndex:i+1 withObject:self.allAssetsCopy[i+1]];
    }
    [self.collectionView reloadData];
    [self.view layoutIfNeeded];
     */
    self.transitionViewBottomSpace.constant=0;
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.tarnsitionView layoutIfNeeded];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
    [self.collectionView reloadData];
}

-(IBAction)hideTransitinWindowAction:(UIButton *)sender {
    [UIView animateWithDuration:0.5 animations:^{
        self.transitionViewBottomSpace.constant=-self.tarnsitionView.frame.size.height;
        [self.tarnsitionView layoutIfNeeded];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {}];
}

#pragma mark - Audion and Media Pickers

- (IBAction)openAudioPickerAction:(UIButton *)sender {
    if (![Util isPremiumUser]) {
        [Util presentPremiumPage:self];
        return;
    }
    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAny];
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = NO;
    mediaPicker.prompt = @"Select songs to play";
    [self presentViewController:mediaPicker animated:YES completion:nil];
    
}
- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{
    if (mediaItemCollection) {
        for (MPMediaItem *item in mediaItemCollection.items) {
            NSLog(@"%@",item.assetURL);
            self.selectedAudioFileUrl = item.assetURL;
            self.selectedURL=nil;
        }
    }
    [self dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"Gallery-Player" sender:nil];
        });
    }];
}

-(void)mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)importFirstVideoAction:(UIButton *)sender {
    [self openGalleryAction:nil];
}

- (void)videoOutputWithTransitionsIndex:(int)value andAnimation:(NSString *)animation andForFirst:(BOOL)isFirst sAndResponseBlock:(EditingResponse)responseBlock
{
    NSDictionary *options = @{AVURLAssetPreferPreciseDurationAndTimingKey:@YES};
    AVURLAsset *videoAsset3 ;;
    AVAsset *asset ;
    if ([animation isEqualToString:@"Dissolve"] || isFirst) {
        asset = [AVAsset assetWithURL:self.allAssets[value]];
        videoAsset3 = [AVURLAsset URLAssetWithURL:self.allAssets[value] options:options];
    }
    else if([animation isEqualToString:@"Slide_Right"] || [animation isEqualToString:@"Fade Light"] || [animation isEqualToString:@"Fade Dark"] || [animation isEqualToString:@"Pixels"]){
        asset = [AVAsset assetWithURL:self.allAssets[value + 1]];
        videoAsset3 = [AVURLAsset URLAssetWithURL:self.allAssets[value + 1] options:options];
    }
    // 1 - Early exit if there's no video file selected
    if (!asset) {
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Load a Video Asset First"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    // 3 - Video track
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    
    //CMTimeRange newTimeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
    AVAssetTrack *videoAsset3Track = [[videoAsset3 tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CMTime duration3 = videoAsset3Track.timeRange.duration;
    NSError *error ;
    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, duration3)
                        ofTrack:videoAsset3Track
                         atTime:kCMTimeZero error:&error];
    if (error) {
        NSLog(@"Error Gotten = %@", error.description);
    }
    //audio track
    if([[asset tracksWithMediaType:AVMediaTypeAudio] count]){
        AVMutableCompositionTrack *soundtrackTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        AVAssetTrack *audioAsset3Track = [[videoAsset3 tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        CMTime audioDuration3 = audioAsset3Track.timeRange.duration;
        [soundtrackTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioDuration3)
                            ofTrack:audioAsset3Track
                             atTime:kCMTimeZero error:&error];
    }
    // 3.1 - Create AVMutableVideoCompositionInstruction
    AVMutableVideoCompositionInstruction *mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
    
    // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
    AVMutableVideoCompositionLayerInstruction *videolayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    AVAssetTrack *videoAssetTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_  = NO;
    CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
    if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ = UIImageOrientationRight;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ =  UIImageOrientationLeft;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
        videoAssetOrientation_ =  UIImageOrientationUp;
    }
    if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
        videoAssetOrientation_ = UIImageOrientationDown;
    }
    [videolayerInstruction setTransform:videoAssetTrack.preferredTransform atTime:kCMTimeZero];
    [videolayerInstruction setOpacity:0.0 atTime:asset.duration];
    
    // 3.3 - Add instructions
    mainInstruction.layerInstructions = [NSArray arrayWithObjects:videolayerInstruction,nil];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    
    CGSize naturalSize;
    if(isVideoAssetPortrait_){
        naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
    } else {
        naturalSize = videoAssetTrack.naturalSize;
    }
    
    float renderWidth, renderHeight;
    renderWidth = naturalSize.width;
    renderHeight = naturalSize.height;
    mainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    if([animation isEqualToString:@"Slide_Right"]){
        [self applyVideoForEndVideoEffectsToComposition:mainCompositionInst andSize:naturalSize andTime:asset.duration andTransition:animation andIndex:value];
    }
    else if([animation isEqualToString:@"Crossing"]){
        [self applyVideoForCrossingVideoEffectsToComposition:mainCompositionInst andSize:naturalSize andTime:asset.duration andTransition:animation andIndex:value];
    }
    else if([animation isEqualToString:@"Fade Dark"]){
        if (isFirst) {
            [self applyFadingVideoEffectsToCompositionFirstVideo:mainCompositionInst size:naturalSize andColor:[UIColor blackColor] andTime:asset.duration];
        }
        else{
            [self applyFadingVideoEffectsToCompositionLastVideo:mainCompositionInst size:naturalSize andColor:[UIColor blackColor] andTime:asset.duration];
        }
    }
    else if([animation isEqualToString:@"Fade Light"]){
        if (isFirst) {
            [self applyFadingVideoEffectsToCompositionFirstVideo:mainCompositionInst size:naturalSize andColor:[UIColor whiteColor] andTime:asset.duration];
        }
        else{
            [self applyFadingVideoEffectsToCompositionLastVideo:mainCompositionInst size:naturalSize andColor:[UIColor whiteColor] andTime:asset.duration];
        }
    }
    else if([animation isEqualToString:@"Pixels"]){
        if (isFirst) {
            [self applyVideoPixelsForStartVideoEffectsToComposition:mainCompositionInst andSize:naturalSize andTime:asset.duration andTransition:animation andIndex:value];
        }
        else{
            
            [self applyVideoPixelsForEndVideoEffectsToComposition:mainCompositionInst andSize:naturalSize andTime:asset.duration andTransition:animation andIndex:value];
        }
    }
    
    // 4 - Get path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"FinalVideos-%d.mov",arc4random() % 10000000]];
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    if([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs]){
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    }
    
    // 5 - Create exporter
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                      presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
    exporter.videoComposition = mainCompositionInst;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exporter.status == AVAssetExportSessionStatusCompleted) {
                responseBlock(exporter.outputURL , YES , nil);
            }
            else{
                responseBlock(nil , NO , exporter.error);
            }
        });
        
    }];
}


-(void)reloadVideoPlayer {
    _playerViewController.player = [AVPlayer playerWithURL:self.allAssets[0]];
}


-(UIImage*)testGenerateThumbNailDataWithVideo:(NSURL *)videoURL{
    CMTime time;
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    if (isFirstVideo) {
        int timeT =  CMTimeGetSeconds(asset.duration)*60.0;
        time =CMTimeMakeWithSeconds(timeT, 600);
    }
    else{
        time = CMTimeMakeWithSeconds(0.0, 600);
    }
    
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *currentImg = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    
    return currentImg;
}

-(void)exportDidFinish:(AVAssetExportSession*)session :(int)val{
    
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        [self.allAssets replaceObjectAtIndex:val withObject:outputURL];
        [self.allAssetsWithTransitions replaceObjectAtIndex:val withObject:outputURL];
        
        if (val == transitionBtnTag + 1) {
            [SVProgressHUD dismiss];
            UIButton *btnToChange = self.transitionBtns[transitionBtnTag];
            [btnToChange setImage:[UIImage imageNamed:@"transitionBtnSelectdBG"] forState:UIControlStateNormal];
            
        }
        //[self setImages];
    }
    else{
        
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:session.error.description andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        
        
    }
}


- (void)applyVideoEffectsToComposition:(AVMutableVideoComposition *)composition size:(CGSize)size{
    // 1 - Set up the text layer
    CATextLayer *subtitle1Text = [[CATextLayer alloc] init];
    [subtitle1Text setFont:@"Helvetica-Bold"];
    [subtitle1Text setFontSize:36];
    [subtitle1Text setFrame:CGRectMake(0, 0, size.width, 100)];
    [subtitle1Text setString:@"YES BABY>....."];
    [subtitle1Text setAlignmentMode:kCAAlignmentCenter];
    [subtitle1Text setForegroundColor:[[UIColor whiteColor] CGColor]];
    subtitle1Text.backgroundColor = [[UIColor orangeColor] CGColor];
    // 2 - The usual overlay
    CALayer *overlayLayer = [CALayer layer];
    [overlayLayer addSublayer:subtitle1Text];
    overlayLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [overlayLayer setMasksToBounds:YES];
    
    // the code for the opacity animation which then removes the text
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animation setDuration:2]; //duration
    [animation setFromValue:[NSNumber numberWithFloat:1.0]];
    [animation setToValue:[NSNumber numberWithFloat:0.0]];
    [animation setBeginTime:5]; // time to show text
    [animation setRemovedOnCompletion:NO];
    [animation setFillMode:kCAFillModeForwards];
    [subtitle1Text addAnimation:animation forKey:@"animateOpacity"];
    ////
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];}



- (void)applyVideoForStartVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];
    
    
    
    
    AVPlayerViewController *nplayerViewController = [[AVPlayerViewController alloc] init];
    nplayerViewController.delegate = self;
    nplayerViewController.view.frame = CGRectMake(0, 0,animateView.frame.size.width, animateView.frame.size.height);
    
    nplayerViewController.showsPlaybackControls = NO;
    
    
    // First create an AVPlayerItem
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:self.allAssets[index + 1]];
    

    
    
    nplayerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    nplayerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    //[nplayerViewController.player play];
    [animateView addSubview:nplayerViewController.view];

    
    
    
    
    //UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    //[animateView addSubview:imageView];
    
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];

    
    

    if ([transition isEqualToString:@"Slide_Left"]) {

        //[imageView setImage:secondthumbNailImageForOverlay];
        CABasicAnimation *animation ;
        animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
        animation.duration=duration.value/duration.timescale;
        animation.repeatCount=0;
        animation.autoreverses=NO;
        animation.fromValue=[NSNumber numberWithFloat:size.width * (duration.value/duration.timescale)];
        animation.toValue=[NSNumber numberWithFloat:0.0];
        animation.beginTime = AVCoreAnimationBeginTimeAtZero;
        animation.removedOnCompletion = NO;
        [overlayLayer2 addAnimation:animation forKey:@"transform.translation.x"];
    
    
    
    }
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

-(void)applyVideoForEndVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.allAssets[index] options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *error = NULL;
    generateImg.appliesPreferredTrackTransform = YES ;
    generateImg.requestedTimeToleranceAfter = kCMTimeZero ;
    generateImg.requestedTimeToleranceBefore = kCMTimeZero ;
    int timeT =  CMTimeGetSeconds(asset.duration);
    CMTime time =CMTimeMakeWithSeconds(timeT, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    if (error) {
        NSLog(@"error==%@", error);
    }
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    [animateView addSubview:imageView];
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0, 0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    CALayer *overlayLayer1 = [CALayer layer];
    if ([transition isEqualToString:@"Slide_Right"]) {
        UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
        CGImageRelease(refImg);
        [imageView setImage:FrameImage];
        
        CABasicAnimation *animation =
        [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
        animation.duration=duration.value/duration.timescale;
        animation.repeatCount=0;
        animation.autoreverses=NO;
        animation.fromValue=[NSNumber numberWithFloat:0.0];
        animation.toValue=[NSNumber numberWithFloat:size.width * (duration.value/duration.timescale)];
        animation.beginTime = AVCoreAnimationBeginTimeAtZero;
        animation.removedOnCompletion = NO;
        
        [overlayLayer2 addAnimation:animation forKey:@"transform.translation.x"];
    }
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    
    [parentLayer addSublayer:overlayLayer2];
    [parentLayer addSublayer:overlayLayer1];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

-(void)applyVideoForCrossingVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.allAssets[index] options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *error = NULL;
    generateImg.appliesPreferredTrackTransform = YES ;
    generateImg.requestedTimeToleranceAfter = kCMTimeZero ;
    generateImg.requestedTimeToleranceBefore = kCMTimeZero ;
    int timeT =  CMTimeGetSeconds(asset.duration);
    CMTime time =CMTimeMakeWithSeconds(timeT, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    if (error) {
        NSLog(@"error==%@", error);
    }
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    [animateView addSubview:imageView];
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0, 0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    CALayer *overlayLayer1 = [CALayer layer];
    if ([transition isEqualToString:@"Crossing"]) {
        UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];

        
        //second layer View
        UIImageView *imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        [overlayLayer1 addSublayer:imageView1.layer];
        overlayLayer1.frame = CGRectMake(0, 0, size.width, size.height);
        [overlayLayer1 setMasksToBounds:YES];
        imageView1.alpha = 1.0;
        [self makeTriangulerShapeOfSecondView:animateView];
        [self makeTriangulerShapeOfFirstView:imageView1];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [imageView1 setImage:FrameImage];
            [imageView setImage:FrameImage];
        });
        CGImageRelease(refImg);

        
        CABasicAnimation *animation =
        [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
        animation.duration=(duration.value/duration.timescale)+1;
        animation.repeatCount=0;
        animation.autoreverses=NO;
        animation.fromValue=[NSNumber numberWithFloat:0.0];
        animation.toValue=[NSNumber numberWithFloat:size.width * (duration.value/duration.timescale)];
        animation.beginTime = AVCoreAnimationBeginTimeAtZero;
        animation.removedOnCompletion = NO;
        [overlayLayer2 addAnimation:animation forKey:@"transform.translation.x"];
        
        CABasicAnimation *animation1 =
        [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
        animation1.duration=(duration.value/duration.timescale)+1;
        animation1.repeatCount=0;
        animation1.autoreverses=NO;
        animation1.fromValue=[NSNumber numberWithFloat:0.0];
        animation1.toValue=[NSNumber numberWithFloat:-size.width *(duration.value/duration.timescale)];
        animation1.beginTime = AVCoreAnimationBeginTimeAtZero;
        animation1.removedOnCompletion = NO;
        
        [overlayLayer1 addAnimation:animation1 forKey:@"transform.translation.x"];
        
        
        //animation for y axis
        CABasicAnimation *animationY =
        [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
        animationY.duration=(duration.value/duration.timescale)+1;
        animationY.repeatCount=0;
        animationY.autoreverses=NO;
        animationY.fromValue=[NSNumber numberWithFloat:0.0];
        animationY.toValue=[NSNumber numberWithFloat:-size.height *(duration.value/duration.timescale)];
        animationY.beginTime = AVCoreAnimationBeginTimeAtZero;
        animationY.removedOnCompletion = NO;
        
        [overlayLayer1 addAnimation:animationY forKey:@"transform.translation.y"];
        
        CABasicAnimation *animationY2 =
        [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
        animationY2.duration=(duration.value/duration.timescale)+1;
        animationY2.repeatCount=0;
        animationY2.autoreverses=NO;
        animationY2.fromValue=[NSNumber numberWithFloat:0.0];
        animationY2.toValue=[NSNumber numberWithFloat:size.height * (duration.value/duration.timescale)];
        animationY2.beginTime = AVCoreAnimationBeginTimeAtZero;
        animationY2.removedOnCompletion = NO;
        [overlayLayer2 addAnimation:animationY2 forKey:@"transform.translation.y"];
        
    }
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    
    [parentLayer addSublayer:overlayLayer2];
    [parentLayer addSublayer:overlayLayer1];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

- (void)applyVideoFadeForStartVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index andColor :(UIColor *)color
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];

    UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    [animateView addSubview:imageView];
    
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];

    if ([transition containsString:@"Fade"]) {
        
        animateView.backgroundColor = [UIColor clearColor];
        [imageView removeFromSuperview];
        overlayLayer2.backgroundColor = color.CGColor;
        [CATransaction begin]; {
            [CATransaction setCompletionBlock:^{
                //animateView.alpha = 0.0;
                NSLog(@"Completed Animations");
                //overlayLayer2.hidden = YES;

            }];
            CABasicAnimation *animation ;
            animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
            animation.duration=0.5;
            
            
            animation.beginTime = CACurrentMediaTime() + ((duration.value/duration.timescale) - 0.5);
            
            animation.beginTime = CACurrentMediaTime();
            
            
            animation.repeatCount=0;
            animation.autoreverses=NO;
            animation.fromValue=[NSNumber numberWithFloat:0.0];
            animation.toValue=[NSNumber numberWithFloat:1.0];
            animation.beginTime = AVCoreAnimationBeginTimeAtZero;
            animation.removedOnCompletion = NO;
            [overlayLayer2 addAnimation:animation forKey:@"opacity"];
        } [CATransaction commit];
    }
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

- (void)applyVideoFadeForEndVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index andColor :(UIColor *)color
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    [animateView addSubview:imageView];
    
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    
    if ([transition containsString:@"Fade"]) {
        overlayLayer2.backgroundColor = color.CGColor;
        
        animateView.opaque= NO;

        [imageView removeFromSuperview];
        
        [CATransaction begin]; {
            [CATransaction setCompletionBlock:^{
                animateView.alpha = 0.0;
                NSLog(@"Completed Animations");
            }];
            CABasicAnimation *animation =
            [CABasicAnimation animationWithKeyPath:@"opacity"];
            
            animation.duration=  0.5 ;
            animation.repeatCount=0;
            animation.autoreverses=NO;
            animation.fromValue=[NSNumber numberWithFloat:1.0];
            animation.toValue=[NSNumber numberWithFloat:-0.0];
            animation.beginTime = AVCoreAnimationBeginTimeAtZero;
            animation.removedOnCompletion = NO;
            [overlayLayer2 addAnimation:animation forKey:@"opacity"];
        } [CATransaction commit];
    }
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

- (void)applyVideoPixelsForStartVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.allAssets[index] options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *error = NULL;
    generateImg.appliesPreferredTrackTransform = YES ;
    generateImg.requestedTimeToleranceAfter = kCMTimeZero ;
    generateImg.requestedTimeToleranceBefore = kCMTimeZero ;
    int timeT =  CMTimeGetSeconds(asset.duration);
    CMTime time =CMTimeMakeWithSeconds(timeT, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    if (error) {
        NSLog(@"error==%@", error);
    }

    UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    [animateView addSubview:imageView];
    
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    
    if ([transition isEqualToString:@"Pixels"]) {
        UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
        CGImageRelease(refImg);
        imageView.image=nil;
        GPUImagePixellateFilter *filter = [[GPUImagePixellateFilter alloc] init];
        FrameImage = [filter imageByFilteringImage:FrameImage];
        
        [imageView setImage:FrameImage];
        
        CABasicAnimation *animation;
        animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.duration=duration.value/duration.timescale;
        animation.repeatCount=0;
        animation.autoreverses=NO;
        animation.fromValue=[NSNumber numberWithFloat:-1.5];
        animation.toValue=[NSNumber numberWithFloat:1.0];
        animation.beginTime = AVCoreAnimationBeginTimeAtZero;
        animation.removedOnCompletion = NO;
        [overlayLayer2 addAnimation:animation forKey:@"opacity"];
        
    }
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

- (void)applyVideoPixelsForEndVideoEffectsToComposition:(AVMutableVideoComposition *)composition andSize:(CGSize)size andTime:(CMTime)duration andTransition:(NSString *)transition andIndex:(int) index
{
    animateView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    animateView.backgroundColor=[UIColor clearColor];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.allAssets[index + 1] options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *error = NULL;
    generateImg.appliesPreferredTrackTransform = YES ;
    generateImg.requestedTimeToleranceAfter = kCMTimeZero ;
    generateImg.requestedTimeToleranceBefore = kCMTimeZero ;
    int timeT =  CMTimeGetSeconds(asset.duration);
    CMTime time =CMTimeMakeWithSeconds(0, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    if (error) {
        NSLog(@"error==%@", error);
    }
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:animateView.bounds];
    [animateView addSubview:imageView];
    
    CALayer *overlayLayer2 = [CALayer layer];
    [overlayLayer2 addSublayer:animateView.layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    [overlayLayer2 setMasksToBounds:YES];
    
    animateView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    
    if ([transition isEqualToString:@"Pixels"]) {
        UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
        CGImageRelease(refImg);
        imageView.image=nil;
        GPUImagePixellateFilter *filter = [[GPUImagePixellateFilter alloc] init];
        FrameImage = [filter imageByFilteringImage:FrameImage];
        
        [imageView setImage:FrameImage];
        
        CABasicAnimation *animation =
        [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.duration=(duration.value/duration.timescale) + 1;
        animation.repeatCount=0;
        animation.autoreverses=NO;
        animation.fromValue=[NSNumber numberWithFloat:1.0];
        animation.toValue=[NSNumber numberWithFloat:-1.5];
        animation.beginTime = AVCoreAnimationBeginTimeAtZero;
        animation.removedOnCompletion = NO;
        [overlayLayer2 addAnimation:animation forKey:@"opacity"];
        
    }
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}




- (void)applyFadingVideoEffectsToCompositionFirstVideo:(AVMutableVideoComposition *)composition size:(CGSize)size andColor :(UIColor *)color andTime:(CMTime)duration
{
    CALayer *overlayLayer2 = [CALayer layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    
    [overlayLayer2 setMasksToBounds:YES];
    overlayLayer2.backgroundColor = [color CGColor];
    overlayLayer2.opacity = 0.0;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration=0.5;
    animation.repeatCount=1;
    animation.autoreverses=NO;
    // animate from invisible to fully visible
    animation.fromValue=[NSNumber numberWithFloat:0.0];
    animation.toValue=[NSNumber numberWithFloat:1.0];
    animation.beginTime = (duration.value/duration.timescale) - 0.5 - 0;
    animation.removedOnCompletion = NO;
    [overlayLayer2 addAnimation:animation forKey:@"animateOpacity"];
    
    
    
    
    
    CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation2.duration=(duration.value/duration.timescale)+10;
    animation2.repeatCount=1;
    animation2.autoreverses=NO;
    // animate from invisible to fully visible
    animation2.fromValue=[NSNumber numberWithFloat:1.0];
    animation2.toValue=[NSNumber numberWithFloat:1.0];
    animation2.beginTime = (duration.value/duration.timescale) - 0.0 - 0;
    animation2.removedOnCompletion = NO;
    [overlayLayer2 addAnimation:animation2 forKey:@"animateOpacity2"];
    
    
    
    
    // 5
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
}

- (void)applyFadingVideoEffectsToCompositionLastVideo:(AVMutableVideoComposition *)composition size:(CGSize)size andColor :(UIColor *)color andTime:(CMTime)duration
{
    CALayer *overlayLayer2 = [CALayer layer];
    overlayLayer2.frame = CGRectMake(0,0, size.width, size.height);
    
    [overlayLayer2 setMasksToBounds:YES];
    overlayLayer2.backgroundColor = [color CGColor];
    overlayLayer2.opacity = 1.0;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration=0.5;
    animation.repeatCount=1;
    animation.autoreverses=NO;
    // animate from invisible to fully visible
    animation.fromValue=[NSNumber numberWithFloat:1.0];
    animation.toValue=[NSNumber numberWithFloat:0.0];
    animation.beginTime = AVCoreAnimationBeginTimeAtZero;
    animation.removedOnCompletion = NO;
    [overlayLayer2 addAnimation:animation forKey:@"animateOpacity"];
    
    
    
    
    CABasicAnimation *animation2 = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation2.duration=(duration.value/duration.timescale);
    animation2.repeatCount=1;
    animation2.autoreverses=NO;
    // animate from invisible to fully visible
    animation2.fromValue=[NSNumber numberWithFloat:0.0];
    animation2.toValue=[NSNumber numberWithFloat:0.0];
    animation2.beginTime = AVCoreAnimationBeginTimeAtZero + 0.5;
    animation2.removedOnCompletion = NO;
    [overlayLayer2 addAnimation:animation2 forKey:@"animateOpacity2"];

    // 5
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer2];
    
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
}

-(void)makeTriangulerShapeOfSecondView:(UIView *)view {
    // Build a triangular path
    /*  UIBezierPath *path = [UIBezierPath new];
     [path moveToPoint:(CGPoint){view.frame.size.width / 2, 0}];
     [path addLineToPoint:(CGPoint){0,0}];
     [path addLineToPoint:(CGPoint){view.frame.size.width,view.frame.size.height}];
     [path addLineToPoint:(CGPoint){view.frame.size.width / 2, 0}];
     */
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){0, 0}];
    [path addLineToPoint:(CGPoint){view.frame.size.width,view.frame.size.height}];
    [path addLineToPoint:(CGPoint){view.frame.size.width,0}];
    [path addLineToPoint:(CGPoint){0, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask;
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask2 = [CAShapeLayer new];
    mask2.frame = view.bounds;
    mask2.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask2;
}
-(void)makeTriangulerShapeOfFirstView:(UIView *)view {
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){0, 0}];
    [path addLineToPoint:(CGPoint){view.frame.size.width, view.frame.size.height}];
    [path addLineToPoint:(CGPoint){0,view.frame.size.height}];
    [path addLineToPoint:(CGPoint){0, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = view.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask;
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask2 = [CAShapeLayer new];
    mask2.frame = view.bounds;
    mask2.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    view.layer.mask = mask2;
}

-(IBAction)backClearAction:(UIButton *)sender {
    
    [self reloadVideoPlayer];
/*
    for (UIView *view in self.dragViews) {
        [self.dragAndDropController unregisterDragSource:view];
        [self.dragAndDropController unregisterDropTarget:view];
    }
    for (UIButton *btn in self.transitionBtns) {
        [btn setImage:[UIImage imageNamed:@"transitionBtnBG"] forState:UIControlStateNormal];
    }
    [SVProgressHUD dismiss];
    self.allAssets = [[NSMutableArray alloc] init];
    self.allAssetsCopy = [[NSMutableArray alloc] init];
    [self setImages];
    
    [self.audioVisualizationView setHidden:YES];
 */
}


-(NSMutableArray *)getAllFrameImages :(AVAsset *)asset {
    NSMutableArray *imagesArray = [[NSMutableArray alloc] init];
    

    NSArray *movieTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *movieTrack = [movieTracks objectAtIndex:0];
    
    //Make the image Generator
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    //Create a variables for the time estimation
    Float64 durationSeconds = CMTimeGetSeconds(asset.duration);
    Float64 timePerFrame = 1.0 / (Float64)movieTrack.nominalFrameRate;
    Float64 totalFrames = durationSeconds * movieTrack.nominalFrameRate;
    
    //Step through the frames
    for (int counter = 0; counter <= totalFrames; counter++){
        CMTime actualTime;
        Float64 secondsIn = ((float)counter/totalFrames)*durationSeconds;
        CMTime imageTimeEstimate = CMTimeMakeWithSeconds(secondsIn, 600);
        NSError *error;
        CGImageRef image = [imageGenerator copyCGImageAtTime:imageTimeEstimate actualTime:&actualTime error:&error];
      
        [imagesArray addObject: [[UIImage alloc] initWithCGImage:image]] ;
        CGImageRelease(image);    
    }
    
    
    UITableView *dummytableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    
   
    dummytableView.delegate = self ;
    dummytableView.dataSource = self ;

    [self.view addSubview:dummytableView];
    
    
    NSRange endRange = NSMakeRange(imagesArray.count >= 60 ? imagesArray.count - 60 : 0, MIN(imagesArray.count, 60));
    //dummyImagesArray = [[imagesArray subarrayWithRange:endRange] mutableCopy];
    
    dummyImagesArray = [[NSMutableArray alloc] initWithArray:imagesArray];

    [dummytableView reloadData];
    return imagesArray;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dummyImagesArray.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *Ncell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (Ncell == nil)
        Ncell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSLog(@"Creating cell for section: %li and row: %li", (long)[indexPath section], (long)[indexPath row]);
    
    UIView *removingView = [Ncell viewWithTag:23];
    [removingView removeFromSuperview];
    
    UIImageView *nImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, Ncell.size.width, Ncell.size.height)];
    nImageView.image = dummyImagesArray[indexPath.row];
    [Ncell addSubview:nImageView];
    nImageView.tag = 23;
    nImageView.contentMode = UIViewContentModeScaleAspectFit;
    Ncell.textLabel.text = [NSString stringWithFormat:@"%li", (long)indexPath.row];
    return Ncell;
}

-(void)composingVideosTogetherForFirstAsset :(AVAsset *)fistAsset andSecontAsset :(AVAssetReader *)secondAsset andComposition :(AVMutableVideoComposition *)videoComposition  {
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], AVURLAssetPreferPreciseDurationAndTimingKey, nil];
    AVMutableComposition *composition = [AVMutableComposition composition];
    CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(4, 1));
    //AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    
    // Track B
    //NSURL *urlVideo2 = [[NSBundle mainBundle] URLForResource:@"b" withExtension:@"mov"];
    //AVAsset *video2 = [AVURLAsset URLAssetWithURL:urlVideo2 options:options];
    
    
    
    AVMutableCompositionTrack *videoTrack2 = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:0];
    NSArray *videoAssetTracks2 = [fistAsset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *videoAssetTrack2 = ([videoAssetTracks2 count] > 0 ? [videoAssetTracks2 objectAtIndex:0] : nil);
    [videoTrack2 insertTimeRange:timeRange ofTrack:videoAssetTrack2 atTime:kCMTimeZero error:&error];
    
    AVMutableVideoCompositionLayerInstruction *to = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack2];
    [to setOpacity:.5 atTime:kCMTimeZero];
    
    //[to setOpacityRampFromStartOpacity:<#(float)#> toEndOpacity:<#(float)#> timeRange:<#(CMTimeRange)#>]
    //[to setTransform:CGAffineTransformScale(videoAssetTrack2.preferredTransform, .5, .5) atTime:kCMTimeZero];
    
    // Track A
    //NSURL *urlVideo = [[NSBundle mainBundle] URLForResource:@"a" withExtension:@"mov"];
    //AVURLAsset *video = [AVURLAsset URLAssetWithURL:urlVideo options:options];
    AVURLAsset *video = [AVURLAsset URLAssetWithURL:self.allAssets[1] options:options];

    
    AVMutableCompositionTrack *videoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:1];
    NSArray *videoAssetTracks = [video tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *videoAssetTrack = ([videoAssetTracks count] > 0 ? [videoAssetTracks objectAtIndex:0] : nil);
    [videoTrack insertTimeRange:timeRange ofTrack:videoAssetTrack atTime:kCMTimeZero error:nil];
    
    AVMutableVideoCompositionLayerInstruction *from = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    [from setOpacity:.5 atTime:kCMTimeZero];
    
    // Video Compostion
    AVMutableVideoCompositionInstruction *transition = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    transition.backgroundColor = [[UIColor clearColor] CGColor];
    transition.timeRange = timeRange;
    transition.layerInstructions = [NSArray arrayWithObjects:to, from, nil];
    videoComposition.instructions = [NSArray arrayWithObjects:transition,  nil];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    videoComposition.renderSize = composition.naturalSize; // CGSizeMake(480, 360);
}





- (void) overlapVideosWithIndex :(int)index andCutUrl :(NSURL *)cutUrl andTransition :(NSString *)transition sAndResponseBlock:(EditingResponse)responseBlock{
    
    //Here we load our movie Assets using AVURLAsset
    
    //AVURLAsset* firstAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"sampleVideo" ofType:@"mp4"]] options:nil];
    //AVURLAsset * secondAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"sampleVideo" ofType:@"mp4"]] options:nil];
    
    
    AVURLAsset* firstAsset = [AVURLAsset URLAssetWithURL:cutUrl options:nil];
    AVURLAsset * secondAsset = [AVURLAsset URLAssetWithURL:self.allAssets[index+1] options:nil];

    //Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    
    //Here we are creating the first AVMutableCompositionTrack.See how we are adding a new track to our AVMutableComposition.
    AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    //Now we set the length of the firstTrack equal to the length of the firstAsset and add the firstAsset to out newly created track at kCMTimeZero so video plays from the start of the track.
    
    if ([firstAsset tracksWithMediaType:AVMediaTypeVideo].count) {
        [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    
    //Now we repeat the same process for the 2nd track as we did above for the first track.
    AVMutableCompositionTrack *secondTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    if ([secondAsset tracksWithMediaType:AVMediaTypeVideo].count) {
        [secondTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    
    if([[secondAsset tracksWithMediaType:AVMediaTypeAudio] count]){
        AVMutableCompositionTrack *soundtrackTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [soundtrackTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeAudio] firstObject] atTime:kCMTimeZero error:nil];
    }
    
    
    //See how we are creating AVMutableVideoCompositionInstruction object.This object will contain the array of our AVMutableVideoCompositionLayerInstruction objects.You set the duration of the layer.You should add the lenght equal to the lingth of the longer asset in terms of duration.

    AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, secondAsset.duration);
    
    //We will be creating 2 AVMutableVideoCompositionLayerInstruction objects.Each for our 2 AVMutableCompositionTrack.here we are creating AVMutableVideoCompositionLayerInstruction for out first track.see how we make use of Affinetransform to move and scale our First Track.so it is displayed at the bottom of the screen in smaller size.(First track in the one that remains on top).
    //Note: You have to apply transformation to scale and move according to your video size.
    AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];

    AVAssetTrack *videoAssetTrack = [[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_  = NO;
    CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
    if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ = UIImageOrientationRight;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ =  UIImageOrientationLeft;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
        videoAssetOrientation_ =  UIImageOrientationUp;
    }
    if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
        videoAssetOrientation_ = UIImageOrientationDown;
    }
    CGSize naturalSize;
    if(isVideoAssetPortrait_){
        naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
    } else {
        naturalSize = videoAssetTrack.naturalSize;
    }
    
    float renderWidth, renderHeight;
    renderWidth = naturalSize.width;
    renderHeight = naturalSize.height;
    
    
    AVAssetTrack *videoAssetTrack2 = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_2  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_2  = NO;
    CGAffineTransform videoTransform2 = videoAssetTrack2.preferredTransform;
    if (videoTransform2.a == 0 && videoTransform2.b == 1.0 && videoTransform2.c == -1.0 && videoTransform2.d == 0) {
        videoAssetOrientation_2 = UIImageOrientationRight;
        isVideoAssetPortrait_2 = YES;
    }
    if (videoTransform2.a == 0 && videoTransform2.b == -1.0 && videoTransform2.c == 1.0 && videoTransform2.d == 0) {
        videoAssetOrientation_2 =  UIImageOrientationLeft;
        isVideoAssetPortrait_2 = YES;
    }
    if (videoTransform2.a == 1.0 && videoTransform2.b == 0 && videoTransform2.c == 0 && videoTransform2.d == 1.0) {
        videoAssetOrientation_2 =  UIImageOrientationUp;
    }
    if (videoTransform2.a == -1.0 && videoTransform2.b == 0 && videoTransform2.c == 0 && videoTransform2.d == -1.0) {
        videoAssetOrientation_2 = UIImageOrientationDown;
    }
    CGSize naturalSize2;
    if(isVideoAssetPortrait_2){
        naturalSize2 = CGSizeMake(videoAssetTrack2.naturalSize.height, videoAssetTrack2.naturalSize.width);
    } else {
        naturalSize2 = videoAssetTrack2.naturalSize;
    }
    
    CGAffineTransform Scale = CGAffineTransformMakeScale(naturalSize.width / naturalSize2.width, naturalSize.height / naturalSize2.height);
    CGAffineTransform Move = CGAffineTransformMakeTranslation(0,0);
    [FirstlayerInstruction setTransform:CGAffineTransformConcat(Scale,Move) atTime:kCMTimeZero];

    CMTime newStart = CMTimeMakeWithSeconds(2 , secondAsset.duration.timescale);
    
    if ([transition isEqualToString:@"Dissolve"]) {
        [FirstlayerInstruction setOpacityRampFromStartOpacity:1.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(kCMTimeZero, newStart)];
        [FirstlayerInstruction setOpacityRampFromStartOpacity:0.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(newStart, secondAsset.duration)];
    }
    else if ([transition isEqualToString:@"Scale Corner"]){
        CGAffineTransform NewScale = CGAffineTransformMakeScale(0.0, 0.0);
        CGAffineTransform NewMove = CGAffineTransformMakeTranslation(0.0,0.0);

        [FirstlayerInstruction setTransformRampFromStartTransform:CGAffineTransformConcat(Scale,Move) toEndTransform:CGAffineTransformConcat(NewScale,NewMove) timeRange:CMTimeRangeMake(kCMTimeZero, newStart)];
        [FirstlayerInstruction setTransformRampFromStartTransform:CGAffineTransformConcat(NewScale,NewMove) toEndTransform:CGAffineTransformConcat(NewScale,NewMove) timeRange:CMTimeRangeMake(newStart, secondAsset.duration)];
    }
    else if ([transition isEqualToString:@"Rotate"]){
        CGAffineTransform NewScale = CGAffineTransformMakeRotation(M_PI_2);
        [FirstlayerInstruction setTransformRampFromStartTransform:CGAffineTransformConcat(Scale,Move) toEndTransform:CGAffineTransformConcat(NewScale,Move) timeRange:CMTimeRangeMake(kCMTimeZero, newStart)];
        [FirstlayerInstruction setTransformRampFromStartTransform:CGAffineTransformConcat(NewScale,Move) toEndTransform:CGAffineTransformConcat(NewScale,Move) timeRange:CMTimeRangeMake(newStart, secondAsset.duration)];
    }
    else if ([transition isEqualToString:@"Scale"]){
        CGAffineTransform NewScale = CGAffineTransformMakeScale(0.0, 0.0);
        CGAffineTransform NewMove = CGAffineTransformMakeTranslation(naturalSize.width / 2,naturalSize.height / 2);
        
        [FirstlayerInstruction setTransformRampFromStartTransform:CGAffineTransformConcat(Scale,Move) toEndTransform:CGAffineTransformConcat(NewScale,NewMove) timeRange:CMTimeRangeMake(kCMTimeZero, newStart)];
        [FirstlayerInstruction setTransformRampFromStartTransform:CGAffineTransformConcat(NewScale,NewMove) toEndTransform:CGAffineTransformConcat(NewScale,NewMove) timeRange:CMTimeRangeMake(newStart, secondAsset.duration)];
    }
    else if ([transition isEqualToString:@"Glitch"]) {
        //[FirstlayerInstruction setOpacityRampFromStartOpacity:1.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(kCMTimeZero, newStart)];
        [FirstlayerInstruction setOpacityRampFromStartOpacity:0.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(firstAsset.duration, secondAsset.duration)];
    }
    
    
    //Here we are creating AVMutableVideoCompositionLayerInstruction for out second track.see how we make use of Affinetransform to move and scale our second Track.
    AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:secondTrack];
    
    //Now we add our 2 created AVMutableVideoCompositionLayerInstruction objects to our AVMutableVideoCompositionInstruction in form of an array.
    MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,SecondlayerInstruction,nil];;
    
    //Now we create AVMutableVideoComposition object.We can add mutiple AVMutableVideoCompositionInstruction to this object.We have only one AVMutableVideoCompositionInstruction object in our example.You can use multiple AVMutableVideoCompositionInstruction objects to add multiple layers of effects such as fade and transition but make sure that time ranges of the AVMutableVideoCompositionInstruction objects dont overlap.
    AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
    MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
    MainCompositionInst.frameDuration = CMTimeMake(1, 30);
    
    MainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"OverLapedVideo-%d.mov",arc4random() % 100000000]];
    if([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
    {
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    }
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    [exporter setVideoComposition:MainCompositionInst];
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (exporter.status == AVAssetExportSessionStatusCompleted) {
                 responseBlock(exporter.outputURL , YES , nil);
             }
             else{
                 responseBlock(nil , NO , exporter.error);
             }
         });
     }];
}

- (void) overlapVideosWithSpecialEffectWithIndex :(int)index andCutUrl :(NSURL *)cutUrl andTransition :(NSString *)transition sAndResponseBlock:(EditingResponse)responseBlock{
    
    AVURLAsset* firstAsset = [AVURLAsset URLAssetWithURL:cutUrl options:nil];
    AVURLAsset * secondAsset = [AVURLAsset URLAssetWithURL:self.allAssets[index+1] options:nil];
    AVURLAsset * thirdAsset = [AVURLAsset URLAssetWithURL:self.allAssets[index+1] options:nil];

    //Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    
    //Here we are creating the first AVMutableCompositionTrack.See how we are adding a new track to our AVMutableComposition.
    AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    //Now we set the length of the firstTrack equal to the length of the firstAsset and add the firstAsset to out newly created track at kCMTimeZero so video plays from the start of the track.
    
    if ([firstAsset tracksWithMediaType:AVMediaTypeVideo].count) {
        [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    
    //Now we repeat the same process for the 2nd track as we did above for the first track.
    AVMutableCompositionTrack *secondTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    if ([secondAsset tracksWithMediaType:AVMediaTypeVideo].count) {
        [secondTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    

    if([[secondAsset tracksWithMediaType:AVMediaTypeAudio] count]){
        AVMutableCompositionTrack *soundtrackTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [soundtrackTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeAudio] firstObject] atTime:kCMTimeZero error:nil];
    }
    
    AVMutableCompositionTrack *thirdTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    if ([thirdAsset tracksWithMediaType:AVMediaTypeVideo].count) {
        [thirdTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, thirdAsset.duration) ofTrack:[[thirdAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    }
    //See how we are creating AVMutableVideoCompositionInstruction object.This object will contain the array of our AVMutableVideoCompositionLayerInstruction objects.You set the duration of the layer.You should add the lenght equal to the lingth of the longer asset in terms of duration.
    
    AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, secondAsset.duration);
    
    //We will be creating 2 AVMutableVideoCompositionLayerInstruction objects.Each for our 2 AVMutableCompositionTrack.here we are creating AVMutableVideoCompositionLayerInstruction for out first track.see how we make use of Affinetransform to move and scale our First Track.so it is displayed at the bottom of the screen in smaller size.(First track in the one that remains on top).
    //Note: You have to apply transformation to scale and move according to your video size.
    AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
    
    AVAssetTrack *videoAssetTrack = [[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_  = NO;
    CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
    if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ = UIImageOrientationRight;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ =  UIImageOrientationLeft;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
        videoAssetOrientation_ =  UIImageOrientationUp;
    }
    if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
        videoAssetOrientation_ = UIImageOrientationDown;
    }
    CGSize naturalSize;
    if(isVideoAssetPortrait_){
        naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
    } else {
        naturalSize = videoAssetTrack.naturalSize;
    }
    
    float renderWidth, renderHeight;
    renderWidth = naturalSize.width;
    renderHeight = naturalSize.height;
    
    
    AVAssetTrack *videoAssetTrack2 = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_2  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_2  = NO;
    CGAffineTransform videoTransform2 = videoAssetTrack2.preferredTransform;
    if (videoTransform2.a == 0 && videoTransform2.b == 1.0 && videoTransform2.c == -1.0 && videoTransform2.d == 0) {
        videoAssetOrientation_2 = UIImageOrientationRight;
        isVideoAssetPortrait_2 = YES;
    }
    if (videoTransform2.a == 0 && videoTransform2.b == -1.0 && videoTransform2.c == 1.0 && videoTransform2.d == 0) {
        videoAssetOrientation_2 =  UIImageOrientationLeft;
        isVideoAssetPortrait_2 = YES;
    }
    if (videoTransform2.a == 1.0 && videoTransform2.b == 0 && videoTransform2.c == 0 && videoTransform2.d == 1.0) {
        videoAssetOrientation_2 =  UIImageOrientationUp;
    }
    if (videoTransform2.a == -1.0 && videoTransform2.b == 0 && videoTransform2.c == 0 && videoTransform2.d == -1.0) {
        videoAssetOrientation_2 = UIImageOrientationDown;
    }
    CGSize naturalSize2;
    if(isVideoAssetPortrait_2){
        naturalSize2 = CGSizeMake(videoAssetTrack2.naturalSize.height, videoAssetTrack2.naturalSize.width);
    } else {
        naturalSize2 = videoAssetTrack2.naturalSize;
    }
    
    
    
    
    CGAffineTransform Scale = CGAffineTransformMakeScale((naturalSize.width / naturalSize2.width) * 0.8, (naturalSize.height / naturalSize2.height) * 0.8);
    
    
    
    CGAffineTransform Move = CGAffineTransformMakeTranslation((naturalSize.width / 2) - ((naturalSize.width * 0.8)/2) ,(naturalSize.height / 2) - ((naturalSize.height * 0.8)/2));

    [FirstlayerInstruction setTransform:CGAffineTransformConcat(Scale,Move) atTime:kCMTimeZero];
    
    
    [FirstlayerInstruction setOpacityRampFromStartOpacity:0.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(firstAsset.duration, secondAsset.duration)];

    
    
    //Here we are creating AVMutableVideoCompositionLayerInstruction for out second track.see how we make use of Affinetransform to move and scale our second Track.
    AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:secondTrack];
    CGAffineTransform SecondScale = CGAffineTransformMakeScale(1.0f,1.0f);
    CGAffineTransform SecondMove = CGAffineTransformMakeTranslation(0,0);
    [SecondlayerInstruction setTransform:CGAffineTransformConcat(SecondScale,SecondMove) atTime:kCMTimeZero];
    
    
    //Here we are creating AVMutableVideoCompositionLayerInstruction for out second track.see how we make use of Affinetransform to move and scale our second Track.
    AVMutableVideoCompositionLayerInstruction *ThirdlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:thirdTrack];
    CGAffineTransform ThirdScale = CGAffineTransformMakeScale((naturalSize.width / naturalSize.width) * 0.6, (naturalSize.height / naturalSize.height) * 0.6);
    

    
    
    
    CGAffineTransform ThirdMove = CGAffineTransformMakeTranslation((naturalSize.width / 2) - ((naturalSize.width * 0.6)/2) ,(naturalSize.height / 2) - ((naturalSize.height * 0.6)/2));
    
    
    
    
    [ThirdlayerInstruction setTransform:CGAffineTransformConcat(ThirdScale,ThirdMove) atTime:kCMTimeZero];
    
    CMTime newStart = CMTimeMakeWithSeconds(1 , secondAsset.duration.timescale);

    [ThirdlayerInstruction setOpacityRampFromStartOpacity:0.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(kCMTimeZero, newStart)];
    
    CMTime newStart1 = CMTimeMakeWithSeconds(2.5 , secondAsset.duration.timescale);

    
    [ThirdlayerInstruction setOpacityRampFromStartOpacity:1.0 toEndOpacity:1.0 timeRange:CMTimeRangeMake(newStart, newStart)];


    
    [ThirdlayerInstruction setOpacityRampFromStartOpacity:0.0 toEndOpacity:0.0 timeRange:CMTimeRangeMake(newStart1, secondAsset.duration)];

    
    //Now we add our 2 created AVMutableVideoCompositionLayerInstruction objects to our AVMutableVideoCompositionInstruction in form of an array.
   // MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,SecondlayerInstruction,ThirdlayerInstruction,nil];;
    
    
    MainInstruction.layerInstructions = [NSArray arrayWithObjects: ThirdlayerInstruction, FirstlayerInstruction, SecondlayerInstruction, nil];;

    
    //Now we create AVMutableVideoComposition object.We can add mutiple AVMutableVideoCompositionInstruction to this object.We have only one AVMutableVideoCompositionInstruction object in our example.You can use multiple AVMutableVideoCompositionInstruction objects to add multiple layers of effects such as fade and transition but make sure that time ranges of the AVMutableVideoCompositionInstruction objects dont overlap.
    AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
    MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
    MainCompositionInst.frameDuration = CMTimeMake(1, 30);
    
    MainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"OverLapedVideo-%d.mov",arc4random() % 100000000]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
    {
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    }
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    [exporter setVideoComposition:MainCompositionInst];
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             if (exporter.status == AVAssetExportSessionStatusCompleted) {
                 responseBlock(exporter.outputURL , YES , nil);
             }
             else{
                 responseBlock(nil , NO , exporter.error);
             }
         });
     }];
}








-(void)cutVideoOfAsset :(AVAsset *)anAsset andIndex :(NSInteger)index andTransition :(NSString *)transition sAndResponseBlock:(EditingResponse)responseBlock{
    
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetHighestQuality];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs ;
        
        if(self.selectedURL != nil){
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalVideoEdtor-%d.mov", arc4random() % 1000]];
            exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        }
        else{
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalAudio-%d.m4a", arc4random() % 1000]];
            exportSession.outputFileType = AVFileTypeMPEG4;
            
        }
        NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
        
        exportSession.outputURL = outputurl;
        CMTime start = CMTimeMakeWithSeconds((anAsset.duration.value/anAsset.duration.timescale ) - (2) , anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(2, anAsset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        exportSession.timeRange = range;
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                    
                    if ([transition isEqualToString:@"Squared"]) {
                        [self overlapVideosWithSpecialEffectWithIndex:index andCutUrl:exportSession.outputURL andTransition:transition sAndResponseBlock:responseBlock];
                    }
                    else{
                        [self overlapVideosWithIndex:index andCutUrl:exportSession.outputURL andTransition:transition sAndResponseBlock:responseBlock];
                    }
                    /*[self.allAssets replaceObjectAtIndex:index withObject:exportSession.outputURL];
                    [self reloadVideoPlayer];
                    [self setImages];
                    [SVProgressHUD dismiss];*/
                    
                } else if (exportSession.status == AVAssetExportSessionStatusFailed)
                {
                    NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Error in Trimming video" andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                    
                } else if (exportSession.status == AVAssetExportSessionStatusCancelled)
                {
                    NSLog(@"Export canceled");
                    
                }
            });
        }];
    }
}




-(void)joinVideoWithGlitchWithIndex :(int)index sAndResponseBlock:(EditingResponse)responseBlock {
    
    
    //AVURLAsset *video01 = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"1" ofType:@"mp4"]] options:nil];
    AVURLAsset * video01 = [AVURLAsset URLAssetWithURL:self.allAssets[index] options:nil];

    AVURLAsset * video02 = [AVURLAsset URLAssetWithURL:self.allAssets[index+1] options:nil];
    
    
    
    
    CGFloat totalDuration;
    totalDuration = 0;     //initialization, keep it 0
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *composedTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    
    for (int i = 0; i < 2 ; i++)       //arrVideoPath contains all video paths
        
    {
        if (i == 0)
        {
            [composedTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, video01.duration) ofTrack:[[video01 tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
        }
        else
        {

            
            float duration1 = CMTimeGetSeconds([video01 duration]);
            totalDuration = totalDuration + duration1;
            CMTime time1 = CMTimeMakeWithSeconds(totalDuration, 1);
            
            [composedTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, video02.duration) ofTrack:[[video02 tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:time1 error:nil];
        }
    }
        NSString* documentsDirectory= [self applicationDocumentsDirectory];
        NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"glitched_video-%d.mov", arc4random() % 1000000]];
        
        

        
        NSURL* urlVideoMain = [[NSURL alloc] initFileURLWithPath: myDocumentPath];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:myDocumentPath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:myDocumentPath error:nil];
        }        //removes previous video at same path, essential
        
        
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetHighestQuality];
        
        exporter.outputURL = urlVideoMain;
        
        exporter.outputFileType = @"com.apple.quicktime-movie";
        
        exporter.shouldOptimizeForNetworkUse = YES;
        
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            dispatch_async (dispatch_get_main_queue(), ^{
                switch ([exporter status]) {
                        
                    case AVAssetExportSessionStatusFailed:
                        responseBlock(nil , NO , exporter.error);
                        break;
                        
                    case AVAssetExportSessionStatusCancelled:
                        
                        break;
                        
                    case AVAssetExportSessionStatusCompleted:
                        responseBlock(exporter.outputURL , YES , nil);
                        break;
                        
                    default:
                        
                        break;
                        
                }
            });

            
            
        }];
}





-(void)cutOffLastPartVideoOfAsset :(AVAsset *)anAsset andIndex :(NSInteger)index{
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetHighestQuality];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs ;
        if(self.selectedURL != nil){
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalVideoEdtor-%d.mov", arc4random() % 1000]];
            exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        }
        else{
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
            [NSString stringWithFormat:@"FinalAudio-%d.m4a", arc4random() % 1000]];
            exportSession.outputFileType = AVFileTypeMPEG4;
        }
        NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
        exportSession.outputURL = outputurl;
        CMTime start = CMTimeMakeWithSeconds(0 , anAsset.duration.timescale);
        //CMTime start = CMTimeMakeWithSeconds((anAsset.duration.value/anAsset.duration.timescale ) - (2) , anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds((anAsset.duration.value/anAsset.duration.timescale ) - (2), anAsset.duration.timescale);

        CMTimeRange range = CMTimeRangeMake(start, duration);
        exportSession.timeRange = range;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                    
                    [self.allAssets replaceObjectAtIndex:index withObject:exportSession.outputURL];
                    [self reloadVideoPlayer];
                    [self setImages];
                    
                } else if (exportSession.status == AVAssetExportSessionStatusFailed)
                {
                    NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Error in Trimming video" andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                    
                } else if (exportSession.status == AVAssetExportSessionStatusCancelled)
                {
                    NSLog(@"Export canceled");
                    
                }
            });
        }];
    }
}

-(void)applyTransitionsToAll {
    for (NSString *string in transitionsArray) {
        if (!string.length) {
            break;
        }
        NSLog(@"Transition is %@", string);
    }
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}
- (IBAction)unwindToVideosJoinView:(UIStoryboardSegue*)sender
{

}


@end
