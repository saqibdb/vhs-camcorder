//
//  MyVideoCollectionViewCell.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 3/4/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MyVideoCollectionViewCell : UICollectionViewCell <AVPlayerViewControllerDelegate, UIGestureRecognizerDelegate> {
    AVPlayerViewController *playerViewController;
    UIButton               *vDeleteBTN;
    UIButton               *vSaveBTN;
    UILabel                *deleteStatus;
    NSURL                  *videoUrl;
}

- (void)updateContentPlayerWithAssetUrl:(NSURL*)videUrl;

@end
