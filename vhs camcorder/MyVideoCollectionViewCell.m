//
//  MyVideoCollectionViewCell.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 3/4/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import "MyVideoCollectionViewCell.h"
#import "MBProgressHUD.h"
#import <Photos/Photos.h>
#import "Util.h"

@implementation MyVideoCollectionViewCell

- (void)updateContentPlayerWithAssetUrl:(NSURL*)videUrl {
    [MBProgressHUD showHUDAddedTo:self.contentView animated:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!playerViewController) {
            playerViewController = [[AVPlayerViewController alloc] init];
            playerViewController.delegate = self;
            playerViewController.videoGravity = kCAGravityResizeAspectFill;
            playerViewController.showsPlaybackControls = NO;
            playerViewController.view.frame = CGRectMake(0,0,self.contentView.frame.size.width,self.contentView.frame.size.height);
            [self.contentView addSubview:playerViewController.view];
            
            videoUrl = videUrl;
            
            // First create an AVPlayerItem
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:videUrl];
            playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
            playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
            playerViewController.showsPlaybackControls = YES;
            
            UITapGestureRecognizer *singleTapRecognizer1 = [[UITapGestureRecognizer alloc]
                                                            initWithTarget:self
                                                            action:@selector(singleTapped:)];
            singleTapRecognizer1.numberOfTapsRequired = 1;
            singleTapRecognizer1.delegate = self;
            [playerViewController.view addGestureRecognizer:singleTapRecognizer1];
            
            CGFloat height = 50.0f;
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) height = 70.0f;
            
            deleteStatus = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 0, self.contentView.frame.size.width, height)];
            [deleteStatus setHidden:YES];
            [deleteStatus setText:@"DELETED"];
            [deleteStatus setTextColor:[UIColor whiteColor]];
            [deleteStatus setBackgroundColor:[UIColor redColor]];
            [deleteStatus setTextAlignment:NSTextAlignmentCenter];
            [deleteStatus setFont:[UIFont fontWithName:@"VCR_OSD_MONO_1.001.ttf" size:height]];
            [deleteStatus setCenter:CGPointMake(self.contentView.frame.size.width/2.0f,
                                                self.contentView.frame.size.height/2.0f)];
            [self.contentView addSubview:deleteStatus];
            
            UIImage *vDeleteImage = [UIImage imageNamed:@"delete"];
            CGPoint  center       = CGPointMake(self.contentView.frame.size.width - vDeleteImage.size.width*0.75f,
                                                vDeleteImage.size.height);
            
            vDeleteBTN = [UIButton buttonWithType:UIButtonTypeCustom];
            [vDeleteBTN setImage:vDeleteImage forState:UIControlStateNormal];
            [vDeleteBTN setFrame:CGRectMake(0, 0, vDeleteImage.size.width, vDeleteImage.size.height)];
            [vDeleteBTN setCenter:center];
            [vDeleteBTN addTarget:self action:@selector(removeVideo:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:vDeleteBTN];
            
            UIImage *vSaveImage = [UIImage imageNamed:@"myvid_save"];
            vSaveBTN = [UIButton buttonWithType:UIButtonTypeCustom];
            [vSaveBTN setImage:vSaveImage forState:UIControlStateNormal];
            [vSaveBTN setFrame:CGRectMake(0, 0, vSaveImage.size.width, vSaveImage.size.height)];
            [vSaveBTN setCenter:CGPointMake(center.x, center.y + 1.25f*vSaveImage.size.height)];
            [vSaveBTN addTarget:self action:@selector(vSaveBTN:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:vSaveBTN];
            
        } else {
            // Replace current AVPlayerItem
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:videUrl];
            [playerViewController.player replaceCurrentItemWithPlayerItem:playerItem];
        }
        
        [MBProgressHUD hideHUDForView:self.contentView animated:YES];
    });
}

- (void)removeVideo:(id)sender {
    if ([[NSFileManager defaultManager] fileExistsAtPath:videoUrl.path]) {
        [[NSFileManager defaultManager] removeItemAtPath:videoUrl.path error:nil];
        
        playerViewController.showsPlaybackControls = NO;
        playerViewController.player = nil;
        
        [vSaveBTN   setHidden:YES];
        [vDeleteBTN   setHidden:YES];
        [deleteStatus setHidden:NO];
    }
}

- (void)vSaveBTN:(id)sender {
    __block PHObjectPlaceholder *placeholder;
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:videoUrl];
        placeholder = [createAssetRequest placeholderForCreatedAsset];
        
    } completionHandler:^(BOOL success, NSError *error) {
        NSString *message = @"Saved To Camera Roll";
        if (success) {}
        else {
            message = @"Failed To Save";
            NSLog(@"%@", error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [Util showAlertOnView:[((UIButton *)sender) superview] WithMessage:message];
        });
    }];
    
}

- (void)singleTapped:(UITapGestureRecognizer*)recognizer {
    if ([deleteStatus isHidden]) {
        [vDeleteBTN setHidden:![vDeleteBTN isHidden]];
        [vSaveBTN setHidden:![vSaveBTN isHidden]];
    }
}

#pragma mark - gesture delegate
// this allows you to dispatch touches
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}
// this enables you to handle multiple recognizers on single view
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end
