//
//  MyVideosViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 3/4/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyVideosViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
