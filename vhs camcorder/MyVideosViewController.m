//
//  MyVideosViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 3/4/17.
//  Copyright © 2017 Shadi Osta. All rights reserved.
//

#import "MyVideosViewController.h"
#import "MyVideoCollectionViewCell.h"
#import "Util.h"
#import "MBProgressHUD.h"

@interface MyVideosViewController () {
    NSArray *myVideosArray;
    NSString *myVidDirPath;
    
    __weak IBOutlet UICollectionView *vCollectionView;
}

@end

@implementation MyVideosViewController

static NSString * const reuseIdentifier = @"ContentCell";

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Register cell classes
    //[collectionView registerClass:[MyVideoCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    NSURL *outurl = [Util getOutputVideoURL];
    myVidDirPath  = [outurl.path stringByDeletingLastPathComponent];
    myVideosArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:myVidDirPath error:nil];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return myVideosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell
    MyVideoCollectionViewCell *cell = [collectionView
                                       dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                       forIndexPath:indexPath];

    NSString *filename = [myVideosArray objectAtIndex:indexPath.row];
    NSString *filePath = [myVidDirPath stringByAppendingString:[NSString stringWithFormat:@"/%@", filename]];
    [cell updateContentPlayerWithAssetUrl:[NSURL fileURLWithPath:filePath]];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

// Uncomment this method to specify if the specified item should be highlighted during tracking
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        return YES;
//    }
//
//    return NO;
//}

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */

#pragma mark -
#pragma mark UICollectionViewFlowLayoutDelegate


 /*- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
 {
     if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) return CGSizeMake(0, 160);
     return CGSizeMake(0, 80);
 }*/

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //long row = [indexPath row];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) return CGSizeMake(256, 256);
    return CGSizeMake(128, 128);
}

@end
