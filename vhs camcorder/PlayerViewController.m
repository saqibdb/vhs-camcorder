//
//  PlayerViewController.m
//  VidEditor
//
//  Created by iBuildx_Mac_Mini on 10/19/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import "PlayerViewController.h"
#import "ALiAsset.h"
#import "GUIPlayerView.h"
#import "EditViewController.h"
#import "AMSmoothAlertView.h"
#import "SASliderRight.h"
#import "SoundManager.h"
#import "FVSoundWaveView.h"
#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
@interface PlayerViewController (){
    
    NSTimer            *previewTimer;
    CGFloat             timeSpent;
    SAVideoRangeSlider *vidSlider ;
    SASliderRight *rightView;
    FVSoundWaveView* _soundWaveView;
    float audioLeftPosition;
    float audioRighyPosition;
}

@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view layoutIfNeeded] ;
    self.trimBtn.layer.cornerRadius = 3.0 ;
    [self.trimBtn setClipsToBounds:YES] ;
    NSString *tempDir = NSTemporaryDirectory();
    self.tmpVideoPath = [tempDir stringByAppendingPathComponent:@"tmpMov.mov"];
    if(self.selectedURL != nil){
        self.selectedAudioFileUrl = nil;
        [self.audioTrimView setHidden:YES];
        [self.trimView setHidden:NO];
        
        if (!_playerViewController) {
            _playerViewController = [[AVPlayerViewController alloc] init];
            _playerViewController.delegate = self;
            _playerViewController.view.frame = self.playerView.bounds;
            _playerViewController.showsPlaybackControls = NO;
            // First create an AVPlayerItem
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:self.selectedURL];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
            _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
            _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
            [self.playerView addSubview:_playerViewController.view];
           // UIImage *normImage = [UIImage imageNamed:@"preview"];
           
        }
        
        
        vidSlider = [[SAVideoRangeSlider alloc] initWithFrame:CGRectMake(0, 0, self.trimView.frame.size.width, 50) videoUrl:self.selectedURL  ];
        
        vidSlider.bubleText.font = [UIFont systemFontOfSize:12];
        [vidSlider setPopoverBubbleSize:120 height:60];
        // self.mySAVideoRangeSlider.maxGap=30;
        
        // Yellow
        vidSlider.topBorder.backgroundColor = [UIColor colorWithRed: 0.996 green: 0.951 blue: 0.502 alpha: 0];
        vidSlider.bottomBorder.backgroundColor = [UIColor colorWithRed: 0.992 green: 0.902 blue: 0.004 alpha: 0];
        
        
        vidSlider.delegate = self;
        
        
        [self.trimView addSubview:vidSlider];
    }
    
    else{
        
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:self.selectedAudioFileUrl error:nil];
        _player.delegate=self;
        
        [self.musicImage setHidden:NO];
        AVAsset *asset = [AVAsset assetWithURL:self.selectedAudioFileUrl];
        
        CMTime videoDuration = asset.duration;
        float videoDurationSeconds = CMTimeGetSeconds(videoDuration);
        
        
        RETrimControl *trimControl = [[RETrimControl alloc] initWithFrame:self.audioTrimView.bounds];
        trimControl.length = videoDurationSeconds; // 200 seconds
        trimControl.delegate = self;
        
        
        _soundWaveView = [[FVSoundWaveView alloc] initWithFrame:self.audioTrimView.bounds];
        [self.audioTrimView addSubview:_soundWaveView];
        [self.audioTrimView addSubview:trimControl];
        
        
        _soundWaveView.soundURL =  self.selectedAudioFileUrl;
        
        [self.audioTrimView setHidden:NO];
        [self.trimView setHidden:YES];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    _soundWaveView.soundURL = self.selectedAudioFileUrl;
    
}
- (void)viewDidLayoutSubviews
{
    [self.audioTrimScrollView setContentSize:CGSizeMake(self.audioTrimView.frame.size.width, self.audioTrimView.frame.size.height+10)];
    
}

- (void)trimControl:(RETrimControl *)trimControl didChangeLeftValue:(CGFloat)leftValue rightValue:(CGFloat)rightValue
{
    NSLog(@"Left = %f, right = %f", leftValue, rightValue);
    audioLeftPosition = leftValue;
    audioRighyPosition= rightValue;
    [_player setVolume:self.voleumSlider.value];
    _player.currentTime = audioLeftPosition;
    [_player play];
    self.playBtn.tag = 00;
    [self.playBtn setImage:[UIImage imageNamed:@"pauseButton"] forState:UIControlStateNormal];
}
#pragma mark - Process

- (void)playVideo:(id)sender
{
   
    [[_playerViewController player] play];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - SAVideoRangeSliderDelegate

- (void)videoRange:(SAVideoRangeSlider *)videoRange didChangeLeftPosition:(CGFloat)leftPosition rightPosition:(CGFloat)rightPosition
{
    self.startTime =CMTimeMake(leftPosition, 1)  ;
    self.stopTime = CMTimeMake(rightPosition, 1) ;
    
    self.startTimeFL = leftPosition ;
    self.stopTimeFL = rightPosition ;
    
    
    CMTime targetTime = CMTimeMakeWithSeconds(leftPosition, NSEC_PER_SEC);
    [[[_playerViewController player] currentItem] seekToTime:targetTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PlayerToEditor"]) {
        EditViewController *destinationViewControler = segue.destinationViewController ;
        destinationViewControler.selectedURL = self.selectedURL ;
    }
}
- (BOOL)getVideoOrientationFromAsset:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty){
        //return UIImageOrientationLeft; //return UIInterfaceOrientationLandscapeLeft;
        
        return YES;
    }
    else if (txf.tx == 0 && txf.ty == 0){
        //return UIImageOrientationRight; //return UIInterfaceOrientationLandscapeRight;
        return YES;
    }
    else if (txf.tx == 0 && txf.ty == size.width){
        // return UIImageOrientationDown; //return UIInterfaceOrientationPortraitUpsideDown;
        return NO;
    }
    else{
        //return UIImageOrientationUp;  //return UIInterfaceOrientationPortrait;
        return NO;
    }
}

-(void)cutVideo
{
    [_player stop];
    AVAsset *anAsset;
    if(self.selectedURL != nil){
        self.startTimeFL = vidSlider.leftPosition;
        self.stopTimeFL  = vidSlider.rightPosition;
        anAsset = [[AVURLAsset alloc] initWithURL:self.selectedURL options:nil];
    }
    else{
        self.startTimeFL = audioLeftPosition;
        self.stopTimeFL  = audioRighyPosition;
        anAsset = [[AVURLAsset alloc] initWithURL:self.selectedAudioFileUrl options:nil];
        
    }
    
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetHighestQuality];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs ;
        
        if(self.selectedURL != nil){
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalVideoEdtor-%d.mov", arc4random() % 1000]];
            exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        }
        else{
            
            myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                           [NSString stringWithFormat:@"FinalAudio-%d.m4a", arc4random() % 1000]];
            exportSession.outputFileType = AVFileTypeMPEG4;
            
        }
        NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
        
        exportSession.outputURL = outputurl;
        
        
        CMTime start = CMTimeMakeWithSeconds(self.startTimeFL, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(self.stopTimeFL-self.startTimeFL, anAsset.duration.timescale);
        
        
        
        
        
        
        CMTimeRange range = CMTimeRangeMake(start, duration);
        exportSession.timeRange = range;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                    
                    if(self.selectedURL != nil){
                        self.selectedURL=outputurl;
                        [self performSegueWithIdentifier:@"PlayerToEditor" sender:nil];
                    }
                    else{
                        self.isWithAudio =YES;
                        self.selectedURL=outputurl;
                        [self performSegueWithIdentifier:@"BackToGallery" sender:nil];
                        
                    }
                } else if (exportSession.status == AVAssetExportSessionStatusFailed)
                {
                    NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Error in Trimming video" andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                    
                } else if (exportSession.status == AVAssetExportSessionStatusCancelled)
                {
                    NSLog(@"Export canceled");
                    
                }
            });
        }];
    }
    
    
    
    
    
}

-(NSString*) applicationDocumentsDirectory
{
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSLog(@"%@",basePath);
    return basePath;
    
}
- (IBAction)middleBarCancelButtonAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"BackToGallery" sender:nil];
}

- (IBAction)middleBarPreviewAction:(UIButton *)sender {
    
       if(sender.tag == 11){
        sender.tag = 00;
        [self.playBtn setImage:[UIImage imageNamed:@"pauseButton"] forState:UIControlStateNormal];
        
        [_player setVolume:self.voleumSlider.value];
        _player.currentTime = audioLeftPosition;
        [self.player play];
        [[_playerViewController player] play];
        
    }
    else{
        sender.tag = 11 ;
        
        [self.playBtn setImage:[UIImage imageNamed:@"previewTriangle"] forState:UIControlStateNormal];
        [self.player pause];
        [[_playerViewController player] pause];
        
        
       
        
    }

}

- (IBAction)voleumSliderAction:(UISlider *)sender {
    
   
    [_player setVolume:self.voleumSlider.value];
    
}

- (IBAction)backAction:(id)sender {
    
    self.isWithAudio=NO;
    [self performSegueWithIdentifier:@"BackToGallery" sender:nil];
}

- (IBAction)saveAction:(id)sender {
    
    [self cutVideo];
    
}

#pragma mark - Video Player Delegate

-(void)itemDidFinishPlaying:(NSNotification *) notification {
      [self.playBtn setImage:[UIImage imageNamed:@"previewTriangle"] forState:UIControlStateNormal];
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}
- (IBAction)unwindToPlayer:(UIStoryboardSegue*)sender
{
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

@end
