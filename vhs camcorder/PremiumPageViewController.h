//
//  PremiumPageViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/11/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PremiumPageViewController : UIViewController

-(IBAction) goBack;
-(IBAction) goPremium;
-(IBAction) restorePurchases;

@end
