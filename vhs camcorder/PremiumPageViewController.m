//
//  PremiumPageViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/11/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "PremiumPageViewController.h"
#import "IAPHelper.h"
#import "MBProgressHUD.h"
#import "Util.h"

@interface PremiumPageViewController ()

@end

@implementation PremiumPageViewController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark - Navigation

- (IBAction)goBack {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)goPremium {
    if (![Util isInternetReachable]) {
        [self showInternetAlert];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(purchaseTransaction:)
                                                 name:IAPHelperProductPurchasingNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(purchaseTransaction:)
                                                 name:IAPHelperProductPurchasedNotification
                                               object:nil];

    [[IAPHelper shared] buyProduct:[[[IAPHelper shared] productsList] objectAtIndex:0]];
}

- (IBAction)restorePurchases {
    if (![Util isInternetReachable]) {
        [self showInternetAlert];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Restoring Transactions";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restoredTransactions:)
                                                 name:IAPHelperProductPurchasedNotification
                                               object:nil];

    [[IAPHelper shared] restoreCompletedTransactions];
}

- (void)purchaseTransaction:(NSNotification*)notification {
    if ([notification.name isEqualToString:IAPHelperProductPurchasingNotification]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Purchasing";
        
    } else if ([notification.name isEqualToString:IAPHelperProductPurchasedNotification]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasingNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasedNotification object:nil];
        
        NSDictionary* userInfo = notification.userInfo;
        BOOL success = [userInfo[@"status"] boolValue];
        if (!success) {
            NSString *title   = @"Transaction Failure";
            NSString *message = userInfo[@"message"];
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:title
                                                  message:message
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                              handler:^(UIAlertAction *action) {
                                                                  [self goBack];
                                                              }]];
            [self presentViewController:alertController animated:YES completion:nil];
            
        } else [self goBack];
    } else [self goBack];
}

- (void)restoredTransactions:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelperProductPurchasedNotification object:nil];
    
    if ([notification.name isEqualToString:IAPHelperProductPurchasedNotification]) {
        NSString *title = @"Restore";
        NSString *message = @"All Transactions have been restored!";
        NSDictionary* userInfo = notification.userInfo;
        BOOL success = [userInfo[@"status"] boolValue];
        if (!success) {
            title   = @"Restore Failure";
            message = userInfo[@"message"];
        }
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                              [self goBack];
                                                          }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else [self goBack];
}

- (void)showInternetAlert {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Network Unavailable!"
                                          message:@"Please check your internet connection."
                                          preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {

                                                      }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
