//
//  SCWatermarkOverlayView.h
//  SCRecorderExamples
//
//  Created by Simon CORSIN on 16/06/15.
//
//

#import <UIKit/UIKit.h>
#import "SCVideoConfiguration.h"
#import "Util.h"

@interface SCWatermarkOverlayView : UIView<SCVideoOverlay>

@property (strong, nonatomic) NSDate   *date;
@property (strong, nonatomic) UILabel  *timeLabel;
@property (strong, nonatomic) UILabel  *retroLabel;
@property (strong, nonatomic) UILabel  *dateLabel;
@property (strong, nonatomic) UILabel  *watermarkLabel;
@property (strong, nonatomic) UILabel  *playLabel;

@property (strong, nonatomic) NSMutableArray *vRetroTitleVisibleRanges;

- (void)setFontRatio:(CGFloat)fRatio;
+ (void)setImportedPhoto: (BOOL)imported;
+ (void)setImportedPhotoFontSize: (float)size;
+ (void)setCameraMode:(CameraMode)cammode;

@end
