//
//  SCWatermarkOverlayView.m
//  SCRecorderExamples
//
//  Created by Simon CORSIN on 16/06/15.
//
//

#import "SCWatermarkOverlayView.h"
#import "SCRecorder.h"
#import "Util.h"

@interface SCWatermarkOverlayView() {
    UIImageView *_playTriangle;
    CGFloat      fontRatio;
}
@end

static CameraMode _cammode;
static BOOL isSelfieORimportedPhoto = NO;
static float importedPhotofontSize = 16.0;
static CGFloat inset = 8;

@implementation SCWatermarkOverlayView

+ (void)setImportedPhotoFontSize: (float)size {
    importedPhotofontSize = size;
}

+ (void)setImportedPhoto: (BOOL)imported {
    isSelfieORimportedPhoto = imported;
}

+ (void)setCameraMode:(CameraMode)cammode {
    _cammode = cammode;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {        
        float vIphoneOffset = 1.0;
        /*if ( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ){
            vIphoneOffset = 0.75; // Its an iPhone
        }*/
        
        float fontSize = 16.0*vIphoneOffset;
        if (isSelfieORimportedPhoto) {
            fontSize = importedPhotofontSize;
        } else fontSize = 20.0f;

        _watermarkLabel = [UILabel new];
        _watermarkLabel.textColor = [UIColor whiteColor];
        _watermarkLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:fontSize];
        _watermarkLabel.text = @"RAD VHS";
        
        _dateLabel = [UILabel new];
        _dateLabel.textColor = [UIColor whiteColor];
        _dateLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:fontSize];
        _dateLabel.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_date_created"];

        self.timeLabel = [UILabel new];
        self.timeLabel.textColor = [UIColor whiteColor];
        self.timeLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:fontSize];

        _playLabel = [UILabel new];
        _playLabel.textColor = [UIColor whiteColor];
        _playLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:fontSize];
        _playLabel.text = @"PLAY";
        
        NSString *color = [[NSUserDefaults standardUserDefaults] objectForKey:@"text_color"];
        NSString *pTriangleImage = @"play_triangle3x.png";
        
        if ([color isEqualToString:@"white"]) {
            pTriangleImage = @"play_triangle3x.png";
        } else if ([color isEqualToString:@"red"]) {
            pTriangleImage = @"play_triangle3x_r.png";
        } else if ([color isEqualToString:@"yellow"]) {
            pTriangleImage = @"play_triangle3x_y.png";
        }
        
        _playTriangle = [UIImageView new];
        _playTriangle.image = [UIImage imageNamed:pTriangleImage];
        
        [self addSubview:_watermarkLabel];
        [self addSubview:_dateLabel];
        [self addSubview:self.timeLabel];
        [self addSubview:_playLabel];
        [self addSubview:_playTriangle];
        
        _retroLabel = [UILabel new];
        _retroLabel.hidden = YES;
        _retroLabel.numberOfLines = 0;
        _retroLabel.textAlignment = NSTextAlignmentCenter;
        _retroLabel.textColor = [UIColor whiteColor];
        [self addSubview:_retroLabel];
        
        if (![SCRecorder sharedRecorder].isPhotoImported && isSelfieORimportedPhoto)
            _retroLabel.hidden = NO;
        
        [self updateWithVideoTime:0.0f];
        
        if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_watermark"] ) {
            [_watermarkLabel setHidden:YES];
        }
        
        if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_date"] ) {
            [_dateLabel setHidden:YES];
            [_timeLabel setHidden:YES];
            [_playLabel setHidden:YES];
            [_playTriangle setHidden:YES];
        }
        
        if ([SCRecorder sharedRecorder].isVideoImported || [SCRecorder sharedRecorder].isPhotoImported) {
            [_watermarkLabel setHidden:YES];
        }
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    BOOL isPortraitMode  = [SCRecorder sharedRecorder].videoConfiguration.sizeAsPortrait;
    BOOL isSquareMode    = [SCRecorder sharedRecorder].videoConfiguration.sizeAsSquare;
    inset = 8; if (isSelfieORimportedPhoto) inset = 8*(importedPhotofontSize/16.0);
    
    CGSize size = self.bounds.size;
    
    if (![SCRecorder sharedRecorder].isVideoImported &&
        ![SCRecorder sharedRecorder].isPhotoImported)
    {
        CGFloat rfontSize = (size.width*0.75f*fontRatio);//0.0694444f
        _retroLabel.font  = [UIFont fontWithName:@"VCROSDMONO" size:rfontSize];
        _dateLabel.font  = [UIFont fontWithName:@"VCROSDMONO" size:rfontSize];
        _timeLabel.font  = [UIFont fontWithName:@"VCROSDMONO" size:rfontSize];
        _watermarkLabel.font  = [UIFont fontWithName:@"VCROSDMONO" size:rfontSize];
        _playLabel.font  = [UIFont fontWithName:@"VCROSDMONO" size:rfontSize];
    }
    
    [_dateLabel sizeToFit];
    CGRect dateLabelFrame = _dateLabel.frame;
    
    dateLabelFrame.origin.y = size.height - 2*dateLabelFrame.size.height - inset;
    dateLabelFrame.origin.x = dateLabelFrame.size.width/4.0 - 2*inset;//inset;
    
    if ([SCRecorder sharedRecorder].isVideoImported || _cammode == CameraModePhoto) {
        dateLabelFrame.origin.x = dateLabelFrame.size.width/4.0 - 2*inset;
    }
    if ((isPortraitMode && _cammode == CameraModeVideo)
        || (!isPortraitMode && !isSquareMode && _cammode == CameraModePhoto)
        || (isSquareMode))
    {
        dateLabelFrame.origin.x = dateLabelFrame.size.width/4.0 - inset;
    }
    
    _dateLabel.frame = dateLabelFrame;

    [self.timeLabel sizeToFit];
    CGRect timeLabelFrame = self.timeLabel.frame;
    timeLabelFrame.origin.y = dateLabelFrame.origin.y - dateLabelFrame.size.height - inset;
    timeLabelFrame.origin.x = dateLabelFrame.origin.x;
    self.timeLabel.frame = timeLabelFrame;
    
    [_watermarkLabel sizeToFit];
    CGRect watermarkFrame = _watermarkLabel.frame;
    watermarkFrame.origin.x = size.width - 1.5*watermarkFrame.size.width + 3.0*inset;//dateLabelFrame.origin.x;
    
    if ((isPortraitMode && _cammode == CameraModeVideo)
        || (!isPortraitMode && !isSquareMode && _cammode == CameraModePhoto)
        || (isSquareMode))
    {
        watermarkFrame.origin.x = size.width - 1.5*watermarkFrame.size.width + inset;
    }
    
    watermarkFrame.origin.y = watermarkFrame.size.height + inset;//dateLabelFrame.origin.y - dateLabelFrame.size.height - inset;
    _watermarkLabel.frame = watermarkFrame;
    
    [_playLabel sizeToFit];
    CGRect playLabelFrame = _playLabel.frame;
    playLabelFrame.origin.y = watermarkFrame.origin.y;
    //dateLabelFrame.origin.x = inset;
    playLabelFrame.origin.x = dateLabelFrame.origin.x;
    _playLabel.frame = playLabelFrame;

    [_playTriangle sizeToFit];
    CGRect playTriangleLabelFrame = _playTriangle.frame;
    playTriangleLabelFrame.origin.y = playLabelFrame.origin.y;
    //dateLabelFrame.origin.x = inset;
    
    float vIphoneOffset = 0.80;
    if (isSelfieORimportedPhoto) {
        playTriangleLabelFrame.size = CGSizeMake((importedPhotofontSize/16.0)*0.125*vIphoneOffset*playTriangleLabelFrame.size.width,
                                                 (importedPhotofontSize/16.0)*0.125*vIphoneOffset*playTriangleLabelFrame.size.height);
    } else {
        if (![SCRecorder sharedRecorder].isVideoImported) {
            playTriangleLabelFrame.size = CGSizeMake(0.2*0.6*playTriangleLabelFrame.size.width,
                                                     0.2*0.6*playTriangleLabelFrame.size.height);
            if (isPortraitMode)
                playTriangleLabelFrame.size = CGSizeMake(0.2*0.4*playTriangleLabelFrame.size.width,
                                                         0.2*0.4*playTriangleLabelFrame.size.height);
        } else
            playTriangleLabelFrame.size = CGSizeMake(0.2*vIphoneOffset*playTriangleLabelFrame.size.width,
                                                     0.2*vIphoneOffset*playTriangleLabelFrame.size.height);
    }
    
    playTriangleLabelFrame.origin.x =  playLabelFrame.origin.x + playLabelFrame.size.width + playTriangleLabelFrame.size.width/2;
    _playTriangle.frame             = playTriangleLabelFrame;
    
    [_playTriangle setCenter:CGPointMake(_playTriangle.center.x, _playLabel.center.y)];
    
    [_retroLabel sizeToFit];
    CGRect retroLabelFrame = _retroLabel.frame;
    retroLabelFrame.size   = CGSizeMake(size.width*0.75, size.height*0.5f);
    _retroLabel.frame      = retroLabelFrame;
    [_retroLabel setCenter:CGPointMake(size.width/2.0, size.height/2.0)];
}

- (void)setFontRatio:(CGFloat)fRatio {
    fontRatio = fRatio;
}

- (void)updateWithVideoTime:(NSTimeInterval)time {
    for (NSValue *value in _vRetroTitleVisibleRanges) {        
        NSTimeInterval minTime = [value CGPointValue].x;
        NSTimeInterval maxTime = [value CGPointValue].y;
        if (time >= minTime && time <= maxTime) {
            _retroLabel.hidden = NO;
            break;
        } else _retroLabel.hidden = YES;
    }
}

@end
