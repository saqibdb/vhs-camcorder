//
//  SettingsViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/9/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "SettingsViewController.h"
#import "Util.h"

@implementation SettingsViewController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* fakeDate = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_date_created"];
    
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_watermark"] ) {
        //[self.wNoBox setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [self.wYesBox setImage:[UIImage imageNamed:@"select_box_no1.png"] forState:UIControlStateNormal];
    } else {
        [self.wYesBox setImage:[UIImage imageNamed:@"select_box_yes1.png"] forState:UIControlStateNormal];
        //[self.wNoBox setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
    }
    
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_date"]) {
        //[self.dNoBox setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [self.dYesBox setImage:[UIImage imageNamed:@"select_box_no1.png"] forState:UIControlStateNormal];
    } else {
        [self.dYesBox setImage:[UIImage imageNamed:@"select_box_yes1.png"] forState:UIControlStateNormal];
        //[self.dNoBox setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
    }
    
    UIButton *whiteMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_WHITE];
    UIButton *redMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_RED];
    UIButton *yellowMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_YELLOW];
    NSString *color = [[NSUserDefaults standardUserDefaults] objectForKey:@"text_color"];
    if ([color isEqualToString:@"white"]) {
        [whiteMark setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [redMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        [yellowMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        
    } else if ([color isEqualToString:@"red"]) {
        [redMark setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [whiteMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        [yellowMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        
    } else if ([color isEqualToString:@"yellow"]) {
        [yellowMark setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
        [redMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
        [whiteMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
    }
    
    if ([Util isPremiumUser]) {
        [self.removeAdsBTN setEnabled:NO];
        [self.removeAdsBTN setTitleColor:[UIColor brownColor] forState:UIControlStateDisabled];
    }
        
    [self.dateBTN setTitle:[NSString stringWithFormat:@"%@", fakeDate]
                  forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString* fakeTime = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_time_created"];
    if ([fakeTime isEqualToString:@""] || !fakeTime) fakeTime = [Util getCurrentTime]; // Default text
    NSString* fakeDate = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_date_created"];
    NSString *vCurrentDate = [NSString stringWithFormat:@"%@ %@", fakeDate, fakeTime];
    
    [self.dateBTN setTitle:vCurrentDate forState:UIControlStateNormal];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {}

- (IBAction)setRetroTitles:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add your retro title:"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Ok", nil];
    [alert setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [alert textFieldAtIndex:0].placeholder = @"ADD RETRO TITLES!";
    [alert textFieldAtIndex:1].placeholder = @"(PRESS SETTINGS TO EDIT)";
    [[alert textFieldAtIndex:1] setSecureTextEntry:NO];
    [alert show];
}

- (IBAction) dateAction: (id)sender {
    [self performSegueWithIdentifier:@"date_picker" sender:self];
}

- (IBAction) removeAdsAction: (id)sender {
    if (![Util isPremiumUser]) {
        //[self performSegueWithIdentifier:@"premium" sender:self];
        [Util presentPremiumPage:self];
    } else {
        NSString* title = @"Already Purchased Item!";
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:nil
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(IBAction) rateAction: (id)sender {
    [Util rateApp];
}

- (IBAction) checkBoxAction : (id) sender {
    NSUserDefaults* userDefaults        = [NSUserDefaults standardUserDefaults];
    UIButton        *otherCheckMarkBTN  = nil, *redMark = nil, *whiteMark = nil, *yellowMark = nil;
    UIButton*       btnClicked          = (UIButton*) sender;
    CHECKBOXTYPES   tag                 = (int) btnClicked.tag;
    
    switch (tag) {
        case CHECKBOX_WATERMARK_YES:
            if (![Util isPremiumUser]) {
                [Util presentPremiumPage:self];
                
                return;
            }
            
            //otherCheckMarkBTN = (UIButton*) [self.view viewWithTag:CHECKBOX_WATERMARK_NO];
            
            if ([userDefaults boolForKey:@"off_watermark"]) {
                [userDefaults setBool:NO forKey:@"off_watermark"];
                [btnClicked setImage:[UIImage imageNamed:@"select_box_yes1.png"] forState:UIControlStateNormal];
                
            } else {
                [userDefaults setBool:YES forKey:@"off_watermark"];
                [btnClicked setImage:[UIImage imageNamed:@"select_box_no1.png"] forState:UIControlStateNormal];
            }
            
            //[otherCheckMarkBTN setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            
            break;

        case CHECKBOX_WATERMARK_NO:
            if (![Util isPremiumUser]
                && ![[NSUserDefaults standardUserDefaults] boolForKey:@"donot_show_rate_popup"]) {
                //[self performSegueWithIdentifier:@"premium" sender:self];
                [Util presentPremiumPage:self];
                return;
            }
            
            otherCheckMarkBTN = (UIButton*) [self.view viewWithTag:CHECKBOX_WATERMARK_YES];
            
            [btnClicked setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
            [otherCheckMarkBTN setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];

            [userDefaults setBool:YES forKey:@"off_watermark"];
            
            break;
            
        case CHECKBOX_DISPLAY_DATE_YES:
            //otherCheckMarkBTN = (UIButton*) [self.view viewWithTag:CHECKBOX_DISPLAY_DATE_NO];
            
            if ([userDefaults boolForKey:@"off_date"]) {
                [userDefaults setBool:NO forKey:@"off_date"];
                [btnClicked setImage:[UIImage imageNamed:@"select_box_yes1.png"] forState:UIControlStateNormal];
                
            } else {
                [userDefaults setBool:YES forKey:@"off_date"];
                [btnClicked setImage:[UIImage imageNamed:@"select_box_no1.png"] forState:UIControlStateNormal];
            }
            
            //[otherCheckMarkBTN setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];

            break;

        case CHECKBOX_DISPLAY_DATE_NO:
            otherCheckMarkBTN = (UIButton*) [self.view viewWithTag:CHECKBOX_DISPLAY_DATE_YES];
            
            [btnClicked setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
            [otherCheckMarkBTN setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];

            [userDefaults setBool:YES forKey:@"off_date"];
            
            break;
        case CHECKBOX_COLOR_WHITE:
            redMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_RED];
            yellowMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_YELLOW];
            
            [btnClicked setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
            [redMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            [yellowMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            
            [userDefaults setObject:@"white" forKey:@"text_color"];
            
            break;
        case CHECKBOX_COLOR_RED:
            whiteMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_WHITE];
            yellowMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_YELLOW];
            
            [btnClicked setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
            [whiteMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            [yellowMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            
            [userDefaults setObject:@"red" forKey:@"text_color"];
            
            break;
        case CHECKBOX_COLOR_YELLOW:
            whiteMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_WHITE];
            redMark = (UIButton*) [self.view viewWithTag:CHECKBOX_COLOR_RED];
            
            [btnClicked setImage:[UIImage imageNamed:@"select_box_yes.png"] forState:UIControlStateNormal];
            [whiteMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            [redMark setImage:[UIImage imageNamed:@"select_box_no.png"] forState:UIControlStateNormal];
            
            [userDefaults setObject:@"yellow" forKey:@"text_color"];
            
            break;
            
        default:
            break;
    }
    
    [userDefaults synchronize];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex==0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"ADD RETRO TITLES!" forKey:@"text_1"];
        [[NSUserDefaults standardUserDefaults] setObject:@"(PRESS SETTINGS TO EDIT)" forKey:@"text_2"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else if(buttonIndex==1) {        
        [[NSUserDefaults standardUserDefaults] setObject:[alertView textFieldAtIndex:0].text forKey:@"text_1"];
        [[NSUserDefaults standardUserDefaults] setObject:[alertView textFieldAtIndex:1].text forKey:@"text_2"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}




@end
