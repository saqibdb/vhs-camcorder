//
//  ShareViewController.h
//  PhotoEditor
//
//  Created by Qaiser Butt on 4/9/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "SCRecorder.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "GPUImage.h"
//#import <Google/Analytics.h>
//#import <VideoEditor/VideoEditorVC.h>
#import "Util.h"

@interface ShareViewController : UIViewController <UIDocumentInteractionControllerDelegate,AVPlayerViewControllerDelegate,SCAssetExportSessionDelegate,FBSDKSharingDelegate/*,VideoEditorSDKDelegate*/>
{    
    __weak IBOutlet UIView          *sReviewAlertView;
    IBOutlet UIView                 *contentView;
    IBOutlet UIImageView            *shareView;
    
    AVPlayerViewController          *_playerViewController;
    AVMutableComposition            *mixComposition;
    
             BOOL                    isSavedToDisk;
             BOOL                    shareOnInsta;
             BOOL                    shareOnFB;
             BOOL                    reviewPopShown;
}

@property (nonatomic, strong) NSString *isFrom;
@property (weak, nonatomic) IBOutlet UIView     *loadingView;
@property (weak, nonatomic) IBOutlet UILabel    *loadingLabel;
@property (weak, nonatomic)          NSString   *useTime;
@property (weak, nonatomic)          NSString   *filterName;
@property (weak, nonatomic)          NSString   *vRetroTitleText;

@property (strong, nonatomic)        NSURL      *videUrl;
@property (strong, nonatomic)        NSURL      *customAudioUrl;
@property (strong, nonatomic)        GPUImageMovie *movieFile;
@property (strong, nonatomic)        GPUImageMovieWriter *movieWriter;
@property (strong, nonatomic)        SCAssetExportSession *exportSession;
@property (strong, nonatomic)        NSMutableArray *vRetroTitleVisibleRanges;

@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@property (nonatomic)                CameraMode  cammode;
@property (nonatomic)                UIImage    *capturedPhoto;
@property (nonatomic)                CMTime      vAudioStartTime;
@property (nonatomic)                CGFloat     fontRatio;
@property (nonatomic)                BOOL        useCustomAudio;
//@property (nonatomic)                BOOL        vAddRetroTitle;
@property (nonatomic)                BOOL        isComingFromMontage;
@property (nonatomic)                int         vaporFilterIndex;


- (IBAction)backAction:(UIButton *)sender;

@end
