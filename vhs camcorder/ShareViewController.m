//
//  ShareViewController.m
//  PhotoEditor
//
//  Created by Qaiser Butt on 4/9/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "ShareViewController.h"
#import "MBProgressHUD.h"
#import <Social/Social.h>
#import "SCSaveToCameraRollOperation.h"
#import "SCRecordSessionManager.h"
#import "SCWatermarkOverlayView.h"
#import "ALInterstitialAd.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "AppDelegate.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSavedToDisk   = NO;
    shareOnInsta    = NO;
    shareOnFB       = NO;
    reviewPopShown  = NO;
    
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.delegate = self;
    //_playerViewController.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _playerViewController.showsPlaybackControls = NO;
    [contentView insertSubview:_playerViewController.view belowSubview:self.loadingView];
    _playerViewController.view.hidden = YES;
    
    if (_cammode == CameraModeVideo) {
        if (self.isComingFromMontage) {
            self.loadingView.hidden     = YES;
            self.loadingLabel.hidden    = YES;
            shareView.hidden            = NO;
            
            [self setupVideoPlayerWithAsset:self.videUrl OrComposition:nil isComposition:NO];
            
        } else {
            self.loadingLabel.text      = @"Exporting Video...";
            self.loadingView.hidden     = NO;
            self.loadingLabel.hidden    = NO;
            shareView.hidden            = YES;
            [self performSelector:@selector(exportRecording) withObject:nil afterDelay:1.0];
        }
        //[self exportRecording];
        
    } else {
        self.loadingView.hidden     = YES;
        self.loadingLabel.hidden    = YES;
        shareView.hidden            = NO;
        shareView.image             = self.capturedPhoto;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [self resetPlayerViewFrameSize];
}

#pragma mark - Actions

- (IBAction)backPressed:(id)sender {
    if (!isSavedToDisk && _cammode == CameraModeVideo) {
        /* If not saved by user, Donot include it in MyVideos List as well */
        if ([[NSFileManager defaultManager] fileExistsAtPath:self.videUrl.path])
            [[NSFileManager defaultManager] removeItemAtPath:self.videUrl.path error:nil];
    }
    if (self.isComingFromMontage) {
        [self performSegueWithIdentifier:@"backToMontage" sender:self];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)saveAction:(id)sender {    
    if (isSavedToDisk) {
        [self showAlertWithoutReview:@"Already saved to camera roll!" andTitle:@""];
        return;
    }
    
    if (_cammode == CameraModeVideo) {
        self.loadingView.hidden = NO;
        self.loadingLabel.hidden = NO;
        self.loadingLabel.text = @"Saving To Disk";
        
        if (NO)//(_useCustomAudio)  // This has already been performed
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"FinalVideo-%d.mp4",
                                                                                         arc4random() % 1000000]];
            NSURL *url = [NSURL fileURLWithPath:myPathDocs];
            if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
                [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
            
            AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                              presetName:AVAssetExportPresetHighestQuality];
            exporter.outputURL      = url;
            exporter.outputFileType = AVFileTypeMPEG4;
            [exporter exportAsynchronouslyWithCompletionHandler:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self exportDidFinish:exporter];
                });
            }];
        } else {
            // Save to the album
            __block PHObjectPlaceholder *placeholder;
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetChangeRequest* createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:self.videUrl];
                placeholder = [createAssetRequest placeholderForCreatedAsset];
                
            } completionHandler:^(BOOL success, NSError *error) {
                if (success)
                {
                    NSString* localID = [placeholder localIdentifier];
                    NSString* assetID = [localID stringByReplacingOccurrencesOfString:@"/.*"
                                                                           withString:@""
                                                                              options:NSRegularExpressionSearch
                                                                                range:NSMakeRange(0, [localID length])];
                    NSString* ext = @"mp4";
                    NSString* assetURLStr = [NSString stringWithFormat:@"assets-library://asset/asset.%@?id=%@&ext=%@", ext, assetID, ext];
                    self.videUrl = [NSURL URLWithString:assetURLStr];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self didFinishSavingWithError:self.videUrl andError:error];
                    });
                } else {
                    [self showAlertWithMessage:@"Failed To save the clip. Please try again!" andTitle:@""];
                    NSLog(@"%@", error);
                }
            }];
        }
    } else {
        SCSaveToCameraRollOperation *saveToCameraRoll = [SCSaveToCameraRollOperation new];
        [saveToCameraRoll saveImage:self.capturedPhoto completion:^(NSError * error) {
            [self didFinishSavingWithError:nil andError:error];
        }];
    }
}

-(IBAction)instaAction:(id)sender {
    BOOL isImportedMedia = ([SCRecorder sharedRecorder].isVideoImported || [SCRecorder sharedRecorder].isPhotoImported);
    
    if (![Util isPremiumUser] && (self.isComingFromMontage || isImportedMedia)) {
        [Util presentPremiumPage:self];
        return;
    }
    if (!isSavedToDisk) {
        shareOnInsta = YES;
        [self saveAction:nil];
    } else if (_cammode == CameraModeVideo) [self shareVideoOnInstagram];
    else [self sharePhotoOnInstagram];
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    //NSLog(@"file url %@",fileURL);
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}

-(IBAction)facebookAction:(id)sender {
    BOOL isImportedMedia = ([SCRecorder sharedRecorder].isVideoImported || [SCRecorder sharedRecorder].isPhotoImported);
    
    if (![Util isPremiumUser] && (self.isComingFromMontage || isImportedMedia)) {
        [Util presentPremiumPage:self];
        return;
    }
    if (!isSavedToDisk) {
        shareOnFB = YES;
        [self saveAction:nil];
    } else if (_cammode == CameraModeVideo) [self shareVideoOnFacebook];
    else [self sharePhotoOnFacebook];
}

-(IBAction)rateAction:(id)sender {
    [Util rateApp];
}

- (IBAction)reviewYesClicked:(id)sender {
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        sReviewAlertView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self rateAction:nil];
        reviewPopShown = NO;
        sReviewAlertView.hidden = YES;
    }];
}

- (IBAction)reviewNoClicked:(id)sender {
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        sReviewAlertView.alpha = 0.0;
    } completion:^(BOOL finished) {
        reviewPopShown = NO;
        sReviewAlertView.hidden = YES;
        
        if (![Util isPremiumUser]) {
            //AppDelegate *delgate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            if ([ALInterstitialAd isReadyForDisplay]) [ALInterstitialAd show];
            //if ([delgate.interstitial isReady]) [delgate.interstitial presentFromRootViewController:self];
        }
    }];
}

#pragma mark - Video Editor SDK

- (void)resetPlayerViewFrameSize {
    _playerViewController.view.frame = CGRectMake(_playerViewController.view.frame.origin.x,
                                                  _playerViewController.view.frame.origin.y,
                                                  contentView.frame.size.height,//_playerViewController.view.frame.size.width,
                                                  contentView.frame.size.height);
    _playerViewController.view.center = contentView.center;
}

- (void)displayVideoEditor {
    [self setupVideoPlayerWithAsset:self.videUrl OrComposition:nil isComposition:NO];
    
    /* Disable VideoEditor SDK
     *VideoEditorVC *obj = [[UIStoryboard storyboardWithName:@"Storyboard_VE" bundle:[NSBundle bundleWithIdentifier:@"com.VideoEditor"]]
     instantiateViewControllerWithIdentifier:@"VideoEditorVC"];
     obj.delegate = self;
     obj.videoPath = self.videUrl;
     obj.clientKey = @"57d0425c02739";
     obj.clientSecretKey = @"VES57d0425c022609.25263541";
     //obj.excludedFunctionalities = @[VEFilter]; // User can exclude multiple functionalities while using SDK. Currently we excluded Filter feature.
     [self presentViewController:obj animated:YES completion:nil];*/
}

#pragma mark Processing

- (void)exportRecording {
    SCRecorder *scRecorder = [SCRecorder sharedRecorder];

    if (scRecorder.isVideoImported) {
        AVURLAsset   *asset = [AVURLAsset URLAssetWithURL:self.videUrl options:nil];
        AVAssetTrack *track = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
        CGSize mSize = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
        CGSize mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
        
        [SCWatermarkOverlayView setCameraMode:CameraModeVideo];
        [SCWatermarkOverlayView setImportedPhoto:YES];
        [SCWatermarkOverlayView setImportedPhotoFontSize:20.0*(mediaSize.width/288/*352*/)];
    }
    
    /* Make sure to remove it first */
    [scRecorder removeHDistortionDisplayTimer];
    [scRecorder startHDistortionTimer];
    [scRecorder setIsSaving:YES];
    
    if ([self.filterName isEqualToString:@"CIPhotoEffectFade"]) { // Vapor Effect
        NSString *vaporFileName = @"f6_500"; // This is for imported video case only
        NSURL    *fileURL       = [NSURL fileURLWithPath:[Util getVaporDistortion:vaporFileName]];
        
        if (!scRecorder.isVideoImported) { // If not imported
            NSArray *vEffectThumbs = [NSArray arrayWithObjects:
                                      @"f1", @"f2", @"green_effect", @"f3", @"f4", @"f5",
                                      @"f6_288", @"f7_288", @"f10_288", @"f12_288", @"f13_288",
                                      nil];
            
            vaporFileName = [vEffectThumbs objectAtIndex:_vaporFilterIndex];
            fileURL       = [NSURL fileURLWithPath:[Util getVaporDistortion:vaporFileName]];
        }
        
        scRecorder.vVaporCIImage = [CIImage imageWithContentsOfURL:fileURL];
    }
    
    //[scRecorder scheduleRedBand];
    //[scRecorder.fittingImportVidlines removeAllObjects];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        SCAssetExportSession *exportSession = [[SCAssetExportSession alloc]
                                               initWithAsset:[SCRecorder sharedRecorder].session.assetRepresentingSegments];
        // This has no impact, just to add horizontal lines
        exportSession.videoConfiguration.filter = [SCFilter filterWithCIFilterName:self.filterName];
        exportSession.videoConfiguration.maxFrameRate = 35;
        //exportSession.videoConfiguration.sizeAsSquare = YES;
        exportSession.videoConfiguration.sizeAsSquare   = [SCRecorder sharedRecorder].videoConfiguration.sizeAsSquare;
        exportSession.videoConfiguration.sizeAsPortrait = [SCRecorder sharedRecorder].videoConfiguration.sizeAsPortrait;
        
        if (self.useCustomAudio) exportSession.audioConfiguration.enabled = NO;
        else exportSession.audioConfiguration.enabled = YES;
        
        exportSession.outputUrl = [Util getOutputVideoURL];
        exportSession.outputFileType = AVFileTypeMPEG4;
        exportSession.delegate = self;
        exportSession.contextType = SCContextTypeAuto;
        self.exportSession = exportSession;
        
        UIColor *tcolor = [Util getRetroTextColor:[[NSUserDefaults standardUserDefaults] objectForKey:@"text_color"]];
        SCWatermarkOverlayView *overlay  = [SCWatermarkOverlayView new];
        overlay.vRetroTitleVisibleRanges = _vRetroTitleVisibleRanges;
        
        [overlay setFontRatio:_fontRatio];
        [[overlay timeLabel] setText:self.useTime];
        [[overlay dateLabel] setTextColor:tcolor];
        [[overlay timeLabel] setTextColor:tcolor];
        [[overlay watermarkLabel] setTextColor:tcolor];
        [[overlay playLabel] setTextColor:tcolor];
        
        //if (_vAddRetroTitle) {
        [[overlay retroLabel] setTextColor:tcolor];
        [[overlay retroLabel] setText:self.vRetroTitleText];
        //}
        exportSession.videoConfiguration.overlay = overlay;
        
        CFTimeInterval time = CACurrentMediaTime();
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            self.loadingView.hidden  = YES;
            self.loadingLabel.hidden = YES;
            
            if ([SCRecorder sharedRecorder].isVideoImported) {
                [SCWatermarkOverlayView setImportedPhoto:NO];
                [SCWatermarkOverlayView setImportedPhotoFontSize:16.0];
            }
            
            [[SCRecorder sharedRecorder] removeHDistortionDisplayTimer];
            [SCRecorder sharedRecorder].vVaporCIImage = nil;
            //[[SCRecorder sharedRecorder] removeScheduledRedBand];
            
            if (!exportSession.cancelled) {
                NSLog(@"Completed compression in %fs", CACurrentMediaTime() - time);
            }
            
            NSError *error = exportSession.error;
            if (exportSession.cancelled) {
                [self showAlertWithMessage:@"Failed To Load. Please try recording again!" andTitle:@""];
                
            } else if (error == nil) {
                self.videUrl = exportSession.outputUrl;
                if (self.useCustomAudio) [self mergeVideoAudio];
                else [self displayVideoEditor];
            } else {
                if (!exportSession.cancelled)
                    [self showAlertWithMessage:@"Failed To Load. Please record again!" andTitle:@""];
                
                NSLog(@"Error Saving = %@", error.description);
            }
            
            /* Delete Temp Directory */
            //if (![SCRecorder sharedRecorder].isVideoImported) [Util clearTmpDirectory];
            [[SCRecorder sharedRecorder] setIsSaving:NO];
        }];
    });
}

- (void)mergeVideoAudio {
    if (![SCRecorder sharedRecorder].isVideoImported) {
        self.loadingLabel.text   = @"Mixing With Audio...";
        self.loadingView.hidden  = NO;
        self.loadingLabel.hidden = NO;
    }
    
    //if (self.useCustomAudio)
    {
        mixComposition = [AVMutableComposition composition];
        
        AVURLAsset *_audioAsset = [[AVURLAsset alloc]initWithURL:self.customAudioUrl options:nil];
        AVURLAsset *videoAsset  = [[AVURLAsset alloc]initWithURL:self.videUrl options:nil];
        
        CMTimeRange audio_timeRange = CMTimeRangeMake(self.vAudioStartTime/*kCMTimeZero*/, videoAsset.duration);
        
        AVMutableCompositionTrack *b_compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [b_compositionAudioTrack insertTimeRange:audio_timeRange
                                         ofTrack:[[_audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                                          atTime:kCMTimeZero
                                           error:nil];
        
        CMTimeRange video_timeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
        
        AVMutableCompositionTrack *a_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [a_compositionVideoTrack insertTimeRange:video_timeRange
                                         ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                                          atTime:kCMTimeZero
                                           error:nil];
        
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc]
                                          initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
        exporter.outputURL      = [Util getOutputVideoURL];
        exporter.outputFileType = AVFileTypeMPEG4;
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            /* Delete Temp Directory */
            if (![SCRecorder sharedRecorder].isVideoImported) [Util clearTmpDirectory];
            
            /* Discard previously exported video
             * Use current exported video */
            if ([[NSFileManager defaultManager] fileExistsAtPath:self.videUrl.path])
                [[NSFileManager defaultManager] removeItemAtPath:self.videUrl.path error:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self exportDidFinish:exporter];
            });
        }];
    }
}

- (void)setupVideoPlayerWithAsset:(NSURL *)url OrComposition:(AVMutableComposition *)composition isComposition:(BOOL)isComp {
    [self resetPlayerViewFrameSize];
    
    if (isComp) {
        AVPlayerItem *playerItem     = [AVPlayerItem playerItemWithAsset:composition];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    } else {
        AVPlayerItem *playerItem     = [AVPlayerItem playerItemWithURL:url];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    }
    _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    _playerViewController.view.hidden = NO;
    _playerViewController.showsPlaybackControls = YES;
}

- (void)didFinishSavingWithError:(NSURL *)newURL andError:(NSError *)error {
    self.loadingView.hidden = YES;
    self.loadingLabel.hidden = YES;
    
    if (error == nil) {
        [self showAlertWithMessage:@"Saved to camera roll!" andTitle:@""];
        
        self.videUrl  = newURL;
        isSavedToDisk = YES;
        
        if (shareOnInsta) {
            if (_cammode == CameraModeVideo) [self shareVideoOnInstagram];
            else [self sharePhotoOnInstagram];
            shareOnInsta = NO;
        } else if (shareOnFB) {
            if (_cammode == CameraModeVideo) [self shareVideoOnFacebook];
            else [self sharePhotoOnFacebook];
            shareOnFB = NO;
        } else if (![Util isPremiumUser] && !reviewPopShown) {
            //AppDelegate *delgate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            if ([ALInterstitialAd isReadyForDisplay]) [ALInterstitialAd show];
            //if ([delgate.interstitial isReady]) [delgate.interstitial presentFromRootViewController:self];
        }
    } else [self showAlertWithMessage:@"Failed To Save!" andTitle:@""];
}

- (void)exportDidFinish:(AVAssetExportSession*)session {
    self.loadingView.hidden  = YES;
    self.loadingLabel.hidden = YES;
    
    if (session.status == AVAssetExportSessionStatusCompleted) {
        self.videUrl = session.outputURL;
        [self displayVideoEditor];
    } else {
        if ([SCRecorder sharedRecorder].isVideoImported) {
            [self showAlertWithMessage:@"Failed To Mix With Audio. Try Again!" andTitle:@""];
        } else {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Failed To Mix With Audio!"
                                                  message:@"Make sure you're using downloaded music as VHS doesn't support audios unavailable on the device memory like Apple Music or such streaming services."
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {}]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)showAlertWithMessage:(NSString*)msg andTitle:(NSString*)title {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.offset = CGPointMake(hud.offset.x, 150.f);
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:2];
    
    [self reviewAlert];
    
    /*BOOL showPopup = ![[NSUserDefaults standardUserDefaults] boolForKey:@"donot_show_rate_popup"]
    && [[NSUserDefaults standardUserDefaults] boolForKey:@"show_popup"]
    && ![[NSUserDefaults standardUserDefaults] boolForKey:@"off_watermark"]
    && !shareOnInsta
    && !shareOnFB;
    
    if (showPopup) {
        reviewPopShown = YES;
        [self performSelector:@selector(reviewAlert) withObject:nil afterDelay:3];
    }*/
}

-(void)showAlertWithoutReview:(NSString*)msg andTitle:(NSString*)title {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.offset = CGPointMake(hud.offset.x, 150.f);
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}

-(void)reviewAlert {
    NSInteger runTimes = [[NSUserDefaults standardUserDefaults] integerForKey:@"noof_app_run"];
    
    if (runTimes >= 3) {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"noof_app_run"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        reviewPopShown = YES;
        [Util askForReview];
    }
    
    /*[UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        sReviewAlertView.alpha = 1.0;
        sReviewAlertView.hidden = NO;
    } completion:^(BOOL finished) {}];*/
    
    /*NSString* title = @"Remove The Watermark !";
     NSString* msg   = @"Would you like to remove the watermark for FREE?\nPlease rate us with five stars !";
     
     if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is_premium_purchased"]) {
     title = @"Rate The App!";
     msg   = @"Would you like to rate us with five stars!";
     }
     
     UIAlertController *alertController = [UIAlertController
     alertControllerWithTitle:title
     message:msg
     preferredStyle:UIAlertControllerStyleAlert];
     
     [alertController addAction:[UIAlertAction actionWithTitle:@"Yes!" style:UIAlertActionStyleDefault
     handler:^(UIAlertAction *action) {
     [self rateAction:nil];
     reviewPopShown = NO;
     }]];
     
     [alertController addAction:[UIAlertAction actionWithTitle:@"No, thanks!" style:UIAlertActionStyleCancel
     handler:^(UIAlertAction *action) {
     reviewPopShown = NO;
     
     if (![[NSUserDefaults standardUserDefaults] boolForKey:@"is_premium_purchased"]) {
     AppDelegate *delgate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
     if ([ALInterstitialAd isReadyForDisplay]) [ALInterstitialAd show];
     if ([delgate.interstitial isReady]) [delgate.interstitial presentFromRootViewController:self];
     }
     }]];
     
     [self presentViewController:alertController animated:YES completion:nil];*/
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
}

- (void)sharePhotoOnFacebook {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController* fbSLComposeViewController =
        [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [fbSLComposeViewController setInitialText:@""];
        [fbSLComposeViewController addImage:self.capturedPhoto];
        [self presentViewController:fbSLComposeViewController animated:YES completion:nil];
        
        fbSLComposeViewController.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"facebook: CANCELLED");
                    break;
                case SLComposeViewControllerResultDone:
                    //[self reviewAlert];
                    break;
            }
        };
    } else {
        NSString* title = @"Facebook Unavailable";
        NSString* msg   = @"Sorry, we're unable to find a Facebook account on your device.\nPlease setup an account in your devices settings and try again.";
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:msg
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                              
                                                          }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (void)shareVideoOnFacebook {
    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
    video.videoURL = self.videUrl;
    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
    content.video = video;
    
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
    
    /*ACAccountStore* accountStore = [[ACAccountStore alloc] init];
     NSDictionary *options = @{
     ACFacebookAppIdKey: @"258044444584065",
     ACFacebookPermissionsKey: @[@"publish_actions", ],// @"publish_stream", @"publish_actions", @"read_stream"
     @"ACFacebookAudienceKey": ACFacebookAudienceFriends
     };
     ACAccountType *facebookAccountType = [accountStore
     accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
     [accountStore requestAccessToAccountsWithType:facebookAccountType options:options completion:^(BOOL granted, NSError *error) {
     if (granted) {
     __block ACAccount * facebookAccount;
     ACAccountStore* accountStore = [[ACAccountStore alloc] init];
     ACAccountType *facebookAccountType = [accountStore
     accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
     
     
     NSArray *accounts        = [accountStore accountsWithAccountType:facebookAccountType];
     facebookAccount = [accounts lastObject];
     
     NSURL *videourl = [NSURL URLWithString:@"https://graph.facebook.com/me/videos"];
     
     NSString *filePath  = self.videUrl.path;//[[NSBundle mainBundle] pathForResource:@"YourVideoName" ofType:@"mp4"];
     NSURL *pathURL      = self.videUrl;//[[NSURL alloc]initFileURLWithPath:filePath isDirectory:NO];
     NSData *videoData   = [NSData dataWithContentsOfFile:filePath];
     
     NSDictionary *params = @{
     @"title": @"#RADVHS",
     @"description": @"Write Something ..."
     };
     
     SLRequest *uploadRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
     requestMethod:SLRequestMethodPOST
     URL:videourl
     parameters:params];
     
     
     [uploadRequest addMultipartData:videoData
     withName:@"source"
     type:@"video/quicktime"
     filename:[pathURL absoluteString]];
     
     uploadRequest.account = facebookAccount;
     
     self.loadingView.hidden = NO;
     self.loadingLabel.hidden = NO;
     
     self.loadingLabel.text = @"Posting To Facebook ...";
     
     [uploadRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
     NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
     
     if(error) NSLog(@"Error %@", error.localizedDescription);
     else NSLog(@"%@", responseString);
     
     self.loadingView.hidden = YES;
     self.loadingLabel.hidden = YES;
     
     dispatch_async(dispatch_get_main_queue(), ^{
     [self showAlertWithoutReview:@"Posted To your facebook timeline!" andTitle:@""];
     
     if(![[NSUserDefaults standardUserDefaults] boolForKey:@"is_premium_purchased"] && [ALInterstitialAd isReadyForDisplay])
     [ALInterstitialAd show];
     });
     }];
     } else {
     
     if(error) NSLog(@"Error %@", error.localizedDescription);
     
     dispatch_async(dispatch_get_main_queue(), ^{
     [self showAlertWithoutReview:@"Access to facebook is not granted!" andTitle:@""];
     });
     }
     }];*/
}

- (void)sharePhotoOnInstagram {
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
    
    //check for App is install or not
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        NSData *imageData = UIImagePNGRepresentation(self.capturedPhoto); //convert image into .png format.
        NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
        NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
        //NSLog(@"image saved");
        
        CGRect rect = CGRectMake(0 ,0 , 0, 0);
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIGraphicsEndImageContext();
        NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
        //NSLog(@"jpg path %@",jpgPath);
        NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
        //NSLog(@"with File path %@",newJpgPath);
        //NSURL *igImageHookFile = [[NSURL alloc]initFileURLWithPath:newJpgPath];
        NSURL *igImageHookFile = [NSURL URLWithString:newJpgPath];
        //NSLog(@"url Path %@",igImageHookFile);
        
        self.documentController.UTI = @"com.instagram.exclusivegram";
        self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
        self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        NSString *caption = @"#radmoji"; //settext as Default Caption
        self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
        [self.documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
        
    } else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Instagram Not Found!"
                                              message:@"Instagram not installed in this device!\nTo share image please install instagram."
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                          }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)shareVideoOnInstagram {
    NSString *escapedString   = [self.videUrl.absoluteString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *escapedCaption  = [@"#RADVHS" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSURL *instagramURL       = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@", escapedString, escapedCaption]];
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        [[UIApplication sharedApplication] openURL:instagramURL];
    } else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Instagram Not Found!"
                                              message:@"Instagram not installed in this device!\nTo share image please install instagram."
                                              preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                          }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - FBSDKSharingDelegate
/*!
 @abstract Sent to the delegate when the share completes without error or cancellation.
 @param sharer The FBSDKSharing that completed.
 @param results The results from the sharer.  This may be nil or empty.
 */
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"sharer:(id<FBSDKSharing>) didCompleteWithResults");
}

/*!
 @abstract Sent to the delegate when the sharer encounters an error.
 @param sharer The FBSDKSharing that completed.
 @param error The error.
 */
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"sharer:(id<FBSDKSharing>) didFailWithError %@", error.description);
}

/*!
 @abstract Sent to the delegate when the sharer is cancelled.
 @param sharer The FBSDKSharing that completed.
 */
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"sharer:(id<FBSDKSharing>) sharerDidCancel");
}

/*
 #pragma mark - VideoEditorSDK Delegate Method
 
 -(void)videoEditorSDKFinishedWithUrl:(NSURL *)url {
 NSLog(@"Video editing finished url : %@", url);
 
 self.videUrl = url;
 [self setupVideoPlayerWithAsset:self.videUrl OrComposition:nil isComposition:NO];
 }
 
 -(void)videoEditorSDKCanceled {
 [self setupVideoPlayerWithAsset:self.videUrl OrComposition:nil isComposition:NO];
 }*/

- (IBAction)backAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"unwindToVideosJoinView" sender:self];
}
@end
