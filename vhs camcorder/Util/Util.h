//
//  Util.h
//  Meditation
//
//  Created by Qaiser Butt on 12/10/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <StoreKit/StoreKit.h>

typedef NS_ENUM(NSInteger, EffectType) {
    ConstantEffect,
    VariableEffectRGB,
    VariableEffectContrast,
    VariableEffectBirghtness,
    ApplyAllEffects
};

typedef NS_ENUM(NSInteger, CameraMode) {
    CameraModeVideo,
    CameraModePhoto
};

typedef enum : NSUInteger {
    CHECKBOX_E1 = 111,
    CHECKBOX_E2 = 222,
    CHECKBOX_E3 = 333,
    CHECKBOX_E4 = 444,
    CHECKBOX_E5 = 555,
    CHECKBOX_E6 = 666,
    CHECKBOX_DISPLAY_DATE_YES = 777,
    CHECKBOX_DISPLAY_DATE_NO  = 888,
    CHECKBOX_WATERMARK_YES = 999,
    CHECKBOX_WATERMARK_NO  = 1110,
    CHECKBOX_COLOR_WHITE  = 1221,
    CHECKBOX_COLOR_RED  = 1332,
    CHECKBOX_COLOR_YELLOW  = 1443
} CHECKBOXTYPES;

typedef enum : NSUInteger {
    MVImageFlipXAxis,
    MVImageFlipYAxis,
    MVImageFlipXAxisAndYAxis,
} MVImageFlip;

@interface Util : NSObject

+ (UIImage *)flippedImage:(UIImage *)image ByAxis:(MVImageFlip)axis;
+ (void)extractAudioFromVideoWithInputURL:(NSURL*)inputURL andHandler:(void (^)(AVAssetExportSession*))handler;
+ (void)extractAudioFromVideoViaComposition:(NSURL*)inputURL andHandler:(void (^)(AVAssetExportSession*))handler;
+ (UIImage*)cropImage:(UIImage*)image FromScrollView:(UIScrollView*)scrollView;
+ (UIImageOrientation)getVideoOrientationFromAsset:(AVAsset *)asset;
+ (AVAssetReader *)setupReader:(AVAsset * )asset;
+ (UIImage *)imageFromCMSampleBuffer:(CMSampleBufferRef)sampleBuffer withMediaSize: (CGSize)mediaSize andOrient:(UIImageOrientation)videoAssetOrientation;
+ (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image withAdapter:(AVAssetWriterInputPixelBufferAdaptor *)adapter;
+ (CGRect)finalVideoRect:(CGSize)windowSize;

+ (NSString*)getNoiseImageWithIndex:(int)noisecounter;
+ (NSString*)getSideAnimBasePath;
+ (NSString*)getOneLinerDistortion:(int)noisecounter;
+ (NSString*)getImportVidResLineImageWithIndex:(int)noisecounter;
+ (NSString*)getLowResLineImageWithIndex:(int)noisecounter;
+ (NSString*)getLowOneLinerDistortion:(int)noisecounter;
+ (NSString*)getSideLine:(int)noisecounter;
+ (NSString*)getRedBandDistortion;
+ (NSString*)getCurrentTime;
+ (CIImage*) changeViewToImage:(UIView *)view;
+ (NSString*)getVaporDistortion:(NSString *)imageName;
+ (UIColor*) getRetroTextColor:(NSString*)color;
+ (void)     loadDistortedFrames;
+ (void)     resetImportSession;
+ (void)     createMyVideosFolderInDocuments; //VHS Videos would be saved here
+ (void)     clearTmpDirectory;
+ (NSURL*)   getOutputVideoURL;
+ (NSURL*)   getMontageVideoOutputURL;
+ (void)     downloadAndCheckIFRateWindowEnable;
+ (void)     handleRatePopup;
+ (BOOL)     is4InchIPhoneDevice;
+ (UIStoryboard*)grabStoryboard;
+ (void)     presentPremiumPage:(UIViewController*)pController;
+ (void)     resetFakeDateTime;
+ (BOOL)     isPremiumUser;
+ (BOOL)     isInternetReachable;
+ (void)     showAlertOnView:(UIView*)view WithMessage:(NSString*)msg;
+ (void)     rateApp;
+ (void)     askForReview;
+ (BOOL)     isUnder4InchScreen;

@end
