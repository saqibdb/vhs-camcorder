//
//  Util.m
//  Meditation
//
//  Created by Qaiser Butt on 12/10/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "Util.h"
#import "SCRecorder.h"
#import "SCRecordSessionManager.h"
#import "SSZipArchive.h"
#import "PremiumPageViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

@implementation Util

+ (UIImage *)flippedImage:(UIImage *)image ByAxis:(MVImageFlip)axis {

#warning This is just temporary fix for IOS 11
    /* Images might get blur on retina devices using IOS 11 or above */
    NSOperatingSystemVersion ios11_0_0 = (NSOperatingSystemVersion){11, 0, 0};
    if ([[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:ios11_0_0]) {
        // iOS 11.0.0 and above logic
        UIGraphicsBeginImageContextWithOptions(image.size, NO, 1.0);
    } else {
        // iOS 11.0.0 and below logic
        UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if(axis == MVImageFlipXAxis){
        // Do nothing, X is flipped normally in a Core Graphics Context
    } else if(axis == MVImageFlipYAxis){
        // fix X axis
        CGContextTranslateCTM(context, 0, image.size.height);
        CGContextScaleCTM(context, 1.0f, -1.0f);
        
        // then flip Y axis
        CGContextTranslateCTM(context, image.size.width, 0);
        CGContextScaleCTM(context, -1.0f, 1.0f);
    } else if(axis == MVImageFlipXAxisAndYAxis){
        // just flip Y
        CGContextTranslateCTM(context, image.size.width, 0);
        CGContextScaleCTM(context, -1.0f, 1.0f);
    }
    
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return flipedImage;
}

+ (void)extractAudioFromVideoWithInputURL:(NSURL*)inputURL andHandler:(void (^)(AVAssetExportSession*))handler {
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];*/
    NSString *tempDirectory = NSTemporaryDirectory();
    NSString *fileLastName  = [NSString stringWithFormat:@"FinalAudio-%d.m4a", arc4random() % 1000000];
    NSString *myPathDocs    = [tempDirectory stringByAppendingPathComponent:fileLastName];
    NSURL *outputURL = [NSURL fileURLWithPath:myPathDocs];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    
    AVURLAsset* asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeAppleM4A; //For audio file
    exportSession.timeRange = CMTimeRangeMake(kCMTimeZero, [asset duration]);
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
        handler(exportSession);
    }];
}

+ (void)extractAudioFromVideoViaComposition:(NSURL*)inputURL andHandler:(void (^)(AVAssetExportSession*))handler {
    AVMutableComposition *newAudioAsset = [AVMutableComposition composition];
    AVMutableCompositionTrack *dstCompositionTrack = [newAudioAsset
                                                      addMutableTrackWithMediaType:AVMediaTypeAudio
                                                      preferredTrackID:kCMPersistentTrackID_Invalid];
    AVAsset *srcAsset   = [AVAsset assetWithURL:inputURL];
    NSArray *trackArray = [srcAsset tracksWithMediaType:AVMediaTypeAudio];
    if(!trackArray.count) {
        NSLog(@"Track returns empty array for mediatype AVMediaTypeAudio");
        handler(nil);
        return;
    }
    
    AVAssetTrack *srcAssetTrack = [trackArray  objectAtIndex:0];
    CMTimeRange   timeRange     = srcAssetTrack.timeRange; // Extract time range
    
    NSError *err  = nil;
    BOOL inserted = [dstCompositionTrack insertTimeRange:timeRange
                                                 ofTrack:srcAssetTrack
                                                  atTime:kCMTimeZero
                                                   error:&err];
    if(NO == inserted) {
        NSLog(@"Failed to insert audio from the video to mutable avcomposition track");
        handler(nil);
        return;
    }
    
    NSString *tempDirectory = NSTemporaryDirectory();
    NSString *fileLastName  = [NSString stringWithFormat:@"FinalAudio-%d.m4a", arc4random() % 1000000];
    NSString *myPathDocs    = [tempDirectory stringByAppendingPathComponent:fileLastName];
    NSURL    *outputURL     = [NSURL fileURLWithPath:myPathDocs];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:newAudioAsset
                                                                           presetName:AVAssetExportPresetPassthrough];
    exportSession.outputFileType = AVFileTypeAppleM4A; //For audio file
    exportSession.outputURL      = outputURL;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        handler(exportSession);
    }];
}

+ (void)createMyVideosFolderInDocuments {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@",
                           documentsDirectory,
                           @"MyVideos"];
    BOOL isDir;
    NSError * error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:filePath isDirectory:&isDir])
        if(![fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error])
            NSLog(@"error creating directory: %@", error);
}

+ (NSURL*)getOutputVideoURL {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"MyVideos/FinalVideo-%d.mp4", arc4random() % 1000000]];
    
    NSURL *output = [NSURL fileURLWithPath:myPathDocs];
    if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    
    return output;
}

+ (NSURL*)getMontageVideoOutputURL {
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs = [documentsDirectory stringByAppendingPathComponent:
                            [NSString stringWithFormat:@"MyVideos/combined_video-%d.mov", arc4random() % 1000000]];
    NSURL *output = [NSURL fileURLWithPath:myPathDocs];
    if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
        [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
    
    return output;
}

+ (NSString*) getImportVidResLineImageWithIndex:(int)noisecounter {
    NSString *preFolderName = [NSString stringWithFormat:@"%@%@", @"lineDistortion/", @"640X480/"];
    NSString *filename = [NSString stringWithFormat:@"%i", noisecounter];
    
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:preFolderName];
}

+ (NSString*)getNoiseImageWithIndex:(int)noisecounter {
    NSString *preset = [SCRecorder sharedRecorder].bestCapturePreset;
    NSString *preFolderName = @"lineDistortion/";
    NSString *filename = [NSString stringWithFormat:@"%i", noisecounter];
    
    if (preset == AVCaptureSessionPreset1920x1080) {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1920X1080/"];
    } else if (preset == AVCaptureSessionPreset1280x720) {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1280X720/"];
    } else {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1280X720/"];
    }
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:preFolderName];
}

+ (NSString*)getSideAnimBasePath {
    NSString *preset = [SCRecorder sharedRecorder].bestCapturePreset, *preFolderName = @"sides/";
    
    if (preset == AVCaptureSessionPreset1920x1080) {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1920X1080/"];
    } else if (preset == AVCaptureSessionPreset1280x720) {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1280X720/"];
    } else {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1280X720/"];
    }
    
    return preFolderName;
}

+ (NSString*)getOneLinerDistortion:(int)noisecounter {
    NSString *preset = [SCRecorder sharedRecorder].bestCapturePreset;
    NSString *preFolderName = @"oneLiner/";
    NSString *filename = [NSString stringWithFormat:@"%i", noisecounter];
    
    if (preset == AVCaptureSessionPreset1920x1080) {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1920X1080/"];
    } else if (preset == AVCaptureSessionPreset1280x720) {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1280X720/"];
    } else {
        preFolderName = [NSString stringWithFormat:@"%@%@", preFolderName, @"1280X720/"];
    }
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:preFolderName];
}

+ (NSString*)getRedBandDistortion {
    NSString *preset = [SCRecorder sharedRecorder].bestCapturePreset;
    NSString *filename = @"red_band_1280";
    if (preset == AVCaptureSessionPreset1920x1080) filename = @"red_band_1920";
    
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:@"redBand/"];
}

+ (NSString*)getVaporDistortion:(NSString *)imageName {
    return [[NSBundle mainBundle] pathForResource:imageName
                                           ofType:@"png"
                                      inDirectory:@"vaporwave/"];
}

+ (NSString*)getLowResLineImageWithIndex:(int)noisecounter {
    NSString *preFolderName = [NSString stringWithFormat:@"%@%@", @"lineDistortion/", @"192X144/"];
    NSString *filename = [NSString stringWithFormat:@"%i", noisecounter];
    
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:preFolderName];
}

+ (NSString*)getLowOneLinerDistortion:(int)noisecounter {
    NSString *preFolderName = [NSString stringWithFormat:@"%@%@", @"oneLiner/", @"192X144/"];
    NSString *filename = [NSString stringWithFormat:@"%i", noisecounter];
    
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:preFolderName];
}

+ (NSString*)getSideLine:(int)noisecounter {
    NSString *preFolderName = [NSString stringWithFormat:@"%@%@", @"sides/", @"192X144/"];
    NSString *filename = [NSString stringWithFormat:@"%i", noisecounter];
    
    return [[NSBundle mainBundle] pathForResource:filename
                                           ofType:@"png"
                                      inDirectory:preFolderName];
}

+ (UIImage*)cropImage:(UIImage*)image FromScrollView:(UIScrollView*)scrollView {
    
    CGSize imageSize        = image.size;
    CGSize sVContentSize    = scrollView.contentSize;
    CGSize sVFrame          = scrollView.frame.size;
    CGFloat wAspectRatio    = imageSize.width/sVContentSize.width;
    CGFloat hAspectRatio    = imageSize.height/sVContentSize.height;
    
    CGRect visibleRect = CGRectZero;
    UIImageOrientation imageOrientation = image.imageOrientation;
    
    if (imageOrientation == UIImageOrientationRight) {
        if (sVContentSize.width > sVFrame.width) {
            visibleRect.origin.x    = scrollView.contentOffset.y*hAspectRatio*image.scale;
            visibleRect.origin.y    = ((sVContentSize.width - scrollView.contentOffset.x) - sVFrame.width)*wAspectRatio*image.scale;
            visibleRect.size.width  = (sVFrame.height)*hAspectRatio*image.scale;
            visibleRect.size.height = (sVFrame.width)*wAspectRatio*image.scale;
            
        } else {
            visibleRect.origin.x    = scrollView.contentOffset.y*hAspectRatio*image.scale;
            visibleRect.origin.y    = (scrollView.contentOffset.x)*wAspectRatio*image.scale;
            visibleRect.size.width  = (sVFrame.height)*hAspectRatio*image.scale;
            visibleRect.size.height = (sVFrame.width)*wAspectRatio*image.scale;
        }
        
    } else if (imageOrientation == UIImageOrientationLeft) {
        visibleRect.origin.x    = scrollView.contentOffset.y*hAspectRatio*image.scale;
        visibleRect.origin.y    = (scrollView.contentOffset.x)*wAspectRatio*image.scale;
        visibleRect.size.width  = (sVFrame.height)*hAspectRatio*image.scale;
        visibleRect.size.height = (sVFrame.width)*wAspectRatio*image.scale;
        
    } else if (imageOrientation == UIImageOrientationUp) {
        visibleRect.origin.x    = (scrollView.contentOffset.x)*wAspectRatio*image.scale;
        visibleRect.origin.y    = scrollView.contentOffset.y*hAspectRatio*image.scale;
        visibleRect.size.width  = (sVFrame.width)*wAspectRatio*image.scale;
        visibleRect.size.height = (sVFrame.height)*hAspectRatio*image.scale;
        
    } else if (imageOrientation == UIImageOrientationDown) {
        if (sVContentSize.height > sVFrame.height) {
            visibleRect.origin.x    = scrollView.contentOffset.x*wAspectRatio*image.scale;
            visibleRect.origin.y    = ((sVContentSize.height - scrollView.contentOffset.y) - sVFrame.height)*hAspectRatio*image.scale;
            visibleRect.size.width  = sVFrame.width*wAspectRatio*image.scale;
            visibleRect.size.height = sVFrame.height*hAspectRatio*image.scale;
        } else {
            visibleRect.origin.x    = ((sVContentSize.width - scrollView.contentOffset.x) - sVFrame.width)*wAspectRatio*image.scale;
            visibleRect.origin.y    = scrollView.contentOffset.y*hAspectRatio*image.scale;
            visibleRect.size.width  = sVFrame.width*wAspectRatio*image.scale;
            visibleRect.size.height = sVFrame.height*hAspectRatio*image.scale;
        }
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], visibleRect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:imageOrientation];
    CGImageRelease(imageRef);
    
    return cropped;
}

+ (void)trimVideo:(NSURL *)pVideoUrl start:(CGFloat)pStartTime stop:(CGFloat)pStopTime
           output:(NSURL *)outputurl andHandler:(void (^)(void))handler
{
    AVAsset *anAsset = [[AVURLAsset alloc] initWithURL:pVideoUrl options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];

    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetHighestQuality];
        /*
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                                 [NSString stringWithFormat:@"FinalVideo-%d.mov", arc4random() % 1000000]];
        
        NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];*/
        
        exportSession.outputURL = outputurl;
        exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(pStartTime, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(pStopTime - pStartTime, anAsset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        exportSession.timeRange = range;
        
        //[MBProgressHUD showHUDAddedTo:self.view animated:YES].label.text = @"Trimming";
        [exportSession exportAsynchronouslyWithCompletionHandler:handler];
    }
}

+ (UIImageOrientation)getVideoOrientationFromAsset:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty)
        return UIImageOrientationLeft; //return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIImageOrientationRight; //return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIImageOrientationDown; //return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIImageOrientationUp;  //return UIInterfaceOrientationPortrait;
}


+ (CGRect)finalVideoRect:(CGSize)windowSize {
    NSInteger dividend  = (NSInteger) (windowSize.width/16);
    CGFloat   width     = dividend*16;
    return CGRectMake(0, 0, width, width);
}

+ (AVAssetReader *)setupReader:(AVAsset *)asset {
    NSError *error = nil;
    AVAssetReader *assetReader = [[AVAssetReader alloc] initWithAsset:asset error:&error];
    AVAssetTrack  *videoTrack  = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32ARGB] , kCVPixelBufferPixelFormatTypeKey, nil];
    
    AVAssetReaderOutput *assetReaderOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:videoTrack
                                                                                        outputSettings:videoSettings];
    if ([assetReader canAddOutput:assetReaderOutput]) {
        [assetReader addOutput:assetReaderOutput];
    }
    //return [assetReader autorelease];
    return assetReader;
}

+ (UIImage *)imageFromCMSampleBuffer:(CMSampleBufferRef)sampleBuffer withMediaSize:(CGSize)mediaSize andOrient:(UIImageOrientation)videoAssetOrientation
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    /*Lock the image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    
    /*Get information about the image*/
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    /*We unlock the  image buffer*/
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    /*Create a CGImageRef from the CVImageBufferRef*/
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGImageAlphaNoneSkipFirst);
    CGImageRef newImage = CGBitmapContextCreateImage(newContext);
    
    /*We release some components*/
    CGContextRelease(newContext);
    CGColorSpaceRelease(colorSpace);
    
    //UIImageOrientation orientation = UIImageOrientationUp;
    //if (mediaSize.width < mediaSize.height) orientation = UIImageOrientationRight; // portrait
    
    /*We display the result on the image view (We need to change the orientation of the image so that the video is displayed correctly)*/
    UIImage *image = [UIImage imageWithCGImage:newImage scale:1.0 orientation:videoAssetOrientation];
    
    /*We relase the CGImageRef*/
    CGImageRelease(newImage);
    
    return image;
}

+ (CVPixelBufferRef)pixelBufferFromCGImage:(CGImageRef)image withAdapter:(AVAssetWriterInputPixelBufferAdaptor *)adapter
{
    CVPixelBufferRef pxbuffer = NULL;
    
    CVPixelBufferPoolCreatePixelBuffer (NULL, adapter.pixelBufferPool, &pxbuffer);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, CGImageGetWidth(image),
                                                 CGImageGetHeight(image), 8, 4*CGImageGetWidth(image), rgbColorSpace,
                                                 kCGImageAlphaNoneSkipFirst);
    
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image),
                                           CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}

+ (CIImage *) changeViewToImage : (UIView *) view {
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [CIImage imageWithCGImage:img.CGImage];
}

+ (NSString *)getCurrentTime {
    NSDate *now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    BOOL hasAMPM = containsA.location != NSNotFound;
    
    if (hasAMPM) [outputFormatter setDateFormat:@"aa h:mm"];
    else [outputFormatter setDateFormat:@"h:mm"];
    
    return [outputFormatter stringFromDate:now];
}

+ (UIColor *)getRetroTextColor:(NSString*)color {
    UIColor *uicolor = [UIColor whiteColor];
    if ([color isEqualToString:@"white"]) {
        uicolor = [UIColor whiteColor];
    } else if ([color isEqualToString:@"red"]) {
        uicolor = [UIColor blackColor];
    } else if ([color isEqualToString:@"yellow"]) {
        uicolor = [UIColor yellowColor];
    }
    return uicolor;
}

+ (void)loadDistortedFrames {
    /*_recorder.lowReslines = [NSArray arrayWithObjects:
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getLowResLineImageWithIndex:1]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getLowResLineImageWithIndex:2]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getLowResLineImageWithIndex:3]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getLowResLineImageWithIndex:4]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getLowResLineImageWithIndex:5]]],
     nil];
     
     _recorder.fittingImportVidlines = [NSMutableArray new];
     _recorder.importVidlines = [NSArray arrayWithObjects:
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getImportVidResLineImageWithIndex:1]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getImportVidResLineImageWithIndex:2]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getImportVidResLineImageWithIndex:3]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getImportVidResLineImageWithIndex:4]]],
     [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getImportVidResLineImageWithIndex:5]]],
     nil];*/
    
    if (![SCRecorder sharedRecorder].sidelines) {
        [SCRecorder sharedRecorder].sidelines = [NSArray arrayWithObjects:
                                                 [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getSideLine:1]]],
                                                 [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getSideLine:2]]],
                                                 [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getSideLine:3]]],
                                                 [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getSideLine:4]]],
                                                 [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getSideLine:5]]],
                                                 nil];
    }
    
    if (![SCRecorder sharedRecorder].vOneLinerFrames) {
        [SCRecorder sharedRecorder].vOneLinerFrames = [NSArray arrayWithObjects:
                                                       [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getOneLinerDistortion:1]]],
                                                       [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getOneLinerDistortion:2]]],
                                                       [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getOneLinerDistortion:3]]],
                                                       [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getOneLinerDistortion:4]]],
                                                       [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getOneLinerDistortion:5]]],
                                                       nil];
    }
    
    //_recorder.vRedBandCIImage = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[Util getRedBandDistortion]]];
}

+ (void)resetImportSession {
    SCRecorder *_recorder = [SCRecorder sharedRecorder];
    SCRecordSession *recordSession = _recorder.session;
    if (recordSession != nil) {
        _recorder.session = nil;
        
        // If the recordSession was saved, we don't want to completely destroy it
        if ([[SCRecordSessionManager sharedInstance] isSaved:recordSession]) {
            [recordSession endSegmentWithInfo:nil completionHandler:nil];
        } else {
            [recordSession cancelSession:nil];
        }
    }
    
    // PrepareSession
    if (_recorder.session == nil) {
        SCRecordSession *session = [SCRecordSession recordSession];
        session.fileType = AVFileTypeMPEG4;//AVFileTypeQuickTimeMovie;
        _recorder.session = session;
    }
}

+ (void)clearTmpDirectory {
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}

+ (BOOL)isUnder4InchScreen {
    BOOL result = NO;
    
    // detect the height of our screen
    int width = [UIScreen mainScreen].bounds.size.width;
    int height = [UIScreen mainScreen].bounds.size.height;
    int limitValue = 568;
    
    // Check if Landscape
    if (width > height) limitValue = 320;
    
    if (height > limitValue) {
        result = NO;
        // NSLog(@"Device has a greater than 4inch Display.");
    } else {
        result = YES;
        // NSLog(@"Device has a under 4inch Display.");
    }
    
    return result;
}

+ (UIStoryboard *)grabStoryboard {
    UIStoryboard *storyboard;
    
    // detect the height of our screen
    int width = [UIScreen mainScreen].bounds.size.width;
    int height = [UIScreen mainScreen].bounds.size.height;
    int limitValue = 568;
    
    // Check if Landscape
    if (width > height) limitValue = 320;
    
    if (height > limitValue) {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        // NSLog(@"Device has a greater than 4inch Display.");
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_Under4inch" bundle:nil];
        // NSLog(@"Device has a under 4inch Display.");
    }
    
    return storyboard;
    
    /*
     // determine screen size
     int screenHeight = [UIScreen mainScreen].bounds.size.height;
     UIStoryboard *storyboard;
     
     switch (screenHeight) {
     
     // iPhone 4s
     case 480:
     storyboard = [UIStoryboard storyboardWithName:@"Main-4s" bundle:nil];
     break;
     
     // iPhone 5s
     case 568:
     storyboard = [UIStoryboard storyboardWithName:@"Main-5s" bundle:nil];
     break;
     
     // iPhone 6
     case 667:
     storyboard = [UIStoryboard storyboardWithName:@"Main-6" bundle:nil];
     break;
     
     // iPhone 6 Plus
     case 736:
     storyboard = [UIStoryboard storyboardWithName:@"Main-6-Plus" bundle:nil];
     break;
     
     default:
     // it's an iPad
     storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     break;
     }
     
     */
}

+ (BOOL)is4InchIPhoneDevice {    
    // detect the height of our screen
    int width = [UIScreen mainScreen].bounds.size.width;
    int height = [UIScreen mainScreen].bounds.size.height;
    int limitValue = 568;
    BOOL is4InchDevice = NO;
    
    // Check if Landscape
    if (width > height) limitValue = 320;
    
    if (height > limitValue) {
        is4InchDevice = NO;
    } else {
        is4InchDevice = YES;
    }
    
    return is4InchDevice;
}

+ (void)downloadAndCheckIFRateWindowEnable {
    /* Show review popup always */
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"show_popup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return;
    
    /* Disabled Permanently for now */
    NSString *stringURL = @"https://www.dropbox.com/sh/ud3ff7bd0cqw1ts/AAB0D495uC1QhMvfXQLX2wMpa?dl=1";
    NSURL  *url = [NSURL URLWithString:stringURL];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString  *sourceFilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"container.zip"];
        [urlData writeToFile:sourceFilePath atomically:YES];
        
        NSString  *destfilePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"unzipped"];
        BOOL isDir;
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:destfilePath isDirectory:&isDir];
        if (exists) {
            /* file exists */
            if (isDir) {
                /* file is a directory */
                NSError* error = nil;
                [[NSFileManager defaultManager] removeItemAtPath:destfilePath error:&error];
            }
        }
        
        [SSZipArchive unzipFileAtPath:sourceFilePath toDestination:destfilePath];
        
        NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destfilePath error:nil];
        NSString *newPath = nil;
        NSError* err = nil;
        if (directoryContent) {
            NSString* filePath = [NSString stringWithFormat:@"%@/%@", destfilePath,[directoryContent objectAtIndex:0]];
            newPath = [[filePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"popup.plist"];
            [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:newPath error:&err];
        }
        
        NSDictionary *theDict = [NSDictionary dictionaryWithContentsOfFile:newPath];
        BOOL showPopup = [[theDict valueForKey:@"show_popup"] boolValue];
        
        [[NSUserDefaults standardUserDefaults] setBool:showPopup forKey:@"show_popup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)handleRatePopup {
    NSDate *dateClicked = [[NSUserDefaults standardUserDefaults] objectForKey:@"date_clicked_to_rate"];
    if (dateClicked) {
        NSDate *currentDate = [NSDate date];
        if (fabs([currentDate timeIntervalSinceDate:dateClicked]) >= 10)
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"donot_show_rate_popup"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"off_watermark"];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"date_clicked_to_rate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)presentPremiumPage:(UIViewController*)pController {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *vStoryboardName = [Util is4InchIPhoneDevice] ? @"Main_Under4inch" : @"Main";
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:vStoryboardName bundle:nil];
        PremiumPageViewController *premiumController = [mainStoryboard instantiateViewControllerWithIdentifier:@"premium_scene"];
        [pController presentViewController:premiumController animated:YES completion:^{}];
    });
}

+ (void)resetFakeDateTime {
    /* Unknown reason
     * Its reset when recorder is launch after visiting the MOntage scene
     */
    NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
    [dateformater setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateformater setDateFormat:@"MMM.dd yyyy"];
    
    NSString *date = [dateformater stringFromDate: [NSDate date]];
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"fake_date_created"];
    
    /* Reset time for every import session launch */
    [[NSUserDefaults standardUserDefaults] setObject:[Util getCurrentTime] forKey:@"fake_time_created"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isPremiumUser {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"is_premium_purchased"];
}

+ (BOOL)isInternetReachable {
    return !([[Reachability reachabilityForInternetConnection]currentReachabilityStatus] == NotReachable);
}

+ (void)showAlertOnView:(UIView*)view WithMessage:(NSString*)msg {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.offset = CGPointMake(hud.offset.x, 150.f);
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:2];
}

+ (void)askForReview {
    if ([SKStoreReviewController class]) {
        [SKStoreReviewController requestReview];
    }
}

+ (void)rateApp {    
    NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1079944301";
    NSString *templateReviewURLiOS7 = @"itms-apps://itunes.apple.com/app/id1079944301";
    NSString *templateReviewURLiOS8 = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1079944301&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";
    
    int appId = 1079944301;
    
    //ios7 before
    NSString *reviewURL = [templateReviewURL stringByReplacingOccurrencesOfString:@"1079944301" withString:[NSString stringWithFormat:@"%d", appId]];
    
    // iOS 7 needs a different templateReviewURL @see https://github.com/arashpayan/appirater/issues/131
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
    {
        reviewURL = [templateReviewURLiOS7 stringByReplacingOccurrencesOfString:@"1079944301" withString:[NSString stringWithFormat:@"%d", appId]];
    }
    // iOS 8 needs a different templateReviewURL also @see https://github.com/arashpayan/appirater/issues/182
    else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        reviewURL = [templateReviewURLiOS8 stringByReplacingOccurrencesOfString:@"1079944301" withString:[NSString stringWithFormat:@"%d", appId]];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"date_clicked_to_rate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
