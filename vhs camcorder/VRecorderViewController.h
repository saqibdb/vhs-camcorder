//
//  ViewController.h
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecorder.h"
#import "GPUImage.h"
#import <AVKit/AVKit.h>
#import "RGBChannelCompositing.h"
//#import <Google/Analytics.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VRecorderViewController : UIViewController <SCRecorderDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, SCAssetExportSessionDelegate, AVPlayerViewControllerDelegate, MPMediaPickerControllerDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *camBorders;
@property (weak, nonatomic) IBOutlet UIImageView *sidesAnimView;
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UILabel *timeRecordedLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *playLabel;
@property (weak, nonatomic) IBOutlet UILabel *recordLabel;
@property (weak, nonatomic) IBOutlet UILabel *watermarkLabel;
@property (weak, nonatomic) IBOutlet UIButton *recBTN;
@property (weak, nonatomic) IBOutlet UIButton *playBTN;
@property (weak, nonatomic) IBOutlet UIButton *playTriangleBTN;
//@property (strong, nonatomic) SCAssetExportSession *exportSession;

@property (strong, nonatomic) GPUImageMovie *movieFile;
@property (strong, nonatomic) GPUImageMovie *secondmovieFile;
@property (strong, nonatomic) GPUImageMovieWriter *movieWriter;
@property (strong, nonatomic) GPUImageFilter *filter;
@property (strong, nonatomic) GPUImageAlphaBlendFilter *blendFilter;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) GPUImageUIElement *uiElementInput;

-(IBAction) openSettings;
-(IBAction) startRecording;
-(IBAction) openCameraRoll;
-(IBAction)reverseCamera:(id)sender;
-(IBAction)switchFlash:(id)sender;
-(IBAction)musicAction:(id)sender;
-(IBAction)downloadAction:(id)sender;
-(IBAction)photoAction:(id)sender;

@end

