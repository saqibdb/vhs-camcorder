//
//  ViewController.m
//  vhs camcorder
//
//  Created by Qaiser Butt on 2/4/16.
//  Copyright © 2016 Shadi Osta. All rights reserved.
//

#import "VRecorderViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <GLKit/GLKit.h>
#import "SCRecordSessionManager.h"
#import "SCSaveToCameraRollOperation.h"
#import "SettingsViewController.h"
#import "SCWatermarkOverlayView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "ALInterstitialAd.h"
#import "ShareViewController.h"
#import "PremiumPageViewController.h"
#import "RGBChannelCompositing.h"
#import "Util.h"
#import "ImportViewController.h"
#import "MBProgressHUD.h"
#import "AudioRecorder.h"

#define SENTINEL_TILT_SEGMENT -1
#define MAX_DELAY_PER_GLITCH   2.0f

@interface NonRotatingUIImagePickerController : UIImagePickerController

@end

@implementation NonRotatingUIImagePickerController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeRight;
}

@end

@interface VRecorderViewController () {
    __weak IBOutlet NSLayoutConstraint *vSquareAspectRatio;
    __weak IBOutlet NSLayoutConstraint *vLandscapeAspectRatio;
    __weak IBOutlet NSLayoutConstraint *vPortraitAspectRatio;
    
    __weak IBOutlet NSLayoutConstraint *vPreviewSquareAspectRatio;
    __weak IBOutlet NSLayoutConstraint *vPreviewLandscapeAspectRatio;
    __weak IBOutlet NSLayoutConstraint *vPreviewPortraitAspectRatio;
    __weak IBOutlet NSLayoutConstraint *vPlayTextTopConstraint;
    __weak IBOutlet NSLayoutConstraint *vRecordTimeBottomConstraint;
    __weak IBOutlet NSLayoutConstraint *vSideGlitchTrailingConstraint;
    __weak IBOutlet NSLayoutConstraint *vSideGlitchLeadingConstraint;
    __weak IBOutlet UIView             *vFiltersPanel;
    __weak IBOutlet NSLayoutConstraint *vFilterPanelTrailToSuperview;// iPad
    __weak IBOutlet NSLayoutConstraint *vFilterPanelTrailToRightBG;// iPad
    __weak IBOutlet UILabel *vRetroText;
    __weak IBOutlet UIImageView *vGlitchView1;
    __weak IBOutlet UIImageView *vGlitchView2;
    __weak IBOutlet UIImageView *vGlitchView3;
    __weak IBOutlet UIImageView *vGlitchView4;
    __weak IBOutlet UIImageView *vGlitchView5;
    
    __weak IBOutlet UIImageView *vVaporView;
    __weak IBOutlet NSLayoutConstraint *vCamLeadToSuperX;
    __weak IBOutlet UIImageView *vRightBG;
    
    __weak IBOutlet NSLayoutConstraint *vTrailFromControlToRightBG;
    __weak IBOutlet NSLayoutConstraint *vTrailFromControlToSuperview;
    __weak IBOutlet UIButton *vEffectsBTN;
    __weak IBOutlet UILabel *vEffectLabel;
    __weak IBOutlet UIButton *vControlsBTN;
    __weak IBOutlet UILabel *vControlsLabel;
    __weak IBOutlet NSLayoutConstraint *vRecordTimeLeadingConstraint;
    __weak IBOutlet NSLayoutConstraint *vWatermarkTrailingConstraint;
    
    AVPlayerViewController *_playerViewController;
    NSArray<UIImageView*>  *vGlitchFrames;
    
    NSURL               *customAudioUrl;
    NSURL               *importedVideoUrl;
    AVAudioPlayer       *playeraudio;
    //NSTimer             *vRedBandMoveTimer;
    NSTimer             *vGlitchViewControlTimer;
    NSTimer             *vOneLinerFrameChanger;
    NSTimer             *vZoomTimer;
    NSMutableArray      *vRetroTitleVisibleRanges;
    NSArray             *effects;
    NSArray             *vEffectThumbs;
    //NSArray             *vEffectThumbs1;
    NSArray<UIImage*>   *vOneLinerHNoiseFrames;
    UIImage             *_capturedPhoto;

    RGBChannelToneCurve *_CIToneCurveFilter;

    AudioRecorder       *audioRecorder;
    SCRecorder          *_recorder;
    CameraMode           _cammode;
    SCFilterImageView   *scimageview;        // Used while recording is off
    UIImageView         *vRecordingPreview; // Used while recording is on
    
    BOOL _recording;
    BOOL _isLaunch;
    //BOOL _importing;
    BOOL _hasZoomPlusLastTime;
    BOOL resetTilt;
    BOOL dontResetSession;
    BOOL resumeRecording;
    BOOL useCustomAudio;
    
    int zoomCounter;
    int currentEffectIndex;
    int nextTiltAvailableCounter;
    int counter;
    int hGlitchCounter;
    
    CGFloat        hCurrentGlitchSlidingValue;
    CGFloat        defaultFontSize;
    CGFloat        defaultZoomLevel;
    NSTimeInterval retroStartTime;
    NSTimeInterval retroStopTime;
}

@end


@implementation VRecorderViewController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark Interface methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [Util resetFakeDateTime];
    [MBProgressHUD showHUDAddedTo:self.camBorders animated:YES].label.text = @"Loading";
    
    customAudioUrl              = nil;
    _isLaunch                   = YES;
    useCustomAudio              = NO;
    
    //pTotalTimeSec          = 0;
    counter                  = 1;
    hGlitchCounter           = 1;
    nextTiltAvailableCounter = 0;
    _cammode                 = CameraModeVideo;
    currentEffectIndex       = 0;
    retroStartTime           = 0.0;
    retroStopTime            = 0.0;
    hCurrentGlitchSlidingValue = 0.0f;
    
    /*
     * CIPhotoEffectFade = Vaporwave
     * CIColorCube = GREEN EFFECT
     */
    
    effects = [NSArray arrayWithObjects:
               @"CIPhotoEffectInstant",@"CIPhotoEffectChrome",@"CIColorCube",@"CIPhotoEffectProcess",@"CIPhotoEffectNoir",
               @"CIPhotoEffectTonal",
               @"CIPhotoEffectFade",@"CIPhotoEffectFade",@"CIPhotoEffectFade",@"CIPhotoEffectFade",@"CIPhotoEffectFade", nil];
    
    vEffectThumbs = [NSArray arrayWithObjects:@"f1", @"f2", @"green_effect", @"f3", @"f4", @"f5", @"f6", @"f7", @"f10", @"f12", @"f13", nil];
    //vEffectThumbs1 = [NSArray arrayWithObjects:@"f6", @"f7", @"f10", @"f12", @"f13", nil];
    
    vRetroTitleVisibleRanges = [NSMutableArray new];
    audioRecorder            = [[AudioRecorder alloc] init];
    
    /*NSString *newDateString = [Util getCurrentTime];
    _timeLabel.text = newDateString;
    
    [[NSUserDefaults standardUserDefaults] setObject:newDateString forKey:@"fake_time_created"];
    [[NSUserDefaults standardUserDefaults] synchronize];*/
    
    _recorder = [SCRecorder sharedRecorder];
    /*_recorder.captureSessionPreset = AVCaptureSessionPreset352x288;*/
    
    [Util loadDistortedFrames];
    
    /*_recorder.flashMode                 = SCFlashModeOff;
    _recorder.videoOrientation          = AVCaptureVideoOrientationLandscapeRight;
     _recorder.initializeSessionLazily   = NO;*/
    _recorder.delegate                  = self;
    _recorder.previewView               = self.previewView;
    _recorder.vRGBControlRatio          = 17.0f;
    _recorder.vContrastControlRatio     = 0.75f;
    //_recorder.vBirghtnessControlRatio   = 0.0f;
    _recorder.vNoiseControlRatio        = MAX_DELAY_PER_GLITCH - 1.0f;
    
    /*
    // Get the video configuration object
    SCVideoConfiguration *video = _recorder.videoConfiguration;
    video.filter = [SCFilter filterWithCIFilterName:[effects objectAtIndex:currentEffectIndex]];
    video.sizeAsSquare = YES;*/
    
    defaultZoomLevel = [_recorder videoZoomFactor];
    
    /*NSError *error;
    if (![_recorder prepare:&error]) {
        NSLog(@"Prepare error: %@", error.localizedDescription);
    }
    [_recorder setBestCapturePreset:[SCRecorderTools bestCaptureSessionPresetCompatibleWithAllDevices]];*/
    
    NSString *fileName = [self getNextTilt];    
    NSString *stringVideoName = fileName;
    NSString *stringVideoPath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
    NSAssert(stringVideoPath, @"Expected not nil video file");
    
    NSURL *urlVideoFile = [NSURL fileURLWithPath:stringVideoPath];
    NSAssert(urlVideoFile, @"Expected not nil video url");
    
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.delegate = self;
    _playerViewController.view.hidden = YES;
    
    // First create an AVPlayerItem
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:urlVideoFile];
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    // Pass the AVPlayerItem to a new player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(itemDidFinishPlaying:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:playerItem];
    _playerViewController.showsPlaybackControls = NO;
    _playerViewController.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view insertSubview:_playerViewController.view aboveSubview:self.previewView];
    
    
    NSArray<UIImage *> * _Nullable animFrames = @[[UIImage imageWithContentsOfFile:[Util getSideLine:1]],
                                                  [UIImage imageWithContentsOfFile:[Util getSideLine:2]],
                                                  [UIImage imageWithContentsOfFile:[Util getSideLine:3]],
                                                  [UIImage imageWithContentsOfFile:[Util getSideLine:4]],
                                                  [UIImage imageWithContentsOfFile:[Util getSideLine:5]]];
    [_sidesAnimView setAnimationImages:animFrames];
    //[_sidesAnimView startAnimating];
    
    vGlitchFrames = [NSArray arrayWithObjects:vGlitchView1,vGlitchView2,vGlitchView3,vGlitchView4,vGlitchView5, nil];
    
    _CIToneCurveFilter = [RGBChannelToneCurve new];
    [_CIToneCurveFilter setDefaults];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        vEffectsBTN.hidden          = NO;
        vEffectLabel.hidden         = NO;
        
    } else {        
        [vControlsBTN setImage:[UIImage imageNamed:@"light_ipad.png"] forState:UIControlStateNormal];
        [vControlsBTN addTarget:self action:@selector(switchFlash:) forControlEvents:UIControlEventTouchUpInside];
        [vControlsLabel setText:@"LIGHT"];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_watermark"] ) {
        [self.watermarkLabel setHidden:YES];
    } else [self.watermarkLabel setHidden:NO];
    
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"off_date"]) {
        [self.dateLabel setHidden:YES];
        [self.timeLabel setHidden:YES];
        [self.playLabel setHidden:YES];
        [self.playTriangleBTN setHidden:YES];
        
    } else {
        [self.dateLabel setHidden:NO];
        [self.timeLabel setHidden:NO];
        [self.playLabel setHidden:NO];
        [self.playTriangleBTN setHidden:NO];
    }
    
    NSString* fakeTime = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_time_created"];
    if ([fakeTime isEqualToString:@""] || !fakeTime) fakeTime = [Util getCurrentTime]; // Default text    
    NSString *fakeDate = [[NSUserDefaults standardUserDefaults] stringForKey:@"fake_date_created"];
    
    [self.dateLabel setText:fakeDate];
    [self.timeLabel setText:fakeTime];
    
    if (!dontResetSession) [self resetSession];
    else dontResetSession = NO;
    
    for (int index=0; index < [vGlitchFrames count]; index++) {
        UIImageView *view = [vGlitchFrames objectAtIndex:index];
        view.image = [UIImage imageWithContentsOfFile:[Util getOneLinerDistortion:index+1]];
    }
    
    //vRedBand.hidden     = YES;
    
    [_recorder startHDistortionTimer];
    [_sidesAnimView startAnimating];
    
    vGlitchViewControlTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                                               target:self
                                                             selector:@selector(updateGlitchView)
                                                             userInfo:nil
                                                              repeats:YES];
    
    NSString *vEffectName   = [effects objectAtIndex:currentEffectIndex];
    BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
    
    if (isVaporEffect) { //Vapor Effect
        NSString *vaporFileName = [vEffectThumbs objectAtIndex:currentEffectIndex];
        vVaporView.image  = [UIImage imageWithContentsOfFile:[Util getVaporDistortion:vaporFileName]];
        vVaporView.hidden = !isVaporEffect;
    }
    
    /*[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showRedBand) object:nil];
    [self performSelector:@selector(showRedBand) withObject:nil afterDelay:5.0];*/
    
    /*vOneLinerFrameChanger = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                             target:self
                                                           selector:@selector(changeHNoiseFrame)
                                                           userInfo:nil
                                                            repeats:YES];*/
    //timer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];    
    
    NSString *line1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"text_1"];
    NSString *line2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"text_2"];
    NSString *text  = [NSString stringWithFormat:@"%@\n%@", line1, line2];
    vRetroText.text   = text;
    
    NSString *color   = [[NSUserDefaults standardUserDefaults] objectForKey:@"text_color"];
    UIColor  *uicolor = [Util getRetroTextColor:color];
    vRetroText.textColor = uicolor;
    _dateLabel.textColor = uicolor;
    _timeLabel.textColor = uicolor;
    _watermarkLabel.textColor = uicolor;
    _playLabel.textColor = uicolor;
    
    if ([color isEqualToString:@"white"]) {
        [self.playTriangleBTN setImage:[UIImage imageNamed:@"play_triangle3x.png"] forState:UIControlStateNormal];
    } else if ([color isEqualToString:@"red"]) {
        [self.playTriangleBTN setImage:[UIImage imageNamed:@"play_triangle3x_r.png"] forState:UIControlStateNormal];
    } else if ([color isEqualToString:@"yellow"]) {
        [self.playTriangleBTN setImage:[UIImage imageNamed:@"play_triangle3x_y.png"] forState:UIControlStateNormal];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_recorder previewViewFrameChanged];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_isLaunch) {
        defaultFontSize = self.watermarkLabel.font.pointSize;
        self.previewView.autoresizesSubviews = YES;
        
        /*SCFilterImageView */scimageview = [[SCFilterImageView alloc] initWithFrame:self.previewView.bounds];
        scimageview.contentMode = UIViewContentModeScaleAspectFill;
        scimageview.filter = [SCFilter filterWithCIFilterName:[effects objectAtIndex:currentEffectIndex]];
        
        _recorder.SCImageView = scimageview;
        [self.previewView addSubview:scimageview];
        
        vRecordingPreview = [[UIImageView alloc] initWithFrame:self.previewView.bounds];
        vRecordingPreview.contentMode = UIViewContentModeScaleAspectFill;
        vRecordingPreview.hidden = YES;
        
        _recorder.vRecordingPreview = vRecordingPreview;
        [self.previewView addSubview:vRecordingPreview];
        
        _isLaunch            = NO;
    }
    
    resetTilt = YES;
    
    _playerViewController.view.frame = self.previewView.frame;
    _playerViewController.view.hidden = NO;
    
    [[_playerViewController player] play];
    [_recorder startRunning];
    [MBProgressHUD hideHUDForView:self.camBorders animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_recorder stopRunning];
    [_recorder.session deinitialize];
    
    [_recorder removeHDistortionDisplayTimer];
    [_sidesAnimView stopAnimating];
    [self removeGlitchTimer];
    
    for (int index=0; index < [vGlitchFrames count]; index++) {
        UIImageView *view = [vGlitchFrames objectAtIndex:index];
        view.image = nil;
    }
    
    NSString *vEffectName   = [effects objectAtIndex:currentEffectIndex];
    BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
    if (isVaporEffect) { //Vapor Effect
        vVaporView.image  = nil;
        vVaporView.hidden = YES;
    }
    
    /*if (vRedBandMoveTimer) {
        [vRedBandMoveTimer invalidate];
        vRedBandMoveTimer = nil;
    }*/
    
    /*[vOneLinerFrameChanger invalidate];
    vOneLinerFrameChanger = nil;*/
    
    //[timer invalidate];
    //timer = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {    
    if ([segue.destinationViewController isKindOfClass:[ShareViewController class]]) {
        ShareViewController *shareController = segue.destinationViewController;
        if (_cammode == CameraModeVideo) {
            shareController.videUrl         = _recorder.session.outputUrl;
            shareController.customAudioUrl  = customAudioUrl;
            shareController.useTime         = self.timeLabel.text;
            shareController.filterName      = [effects objectAtIndex:currentEffectIndex];
            shareController.vaporFilterIndex = currentEffectIndex;
            shareController.cammode         = _cammode;
            shareController.capturedPhoto   = nil;
            shareController.useCustomAudio  = useCustomAudio;
            shareController.vAudioStartTime = kCMTimeZero;
            shareController.vRetroTitleText = [vRetroText text];
            shareController.fontRatio       = vRetroText.font.pointSize / (vRetroText.frame.size.width);
            shareController.vRetroTitleVisibleRanges = vRetroTitleVisibleRanges;
            
        } else {
            shareController.videUrl         = nil;
            shareController.customAudioUrl  = nil;
            shareController.useTime         = self.timeLabel.text;
            shareController.filterName      = nil;
            shareController.cammode         = _cammode;
            shareController.capturedPhoto   = _capturedPhoto;
            shareController.useCustomAudio  = NO;
            shareController.vAudioStartTime = kCMTimeZero;
            shareController.vRetroTitleText = [vRetroText text];
            shareController.vRetroTitleVisibleRanges = nil;
            shareController.fontRatio       = vRetroText.font.pointSize / (vRetroText.frame.size.width);
        }
    } else if ([segue.destinationViewController isKindOfClass:[ImportViewController class]]) {
        ImportViewController *pController = segue.destinationViewController;
        
        if (_cammode == CameraModeVideo) {
            pController.videoUrl = importedVideoUrl;
            pController.customAudioUrl = customAudioUrl;
            pController.useTime        = self.timeLabel.text;
            pController.cammode        = _cammode;
            pController.capturedPhoto  = nil;
            pController.useCustomAudio = useCustomAudio;
            
        } else {
            pController.videoUrl       = nil;
            pController.customAudioUrl = nil;
            pController.useTime        = self.timeLabel.text;
            pController.cammode        = _cammode;
            pController.capturedPhoto  = _capturedPhoto;
            pController.useCustomAudio = NO;
        }
    }
}

- (IBAction)unwindToRecorderViewController:(UIStoryboardSegue *)segue {
    [_recorder setIsVideoImported:NO];
    [_recorder setIsPhotoImported:NO];
    
    _recorder.vRGBControlRatio          = 17.0f;
    _recorder.vContrastControlRatio     = 0.75f;
    //_recorder.vBirghtnessControlRatio   = 0.0f;
    _recorder.vNoiseControlRatio        = MAX_DELAY_PER_GLITCH - 1.0f;
}

- (IBAction)unwindToRecorder2:(UIStoryboardSegue *)segue {
}

#pragma mark processing

- (void)checkHDistortionTimer {
    if ([_recorder checkHDistortionTimerRunning]) {
        [_recorder removeHDistortionDisplayTimer];
        [self removeGlitchTimer];
        
        [_recorder startHDistortionTimer];
        vGlitchViewControlTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                                                   target:self
                                                                 selector:@selector(updateGlitchView)
                                                                 userInfo:nil
                                                                  repeats:YES];
    }
}

- (void)invalidateZoomTimer {
    if (vZoomTimer) {
        [vZoomTimer invalidate];
        vZoomTimer = nil;
    }
}

- (void)updateGlitchView {
    if (_recorder.showHDistortion) {
        _recorder.showHDistortion = NO;
        
        NSInteger    noofFrames = vGlitchFrames.count;
        NSInteger    index      = arc4random_uniform((uint32_t) noofFrames);
        UIImageView *vGView     = [vGlitchFrames objectAtIndex:index];
        CGFloat      yPos;
        
        hGlitchCounter++;
        
        if (hGlitchCounter > 3) {
            hGlitchCounter = 1;
            yPos = arc4random_uniform((uint32_t) (self.previewView.frame.size.height*0.75f));
            
        } else {
            yPos  = arc4random_uniform((uint32_t) (self.previewView.frame.size.height*0.25f));
            yPos += self.previewView.frame.size.height*0.7f;
        }
        
        yPos += self.previewView.frame.origin.y;
        
        CGRect gFrame     = vGView.frame;
        gFrame.origin.y   = yPos;
        vGView.frame      = gFrame;
        vGView.hidden     = NO;
        
        for (int i=0; i<[vGlitchFrames count]; i++)
            if (i != index) [vGlitchFrames objectAtIndex:i].hidden = YES;
        
    } else {
        vGlitchView1.hidden = YES;
        vGlitchView2.hidden = YES;
        vGlitchView3.hidden = YES;
        vGlitchView4.hidden = YES;
        vGlitchView5.hidden = YES;
    }
}

- (void)removeGlitchTimer {
    if (vGlitchViewControlTimer) {
        [vGlitchViewControlTimer invalidate];
        vGlitchViewControlTimer = nil;
    }
}

- (void)updateTimeRecordedLabel {
    CMTime currentTime = kCMTimeZero;
    
    if (_recorder.session != nil) {
        currentTime = _recorder.session.duration;
    }
    
    int mins  = CMTimeGetSeconds(currentTime)/60;
    int sec   = CMTimeGetSeconds(currentTime) - (mins*60);
    int hours = mins/60;
    
    self.timeRecordedLabel.text = [NSString stringWithFormat:@"%02i:%02i:%02i", hours,mins,sec];
}

- (void)updateTime {
    NSString *newDateString = [Util getCurrentTime];
    _timeLabel.text         = newDateString;
}

- (void)resetSession {
    SCRecordSession *recordSession = _recorder.session;
    
    if (recordSession != nil) {
        _recorder.session = nil;
        
        // If the recordSession was saved, we don't want to completely destroy it
        if ([[SCRecordSessionManager sharedInstance] isSaved:recordSession]) {
            [recordSession endSegmentWithInfo:nil completionHandler:nil];
        } else {
            [recordSession cancelSession:nil];
        }
    }
    
    useCustomAudio = NO;
    customAudioUrl = nil;
    _recorder.audioConfiguration.enabled = YES;
    
    [self prepareSession];
}

- (void)prepareSession {
    if (_recorder.session == nil) {
        
        SCRecordSession *session = [SCRecordSession recordSession];
        session.fileType = AVFileTypeMPEG4;//AVFileTypeQuickTimeMovie;
        
        _recorder.session = session;
    }
    
    [self updateTimeRecordedLabel];
}

- (void)saveAndShowSession:(SCRecordSession *)recordSession withLoadingMessage:(NSString*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.camBorders animated:YES].label.text = message;
        
        [[SCRecordSessionManager sharedInstance] saveRecordSession:recordSession];
        [self saveToCameraRoll];
    });
}

- (void)addTiltSegmentAt:(int)index {
    NSString *fileName = [self getNextLowResTilt];
    NSString *stringVideoPath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSAssert(stringVideoPath, @"Expected not nil video file");
    
    NSURL *url = [NSURL fileURLWithPath:stringVideoPath];
    NSAssert(url, @"Expected not nil video url");

    SCRecordSessionSegment *segment = [SCRecordSessionSegment segmentWithURL:url info:nil];
    if (index == SENTINEL_TILT_SEGMENT) {
        [_recorder.session addSegment:segment];
    } else [_recorder.session insertSegment:segment atIndex:index];
}

-(NSString*)getNextTilt {
    nextTiltAvailableCounter++;
    if (nextTiltAvailableCounter > 4) nextTiltAvailableCounter = 1;

    NSString *preset = _recorder.bestCapturePreset; NSString *fileName = @"";
    if (preset == AVCaptureSessionPreset1920x1080) {
        fileName = [NSString stringWithFormat:@"tilt/1920X1080/%i.mp4", nextTiltAvailableCounter];
    } else if (preset == AVCaptureSessionPreset1280x720) {
        fileName = [NSString stringWithFormat:@"tilt/1280X720/%i.mp4", nextTiltAvailableCounter];
    } else {
        fileName = [NSString stringWithFormat:@"tilt/1280X720/%i.mp4", nextTiltAvailableCounter];
    }
    NSLog(@"getNextTilt = %i", nextTiltAvailableCounter);
    return fileName;
}

-(NSString*)getNextLowResTilt {
    NSLog(@"getNextLowResTilt = %i", nextTiltAvailableCounter);
    NSString *fileName = [NSString stringWithFormat:@"tilt/192X144/%i.mp4", nextTiltAvailableCounter];
    return fileName;
}

/*
- (void)prepareDistortionLines {
    CGSize mediaSize = _capturedPhoto.size;
    
    if (_cammode == CameraModeVideo) {
        AVAsset       *asset     = [SCRecorder sharedRecorder].session.assetRepresentingSegments;
        AVAssetTrack  *track     = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
        CGSize         mSize     = CGSizeApplyAffineTransform(track.naturalSize, track.preferredTransform);
        
        mediaSize = CGSizeMake(fabs(mSize.width), fabs(mSize.height));
    }
    
    CGRect vDrawRect = CGRectMake(0, 0, mediaSize.width, mediaSize.height);
    for (int count =1; count<=5; count++) {
        UIGraphicsBeginImageContextWithOptions(mediaSize, NO, 0.0);
        NSString *filePath   = [Util getLowResLineImageWithIndex:count];
        UIImage  *videoFrame = [UIImage imageWithContentsOfFile:filePath];
        [videoFrame drawInRect:vDrawRect blendMode:kCGBlendModeNormal alpha:1];
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CIImage *ciimage = [CIImage imageWithCGImage:newImage.CGImage];
        [[SCRecorder sharedRecorder].lowReslines addObject:ciimage];
        
        UIGraphicsEndImageContext();
    }
}*/

- (void)saveToCameraRoll {
    //[self prepareDistortionLines];
    if (!useCustomAudio) {
        useCustomAudio = YES;
        customAudioUrl = [audioRecorder getAudioOutputFileURL];
    }
    [self performSegueWithIdentifier:@"share_view" sender:self];
}

- (int)getCounter {
    return counter;
}

- (void)setCounter:(int)newValue {
    counter = newValue;
}

- (void)exportDidFinish:(AVAssetExportSession*)session {
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        SCSaveToCameraRollOperation *saveToCameraRoll = [SCSaveToCameraRollOperation new];
        [saveToCameraRoll saveVideoURL:outputURL completion:^(NSString * _Nullable path, NSError * _Nullable error) {
            if (error == nil) {
                [MBProgressHUD hideHUDForView:self.camBorders animated:YES];
                
                NSString* title = @"Saved To Album!";
                
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:title
                                                      message:nil
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                                  handler:^(UIAlertAction *action) {
                                                                      if(![Util isPremiumUser]
                                                                         && [ALInterstitialAd isReadyForDisplay])
                                                                          [ALInterstitialAd show];
                                                                  }]];
                
                //[self presentViewController:alertController animated:YES completion:nil];
                
            } else {
                NSString* title = @"Failed To Save!";
                
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:title
                                                      message:error.localizedDescription
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                                  handler:^(UIAlertAction *action) {
                                                                      if(![Util isPremiumUser]
                                                                         && [ALInterstitialAd isReadyForDisplay])
                                                                          [ALInterstitialAd show];
                                                                  }]];
                
                [self presentViewController:alertController animated:YES completion:nil];
                [MBProgressHUD hideHUDForView:self.camBorders animated:YES];
            }
            
            [self resetSession];
        }];
    }
}

- (void)nextEffect {
    if ([_recorder isRecording]) {
        return;
    }
    
    NSString *vEffectName   = [effects objectAtIndex:currentEffectIndex];
    SCFilter *vSCFilter     = [SCFilter filterWithCIFilterName:vEffectName];
    UIImage  *vVaporImage   = nil;
    BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
    BOOL      isGreenEffect = [vEffectName isEqualToString:@"CIColorCube"];
    
    if (isGreenEffect) {
        NSURL *fileURL = [NSURL fileURLWithPath:[Util getVaporDistortion:@"green_effect_bg"]];
        [SCRecorder sharedRecorder].vGreenEffectBGCIImage = [CIImage imageWithContentsOfURL:fileURL];
    } else [SCRecorder sharedRecorder].vGreenEffectBGCIImage = nil;
    
    if (isVaporEffect) { //Vapor Effect
        NSString *vaporFileName = [vEffectThumbs objectAtIndex:currentEffectIndex];
        vVaporImage  = [UIImage imageWithContentsOfFile:[Util getVaporDistortion:vaporFileName]];
    }
    
    vVaporView.image  = vVaporImage;
    vVaporView.hidden = !isVaporEffect;
    
    if (!_recording) [(SCFilterImageView *)_recorder.SCImageView setFilter:vSCFilter];
    _recorder.videoConfiguration.filter = vSCFilter;
}

#pragma mark IBActions

- (IBAction)plusControlEffect:(id)sender {
    UIButton *plusSender = sender;
    CGFloat newRatio;
    
    switch (plusSender.tag) {
        
        case 1010:          //Contrast range 0-2
            newRatio = _recorder.vContrastControlRatio + 0.2f;
            if (newRatio <= 2.0f) [_recorder setVContrastControlRatio:newRatio];
            else [_recorder setVContrastControlRatio:2.0f];
            
            break;
            
        case 2020:         //RGB 0-100
            newRatio = _recorder.vRGBControlRatio + 10.0f;
            if (newRatio <= 100.0f) [_recorder setVRGBControlRatio:newRatio];
            else [_recorder setVRGBControlRatio:100.0f];
            
            break;
            
        case 3030: //Glitch range 0-2
            hCurrentGlitchSlidingValue = hCurrentGlitchSlidingValue + 0.2f;
            if (hCurrentGlitchSlidingValue > 2.0f) hCurrentGlitchSlidingValue = 2.0f;
            
            [_recorder setVNoiseControlRatio:(MAX_DELAY_PER_GLITCH - hCurrentGlitchSlidingValue)];
            [self checkHDistortionTimer];
            
            break;
            
        default:
            break;
    }
}

- (IBAction)minusControlEffect:(id)sender {
    UIButton *plusSender = sender;
    CGFloat newRatio;
    
    switch (plusSender.tag) {
    
        case 1011: //Contrast range 0-2
            newRatio = _recorder.vContrastControlRatio - 0.2f;
            if (newRatio >= 0.0f) [_recorder setVContrastControlRatio:newRatio];
            else [_recorder setVContrastControlRatio:0.0f];
            
            break;
            
        case 2021: //RGB range 0-100
            newRatio = _recorder.vRGBControlRatio - 10.0f;
            if (newRatio >= 0.0f) [_recorder setVRGBControlRatio:newRatio];
            else [_recorder setVRGBControlRatio:0.0f];
            
            break;
            
        case 3031: //Glitch range 0-2
            hCurrentGlitchSlidingValue = hCurrentGlitchSlidingValue - 0.2f;
            if (hCurrentGlitchSlidingValue < 0.0f) hCurrentGlitchSlidingValue = 0.0f;
            
            [_recorder setVNoiseControlRatio:(MAX_DELAY_PER_GLITCH - hCurrentGlitchSlidingValue)];
            [self checkHDistortionTimer];
            
            break;
            
        default:
            break;
    }
}


- (IBAction)rgbControlAction:(id)sender {
    UISlider *slider = (UISlider *) sender;
    [_recorder setVRGBControlRatio:slider.value];
}

- (IBAction)contrastControlAction:(id)sender {
    UISlider *slider = (UISlider *) sender;
    [_recorder setVContrastControlRatio:slider.value];
}

- (IBAction)brightControlAction:(id)sender {
    //UISlider *slider = (UISlider *) sender;
    //[_recorder setVBirghtnessControlRatio:slider.value];
}

- (IBAction)noiseControlAction:(id)sender {
    UISlider *slider = (UISlider *) sender;
    [_recorder setVNoiseControlRatio:(MAX_DELAY_PER_GLITCH - slider.value)];
    [self checkHDistortionTimer];
}

- (IBAction)showFiltersPanel:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        if (vTrailFromControlToRightBG.isActive)
            [self showControlPanel:nil];
        
    if (vFilterPanelTrailToSuperview.isActive) { // Show Now
        if (![_recorder isRecording]) {
            [self.view removeConstraint:vFilterPanelTrailToSuperview];
            [self.view addConstraint:vFilterPanelTrailToRightBG];
            
            //[_recorder stopRunning];
            //[_recorder.session deinitialize];
        }
        
    } else { // Hide Now
        [self.view removeConstraint:vFilterPanelTrailToRightBG];
        [self.view addConstraint:vFilterPanelTrailToSuperview];
        
        //[_recorder startRunning];
    }
}

- (IBAction)showControlPanel:(id)sender {
    if (vFilterPanelTrailToRightBG.isActive) {
        [self showFiltersPanel:nil];
    }
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) return;
    
    if (vTrailFromControlToSuperview.isActive) { // Show Now
        [self.view removeConstraint:vTrailFromControlToSuperview];
        [self.view addConstraint:vTrailFromControlToRightBG];
        //[vControlPanelHandler setTransform:CGAffineTransformScale(vControlPanelHandler.transform, -1.0, 1.0)];
        
    } else { // Hide Now
        [self.view removeConstraint:vTrailFromControlToRightBG];
        [self.view addConstraint:vTrailFromControlToSuperview];
        
        //[vControlPanelHandler setTransform:CGAffineTransformScale(vControlPanelHandler.transform, -1.0, 1.0)];
    }
}

- (IBAction)openCameraRoll {
    if (![Util isPremiumUser]) {
        //[self performSegueWithIdentifier:@"recorder_to_premium" sender:self];
        [Util presentPremiumPage:self];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.camBorders animated:YES].label.text = @"Opening The Gallery";
    
    NonRotatingUIImagePickerController *picker = [[NonRotatingUIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.videoQuality = UIImagePickerControllerQualityType640x480;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString*)kUTTypeImage, (NSString*)kUTTypeMovie, nil];
    //picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)changeMode {
    if ([_recorder isRecording]) {
        return;
    }
    
    SCVideoConfiguration *video = _recorder.videoConfiguration;
    if (vSquareAspectRatio.isActive) {
        _camBorders.hidden = YES;
        _previewView.hidden = YES;
        
        [_camBorders removeConstraint:vSquareAspectRatio];
        [_previewView removeConstraint:vPreviewSquareAspectRatio];
        [_camBorders addConstraint:vLandscapeAspectRatio];
        [_previewView addConstraint:vPreviewLandscapeAspectRatio];

        video.sizeAsPortrait = NO;
        video.sizeAsSquare = NO;
        video.size = CGSizeMake(352, 198/*288*/);

        vPlayTextTopConstraint.constant = 15;
        vRecordTimeBottomConstraint.constant = -15;
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            vSideGlitchTrailingConstraint.constant = 14;
            vSideGlitchLeadingConstraint.constant = -14;
            
        } else {
            vSideGlitchTrailingConstraint.constant = 5;
            vSideGlitchLeadingConstraint.constant = -5;
        }
        
        self.dateLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*0.90f];
        self.timeLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*0.90f];
        self.playLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*0.90f];
        self.timeRecordedLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*0.90f];
        self.watermarkLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*0.90f];
        vRetroText.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*0.90f];
        
        [_recorder stopRunning];
        [_recorder.session deinitialize];
        
        [self performSelector:@selector(updateFilteredViewFrame) withObject:nil afterDelay:0.025f];
        
    } else if (vLandscapeAspectRatio.isActive) {
        _camBorders.hidden = YES;
        _previewView.hidden = YES;
        
        [_camBorders removeConstraint:vLandscapeAspectRatio];
        [_previewView removeConstraint:vPreviewLandscapeAspectRatio];
        [_camBorders addConstraint:vPortraitAspectRatio];
        [_previewView addConstraint:vPreviewPortraitAspectRatio];

        video.sizeAsPortrait = YES;
        video.sizeAsSquare = NO;
        video.size = CGSizeMake(162/*240*/, 288);
        
        CGFloat fontScale = 0.75f;
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            vPlayTextTopConstraint.constant = 45;
            vRecordTimeBottomConstraint.constant = -35;
            vRecordTimeLeadingConstraint.constant = 20;
            vWatermarkTrailingConstraint.constant = -20;
            vSideGlitchTrailingConstraint.constant = 12;
            vSideGlitchLeadingConstraint.constant = -12;
            
        } else {
            vPlayTextTopConstraint.constant = 22;
            vRecordTimeBottomConstraint.constant = -18;
            vRecordTimeLeadingConstraint.constant = 8;
            vWatermarkTrailingConstraint.constant = -8;
            vSideGlitchTrailingConstraint.constant = 4;
            vSideGlitchLeadingConstraint.constant = -4;
            
            fontScale = 0.65f;
        }
        
        self.dateLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*fontScale];
        self.timeLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*fontScale];
        self.playLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*fontScale];
        self.timeRecordedLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*fontScale];
        self.watermarkLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*fontScale];
        vRetroText.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize*fontScale];
        
        [_recorder stopRunning];
        [_recorder.session deinitialize];
        
        [self performSelector:@selector(updateFilteredViewFrame) withObject:nil afterDelay:0.025f];
        
    } else if (vPortraitAspectRatio.isActive) {
        _camBorders.hidden = YES;
        _previewView.hidden = YES;
        
        [_camBorders removeConstraint:vPortraitAspectRatio];
        [_previewView removeConstraint:vPreviewPortraitAspectRatio];
        [_camBorders addConstraint:vSquareAspectRatio];
        [_previewView addConstraint:vPreviewSquareAspectRatio];
        
        video.sizeAsPortrait = NO;
        video.sizeAsSquare = YES;
        video.size = CGSizeMake(288, 288);
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            vPlayTextTopConstraint.constant = 45;
            vRecordTimeBottomConstraint.constant = -35;
            vRecordTimeLeadingConstraint.constant = 35;
            vWatermarkTrailingConstraint.constant = -35;
            vSideGlitchTrailingConstraint.constant = 21;
            vSideGlitchLeadingConstraint.constant = -21;
            
        } else {
            vPlayTextTopConstraint.constant = 22;
            vWatermarkTrailingConstraint.constant = -17;
            vRecordTimeBottomConstraint.constant = -5;
            vRecordTimeLeadingConstraint.constant = 15;
            vSideGlitchTrailingConstraint.constant = 10;
            vSideGlitchLeadingConstraint.constant = -10;
        }
        
        self.dateLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize];
        self.timeLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize];
        self.playLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize];
        self.timeRecordedLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize];
        self.watermarkLabel.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize];
        vRetroText.font = [UIFont fontWithName:@"VCROSDMONO" size:defaultFontSize];
        
        [_recorder stopRunning];
        [_recorder.session deinitialize];
        
        [self performSelector:@selector(updateFilteredViewFrame) withObject:nil afterDelay:0.025f];
    }
}

- (IBAction)previousEffect:(id)sender {
    currentEffectIndex--;
    if (currentEffectIndex < 0) {
        currentEffectIndex = 0;
        return;
    }
    
    [self nextEffect];
}

- (IBAction)nextEffectAction:(id)sender {
    currentEffectIndex++;
    if (currentEffectIndex >= [effects count]) {
        currentEffectIndex = [effects count] - 1.0f;
        return;
    }
    
    [self nextEffect];
}

- (void)updateFilteredViewFrame {
    scimageview.frame = self.previewView.bounds;
    vRecordingPreview.frame = self.previewView.bounds;
    _playerViewController.view.frame = self.previewView.frame;
    
    [scimageview setNeedsDisplay];
    [vRecordingPreview setNeedsDisplay];
    
    [_recorder startRunning];
    
    _camBorders.hidden = NO;
    _previewView.hidden = NO;
}

- (IBAction)tapOnScreenGesture:(UITapGestureRecognizer*)recognizer {
    if (_recording) {
        [_recorder pause:^{
            resetTilt       = YES;
            resumeRecording = YES;

            [_playerViewController.view setHidden:NO];
            [[_playerViewController player] play];
            
            [self addTiltSegmentAt:SENTINEL_TILT_SEGMENT];
            //[_recorder record];
        }];
    } else {
        resetTilt       = YES;
        resumeRecording = NO;

        [_playerViewController.view setHidden:NO];
        [[_playerViewController player] play];
    }
}

-(IBAction) openSettings {
    [self performSegueWithIdentifier:@"settings" sender:self];
}

- (IBAction)addTextOnPreviewLayer:(id)sender {
    BOOL isHidden = [vRetroText isHidden];

    if ([_recorder isRecording]) {
        if (isHidden) {
            retroStartTime = CMTimeGetSeconds(_recorder.session.duration);
        } else {
            retroStopTime = CMTimeGetSeconds(_recorder.session.duration);
            [vRetroTitleVisibleRanges addObject:[NSValue valueWithCGPoint:CGPointMake(retroStartTime, retroStopTime)]];
            
            retroStartTime = 0.0;
            retroStopTime  = 0.0;
        }
    }
    [vRetroText setHidden:!isHidden];
}

- (IBAction)startRecording {
    _cammode = CameraModeVideo;
    
    if (!_recording) {
        if (vFilterPanelTrailToRightBG.isActive) [self showFiltersPanel:nil];
        [vRetroTitleVisibleRanges removeAllObjects];
        
        _recording                = YES;
        vRecordingPreview.hidden  = NO;
        _timeRecordedLabel.hidden = NO;
        scimageview.hidden        = YES;
        _recorder.SCImageView     = nil;
        
        [self.recordLabel setText:@"STOP"];
        [self.recBTN setImage:[UIImage imageNamed:@"rec_on_ipad.png"] forState:UIControlStateNormal];
        
        if (![vRetroText isHidden])
            retroStartTime = CMTimeGetSeconds(_recorder.session.duration);
        
        if (useCustomAudio) [playeraudio play];
        else [audioRecorder beginRecording];
        
        [_recorder record];
        [[SCRecorder sharedRecorder] setTiltDistortionOn:YES];
        
    } else {
        _recording = NO;
        
        _recorder.SCImageView     = scimageview;
        scimageview.hidden        = NO;
        vRecordingPreview.hidden  = YES;
        _timeRecordedLabel.hidden = YES;
        
        [self.recordLabel setText:@"RECORD"];
        [self.recBTN setImage:[UIImage imageNamed:@"rec_ipad.png"] forState:UIControlStateNormal];
        
        [self endVideoCapture];
        
        if (useCustomAudio) [playeraudio pause];
        else [audioRecorder endRecording];
    }
}

- (IBAction)endVideoCapture {
    [_recorder pause:^{
        if (![vRetroText isHidden]) {
            retroStopTime = CMTimeGetSeconds(_recorder.session.duration);
            [vRetroTitleVisibleRanges addObject:[NSValue valueWithCGPoint:CGPointMake(retroStartTime, retroStopTime)]];
            
            retroStartTime = 0.0;
            retroStopTime  = 0.0;
        }
        [self saveAndShowSession:_recorder.session withLoadingMessage:@""];
    }];
}

- (IBAction)zoomPlusUp {
    [self invalidateZoomTimer];
}

- (IBAction)zoomPlusDown {
    if (vZoomTimer) {
        return;
    }
    
    [self zoomPlus];
    vZoomTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(zoomPlus) userInfo:nil repeats:YES];
}

- (IBAction)zoomPlus {
    /*if (_hasZoomPlusLastTime) {
        zoomCounter++;
        if (zoomCounter > 3) {
            zoomCounter = 3;
        }
    }
    
    _hasZoomPlusLastTime = YES;*/

    CGFloat maxZoomPossible = [[SCRecorder sharedRecorder] maxVideoZoomFactor];
    CGFloat zoomDiff = maxZoomPossible - defaultZoomLevel;
    CGFloat zoomStep = 0.025;
    if (zoomDiff > 0) {
        /*CGFloat zoomStep = 0.25;
        float s[4] =
        {
            defaultZoomLevel + zoomStep,
            defaultZoomLevel + zoomStep*2,
            defaultZoomLevel + zoomStep*3,
            defaultZoomLevel + zoomStep*4
        };
        
        [[SCRecorder sharedRecorder] setVideoZoomFactor:s[zoomCounter]];*/
        
        CGFloat newZoomFactor = [[SCRecorder sharedRecorder] videoZoomFactor] + zoomStep;
        if (newZoomFactor <= maxZoomPossible) {
            [[SCRecorder sharedRecorder] setVideoZoomFactor:newZoomFactor];
        } else [self invalidateZoomTimer];
        
    } else {
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            CGFloat scale = self.previewView.transform.a;
            CGFloat newScale = scale + zoomStep;
            if (newScale <= 2.0f) {
                self.previewView.transform = CGAffineTransformMakeScale(newScale, newScale);
            } else [self invalidateZoomTimer];

            /*float s[4] = {1.25,1.5,1.75,2.0};
            self.previewView.transform = CGAffineTransformMakeScale(s[zoomCounter], s[zoomCounter]);*/
            
        } completion:^(BOOL finished) {}];
        
        /*
        // Lesser cases
        CGRect frame = captureVideoPreviewLayer.frame;
        float width = frame.size.width * zoomLevel;
        float height = frame.size.height * zoomLevel;
        float x = (frame.size.width - width)/2;
        float y = (frame.size.height - height)/2;
        captureVideoPreviewLayer.bounds = CGRectMake(x, y, width, height);
         */
    }
}

- (IBAction)zoomMinusUp {
    [self invalidateZoomTimer];
}

- (IBAction)zoomMinusDown {
    if (vZoomTimer) {
        return;
    }
    
    [self zoomMinus];
    vZoomTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(zoomMinus) userInfo:nil repeats:YES];
}

- (IBAction)zoomMinus {
    /*if (!_hasZoomPlusLastTime) {
        zoomCounter--;
        if (zoomCounter < 0) {
            zoomCounter = 0;
        }
    }
    
    _hasZoomPlusLastTime = NO;*/
    
    CGFloat maxZoomPossible = [[SCRecorder sharedRecorder] maxVideoZoomFactor];
    CGFloat zoomDiff = maxZoomPossible - defaultZoomLevel;
    CGFloat zoomStep = 0.025;
    
    if (zoomDiff > 0) {
        CGFloat newZoomFactor = [[SCRecorder sharedRecorder] videoZoomFactor] - zoomStep;
        if (newZoomFactor >= defaultZoomLevel) {
            [[SCRecorder sharedRecorder] setVideoZoomFactor:newZoomFactor];
        } else [self invalidateZoomTimer];
        
        /*CGFloat zoomStep = 0.25;
        float s[4] = {defaultZoomLevel, defaultZoomLevel + zoomStep, defaultZoomLevel + zoomStep*2, defaultZoomLevel + zoomStep*3};
        [[SCRecorder sharedRecorder] setVideoZoomFactor:s[zoomCounter]];*/
    
    } else {
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            CGFloat scale = self.previewView.transform.a;
            CGFloat newScale = scale - zoomStep;
            if (newScale >= 1.0f) {
                self.previewView.transform = CGAffineTransformMakeScale(newScale, newScale);
            } else [self invalidateZoomTimer];
            
            /*float s[4] = {1.0,1.25,1.5,1.75};
            self.previewView.transform = CGAffineTransformMakeScale(s[zoomCounter], s[zoomCounter]);*/
        } completion:^(BOOL finished) {}];
    }
}

- (IBAction)reverseCamera:(id)sender {
    [self tapOnScreenGesture:nil];
    [_recorder switchCaptureDevices];
}

- (IBAction)switchFlash:(id)sender {
    switch (_recorder.flashMode) {
        case SCFlashModeOff:
            _recorder.flashMode = SCFlashModeLight;
            break;
        case SCFlashModeLight:
            _recorder.flashMode = SCFlashModeOff;
            break;
        default:
            break;
    }
}

- (IBAction)musicAction:(id)sender {
    if (![Util isPremiumUser]) {
        //[self performSegueWithIdentifier:@"recorder_to_premium" sender:self];
        [Util presentPremiumPage:self];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.camBorders animated:YES].label.text = @"Loading Music";
    
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = NO;
    [self presentViewController:mediaPicker animated:YES completion:nil];
}

- (IBAction)downloadAction:(id)sender { }

- (IBAction)photoAction:(id)sender {
    [_recorder capturePhoto:^(NSError * _Nullable error, UIImage * _Nullable image) {
        if (image) {
            if (_recorder.device == AVCaptureDevicePositionFront) {
                image = [Util flippedImage:image ByAxis:MVImageFlipXAxis];
            }
            
            BOOL isPortraitMode  = [SCRecorder sharedRecorder].videoConfiguration.sizeAsPortrait;
            BOOL isSquareMode    = [SCRecorder sharedRecorder].videoConfiguration.sizeAsSquare;
            
            CGFloat width  = image.size.height;
            CGFloat offset = image.size.width - width;
            CGRect visibleRect = CGRectMake(offset/2.0, 0, width, image.size.height);
            
            if (isPortraitMode) {
                width       = (9.0f/16.0f)*image.size.height;
                offset      = image.size.width - width;
                visibleRect = CGRectMake(offset/2.0, 0, width, image.size.height);
                
            } else if (!isSquareMode) { // Landscape
                CGFloat height = (9.0f/16.0f)*image.size.width;
                offset = image.size.height - height;
                visibleRect = CGRectMake(0, offset/2.0, image.size.width, height);
            }
            
            CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], visibleRect);
            image = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:image.imageOrientation];
            CGImageRelease(imageRef);
            
            _cammode        = CameraModePhoto;
            _capturedPhoto  = image;
            
            //[self prepareDistortionLines];
            _capturedPhoto = [self applyFiltersOnPhoto:image];
            [self performSegueWithIdentifier:@"share_view" sender:self];
            
        } else {
            NSLog(@"%@", error.localizedDescription);
        }
    }];
}

- (UIImage*)applyFiltersOnPhoto:(UIImage*)photo {
    SCRecorder *recorder = [SCRecorder sharedRecorder];
    
    CIFilter *ciFilter = [CIFilter filterWithName:[effects objectAtIndex:currentEffectIndex]];
    if (ciFilter == nil) return photo;
    
    CIImage *ciPhoto = [CIImage imageWithCGImage:photo.CGImage];
    [ciFilter setValue:ciPhoto forKey:kCIInputImageKey];
    ciPhoto = [ciFilter valueForKey:kCIOutputImageKey];
    
    CGFloat contrast    = recorder.vContrastControlRatio;
    //CGFloat brightness  = recorder.vBirghtnessControlRatio;
    
    CIFilter *contrastFilter = [CIFilter filterWithName:@"CIColorControls"];
    [contrastFilter setDefaults];
    [contrastFilter setValue:ciPhoto forKey:kCIInputImageKey];
    [contrastFilter setValue:[NSNumber numberWithFloat:contrast] forKey:kCIInputContrastKey];
    //[contrastFilter setValue:[NSNumber numberWithFloat:brightness] forKey:kCIInputBrightnessKey];
    ciPhoto = [contrastFilter valueForKey:kCIOutputImageKey];
    
    if (![[ciFilter name] isEqualToString:@"CIPhotoEffectNoir"] &&
        ![[ciFilter name] isEqualToString:@"CIPhotoEffectTonal"])
    {
        [_CIToneCurveFilter setInputImage:ciPhoto];
        [_CIToneCurveFilter setCurrentFXName:[ciFilter name]];
        ciPhoto = [_CIToneCurveFilter outputImage];
    }
    
    NSString *vEffectName   = [effects objectAtIndex:currentEffectIndex];
    BOOL      isVaporEffect = [vEffectName isEqualToString:@"CIPhotoEffectFade"];
    
    if (isVaporEffect) { //Vapor Effect
        NSString *vaporFileName = [vEffectThumbs objectAtIndex:currentEffectIndex];
                  vaporFileName = [vaporFileName stringByAppendingString:@"_288"];
        UIImage  *vVaporImage   = [UIImage imageWithContentsOfFile:[Util getVaporDistortion:vaporFileName]];
        CIImage  *receiver      = [CIImage imageWithCGImage:vVaporImage.CGImage];
        
        CGFloat  horizontalScale = ciPhoto.extent.size.width / receiver.extent.size.width;
        CGFloat  verticalScale   = ciPhoto.extent.size.height / receiver.extent.size.height;
        
        receiver = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
        ciPhoto  = [receiver imageByCompositingOverImage:ciPhoto];
    }
    
    /*CIImage *receiver = [[SCRecorder sharedRecorder].lowReslines objectAtIndex:0];
    CGFloat  scale    = ciPhoto.extent.size.width/receiver.extent.size.width;
    NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
    receiver = [receiver imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
    
    CGSize vSizeReceiver = receiver.extent.size;
    CGSize vSizeImage    = ciPhoto.extent.size;
    if (vSizeReceiver.height > vSizeImage.height ) {
        receiver = [receiver imageByCroppingToRect:CGRectMake(0, 0, vSizeReceiver.width, vSizeImage.height)];
    }
    ciPhoto = [receiver imageByCompositingOverImage:ciPhoto];*/

    NSInteger noofFrames = recorder.vOneLinerFrames.count;
    NSInteger index      = arc4random_uniform((uint32_t) noofFrames);
    CIImage  *receiver   = [recorder.vOneLinerFrames objectAtIndex:index];
    
    CGFloat scale = ciPhoto.extent.size.width/receiver.extent.size.width;
    NSDictionary<NSString *,id> *params = @{kCIInputScaleKey:[NSNumber numberWithFloat:scale]};
    receiver = [receiver imageByApplyingFilter:@"CILanczosScaleTransform" withInputParameters:params];
    
    CGSize vSizeReceiver = receiver.extent.size;
    CGSize vSizeImage    = ciPhoto.extent.size;
    if (vSizeReceiver.height > vSizeImage.height ) {
        receiver = [receiver imageByCroppingToRect:CGRectMake(0, 0, vSizeReceiver.width, vSizeImage.height)];
    }
    
    CGFloat           yPos   = arc4random_uniform((uint32_t) (ciPhoto.extent.size.height/4.0f /*- receiver.extent.size.height*/));
    CGAffineTransform trans  = CGAffineTransformMakeTranslation(0, yPos);
    NSValue          *value  = [NSValue valueWithBytes:&trans objCType:@encode(CGAffineTransform)];
    NSDictionary<NSString *,NSValue*> *tx = @{@"inputTransform": value};
    
    receiver   = [receiver imageByApplyingFilter:@"CIAffineTransform" withInputParameters:tx];
    ciPhoto    = [receiver imageByCompositingOverImage:ciPhoto];
    
    /*CGFloat horizontalScale = ciPhoto.extent.size.width / receiver.extent.size.width;
    CGFloat verticalScale   = ciPhoto.extent.size.height / receiver.extent.size.height;
    
    receiver   = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
    ciPhoto    = [receiver imageByCompositingOverImage:ciPhoto];*/
    
    /*CIImage */receiver = [[SCRecorder sharedRecorder].sidelines objectAtIndex:0];
    CGFloat horizontalScale = ciPhoto.extent.size.width / receiver.extent.size.width;
    CGFloat verticalScale   = ciPhoto.extent.size.height / receiver.extent.size.height;
    
    receiver = [receiver imageByApplyingTransform:CGAffineTransformMakeScale(horizontalScale, verticalScale)];
    ciPhoto  = [receiver imageByCompositingOverImage:ciPhoto];
    
    float vIphoneOffset = 1.0;
    /*if ( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ){
     vIphoneOffset = 0.75; // Its an iPhone
     }*/
    
    [SCWatermarkOverlayView setCameraMode:CameraModePhoto];
    [SCWatermarkOverlayView setImportedPhoto:YES];
    [SCWatermarkOverlayView setImportedPhotoFontSize:16.0*vIphoneOffset*(ciPhoto.extent.size.width/288/*352*/)];
    
    UIColor *tcolor = [Util getRetroTextColor:[[NSUserDefaults standardUserDefaults] objectForKey:@"text_color"]];
    SCWatermarkOverlayView *overlay = [SCWatermarkOverlayView new];
    overlay.frame = CGRectMake(0,0,ciPhoto.extent.size.width,ciPhoto.extent.size.height);
    
    CGFloat fRatio = vRetroText.font.pointSize / (vRetroText.frame.size.width);
    [overlay setFontRatio:fRatio];
    
    [[overlay timeLabel] setText:_timeLabel.text];
    [[overlay timeLabel] setTextColor:tcolor];
    [[overlay dateLabel] setTextColor:tcolor];
    [[overlay watermarkLabel] setTextColor:tcolor];
    [[overlay playLabel] setTextColor:tcolor];
    
    if (![vRetroText isHidden]) {
        [[overlay retroLabel] setTextColor:tcolor];
        [[overlay retroLabel] setText:vRetroText.text];
    }

    ciPhoto = [[Util changeViewToImage : (UIView *) overlay] imageByCompositingOverImage:ciPhoto];
    
    [SCWatermarkOverlayView setCameraMode:CameraModeVideo];
    [SCWatermarkOverlayView setImportedPhoto:NO];
    [SCWatermarkOverlayView setImportedPhotoFontSize:16.0];
    
    CGImageRef cgimage   = [[CIContext contextWithOptions:nil] createCGImage:ciPhoto fromRect:ciPhoto.extent];
    UIImage   *uiimage   = [UIImage imageWithCGImage:cgimage];
    CGImageRelease(cgimage);
    
    return uiimage;
}

#pragma mark - ImagePicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    dontResetSession = YES;

    [MBProgressHUD hideHUDForView:self.camBorders animated:YES];
    [MBProgressHUD showHUDAddedTo:self.camBorders animated:YES].label.text = @"Please Wait";

    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        _capturedPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        _cammode       = CameraModePhoto;
        [[SCRecorder sharedRecorder] setIsPhotoImported:YES];
        [self performSegueWithIdentifier:@"import_screen" sender:self];
        
        /*_capturedPhoto = [self applyFiltersOnPhoto:_capturedPhoto andImported:YES];
        [self performSegueWithIdentifier:@"share_view" sender:self];*/
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeVideo] || [mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL *url = info[UIImagePickerControllerMediaURL];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        importedVideoUrl = url;
        _cammode         = CameraModeVideo;
        
        [[SCRecorder sharedRecorder] setIsVideoImported:YES];
        
        SCRecordSessionSegment *segment = [SCRecordSessionSegment segmentWithURL:url info:nil];
        [_recorder.session addSegment:segment];
        
        [self performSegueWithIdentifier:@"import_screen" sender:self];
        
    } else [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    [MBProgressHUD hideHUDForView:self.camBorders animated:YES];
}

#pragma mark - MPMediaPickerControllerDelegate

- (void)mediaPicker: (MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    dontResetSession = YES;
    
    MPMediaItem *item   = (MPMediaItem *)[mediaItemCollection.items objectAtIndex:0];
        customAudioUrl  = [item valueForProperty:MPMediaItemPropertyAssetURL];
        playeraudio     = [[AVAudioPlayer alloc] initWithContentsOfURL:customAudioUrl error:nil];
    
    useCustomAudio                       = YES;
    _recorder.audioConfiguration.enabled = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - RecorderDelegate

- (void)recorder:(SCRecorder *)recorder didSkipVideoSampleBufferInSession:(SCRecordSession *)recordSession {
    NSLog(@"Skipped video buffer");
}

- (void)recorder:(SCRecorder *)recorder didReconfigureAudioInput:(NSError *)audioInputError {
    NSLog(@"Reconfigured audio input: %@", audioInputError);
}

- (void)recorder:(SCRecorder *)recorder didReconfigureVideoInput:(NSError *)videoInputError {
    NSLog(@"Reconfigured video input: %@", videoInputError);
}

- (void)recorder:(SCRecorder *)recorder didCompleteSession:(SCRecordSession *)recordSession {
    NSLog(@"didCompleteSession:");
    [self endVideoCapture];
}

- (void)recorder:(SCRecorder *)recorder didInitializeAudioInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    if (error == nil) {
        NSLog(@"Initialized audio in record session");
    } else {
        NSLog(@"Failed to initialize audio in record session: %@", error.localizedDescription);
    }
}

- (void)recorder:(SCRecorder *)recorder didInitializeVideoInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    if (error == nil) {
        NSLog(@"Initialized video in record session");
    } else {
        NSLog(@"Failed to initialize video in record session: %@", error.localizedDescription);
    }
}

- (void)recorder:(SCRecorder *)recorder didBeginSegmentInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"Began record segment: %@", error);
}

- (void)recorder:(SCRecorder *)recorder didCompleteSegment:(SCRecordSessionSegment *)segment inSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"Completed record segment at %@: %@ (frameRate: %f)", segment.url, error, segment.frameRate);
}

- (void)recorder:(SCRecorder *)recorder didAppendVideoSampleBufferInSession:(SCRecordSession *)recordSession {
    [self updateTimeRecordedLabel];
}

#pragma mark - Video Player Delegate

- (void)itemDidFinishPlaying:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:[notification object]];
    if (resetTilt) {
        resetTilt                           = NO;
        _playerViewController.view.hidden   = YES;

        NSString *fileName = [self getNextTilt];
        NSString *stringVideoName = fileName;
        NSString *stringVideoPath = [[NSBundle mainBundle] pathForResource:stringVideoName ofType:nil];
        NSAssert(stringVideoPath, @"Expected not nil video file");
        
        NSURL *urlVideoFile = [NSURL fileURLWithPath:stringVideoPath];
        NSAssert(urlVideoFile, @"Expected not nil video url");
        
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:urlVideoFile];
        [_playerViewController.player replaceCurrentItemWithPlayerItem:playerItem];
        
        // Pass the AVPlayerItem to a new player
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(itemDidFinishPlaying:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:playerItem];

        [_playerViewController.player pause];
        
        AVPlayerItem *p = [_playerViewController player].currentItem;
        [p seekToTime:kCMTimeZero];
        
        if (resumeRecording) { [_recorder record]; resumeRecording = NO; }
    }
}

@end
