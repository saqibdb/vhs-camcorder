//
//  VideoThumbnailView.swift
//  VideoThumbnailView
//
//  Created by Toygar Dundaralp on 9/29/15.
//  Copyright (c) 2015 Toygar Dundaralp. All rights reserved.
//

import UIKit
import AVFoundation

public class VideoThumbnailView: UIView , UIScrollViewDelegate    {
    var y : Float64 = 0.0
  public var videoScroll = UIScrollView()
  public var thumImageView = UIImageView()
  public var arrThumbViews = NSMutableArray()
  private var scrollContentWidth = 0.0
  private var videoURL = NSURL()
    private var videoURLs = [NSURL]()
        private var myTimer : Timer?
  
  private var videoDuration = 0.0
  private var activityIndicator = UIActivityIndicatorView()

  init(frame: CGRect, videoURL urls: [NSURL], thumbImageWidth thumbWidth: Double) {
    super.init(frame: frame)
    self.videoURLs = urls
    self.videoDuration = self.getVideosTime(urls: urls)
    activityIndicator = UIActivityIndicatorView(
      frame: CGRect(
        x: self.center.x - 15,
        y: self.frame.size.height / 2 - 15,
        width: 30.0,
        height: 30.0))
    activityIndicator.hidesWhenStopped = true
    activityIndicator.activityIndicatorViewStyle = .white
    activityIndicator.startAnimating()
    addSubview(self.activityIndicator)

    videoScroll = UIScrollView(
      frame: CGRect(
        x: 0.0,
        y: 0.0,
        width: self.frame.size.width,
        height: self.frame.size.height))
    videoScroll.showsHorizontalScrollIndicator = false
    videoScroll.showsVerticalScrollIndicator = false
    videoScroll.bouncesZoom = false
    videoScroll.bounces = false
    self.addSubview(videoScroll)

    self.thumbImageProcessing(
      videoUrl: videoURLs,
      thumbWidth: thumbWidth) { (thumbImages, error) -> Void in
      // println(thumbImages)
    }
        self.layer.masksToBounds = true
  }

  private func thumbImageProcessing(
    videoUrl urls: [NSURL],
    thumbWidth: Double,
    completion: ((_ thumbImages: NSMutableArray?, _ error: NSError?) -> Void)?) {
    //let priority = DISPATCH_QUEUE_PRIORITY_HIGH
    //dispatch_async(dispatch_get_global_queue(priority, 0)) {
    
    
    
    
  
    
    
    var videoDone : Int = 0
    
      for index in 0...Int(self.videoDuration) {
       
        var timeTillNow : Int = 0
        for indexToSelect in 0...urls.count - 1 {
            let url : NSURL = urls[indexToSelect]
            if index <= timeTillNow + Int(self.getVideoTime(url: url)){
                videoDone = indexToSelect
                break
            }
            else{
                timeTillNow = timeTillNow + Int(self.getVideoTime(url: urls[videoDone]))
                
            }
        }
        
        
        
        self.videoURL = urls[videoDone]
        let thumbXCoords = Double(index) * thumbWidth
        self.thumImageView = UIImageView(
          frame: CGRect(
            x: thumbXCoords,
            y: 0.0,
            width: thumbWidth,
            height: Double(self.frame.size.height)))
        self.thumImageView.contentMode = .scaleAspectFit
        self.thumImageView.backgroundColor = UIColor.clear
        self.thumImageView.layer.borderColor = UIColor.gray.cgColor
        self.thumImageView.layer.borderWidth = 0.25
        self.thumImageView.tag = index
       
        self.thumImageView.isUserInteractionEnabled = true
    
        
       
        
        self.thumImageView.image = self.generateVideoThumbs(
          url: self.videoURL,
          second: Double(index - timeTillNow),
          thumbWidth: thumbWidth)
        self.scrollContentWidth = self.scrollContentWidth + thumbWidth
        self.videoScroll.addSubview(self.thumImageView)
        self.videoScroll.sendSubview(toBack: self.thumImageView)
        if let imageView = self.thumImageView as UIImageView? {
          self.arrThumbViews.add(imageView)
                  }
      }
        self.videoScroll.contentSize = CGSize(
        width: Double(self.scrollContentWidth),
        height: Double(self.frame.size.height))
        self.activityIndicator.stopAnimating()
        completion?(self.arrThumbViews, nil)
    
        self.videoScroll.setContentOffset(CGPoint(x: 0.0 , y: 0.0), animated: false)
    
  }
   
    func changeValue(totalTime : TimeInterval, tappedPoint : CGPoint) {
        
        let tappedPoint = tappedPoint
        if self.videoScroll.contentOffset.x > 0
        {
            self.videoScroll.layer.removeAllAnimations()
            self.videoScroll.contentOffset.x = 0
            self.videoScroll.setContentOffset(CGPoint(x: 0.0 , y: 0.0), animated: false)
        }
        
        if self.videoScroll.contentOffset.x <= 0
        {
        self.videoScroll.layer.removeAllAnimations()
        self.videoScroll.contentOffset.x = tappedPoint.x
        self.videoScroll.setContentOffset(CGPoint(x: tappedPoint.x  , y: 0.0), animated: false)
        
        }
        
        
        
        
        
        UIView.animate(withDuration: totalTime, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            
            self.videoScroll.contentOffset.x = self.videoScroll.contentSize.width - 30
            
            }, completion: nil)
        
        
        
//        { (abc) in
//            
//            self.videoScroll.contentOffset.x = 0
//            self.videoScroll.setContentOffset(CGPoint(x: 0.0 , y: 0.0), animated: false)
//        }

    }
    
   
    private func subtractTime( index: Int , urls: [NSURL] , totalTime: Int) -> Int{
        
        var index = index
        var totalTime = totalTime
        if index != 0 {
            totalTime = totalTime - Int(self.getVideoTime(url: urls[index]))
            index = index - 1
            self.subtractTime(index: index, urls: urls, totalTime: totalTime)
        }
        else {
            totalTime = totalTime - Int(self.getVideoTime(url: urls[index]))

            
        }
    
    return totalTime
    }

  private func getVideoTime(url: NSURL) -> Float64 {
    let videoTime = AVURLAsset(url: url as URL, options: nil)
    return CMTimeGetSeconds(videoTime.duration)
  }
    private func getVideosTime(urls: [NSURL]) -> Float64 {
        var totalTimes : Float64 = 0.0
        for url in urls {
            let videoTime = AVURLAsset(url: url as URL, options: nil)
            totalTimes = totalTimes + CMTimeGetSeconds(videoTime.duration)
        }
        return totalTimes
    }

  private func generateVideoThumbs(url: NSURL, second: Float64, thumbWidth: Double) -> UIImage {
    let asset = AVURLAsset(url: url as URL, options: nil)
    let generator = AVAssetImageGenerator(asset: asset)
    generator.maximumSize = CGSize(width: thumbWidth, height: Double(self.frame.size.height))
    generator.appliesPreferredTrackTransform = false
    let thumbTime = CMTimeMakeWithSeconds(second, 1)
    do {
    let ref = try generator.copyCGImage(at: thumbTime, actualTime: nil)
      return UIImage(cgImage: ref)
    }catch {
      print(error)
    }
    return UIImage()
  }

  required public init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
